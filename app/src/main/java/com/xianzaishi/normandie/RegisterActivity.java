package com.xianzaishi.normandie;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.xianzaishi.normandie.bean.LoginAndRegister;
import com.xianzaishi.normandie.bean.NewLoginBean;
import com.xianzaishi.normandie.common.Urls;
import com.xianzaishi.normandie.global.Contants;
import com.xianzaishi.normandie.utils.GetBeanClass;
import com.xianzaishi.normandie.utils.JudgePhoneNumber;
import com.xianzaishi.normandie.utils.SPUtils;
import com.xianzaishi.normandie.utils.ValidateCountDownTimer;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener{
    private TextView tile;
    private EditText phone,validate,passWord,pwdAgain;
    private ImageView isValidateRight,isPWDRight;
    private Button getValidate,sureButton;
    private OkHttpClient client=MyApplication.getOkHttpClient();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        initView();
    }

    private void initView() {
        /**
         * 设置标题
         */
        View view=findViewById(R.id.register_title);
        tile= (TextView) view.findViewById(R.id.text_title);
        tile.setText("注册");

        phone= (EditText) findViewById(R.id.register_phone);
        validate= (EditText) findViewById(R.id.register_validate);
        passWord= (EditText) findViewById(R.id.register_setPassWord);
        pwdAgain= (EditText) findViewById(R.id.register_setPWD_again);

        isValidateRight= (ImageView) findViewById(R.id.register_isValidateRight);
        isPWDRight= (ImageView) findViewById(R.id.register_isPWDRight);

        getValidate= (Button) findViewById(R.id.register_getValidate);
        getValidate.setOnClickListener(this);
        sureButton= (Button) findViewById(R.id.register_sure_button);
        sureButton.setOnClickListener(this);

        /**
         * 判断验证码输入是否正确
         */
        validate.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (!"".equals(charSequence.toString())) {
                    isValidateRight.setVisibility(View.VISIBLE);
                    isValidateRight.setImageResource(R.mipmap.iv_delete_bg);
                    if(charSequence.toString().equals("123456")){
                        isValidateRight.setImageResource(R.mipmap.yanzheng2x);
                        /**
                         * 如果验证码输入正确 密码框恢复可编辑状态
                         */
                            passWord.setEnabled(true);
                            pwdAgain.setEnabled(true);
                    }
                } else {
                    isValidateRight.setVisibility(View.INVISIBLE);
                }
            }
            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        pwdAgain.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            /**
             *  判断第一次设置的密码是否符合规则
             * @param view
             * @param hasFocus
             */
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if(hasFocus){
                    String firstpwd = passWord.getText().toString();
                    if ("".equals(firstpwd)) {
                        Toast.makeText(RegisterActivity.this, "密码不能为空", Toast.LENGTH_SHORT).show();
                    } else if (firstpwd.getBytes().length < 6) {
                        Toast.makeText(RegisterActivity.this, "密码长度不能小于6位", Toast.LENGTH_SHORT).show();
                    }

                    /**
                     * 第二次输入密码时 验证是否与第一次设置的密码相同
                     */
                    pwdAgain.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                        }

                        @Override
                        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                            if (!"".equals(charSequence.toString())) {
                                isPWDRight.setVisibility(View.VISIBLE);
                                isPWDRight.setImageResource(R.mipmap.iv_delete_bg);
                                String firstpwd = passWord.getText().toString();
                                if (charSequence.toString().equals(firstpwd)) {
                                    isPWDRight.setVisibility(View.VISIBLE);
                                    isPWDRight.setImageResource(R.mipmap.yanzheng2x);
                                    sureButton.setEnabled(true);
                                }else {
                                    sureButton.setEnabled(false);
                                }
                            } else {
                                isPWDRight.setVisibility(View.INVISIBLE);
                            }
                        }
                        @Override
                        public void afterTextChanged(Editable editable) {
                        }
                    });
                }
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.register_getValidate:
                String mobileNumber=phone.getText().toString();
                if(JudgePhoneNumber.judgeMobileNumber(this,mobileNumber)){
                    new ValidateCountDownTimer(60000,1000,getValidate).start();//点击获取验证码后开始倒计时
                    Request request=new Request.Builder()
                            .url(String.format(Urls.MINE_SENDCHECKCODE,mobileNumber,1))
                            .build();
                    client.newCall(request).enqueue(new Callback() {
                        @Override
                        public void onFailure(Call call, IOException e) {

                        }
                        @Override
                        public void onResponse(Call call, Response response) throws IOException {
                            if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);
                            response.close();
                        }
                    });
                }
                break;
            case R.id.register_sure_button://确认提交注册信息
                final String mobileNumber1=phone.getText().toString();
                final String pwd=passWord.getText().toString();
                final String verification=validate.getText().toString();
                Request request=new Request.Builder()
                        .url(String.format(Urls.MINE_REGISTER,mobileNumber1,pwd,verification))
                        .build();
                client.newCall(request).enqueue(new Callback() {
                    @Override
                    public void onFailure(Call call, IOException e) {

                    }

                    @Override
                    public void onResponse(Call call, Response response) throws IOException {
                        if(!response.isSuccessful()) throw new IOException("Unexpected code " + response);
                        NewLoginBean loginAndRegister= (NewLoginBean) GetBeanClass.getBean(response,NewLoginBean.class);
                        response.close();
                        switch (loginAndRegister.getResultCode()){
                            /**
                             * 注册成功则直接登录
                             */
                            case 1:
                                String uid=loginAndRegister.getModule().getToken();
                                SPUtils.putStringValue(MyApplication.getContext(), Contants.SP_NAME,Contants.UID,uid);;//保存UID在本地
                                /**
                                 * 获取硬件设备号
                                 */
                                TelephonyManager tm= (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
                                String IMEI=tm.getDeviceId();
                                Request request=new Request.Builder()
                                        .url(String.format(Urls.MINE_LOGIN,mobileNumber1,pwd,IMEI))
                                        .build();
                                client.newCall(request).enqueue(new Callback() {
                                    @Override
                                    public void onFailure(Call call, IOException e) {
                                    }
                                    @Override
                                    public void onResponse(Call call, Response response) throws IOException {
                                        if(!response.isSuccessful()) throw new IOException("Unexpected code " + response);
                                        SPUtils.putStringValue(getApplicationContext(),Contants.SP_NAME,"token",
                                                ((NewLoginBean) GetBeanClass.getBean(response,NewLoginBean.class)).getModule().getToken());//保存token在本地
                                        response.close();
                                        finish();
                                    }
                                });
                                break;
                        }
                    }
                });
                break;
        }
    }
}
