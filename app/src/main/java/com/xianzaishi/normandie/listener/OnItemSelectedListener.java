package com.xianzaishi.normandie.listener;


public interface OnItemSelectedListener {
    void onItemSelected(int index);
}
