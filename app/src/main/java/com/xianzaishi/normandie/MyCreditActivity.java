package com.xianzaishi.normandie;

import android.app.Dialog;
import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.umeng.analytics.MobclickAgent;
import com.xianzaishi.normandie.adapter.CreditLogsAdapter;
import com.xianzaishi.normandie.bean.CreditBean;
import com.xianzaishi.normandie.bean.CreditLogsBean;
import com.xianzaishi.normandie.bean.SelectAddressBean;
import com.xianzaishi.normandie.common.Urls;
import com.xianzaishi.normandie.global.Contants;
import com.xianzaishi.normandie.utils.GetBeanClass;
import com.xianzaishi.normandie.utils.SPUtils;
import com.xianzaishi.normandie.utils.UiUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.internal.tls.OkHostnameVerifier;

public class MyCreditActivity extends AppCompatActivity implements View.OnClickListener{
    private TextView tab1,tab2,tab3,indicator1,indicator2,indicator3,creditLeft;
    private ListView listView;
    private List<CreditLogsBean.DataBean> allList;
    private List<CreditLogsBean.DataBean> getList=new ArrayList<>();
    private List<CreditLogsBean.DataBean> payList=new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_credit);
        initView();
    }

    private void initView() {
        /**
         *  设置标题
         */
        View view=findViewById(R.id.credit_title);
        TextView title= (TextView) view.findViewById(R.id.text_title);
        title.setText("我的积分");
        ImageView back= (ImageView) view.findViewById(R.id.address_go_back);
        back.setOnClickListener(this);
        /**
         *  查找控件初始化
         */
        tab1= (TextView) findViewById(R.id.credit_tab1);
        tab1.setSelected(true);//第一项默认选中
        tab1.setOnClickListener(this);
        tab2= (TextView) findViewById(R.id.credit_tab2);
        tab2.setOnClickListener(this);
        tab3= (TextView) findViewById(R.id.credit_tab3);
        tab3.setOnClickListener(this);
        indicator1= (TextView) findViewById(R.id.credit_indicator1);
        indicator2= (TextView) findViewById(R.id.credit_indicator2);
        indicator3= (TextView) findViewById(R.id.credit_indicator3);
        creditLeft= (TextView) findViewById(R.id.credit_can_use);
        listView= (ListView) findViewById(R.id.credit_listView);
        Button rule= (Button) findViewById(R.id.credit_rule);
        rule.setOnClickListener(this);
        initNetWork();
    }

    private void initNetWork() {
        /**
         *  积分使用情况
         */
        OkHttpClient client=MyApplication.getOkHttpClient();
        String uid= SPUtils.getStringValue(MyApplication.getContext(), Contants.SP_NAME,Contants.UID,"");
        RequestBody formBody=new FormBody.Builder()
                .add("uid",uid)
                .build();
        Request request=new Request.Builder()
                .url(Urls.GET_CREDIT_LOG)
                .post(formBody)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                UiUtils.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        tab1.setEnabled(false);
                        tab2.setEnabled(false);
                        tab3.setEnabled(false);
                    }
                });
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if(!response.isSuccessful()) throw new IOException("Unexpected code"+response);
                final CreditLogsBean creditLogsBean = (CreditLogsBean) GetBeanClass.getBean(response,CreditLogsBean.class);
                response.close();
                if(creditLogsBean.getCode()==1){
                    UiUtils.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            tab1.setEnabled(true);
                            tab2.setEnabled(true);
                            tab3.setEnabled(true);
                            allList=creditLogsBean.getData();
                            Collections.reverse(allList);
                            for(CreditLogsBean.DataBean dataBean:allList){
                                if(dataBean.getAmount()<0){
                                    payList.add(dataBean);
                                }else {
                                    getList.add(dataBean);
                                }
                            }
                            listView.setAdapter(new CreditLogsAdapter(allList,MyCreditActivity.this));
                        }
                    });

                }
            }
        });
        /**
         *  当前积分
         */
        Request request1 = new Request.Builder()
                .url(Urls.GET_CREDIT)
                .post(formBody)
                .build();
        client.newCall(request1).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
            }
            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if(!response.isSuccessful()) throw new IOException("Unexpected code"+response);
                final CreditBean creditBean= (CreditBean) GetBeanClass.getBean(response,CreditBean.class);
                response.close();
                UiUtils.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (creditBean.getCode()==1){
                            String credits=String.valueOf(creditBean.getData());
                            creditLeft.setText(credits);
                        }
                    }
                });

            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.credit_tab1:
                /**
                 *  重新初始化Tab的状态
                 */
                tab1.setSelected(true);
                indicator1.setVisibility(View.VISIBLE);
                tab2.setSelected(false);
                indicator2.setVisibility(View.INVISIBLE);
                tab3.setSelected(false);
                indicator3.setVisibility(View.INVISIBLE);
                listView.setAdapter(new CreditLogsAdapter(allList,MyCreditActivity.this));
                break;
            case R.id.credit_tab2:
                /**
                 *  重新初始化Tab的状态
                 */
                tab1.setSelected(false);
                indicator1.setVisibility(View.INVISIBLE);
                tab2.setSelected(true);
                indicator2.setVisibility(View.VISIBLE);
                tab3.setSelected(false);
                indicator3.setVisibility(View.INVISIBLE);
                listView.setAdapter(new CreditLogsAdapter(getList,MyCreditActivity.this));
                break;
            case R.id.credit_tab3:
                /**
                 *  重新初始化Tab的状态
                 */
                tab1.setSelected(false);
                indicator1.setVisibility(View.INVISIBLE);
                tab2.setSelected(false);
                indicator2.setVisibility(View.INVISIBLE);
                tab3.setSelected(true);
                indicator3.setVisibility(View.VISIBLE);
                listView.setAdapter(new CreditLogsAdapter(payList,MyCreditActivity.this));
                break;
            case R.id.credit_rule:
                dialog();
                break;
            case R.id.address_go_back:
                finish();
                break;
        }
    }
    private void dialog(){
        LayoutInflater inflater=this.getLayoutInflater();
        WindowManager m = this.getWindowManager();
        Display d = m.getDefaultDisplay(); // 获取屏幕宽、高度
        View view=inflater.inflate(R.layout.qr_image,null);
        final Dialog dialog=new Dialog(this,R.style.Dialog);
        dialog.setCancelable(true);
        dialog.setContentView(view);
        ImageView qrImage= (ImageView) view.findViewById(R.id.qr_image);
        qrImage.getLayoutParams().width= (int) (d.getWidth()*0.8);
        qrImage.getLayoutParams().height= (int) (d.getHeight()*0.8);
        qrImage.setImageResource(R.mipmap.creditrule);
        dialog.show();
        Window dialogWindow = dialog.getWindow();

        WindowManager.LayoutParams p = dialogWindow.getAttributes(); // 获取对话框当前的参数值
        p.width = (int) (d.getWidth()*0.8); // 宽度设置为屏幕的0.6，根据实际情况调整
        //p.height = (int) (d.getHeight()*0.8); // 高度设置为屏幕的0.6，根据实际情况调整
        dialogWindow.setGravity(Gravity.CENTER);
        dialogWindow.setAttributes(p);
    }

    @Override
    protected void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
    }
}
