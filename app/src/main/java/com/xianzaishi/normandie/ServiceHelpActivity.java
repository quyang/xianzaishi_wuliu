package com.xianzaishi.normandie;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.TextView;
import android.widget.Toast;

import com.umeng.analytics.MobclickAgent;
import com.xianzaishi.normandie.global.Contants;
import com.xianzaishi.normandie.utils.SPUtils;
import com.xianzaishi.normandie.utils.UiUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * quyang
 * 客服与帮助
 * 2016年9月23日12:05:02
 */
public class ServiceHelpActivity extends BaseActivity {

    public static final int REQUEST_PHONE_CODE = 0;


    private ExpandableListView mLv;
    private ArrayList<String> mGroupArray;
    private ArrayList<List<String>> mChildArray;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.service);


        tvCenterText.setText("客服与帮助");

        mLv = (ExpandableListView) findViewById(R.id.lv);

        Button left = (Button) findViewById(R.id.button4);
        Button right = (Button) findViewById(R.id.button7);
        MyClick click = new MyClick();
        left.setOnClickListener(click);
        right.setOnClickListener(click);


        mLv.setGroupIndicator(null);

        mGroupArray = new ArrayList<>();
        mChildArray = new ArrayList<>();

        mGroupArray.add("鲜在时蓝村路店的地址是哪？营业时间是几点？");
        mGroupArray.add("如何成为鲜在时会员？");
        mGroupArray.add("非会员能在店内交易吗？");
        mGroupArray.add("线上购物可以选择到付吗？");
        mGroupArray.add("不想要了可以退款吗？");

        //每个father下的数据集合
        ArrayList<String> items1 = new ArrayList<>();
        ArrayList<String> items2 = new ArrayList<>();
        ArrayList<String> items3 = new ArrayList<>();
        ArrayList<String> items4 = new ArrayList<>();
        ArrayList<String> items5 = new ArrayList<>();
        items1.add("上海市浦东新区东方路与蓝村路口富都广场1L。营业时间是8:00-22:00");
        items2.add("注册成为会员方式：通过APP下载，输入手机号注册即可以成为会员");
        items3.add(" 我们是生鲜会员店，必须注册成为会员才可以下单。");
        items4.add("很抱歉，我们是订单支付完成以后才能配送。");
        items5.add("生鲜商品不支持无理由退换货；\n" +
                "     订单状态为“已支付”时，可以直接申请取消订单，退款和使用的优惠券会在3-7个工作日内返回到原支付账户中；\n" +
                "     订单状态为“配送中”时，无法取消订单，给您带来的不便尽请谅解。\n" +
                "     订单状态为“已完成”时，若收到的商品有任何质量问题，您可以联系客服进行处理。在商品签收24小时内，可以进入【我的】-【我的订单】-【订单详情】-【申请售后】来进行退款。");
        mChildArray.add(items1);
        mChildArray.add(items2);
        mChildArray.add(items3);
        mChildArray.add(items4);
        mChildArray.add(items5);


        mLv.setAdapter(new MyBaseExpandableListAdapter());


    }

    public class MyClick implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            switch (v.getId()) {

                case R.id.button4:
                    String uid=SPUtils.getStringValue(MyApplication.getContext(), Contants.SP_NAME,Contants.UID,null);
                    if (uid==null){
                        Intent intent1=new Intent(ServiceHelpActivity.this,NewLoginActivity.class);
                        startActivity(intent1);
                    }else {
                        Intent intent = new Intent(getApplicationContext(), FanKuiActivity.class);
                        startActivity(intent);
                    }
                    break;
                case R.id.button7:

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (ContextCompat.checkSelfPermission(ServiceHelpActivity.this, Manifest.permission.CALL_PHONE)
                                != PackageManager.PERMISSION_GRANTED) {

                            ActivityCompat.requestPermissions(ServiceHelpActivity.this, new String[]{Manifest.permission.CALL_PHONE},
                                    REQUEST_PHONE_CODE);
                        } else {
                            call();
                        }
                    } else {
                        call();
                    }

                    break;
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case REQUEST_PHONE_CODE:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    call();
                } else {
                    Toast.makeText(ServiceHelpActivity.this, "权限申请失败", Toast.LENGTH_LONG).show();
                }
                break;
        }

    }

    //call phone
    public void call() {
        Intent intent = new Intent();
        intent.setAction("android.intent.action.CALL");
        intent.setData(Uri.parse("tel:" + "4006189885"));//mobile为你要拨打的电话号码，模拟器中为模拟器编号也可
        startActivity(intent);
    }

    //get view
    private TextView getGenericView(String string,boolean isChild) {
        AbsListView.LayoutParams layoutParams = new AbsListView.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);

        if (!isChild) {
            //layoutParams.height = 85;
            layoutParams.height = 140;
        }


        TextView textView = new TextView(this);
        textView.setLayoutParams(layoutParams);

        if (isChild) {
            textView.setBackgroundColor(UiUtils.getColor(R.color.colorLine));
        }

        textView.setGravity(Gravity.CENTER_VERTICAL | Gravity.LEFT);
        textView.setMaxLines(100);

        textView.setPadding(40, 0, 0, 0);
        textView.setText(string);
        return textView;
    }


    private class MyBaseExpandableListAdapter extends BaseExpandableListAdapter {


        @Override
        public int getGroupCount() {
            return mGroupArray.size();
        }

        @Override
        public int getChildrenCount(int groupPosition) {
            return mChildArray.get(groupPosition).size();
        }

        @Override
        public Object getGroup(int groupPosition) {
            return mChildArray.get(groupPosition);
        }

        @Override
        public Object getChild(int groupPosition, int childPosition) {
            return mChildArray.get(groupPosition).get(childPosition);
        }

        @Override
        public long getGroupId(int groupPosition) {
            return groupPosition;
        }


        @Override
        public long getChildId(int groupPosition, int childPosition) {
            return childPosition;
        }

        @Override
        public boolean hasStableIds() {
            return false;
        }

        @Override
        public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {

            return getGenericView(mGroupArray.get(groupPosition), false);

        }

        @Override
        public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
            return getGenericView(mChildArray.get(groupPosition).get(childPosition), true);
        }

        @Override
        public boolean isChildSelectable(int groupPosition, int childPosition) {
            return true;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
    }
}
