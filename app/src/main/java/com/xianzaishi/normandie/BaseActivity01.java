package com.xianzaishi.normandie;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;

import com.xianzaishi.normandie.utils.UiUtils;

/**
 * quyang
 * 2016年8月20日00:03:19
 */
public abstract class BaseActivity01 extends AppCompatActivity {


    public View mErrorView;
    public View mLoadingView;
    public View mEmptyView;
    public FrameLayout flContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base01);

        //找到真布局
        flContainer = (FrameLayout) findViewById(R.id.fl_container);

        initView();
    }

    private void initView() {


        //初始化mErrorView
        if (mErrorView == null) {
            mErrorView = createErrorView();
            mErrorView.setVisibility(View.INVISIBLE);
            flContainer.addView(mErrorView);
        }

        if (mLoadingView == null) {
            mLoadingView = createLoadingView();
            flContainer.addView(mLoadingView);
        }

        if (mEmptyView == null) {
            mEmptyView = createEmptyView();
            mEmptyView.setVisibility(View.INVISIBLE);
            flContainer.addView(mEmptyView);
        }


        loadShuju();
    }


    //请求服务器数据
    public abstract void loadDataFromNet();

    //请求服务器数据
    public void loadShuju() {
        loadDataFromNet();
    }


    private View createLoadingView() {
        return UiUtils.inflateView(R.layout.layout_loading);
    }

    private View createEmptyView() {
        return UiUtils.inflateView(R.layout.layout_empty);
    }


    private View createErrorView() {
        View view = UiUtils.inflateView(R.layout.layout_error);
        Button btn_retry = (Button) view.findViewById(R.id.btn_retry);
        btn_retry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadShuju();
            }
        });
        return view;
    }


    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.from_left_in, R.anim.to_right_out);
    }

    @Override
    public void startActivity(Intent intent) {
        super.startActivity(intent);
        overridePendingTransition(R.anim.from_right_in, R.anim.to_left_out);
    }

}
