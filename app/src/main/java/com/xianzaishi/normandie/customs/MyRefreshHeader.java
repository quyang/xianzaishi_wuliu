package com.xianzaishi.normandie.customs;

import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.xianzaishi.normandie.MyApplication;
import com.xianzaishi.normandie.R;

import in.srain.cube.views.ptr.PtrFrameLayout;
import in.srain.cube.views.ptr.PtrUIHandler;
import in.srain.cube.views.ptr.indicator.PtrIndicator;


/**
 * Created by Administrator on 2016/11/1.
 */

public class MyRefreshHeader extends FrameLayout implements PtrUIHandler {
    private AnimationDrawable mAnimationDrawable;
    private final static float HEADER_RATIO = 0.42f;
    public MyRefreshHeader(Context context) {
        super(context);
        init();
    }

    public MyRefreshHeader(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public MyRefreshHeader(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }
    private void init() {
        View view= LayoutInflater.from(MyApplication.getContext()).inflate(R.layout.refresh_header,this);
        ImageView imageView= (ImageView) view.findViewById(R.id.refresh_image);
        int screenWidth = getContext().getResources().getDisplayMetrics().widthPixels;
        imageView.getLayoutParams().width= (int) (screenWidth*0.65f);
        imageView.getLayoutParams().height= (int) (HEADER_RATIO*imageView.getLayoutParams().width);
        mAnimationDrawable= (AnimationDrawable) imageView.getBackground();
    }

    @Override
    public void onUIReset(PtrFrameLayout frame) {

    }

    @Override
    public void onUIRefreshPrepare(PtrFrameLayout frame) {

    }

    @Override
    public void onUIRefreshBegin(PtrFrameLayout frame) {

    }

    @Override
    public void onUIRefreshComplete(PtrFrameLayout frame) {
        mAnimationDrawable.stop();
    }

    @Override
    public void onUIPositionChange(PtrFrameLayout frame, boolean isUnderTouch, byte status, PtrIndicator ptrIndicator) {
        mAnimationDrawable.start();
    }
}
