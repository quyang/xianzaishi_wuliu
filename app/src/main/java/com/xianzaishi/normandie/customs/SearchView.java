package com.xianzaishi.normandie.customs;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.xianzaishi.normandie.R;
import com.xianzaishi.normandie.SearchResultActivity;
import com.xianzaishi.normandie.utils.ToastUtils;


/**
 * Created by Administrator on 2016/7/23.
 */
public class SearchView extends LinearLayout implements View.OnClickListener {

    /**
     * 输入框
     */
    private AutoCompleteTextView etInput;

    /**
     * 删除键
     */
    private ImageView ivDelete;
    /**
     * 上下文
     */
    private Context mContext;
    /**
     * 存储搜索历史
     */
    private SharedPreferences sharedPreferences;

    /**
     * 编辑对象
     */
    private SharedPreferences.Editor editor;

    /**
     *  StringBuffer 用来存储搜索历史
     */
    private StringBuffer stringBuffer;
    /**
     *  搜索过的字符串组成的数组
     */
    private String[] searchedArray;

    public SearchView(Context context) {
        super(context);
        mContext=context;
        LayoutInflater.from(context).inflate(R.layout.searchview_layout, this);
        initViews();
    }

    public SearchView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext=context;
        LayoutInflater.from(context).inflate(R.layout.searchview_layout, this);
        initViews();
    }

    private void initViews() {
        etInput = (AutoCompleteTextView) findViewById(R.id.search_et_input);
        ivDelete = (ImageView) findViewById(R.id.search_iv_delete);
        ivDelete.setOnClickListener(this);

        sharedPreferences=mContext.getSharedPreferences("history",Context.MODE_PRIVATE);
        editor=sharedPreferences.edit();
        stringBuffer=new StringBuffer();

        etInput.addTextChangedListener(new EditChangedListener());
        etInput.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    String searchString=getSearchText();
                    if(!searchString.equals("")){
                        notifyStartSearching(searchString);
                        saveSearchHistory(searchString);//保存搜索历史
                    }else {
                        ToastUtils.showToast("搜索内容不能为空");
                    }
                }
                return true;
            }
        });
    }
    public void saveSearchHistory(String searchString){
        stringBuffer.setLength(0);//每次使用前先清空

        if(sharedPreferences.getString("searched","").equals("")){//没有存储过
            editor.putString("searched",searchString+",");
            editor.commit();
        }else{//存储过，并且搜索的内容已经收录
            if(sharedPreferences.getString("searched","").contains(searchString)){
                String newString=sharedPreferences.getString("searched","").replace(searchString+",","");
                editor.putString("searched",stringBuffer.append(searchString+",")
                        .append(newString)
                        .toString());
                editor.commit();

            }else {//存储过，搜索内容未收录，且收录的词条小于10条
                if (sharedPreferences.getString("searched","").split(",").length<10){
                    editor.putString("searched",stringBuffer.append(searchString+",")
                            .append(sharedPreferences.getString("searched",""))
                            .toString());
                    editor.commit();
                }else {//存储过，搜索内容未收录，且收录的词条等于10条
                    int i=sharedPreferences.getString("searched","").lastIndexOf(",");
                    int j=sharedPreferences.getString("searched","").substring(0,i).lastIndexOf(",");
                    editor.putString("searched",stringBuffer.append(searchString+",")
                            .append(sharedPreferences.getString("searched","")
                                    .substring(0,j+1))
                            .toString());
                    editor.commit();
                }
            }
        }
    }
    /**
     * 设置自动补全adapter
     */
    public void setAutoCompleteAdapter(ArrayAdapter<String> adapter) {
        etInput.setAdapter(adapter);
    }

    public String getSearchText() {
        return etInput.getText().toString().trim();
    }

    /**
     * 通知监听者 进行搜索操作
     *
     * @param text
     */
    private void notifyStartSearching(String text) {
        //隐藏软键盘
        InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(0, InputMethodManager.HIDE_NOT_ALWAYS);
        if(mContext instanceof SearchResultActivity){
            ((SearchResultActivity) mContext).initListView(text);
        }else {
            Intent intent = new Intent(mContext, SearchResultActivity.class);
            intent.putExtra("search", text);
            mContext.startActivity(intent);
        }
    }

    @Override
    public void onClick(View view) {
        etInput.setText("");
        ivDelete.setVisibility(GONE);
    }

    public void setHint(CharSequence charSequence){
        etInput.setHint(charSequence);
    }
    public void setTexts(CharSequence charSequence){
        etInput.setText(charSequence);
    }
    private class EditChangedListener implements TextWatcher {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            if (!"".equals(charSequence.toString())) {
                ivDelete.setVisibility(VISIBLE);
            } else {
                ivDelete.setVisibility(GONE);
            }
        }

        @Override
        public void afterTextChanged(Editable editable) {
        }
    }
}
