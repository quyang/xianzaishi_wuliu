package com.xianzaishi.normandie;

import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.xianzaishi.normandie.utils.UiUtils;

/**
 * quyang
 *
 * 2016年8月18日20:40:27
 */
public class BaseActivity extends FragmentActivity implements View.OnClickListener{


    public ImageView tvBack;
    public TextView tvCenterText;
    public FrameLayout flContainer;
    public View childView;

    @Override
    public void setContentView( int layoutResID) {

        //填充布局
        View view = UiUtils.inflateView(R.layout.activity_base);

        //找到组件
        tvBack = (ImageView) view.findViewById(R.id.iv_back_base);
        tvCenterText = (TextView) view.findViewById(R.id.tv_base_center_text);
        flContainer = (FrameLayout) view.findViewById(R.id.fl_container);

        tvBack.setOnClickListener(this);

        //填充传递过来的布局
        childView = getLayoutInflater().inflate(layoutResID, null);
        flContainer.addView(childView);

        super.setContentView(view);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.iv_back_base:
                finish();
                break;
        }
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.from_left_in,R.anim.to_right_out);
    }

    @Override
    public void startActivity(Intent intent) {
        super.startActivity(intent);
        overridePendingTransition(R.anim.from_right_in,R.anim.to_left_out);
    }
}
