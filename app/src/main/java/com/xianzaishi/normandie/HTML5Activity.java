package com.xianzaishi.normandie;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.umeng.analytics.MobclickAgent;
import com.umeng.socialize.ShareAction;
import com.umeng.socialize.UMShareAPI;
import com.umeng.socialize.UMShareListener;
import com.umeng.socialize.bean.SHARE_MEDIA;
import com.umeng.socialize.media.UMImage;
import com.umeng.socialize.shareboard.ShareBoardConfig;
import com.umeng.socialize.shareboard.SnsPlatform;
import com.umeng.socialize.utils.ShareBoardlistener;
import com.xianzaishi.normandie.bean.GeneralBean;
import com.xianzaishi.normandie.bean.ShoppingCarCountBean;
import com.xianzaishi.normandie.common.Urls;
import com.xianzaishi.normandie.global.Contants;
import com.xianzaishi.normandie.utils.GetBeanClass;
import com.xianzaishi.normandie.utils.SPUtils;
import com.xianzaishi.normandie.utils.ToastUtils;
import com.xianzaishi.normandie.utils.UiUtils;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class HTML5Activity extends AppCompatActivity implements View.OnClickListener{
    private WebView mWebView;
    private ImageView shareBt,shoppingCar;
    private TextView shoppingCount;
    private int shoppingCounts=0;
    private String url,titleStr;
    private ShareBoardlistener shareBoardlistener;
    private OkHttpClient client=MyApplication.getOkHttpClient();
    private ClipboardManager myClipboard;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_html5);
        initView();
    }
    private void initView() {
        Intent intent=getIntent();
        url=intent.getStringExtra("url");
        titleStr=intent.getStringExtra("title")==null?"鲜在时":(intent.getStringExtra("title")).equals("")?"鲜在时":intent.getStringExtra("title");
        myClipboard = (ClipboardManager)getSystemService(CLIPBOARD_SERVICE);
        //标题和返回上一页按钮
        ImageView back= (ImageView) findViewById(R.id.address_go_back);
        back.setOnClickListener(this);
        TextView title= (TextView) findViewById(R.id.text_title);
        title.setText(titleStr);
        //购物车和购物车数量以及分享按钮
        shoppingCount= (TextView) findViewById(R.id.shopping_trolley_count);
        shoppingCar= (ImageView) findViewById(R.id.shopping_trolley);
        shoppingCar.setOnClickListener(this);
        shareBt= (ImageView) findViewById(R.id.share_icon);
        shareBt.setOnClickListener(this);
        //请求服务器获取购物车数量
        getShoppingCount();
        //webView相关
        mWebView= (WebView) findViewById(R.id.html5_webView);
        WebSettings settings = mWebView.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setUseWideViewPort(true);
        settings.setLoadWithOverviewMode(true);
        settings.setBuiltInZoomControls(true);
        settings.setSupportZoom(true);
        settings.setDomStorageEnabled(true);
        mWebView.addJavascriptInterface(this,"showSendMsg");
        mWebView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                mWebView.loadUrl("javascript:window.setXzsVersion('android','1.1.4')");
            }
        });
        mWebView.loadUrl(url);
    }

    /**
     * 获取购物车商品数量
     */
    private void getShoppingCount() {

        String uid= SPUtils.getStringValue(MyApplication.getContext(), Contants.SP_NAME,Contants.UID,"");
        RequestBody requestForm=new FormBody.Builder()
                .add("uid",uid)
                .build();
        final Request request=new Request.Builder()
                .url(Urls.GET_ALL_COUNT)
                .post(requestForm)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if(!response.isSuccessful()) throw new IOException("Unexpected code"+response);
                final ShoppingCarCountBean shoppingCarCountBean = (ShoppingCarCountBean) GetBeanClass.getBean(response,ShoppingCarCountBean.class);
                response.close();
                if (shoppingCarCountBean.getCode()==1){
                    UiUtils.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if(!"0".equals(shoppingCarCountBean.getData())){
                                shoppingCount.setVisibility(View.VISIBLE);
                                shoppingCount.setText(shoppingCarCountBean.getData());
                                shoppingCounts=Integer.valueOf(shoppingCarCountBean.getData());
                            }else {
                                shoppingCount.setText("");
                                shoppingCount.setVisibility(View.GONE);
                            }
                        }
                    });
                }
            }
        });
    }

    /**
     * 供H5页面调用跳转商品详情页
     */
    @JavascriptInterface
    public void postMessage(String targetId){
        Intent intent1 = new Intent(HTML5Activity.this, GoodsDetailsActivity.class);
        intent1.putExtra(Contants.ITEM_ID, Integer.valueOf(targetId));
        intent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent1);
    }

    /**
     *  供H5页面调用加购
     */
    @JavascriptInterface
    public void addItemToCart(String itemId,String skuId){
        String uid= SPUtils.getStringValue(MyApplication.getContext(), Contants.SP_NAME,Contants.UID,null);
        if (uid!=null){
            addIntoCar(itemId,skuId,uid);
        }else {
            Intent intent1 = new Intent(HTML5Activity.this, GoodsDetailsActivity.class);
            intent1.putExtra(Contants.ITEM_ID, Integer.valueOf(itemId));
            intent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent1);
        }
    }

    private void addIntoCar(String itemId,String skuId,String uid) {
        if(uid==null){
            startActivity(new Intent(HTML5Activity.this, NewLoginActivity.class));
        }else {
            RequestBody formBody = new FormBody.Builder()
                    .add("uid", uid)
                    .add("itemId", itemId)
                    .add("itemCount", "1")
                    .add("skuId", skuId)
                    .build();
            Request request = new Request.Builder()
                    .url(Urls.ADD_TO_TROLLEY)
                    .post(formBody)
                    .build();
            client.newCall(request).enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    ToastUtils.showToast("网络不给力哦");
                }

                @Override
                public void onResponse(Call call, final Response response) throws IOException {
                    if (!response.isSuccessful()){return;}
                    final GeneralBean generalBean = (GeneralBean) GetBeanClass.getBean(response, GeneralBean.class);
                    UiUtils.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (generalBean!=null&&generalBean.success) {
                                SPUtils.putBooleanValue(MyApplication.getContext(), Contants.SP_NAME, "countChanged", true);
                                shoppingCounts = ++shoppingCounts;
                                if (shoppingCounts != 0) {
                                    shoppingCount.setVisibility(View.VISIBLE);
                                }
                                shoppingCount.setText(String.valueOf(shoppingCounts));
                                Toast toast=Toast.makeText(HTML5Activity.this,"加购成功！",Toast.LENGTH_SHORT);
                                toast.setGravity(Gravity.CENTER,0,0);
                                toast.show();
                            }else {
                                ToastUtils.showToast("添加购物车失败");
                            }
                        }
                    });
                }
            });
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.address_go_back:
                finish();
                break;
            case R.id.shopping_trolley://跳转购物车
                MobclickAgent.onEvent(HTML5Activity.this,"goToCart");
                Intent intent=new Intent(HTML5Activity.this,MainActivity.class);
                intent.putExtra("pageNo",3);
                startActivity(intent);
                break;
            case R.id.share_icon://分享此页面
                share();
                break;
        }

    }

    /**
     *  分享相关
     */
    private void share() {
        shareBoardlistener = new  ShareBoardlistener() {

            @Override
            public void onclick(SnsPlatform snsPlatform, SHARE_MEDIA share_media) {
                if (share_media!=null){
                    new ShareAction(HTML5Activity.this).setPlatform(share_media).setCallback(umShareListener)
                            .withText("精彩活动等你参与")
                            .withTitle(titleStr)
                            .withTargetUrl(url)
                            .withMedia(new UMImage(HTML5Activity.this,R.mipmap.ic_launcher))
                            .share();
                }else if (snsPlatform.mKeyword.equals("umeng_copybutton_custom")){
                    ClipData myClipData=ClipData.newPlainText("text",url);
                    myClipboard.setPrimaryClip(myClipData);
                    ToastUtils.showToast("复制链接成功！");
                }
            }
        };

        ShareBoardConfig config = new ShareBoardConfig();
        config.setShareboardPostion(ShareBoardConfig.SHAREBOARD_POSITION_BOTTOM)
                .setMenuItemBackgroundShape(ShareBoardConfig.BG_SHAPE_CIRCULAR)
                .setCancelButtonVisibility(false)
                .setTitleVisibility(false);
        new ShareAction(HTML5Activity.this)
                .setDisplayList(SHARE_MEDIA.WEIXIN,SHARE_MEDIA.WEIXIN_CIRCLE)
                .addButton("umeng_copybutton_custom","umeng_copybutton_custom","copy_icon","copy_icon")
                .setShareboardclickCallback(shareBoardlistener)
                .open(config);

    }


    private UMShareListener umShareListener=new UMShareListener() {
        @Override
        public void onResult(SHARE_MEDIA share_media) {

        }
        @Override
        public void onError(SHARE_MEDIA share_media, Throwable throwable) {

        }
        @Override
        public void onCancel(SHARE_MEDIA share_media) {

        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        UMShareAPI.get(this).onActivityResult(requestCode, resultCode, data);
    }
}
