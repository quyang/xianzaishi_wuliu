package com.xianzaishi.normandie.http;

import com.xianzaishi.normandie.MyApplication;
import com.xianzaishi.normandie.utils.UiUtils;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * quyang
 * <p>
 * Created by Administrator on 2016/8/24.
 */
public abstract class BaseProtocol {


    //get方式请求网络数据
    public void getData() {

        OkHttpClient okHttpClient = MyApplication.getOkHttpClient();
        final Request request = new Request.Builder()
                .url(getUrl())
                .build();

        okHttpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                UiUtils.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mListener.onFail();
                    }
                });
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                final String string = response.body().string();
                UiUtils.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mListener.onSuccess(string);
                    }
                });
            }
        });
    }


    public abstract String getUrl();

    public abstract RequestBody getRequestBody();

    public interface OnDataFromNetListener {
        void onSuccess(String json);

        void onFail();
    }

    private OnDataFromNetListener mListener;

    public void setOnDataFromNetListener(OnDataFromNetListener listener) {
        mListener = listener;
    }

}
