package com.xianzaishi.normandie.http;

import com.google.gson.Gson;
import com.xianzaishi.normandie.interfaces.OnDataFromServerListener;

/**
 * quyang
 * 请求网络,返回实体类(可通用)
 * Created by Administrator on 2016/9/3.
 */
public class GeneralProtocal<T> {


    public Class<T> mClazz;


    public GeneralProtocal(Class<T> clazz) {
        mClazz = clazz;
    }

    public void getDataByGET(String url) {
        AllOrderProtocol protocal = new AllOrderProtocol();
        protocal.setUrl(url);
        protocal.getData();
        protocal.setOnDataFromNetListener(new BaseProtocol.OnDataFromNetListener() {
            @Override
            public void onSuccess(final String result) {
                mListener.onSuccess(parseJson(result));
            }

            @Override
            public void onFail() {
                mListener.onFail();
            }
        });

    }


    private T parseJson(String json) {
        Gson gson = new Gson();
        return gson.fromJson(json, mClazz);
    }


    private OnDataFromServerListener mListener;

    public void setOnDataFromServerListener(OnDataFromServerListener listener) {
        mListener = listener;
    }

}
