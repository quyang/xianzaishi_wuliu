package com.xianzaishi.normandie.http;

import android.os.AsyncTask;

import com.xianzaishi.normandie.interfaces.OnDataFromServerListener;

/**
 * quyang
 * 请求服务器 回调json
 * Created by Administrator on 2016/9/3.
 */
public class MyOrderPagerRefreshTask extends AsyncTask<String,Void,Void>{


    @Override
    protected Void doInBackground(String... urls) {
        AllOrderProtocol protocal = new AllOrderProtocol();
        protocal.setUrl(urls[0]);
        protocal.getData();
        protocal.setOnDataFromNetListener(new BaseProtocol.OnDataFromNetListener() {
            @Override
            public void onSuccess(String json) {
                mListener.onSuccess(json);
            }

            @Override
            public void onFail() {
                mListener.onFail();
            }
        });
        return null;
    }

    private OnDataFromServerListener mListener;

    public void setOnDataFromServerListener(OnDataFromServerListener listener){
        mListener = listener;
    }

}
