package com.xianzaishi.normandie.http;

import okhttp3.RequestBody;

/**
 * quyang
 * <p/>
 * Created by Administrator on 2016/9/2.
 */
public class HomeProtocolTwoPage extends BaseProtocol {

    public String url;

    public void setUrl(String url) {
        this.url = url;
    }


    @Override
    public String getUrl() {
        return url;
    }

    @Override
    public RequestBody getRequestBody() {
        return null;
    }
}
