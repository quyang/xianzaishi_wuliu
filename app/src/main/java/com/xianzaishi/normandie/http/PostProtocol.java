package com.xianzaishi.normandie.http;

import okhttp3.RequestBody;

/**
 * Created by Administrator on 2016/9/24.
 */

public class PostProtocol extends GeneralPostProtocol {


    private String url;

    private RequestBody mBody;

    public void setUrl(String url) {
        this.url = url;
    }


    public void setRequestBody(RequestBody body) {
        this.mBody = body;
    }

    @Override
    public RequestBody getRequestBody() {
        return mBody;
    }

    @Override
    public String getUrl() {
        return url;
    }
}
