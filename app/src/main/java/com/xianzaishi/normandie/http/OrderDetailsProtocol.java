package com.xianzaishi.normandie.http;

import okhttp3.RequestBody;

/**
 * quyang
 * Created by Administrator on 2016/8/24.
 */
public class OrderDetailsProtocol extends BaseProtocol {

    public String url;

    public void setUrl(String url){
        this.url = url;
    }


    @Override
    public String getUrl() {
        return url;
    }

    @Override
    public RequestBody getRequestBody() {
        return null;
    }
}
