package com.xianzaishi.normandie.http;

import okhttp3.RequestBody;

/**
 * quyang
 * Created by Administrator on 2016/8/24.
 */
public class AllOrderProtocol extends BaseProtocol {

    public String url;
    public RequestBody formBody;

    public void setUrl(String url) {
        this.url = url;
    }


    public void setRequestBody(RequestBody formBody) {
        this.formBody = formBody;
    }


    @Override
    public String getUrl() {
        return url;
    }

    @Override
    public RequestBody getRequestBody() {
        return formBody;
    }


}
