package com.xianzaishi.normandie.http;

import android.app.Activity;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.widget.Toast;

import com.xianzaishi.normandie.R;
import com.xianzaishi.normandie.interfaces.OnDataFromServerListener;
import com.xianzaishi.normandie.utils.MyThreadPoolManager;
import com.xianzaishi.normandie.utils.ToastUtils;
import com.xianzaishi.normandie.utils.UiUtils;

/**
 * 我的订单每个子页面的下拉请求网络
 * Created by Administrator on 2016/9/9.
 */
public class OrderPagerProtocal<T> {


    public boolean isLoaidng = false;

    /**
     * 下拉请求网络
     * @param activity
     * @param url
     * @param mSwipeRefreshWidget
     * @param recycler
     */
    public void inimSwipeRefresh(final Activity activity, final String url,
                                 final SwipeRefreshLayout mSwipeRefreshWidget,
                                 RecyclerView recycler, final Class<T> clazz) {


        //设置颜色
        mSwipeRefreshWidget.setColorSchemeResources(R.color.colorRed, R.color.colorSwipe, R.color.colorSwipe01);

        //设置刷新监听
        mSwipeRefreshWidget.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                if (isLoaidng) {
                    Toast.makeText(activity, "正在请求网络请稍等", Toast.LENGTH_LONG).show();
                    return;
                }

                MyThreadPoolManager.getInstance().execute(new Runnable() {
                    @Override
                    public void run() {

                        isLoaidng = true;

                        //请求网络
                        GeneralProtocal<T> pagerProtocol = new GeneralProtocal<T>(clazz);
                        pagerProtocol.getDataByGET(url);
                        pagerProtocol.setOnDataFromServerListener(new OnDataFromServerListener() {
                            @Override
                            public void onSuccess(Object result) {
                                final T bean = (T) result;
                                ToastUtils.showToast(bean.toString());

                                UiUtils.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        listener.onSuccess(bean);
                                        mSwipeRefreshWidget.setRefreshing(false);
                                        isLoaidng = false;
                                    }
                                });

                            }

                            @Override
                            public void onFail() {
                                UiUtils.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(activity, "请求网络失败", Toast.LENGTH_LONG).show();
                                        listener.onFail();
                                        mSwipeRefreshWidget.setRefreshing(false);
                                        isLoaidng = false;

                                    }
                                });
                            }
                        });


                    }
                });

            }
        });

        // 这句话是为了，第一次进入页面的时候显示加载进度条
        mSwipeRefreshWidget.setProgressViewOffset(false, 0, (int) TypedValue
                .applyDimension(TypedValue.COMPLEX_UNIT_DIP, 24, activity.getResources()
                        .getDisplayMetrics()));

        recycler.setHasFixedSize(true);
    }


    //回调
    private OnDataFromServerListener listener;
    public void setOnDataFromServerListener(OnDataFromServerListener listener){
        this.listener = listener;

    }

}
