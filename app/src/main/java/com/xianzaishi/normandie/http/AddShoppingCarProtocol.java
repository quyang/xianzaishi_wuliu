package com.xianzaishi.normandie.http;

import okhttp3.RequestBody;

/**
 * 添加到购物车将商品信息发送到服务器
 * quyang
 * Created by Administrator on 2016/8/26.
 */
public class AddShoppingCarProtocol extends BaseProtocol {


    public String url;

    public void setUrl(String url){
        this.url = url;
    }

    @Override
    public String getUrl() {
        return url;
    }

    @Override
    public RequestBody getRequestBody() {
        return null;
    }
}
