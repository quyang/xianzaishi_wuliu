package com.xianzaishi.normandie.http;

import com.xianzaishi.normandie.MyApplication;
import com.xianzaishi.normandie.interfaces.OnDataFromPostServerListener;
import com.xianzaishi.normandie.utils.UiUtils;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by Administrator on 2016/9/24.
 */

public abstract class GeneralPostProtocol {


    /**
     * post方式请求网络
     */
    public void getDataByPOST() {

//        OkHttpClient okHttpClient = new OkHttpClient();
        OkHttpClient okHttpClient = MyApplication.getOkHttpClient();

//
//        FormBody formoody = new FormBody.Builder()
//                .add("uid", "10025")
//                .add("pageSize", "10")
//                .add("curPage", "0")
//                .add("status", "0")
//                .build();

        //"http://trade.xianzaishi.net/order/getOrders.json"

        Request request = new Request
                .Builder()
                .url(getUrl())
                .post(getRequestBody())
                .build();


        okHttpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

                UiUtils.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mListener.onFail();
                    }
                });

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                final String string = response.body().string();
                UiUtils.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mListener.onSuccess(string);
                    }
                });

            }
        });
    }


    public abstract RequestBody getRequestBody();

    public abstract String getUrl();


    private OnDataFromPostServerListener mListener;

    public void setOnDataFromPostServerListener(OnDataFromPostServerListener listener) {
        mListener = listener;
    }


}
