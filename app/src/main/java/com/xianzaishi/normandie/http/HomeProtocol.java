package com.xianzaishi.normandie.http;

import okhttp3.RequestBody;

/**
 * 首页请求服务器
 * quyang
 * Created by Administrator on 2016/8/30.
 */
public class HomeProtocol extends BaseProtocol {

    public String url;

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public String getUrl() {
        return url;
    }

    @Override
    public RequestBody getRequestBody() {
        return null;
    }
}
