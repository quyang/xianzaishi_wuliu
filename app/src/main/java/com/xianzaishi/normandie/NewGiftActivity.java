package com.xianzaishi.normandie;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;

import com.xianzaishi.normandie.global.Contants;
import com.xianzaishi.normandie.utils.SPUtils;

public class NewGiftActivity extends AppCompatActivity implements View.OnClickListener{
    private Button cancel,goLook;
    private ImageView imageView;

     @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_gift);
        initView();
    }

    private void initView() {
        SPUtils.putBooleanValue(MyApplication.getContext(), Contants.SP_NAME,"isNewUser",false);
        cancel = (Button) findViewById(R.id.newGift_cancel);
        cancel.setOnClickListener(this);
        goLook= (Button) findViewById(R.id.newGift_goLook);
        goLook.setOnClickListener(this);
        imageView= (ImageView) findViewById(R.id.newGift_image);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Animation anim= AnimationUtils.loadAnimation(this,R.anim.animation_store_detail);
        //imageView.startAnimation(anim);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.newGift_cancel:
                finish();
                break;
            case R.id.newGift_goLook:
                startActivity(new Intent(NewGiftActivity.this,CouponsActivity.class));
                finish();
                break;
        }
    }
}
