package com.xianzaishi.normandie;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.umeng.analytics.MobclickAgent;
import com.xianzaishi.normandie.adapter.CustomFragmentAdapter;
import com.xianzaishi.normandie.bean.ShoppingCarCountBean;
import com.xianzaishi.normandie.common.Urls;
import com.xianzaishi.normandie.customs.CustomViewPager;
import com.xianzaishi.normandie.fragment.CategoryFragment;
import com.xianzaishi.normandie.fragment.HomeFragment;
import com.xianzaishi.normandie.fragment.MineFragment;
import com.xianzaishi.normandie.fragment.ShoppingTrolleyFragment;
import com.xianzaishi.normandie.fragment.TakeOutFragment;
import com.xianzaishi.normandie.global.Contants;
import com.xianzaishi.normandie.utils.GetBeanClass;
import com.xianzaishi.normandie.utils.SPUtils;
import com.xianzaishi.normandie.utils.UiUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import cn.jpush.android.api.JPushInterface;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class MainActivity extends CheckPermissionsActivity implements RadioGroup.OnCheckedChangeListener,
        ViewPager.OnPageChangeListener ,View.OnClickListener{

    private CustomViewPager viewPager;
    //private RadioGroup radioGroup;
    private List<Fragment> fragments;
    private TextView shoppingCount;
    private List<RadioButton> radioButtons=new ArrayList<>();
    public int shoppingCounts=0;
    private ProgressDialog dialog;
    private int mVersionCode;
    private String mVersionName;
    private boolean isFirst=true;
    private OkHttpClient client = MyApplication.getOkHttpClient();
    private static final MediaType MEDIA_TYPE
            = MediaType.parse("application/json; charset=utf-8");
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        findView();
        checkVersion();
        getAdPic();
        uploadingMessage();
    }

    /**
     * 上传给服务器必要的个性化信息
     */
    private void uploadingMessage() {
        String regitstId=JPushInterface.getRegistrationID(this);
        int version=getVersionCode();
        String uid=SPUtils.getStringValue(this,Contants.SP_NAME,Contants.UID,null);
        String token=SPUtils.getStringValue(this,Contants.SP_NAME,Contants.TOKEN,null);
        if (regitstId.equals("")){
            return;
        }
        if (uid==null||token==null) {
            String json = "{\"regitstId\":\"" + regitstId + "\",\"version\":\"" + version + "\"}";
            uploadingToService(json);
        }else {
            String json = "{\"regitstId\":\"" + regitstId + "\",\"version\":\"" + version + "\",\"token\":\"" + token + "\",\"uuid\":\"" + uid + "\"}";
            uploadingToService(json);
        }
    }

    private void uploadingToService(String json) {
        RequestBody requestBody = RequestBody.create(MEDIA_TYPE, json);
        Request request=new Request.Builder()
                .post(requestBody)
                .url(Urls.UPLOADING_MESSAGE)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

            }
            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (!response.isSuccessful()){
                    response.close();
                    return;
                }
                response.close();
            }
        });
    }

    /**
     *  获取首页广告图链接
     */
    private void getAdPic() {
        Request request=new Request.Builder()
                .url(Urls.GET_AD_PIC)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (!response.isSuccessful()){
                    response.close();
                    return;
                }
                try {
                    JSONObject jsonObject= new JSONObject(response.body().string());
                    String adPicUrl=jsonObject.getString("indexpic");
                    SPUtils.putStringValue(MyApplication.getContext(),Contants.SP_NAME,"adPicUrl",adPicUrl);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void findView() {
        viewPager = (CustomViewPager) findViewById(R.id.viewPager);
        /**
         *  购物车商品数量指示器
         */
        shoppingCount= (TextView) findViewById(R.id.shopping_trolley_count);

        /**
         *  找到四个RadioButton放到集合radioButtons里
         */

        RadioButton home= (RadioButton) findViewById(R.id.home_button);
        home.setTag(0);
        home.setChecked(true);
        home.setOnClickListener(this);
        radioButtons.add(home);
        RadioButton category= (RadioButton) findViewById(R.id.classification_button);
        category.setTag(1);
        category.setOnClickListener(this);
        radioButtons.add(category);
        RadioButton takeOut= (RadioButton) findViewById(R.id.takeOut_button);
        takeOut.setTag(2);
        takeOut.setOnClickListener(this);
        radioButtons.add(takeOut);
        RadioButton shoppingTrolley= (RadioButton) findViewById(R.id.shoppingTrolley_button);
        shoppingTrolley.setTag(3);
        shoppingTrolley.setOnClickListener(this);
        radioButtons.add(shoppingTrolley);
        RadioButton mineButton= (RadioButton) findViewById(R.id.mine_button);
        mineButton.setTag(4);
        mineButton.setOnClickListener(this);
        radioButtons.add(mineButton);
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if (isFirst){
            initView();
            isFirst=false;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        String uid=SPUtils.getStringValue(MyApplication.getContext(),Contants.SP_NAME,Contants.UID,null);
        if (uid==null){
            shoppingCount.setVisibility(View.GONE);
        }
        getShoppingCount();

        MobclickAgent.onResume(this);//友盟统计时长
    }

    @Override
    protected void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        if(savedInstanceState!= null)

        {
            String FRAGMENTS_TAG = "Android:support:fragments";
            savedInstanceState.remove(FRAGMENTS_TAG);
        }

    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        int pageNo=intent.getIntExtra("pageNo",-1);
        if (pageNo!=-1) {
            viewPager.setCurrentItem(pageNo);
        }
    }

    private void initView() {
        fragments = new ArrayList<>();
        //实例化Fragment添加到集合中
        HomeFragment homeFragment = new HomeFragment();
        CategoryFragment categoryFragment = new CategoryFragment();
        TakeOutFragment takeOutFragment=new TakeOutFragment();
        ShoppingTrolleyFragment shoppingTrolleyFragment = new ShoppingTrolleyFragment();
        MineFragment mineFragment = new MineFragment();

        fragments.add(homeFragment);
        fragments.add(categoryFragment);
        fragments.add(takeOutFragment);
        fragments.add(shoppingTrolleyFragment);
        fragments.add(mineFragment);
        //给ViewPager填充数据

        viewPager.setAdapter(new CustomFragmentAdapter(getSupportFragmentManager(), fragments));
        viewPager.setOffscreenPageLimit(4);
        viewPager.addOnPageChangeListener(this);
        //给RadioButton设置监听
        //radioGroup = (RadioGroup) findViewById(R.id.radio_group);
        //radioGroup.setOnCheckedChangeListener(this);
        //((RadioButton) radioGroup.getChildAt(0)).setChecked(true);

    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        /*if (position==2) {
            ((RadioButton) radioGroup.getChildAt(position).findViewById(R.id.shoppingTrolley_button)).setChecked(true);
        }else {
            ((RadioButton) radioGroup.getChildAt(position)).setChecked(true);
        }*/
        for (int i = 0; i < 5; i++) {
            if(position==i){
                //((RadioButton)findViewById(radioButtons.get(position))).setChecked(true);
                radioButtons.get(position).setChecked(true);
            }else {
                //((RadioButton)findViewById(radioButtons.get(i))).setChecked(false);
                radioButtons.get(i).setChecked(false);
            }
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    /**
     *  View.OnClickListener
     * @param view
     */
    @Override
    public void onClick(View view) {
        for(RadioButton radioButton:radioButtons){
            if (radioButton.getId()==view.getId()){
                //((RadioButton)findViewById(id)).setChecked(true);
                radioButton.setChecked(true);
                viewPager.setCurrentItem((Integer) radioButton.getTag());
                //友盟统计 自定义事件
                switch (view.getId()){
                    case R.id.home_button:
                        MobclickAgent.onEvent(this,"TabHome");
                        break;
                    case R.id.takeOut_button:
                        MobclickAgent.onEvent(this,"TabTakeOut");
                        break;
                    case R.id.classification_button:
                        MobclickAgent.onEvent(this,"TabSort");
                        break;
                    case R.id.shoppingTrolley_button:
                        MobclickAgent.onEvent(this,"TabCart");
                        break;
                    case R.id.mine_button:
                        MobclickAgent.onEvent(this,"TabMine");
                        break;
                }

            }else {
                //((RadioButton)findViewById(id)).setChecked(false);
                radioButton.setChecked(false);
            }
        }
    }

    /**
     * 自定义两个fragment之间跳转的接口
     */
    public interface Fragment2Fragment {
        void gotoFragment(CustomViewPager viewPager);
    }

    private Fragment2Fragment fragment2Fragment;

    public void setFragment2Fragment(Fragment2Fragment fragment2Fragment) {
        this.fragment2Fragment = fragment2Fragment;
    }

    /**
     * 给该Activity设置一个调用此接口中定义此方法的方法
     */
    public void forSkip() {
        if (fragment2Fragment != null) {
            fragment2Fragment.gotoFragment(viewPager);
        }
    }

    @Override
    public void onCheckedChanged(RadioGroup radioGroup, int i) {
        switch (i) {
            case R.id.home_button:
                viewPager.setCurrentItem(0);
                break;
            case R.id.classification_button:
                viewPager.setCurrentItem(1);
                break;
            case R.id.takeOut_button:
                viewPager.setCurrentItem(2);
            case R.id.shoppingTrolley_button:
                MobclickAgent.onEvent(this,"goToCart");
                viewPager.setCurrentItem(3);
                break;
            case R.id.mine_button:
                viewPager.setCurrentItem(4);
                break;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        //退出这界面的时候清空购物车
        //cleanShoppingCar();
    }



    /**
     * 获取购物车商品数量
     */
    private void getShoppingCount() {
        OkHttpClient client=MyApplication.getOkHttpClient();
        String uid= SPUtils.getStringValue(MyApplication.getContext(), Contants.SP_NAME,Contants.UID,"");
        RequestBody requestForm=new FormBody.Builder()
                .add("uid",uid)
                .build();
        final Request request=new Request.Builder()
                .url(Urls.GET_ALL_COUNT)
                .post(requestForm)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if(!response.isSuccessful()) throw new IOException("Unexpected code"+response);
                final ShoppingCarCountBean shoppingCarCountBean = (ShoppingCarCountBean) GetBeanClass.getBean(response,ShoppingCarCountBean.class);
                response.close();
                if (shoppingCarCountBean.getCode()==1){
                    UiUtils.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if(!"0".equals(shoppingCarCountBean.getData())){
                                shoppingCount.setVisibility(View.VISIBLE);
                                shoppingCount.setText(shoppingCarCountBean.getData());
                                shoppingCounts=Integer.valueOf(shoppingCarCountBean.getData());
                            }else {
                                shoppingCount.setText("");
                                shoppingCount.setVisibility(View.GONE);
                            }
                        }
                    });
                }
            }
        });
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {

        if(keyCode==4){
            dialog();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
    /**
     *  退出程序时弹出dialog
     */
    private void dialog(){
        LayoutInflater inflater=this.getLayoutInflater();
        View view=inflater.inflate(R.layout.commodity_delete_dialog,null);
        final Dialog dialog1=new Dialog(this,R.style.Dialog);
        dialog1.setCancelable(false);
        dialog1.setContentView(view);
        TextView sure= (TextView) view.findViewById(R.id.commodity_dialog_sure);
        TextView cancel= (TextView) view.findViewById(R.id.commodity_dialog_cancel);
        TextView hint= (TextView) view.findViewById(R.id.dialog_hint);
        TextView title= (TextView) view.findViewById(R.id.dialog_title);
        title.setVisibility(View.GONE);
        hint.setText("确认退出“鲜在时”么？");
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.WRAP_CONTENT);
        lp.setMargins(0,80,0,25);
        hint.setLayoutParams(lp);
        hint.setGravity(Gravity.CENTER);
        sure.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                dialog1.dismiss();
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog1.dismiss();
            }
        });

        dialog1.show();

        Window dialogWindow = dialog1.getWindow();
        WindowManager m = this.getWindowManager();
        Display d = m.getDefaultDisplay(); // 获取屏幕宽、高度
        WindowManager.LayoutParams p = dialogWindow.getAttributes(); // 获取对话框当前的参数值
        p.width = (int) (d.getWidth() * 0.6); // 宽度设置为屏幕的0.6，根据实际情况调整
        dialogWindow.setAttributes(p);
    }

    /**
     *  版本更新
     */
    Handler mHandler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 1://更新下载进度
                    int progress = msg.arg1;
                    dialog.setProgress(progress);
                    break;
                case 2://显示升级弹窗
                    showUpdateDialog();
                    break;
                default:
                    break;
            }
            super.handleMessage(msg);
        }
    };

    //检测版本更新
    private void checkVersion() {

        final Request request = new Request
                .Builder()
                .url(Urls.CHECK_VERSION)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
            }
            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (response.isSuccessful()) {
                    String result = response.body().string();
                    System.out.println(result);
                    try {
                        JSONObject jsonObject = new JSONObject(result);
                        mVersionCode = jsonObject.getInt("android_version");
                        mVersionName = jsonObject.getString("versionname");
                        long remind=jsonObject.getInt("remind");
                        long current=System.currentTimeMillis();
                        String upTime=SPUtils.getStringValue(MyApplication.getContext(),Contants.SP_NAME,"upTime",null);
                        //这个是本地版本号
                        int localVersionCode = getVersionCode();
                        if (localVersionCode < mVersionCode&&upTime==null) {
                            //有更新
                            mHandler.sendEmptyMessage(2);
                            SPUtils.putStringValue(MyApplication.getContext(),Contants.SP_NAME,"upTime",current+"");
                        }else if(localVersionCode < mVersionCode&&(current-Long.valueOf(upTime))>remind*1000){
                            mHandler.sendEmptyMessage(2);
                            SPUtils.putStringValue(MyApplication.getContext(),Contants.SP_NAME,"upTime",current+"");
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    //获取版本号
    private int getVersionCode() {
        //获取包管理器
        PackageManager pm = getPackageManager();
        try {
            PackageInfo packageInfo = pm.getPackageInfo(getPackageName(), 0);
            return packageInfo.versionCode;//版本号
        } catch (PackageManager.NameNotFoundException e) {
            //包名没有找到的
            e.printStackTrace();
        }
        return -1;
    }


    //显示升级弹窗
    protected void showUpdateDialog() {
        View updateView=LayoutInflater.from(this).inflate(R.layout.update_view,null);
        final Dialog dialog=new Dialog(this, R.style.Dialog);
        dialog.setCancelable(false);
        dialog.setContentView(updateView);
        TextView cancel= (TextView) updateView.findViewById(R.id.cancel_button);
        Button update= (Button) updateView.findViewById(R.id.update_button);
        TextView content= (TextView) updateView.findViewById(R.id.content);
        content.setText("鲜在时1.1.4版本更新内容：\n1.新增配送提示—购物从未如此贴心\n2.更多的活动版块—购物从未如此丰富多彩\n3." +
                "活动商品直接加入购物车—购物从未如此快捷");
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                downLoad();
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    public void downLoad() {

        dialog = new ProgressDialog(this);
        dialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        dialog.setTitle("下载进度");
        dialog.setCancelable(true);
        dialog.show();
        OkHttpClient client = new OkHttpClient();
        Request request = new Request
                .Builder()
                .url(Urls.DOWN_LOAD_APK)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
            }
            @Override
            public void onResponse(Call call, Response response) throws IOException {
                InputStream is = null;
                byte[] buf = new byte[2048];
                int len = 0;
                FileOutputStream fos = null;
                //文件路径
                File path = Environment.getExternalStorageDirectory();
                try {
                    is = response.body().byteStream();
                    long total = response.body().contentLength();
                    File file = new File(path, "xianzaishi.apk");
                    fos = new FileOutputStream(file);
                    long sum = 0;
                    while ((len = is.read(buf)) != -1) {
                        fos.write(buf, 0, len);
                        sum += len;
                        int progress = (int) (sum * 1.0f / total * 100);
                        Message msg = mHandler.obtainMessage();
                        msg.what = 1;
                        msg.arg1 = progress;
                        mHandler.sendMessage(msg);
                    }
                    fos.flush();
                    //跳到安装页面
                    Intent intent = new Intent();
                    intent.setAction(Intent.ACTION_VIEW);
                    intent.addCategory(Intent.CATEGORY_DEFAULT);
                    intent.setDataAndType(Uri.fromFile(file),
                            "application/vnd.android.package-archive");
                    //startActivityForResult(intent, 0);
                    startActivity(intent);
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    try {
                        if (is != null)
                            is.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    try {
                        if (fos != null)
                            fos.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

    }

    @Override
    protected void onPause() {
        super.onPause();

        MobclickAgent.onPause(this);//友盟统计时长
    }
}
