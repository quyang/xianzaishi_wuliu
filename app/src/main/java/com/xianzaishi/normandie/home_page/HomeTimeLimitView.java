package com.xianzaishi.normandie.home_page;

import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.umeng.analytics.MobclickAgent;
import com.xianzaishi.normandie.GoodsDetailsActivity;
import com.xianzaishi.normandie.HuoDongActivity;
import com.xianzaishi.normandie.MainActivity;
import com.xianzaishi.normandie.MyApplication;
import com.xianzaishi.normandie.R;
import com.xianzaishi.normandie.adapter.TimeLimitGoodsAdapter;
import com.xianzaishi.normandie.adapter.TimeLimitIndicatorAdapter;
import com.xianzaishi.normandie.bean.Home2TopBean;
import com.xianzaishi.normandie.bean.NewHomePageBean;
import com.xianzaishi.normandie.bean.SystemTimeBean;
import com.xianzaishi.normandie.common.Urls;
import com.xianzaishi.normandie.global.Contants;
import com.xianzaishi.normandie.utils.GetBeanClass;
import com.xianzaishi.normandie.utils.GlideUtils;
import com.xianzaishi.normandie.utils.NetUtils;
import com.xianzaishi.normandie.utils.UiUtils;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by ShenLang on 2016/12/20.
 * 首页限时抢购
 */

public class HomeTimeLimitView extends HomeBaseView<NewHomePageBean.ModuleBean> implements View.OnClickListener{
    private RecyclerView timeLimitIndicator,timeLimitGoods;
    private ImageView timeLimitBanner;
    private ArrayList<NewHomePageBean.ModuleBean.InnerStepInfoBean> timeLimitLists=new ArrayList<>();
    private List<NewHomePageBean.ModuleBean.InnerStepInfoBean.ItemsBean> timeLimitGoodsLists=new ArrayList<>();
    private NewHomePageBean.ModuleBean moduleBean;
    private OkHttpClient client= MyApplication.getOkHttpClient();
    private static final MediaType MEDIA_TYPE
            = MediaType.parse("application/json; charset=utf-8");
    private int selectedPosition;

    public HomeTimeLimitView(MainActivity activity) {
        super(activity);
    }

    @Override
    protected void getView(NewHomePageBean.ModuleBean moduleBean, LinearLayout linearLayout,long currentTime,String color) {
        this.moduleBean=moduleBean;
        View view=mInflate.inflate(R.layout.home_time_limit_view,null);
        timeLimitBanner= (ImageView) view.findViewById(R.id.home_time_limit_banner);
        timeLimitBanner.setOnClickListener(this);
        timeLimitIndicator= (RecyclerView) view.findViewById(R.id.home_time_limit_indicator);
        timeLimitGoods= (RecyclerView) view.findViewById(R.id.home_time_limit_goods);
        timeLimitIndicator.setLayoutManager(new LinearLayoutManager(mActivity,LinearLayoutManager.HORIZONTAL,false));
        timeLimitGoods.setLayoutManager(new LinearLayoutManager(mActivity,LinearLayoutManager.HORIZONTAL,false));

        linearLayout.addView(view);
        dealWithTheData(moduleBean,currentTime);
    }

    private void dealWithTheData(NewHomePageBean.ModuleBean moduleBean,long currentTime) {
        if(moduleBean.getPicList()!=null){
            GlideUtils.LoadImage(mActivity,moduleBean.getPicList().get(0).getPicUrl(),timeLimitBanner);
        }
        timeLimitLists= (ArrayList<NewHomePageBean.ModuleBean.InnerStepInfoBean>) moduleBean.getInnerStepInfo();
        SimpleDateFormat format=new SimpleDateFormat("kk", Locale.CHINA);
        String hourStr=format.format(currentTime);
        int hour=Integer.valueOf(hourStr);
        initTimeLimitShopping(hour);
    }


    /**
     *  限时抢购模块
     */
    private void initTimeLimitShopping(int currentHour) {
        final ArrayList<NewHomePageBean.ModuleBean.InnerStepInfoBean> list=new ArrayList<>();
        selectedPosition=-1;

        for (NewHomePageBean.ModuleBean.InnerStepInfoBean bean:timeLimitLists){

            String[] time=bean.getTitle().split("=");
            int timeStart=Integer.valueOf(time[0]);
            int timeEnd=Integer.valueOf(time[1]);

            if (currentHour<timeStart){
                bean.setTitle(String.format(Locale.CHINESE,"%02d",timeStart)+":00-"+String.format(Locale.CHINESE,"%02d",timeEnd)+":00\n未开始");
                bean.setChecked(false);
            }else if(currentHour>=timeStart&&currentHour<timeEnd){
                bean.setTitle(String.format(Locale.CHINESE,"%02d",timeStart)+":00-"+String.format(Locale.CHINESE,"%02d",timeEnd)+":00\n抢购中");
                bean.setChecked(true);
                selectedPosition=timeLimitLists.indexOf(bean);
            }else{
                bean.setTitle(String.format(Locale.CHINESE,"%02d",timeStart)+":00-"+String.format(Locale.CHINESE,"%02d",timeEnd)+":00\n已结束");
                bean.setChecked(false);
            }
            list.add(bean);
        }

        if(selectedPosition==-1){
            NewHomePageBean.ModuleBean.InnerStepInfoBean moduleBean0=list.get(list.size()-1);
            moduleBean0.setChecked(true);
            String s=moduleBean0.getTitle().substring(0,12);
            moduleBean0.setTitle(s+"抢购中");
            selectedPosition=list.size()-1;
        }else if (selectedPosition<list.size()-1){
            NewHomePageBean.ModuleBean.InnerStepInfoBean moduleBean0=list.get(selectedPosition+1);
            String s=moduleBean0.getTitle().substring(0,12);
            moduleBean0.setTitle(s+"即将开始");
        }

        UiUtils.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                final TimeLimitIndicatorAdapter adapter=new TimeLimitIndicatorAdapter(mActivity,list);
                final TimeLimitGoodsAdapter adapter1=new TimeLimitGoodsAdapter(timeLimitGoodsLists,mActivity);
                timeLimitIndicator.setAdapter(adapter);
                timeLimitGoods.setAdapter(adapter1);
                if (list.get(selectedPosition).getItems()==null){
                    return;
                }
                timeLimitGoodsLists.addAll(list.get(selectedPosition).getItems());
                adapter1.notifyDataSetChanged();
                timeLimitIndicator.getLayoutManager().scrollToPosition(selectedPosition);
                adapter.setOnTimeLimitClickListener(new TimeLimitIndicatorAdapter.OnTimeLimitClickListener() {
                    @Override
                    public void onTimeLimitClickListener(int position) {
                        for (int i = 0; i < list.size(); i++) {
                            if (i==position){
                                timeLimitIndicator.smoothScrollToPosition(i);
                                list.get(i).setChecked(true);
                                timeLimitGoodsLists.clear();
                                timeLimitGoodsLists.addAll(list.get(i).getItems());
                                adapter1.notifyDataSetChanged();
                            }else {
                                list.get(i).setChecked(false);
                            }
                        }
                        adapter.notifyDataSetChanged();
                    }
                });
            }
        });

    }

    /**
     * 根据楼层ID获取最新数据
     */
    private void getDataByStepId(final List<Home2TopBean.ModuleBean.ItemsBean> timeLimitGoodsLists, final TimeLimitGoodsAdapter adapter1, int stepId) {
        String str = "{\"pageId\":\"30\",\"stepIds\":\""+stepId+"\"}";

        RequestBody requestBody = RequestBody.create(MEDIA_TYPE, str);

        Request request = new Request.Builder()
                .post(requestBody)
                .url(Urls.HOME_DATA_BY_STEPID)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
            }
            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (response.isSuccessful()){
                    final Home2TopBean bean= (Home2TopBean) GetBeanClass.getBean(response,Home2TopBean.class);
                    if (bean!=null&&bean.isSuccess()&&bean.getModule()!=null) {
                        UiUtils.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                timeLimitGoodsLists.addAll(bean.getModule().get(0).items);
                                adapter1.notifyDataSetChanged();
                            }
                        });

                    }
                }
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (moduleBean.getPicList().get(0).getTargetType()){
            case 0://不能点击
                break;
            case 1://活动页
                Intent intent = new Intent(mActivity, HuoDongActivity.class);
                String targetId=moduleBean.getPicList().get(0).getTargetId();
                intent.putExtra("targetId", targetId);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                mActivity.startActivity(intent);
                break;
            case 2://商品详情页
                MobclickAgent.onEvent(mActivity,"tapItem");
                Intent intent1 = new Intent(mActivity, GoodsDetailsActivity.class);
                String targetId1=moduleBean.getPicList().get(0).getTargetId();
                intent1.putExtra(Contants.ITEM_ID, Integer.valueOf(targetId1));
                intent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                mActivity.startActivity(intent1);
                break;
            case 3://领券
                NetUtils.obtainCoupons(mActivity,moduleBean.getPicList().get(0).getTargetId());
                break;
            case 4://
                NetUtils.toHTML5(mActivity,moduleBean.getPicList().get(0).getTargetId(),moduleBean.getPicList().get(0).getTitleInfo());
                break;
        }
    }
}
