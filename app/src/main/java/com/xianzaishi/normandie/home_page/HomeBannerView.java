package com.xianzaishi.normandie.home_page;

import android.os.Handler;
import android.os.Message;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.GridLayoutManager;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.xianzaishi.normandie.MainActivity;
import com.xianzaishi.normandie.MyApplication;
import com.xianzaishi.normandie.R;
import com.xianzaishi.normandie.adapter.AutoRecyclePagerAdapter2;
import com.xianzaishi.normandie.adapter.FoorTwoAdapter;
import com.xianzaishi.normandie.bean.CategoryBean;
import com.xianzaishi.normandie.bean.Home2TopBean;
import com.xianzaishi.normandie.bean.NewHomePageBean;
import com.xianzaishi.normandie.customs.FixedSpeedScroller;
import com.xianzaishi.normandie.customs.MyRecyclerView;
import com.xianzaishi.normandie.global.Contants;
import com.xianzaishi.normandie.utils.GetBeanClass;
import com.xianzaishi.normandie.utils.UiUtils;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by ShenLang on 2016/12/20.
 * 首页顶部轮播图模块
 */

public class HomeBannerView extends HomeBaseView<List<NewHomePageBean.ModuleBean.PicListBean>>{

    private ViewPager mViewPager;
    private LinearLayout indicatorContainer;
    private List<ImageView> guideArray;
    private int bannerCount;

    /**
     * 实现viewpager自滚动的相关变量
     */
    private final int KEEP_SCROLL=0;
    private final long DELAY_TIME=5000;
    private boolean FIRST;
    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (msg.what==KEEP_SCROLL){
                mViewPager.setCurrentItem(mViewPager.getCurrentItem()+1,true);
                enqueueBannerLoopMessage();
            }
        }
    };

    public HomeBannerView(MainActivity activity) {
        super(activity);
    }

    @Override
    protected void getView(List<NewHomePageBean.ModuleBean.PicListBean> picList, LinearLayout linearLayout,long c,String color) {



        View view=mInflate.inflate(R.layout.home_banner_view,null);
        mViewPager= (ViewPager) view.findViewById(R.id.home_viewPager_top);
        indicatorContainer= (LinearLayout) view.findViewById(R.id.home_guide_parent);
        linearLayout.addView(view);
        guideArray=new ArrayList<>();
        dealWithTheData(picList);

    }

    /**
     *  初始化首页分类楼层
     */
    private void initCategoryFloor() {
        OkHttpClient client= MyApplication.getOkHttpClient();
        Request request=new Request.Builder()
                .url(Contants.CATEGORY_URL)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (response.isSuccessful()){
                    final CategoryBean categoryBean= (CategoryBean) GetBeanClass.getBean(response,CategoryBean.class);
                    if (categoryBean!=null&&categoryBean.getResultCode()==1){
                        UiUtils.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                ArrayList<CategoryBean.ModuleBean> categoryLists=new ArrayList<CategoryBean.ModuleBean>();
                                int i=0;
                                for (CategoryBean.ModuleBean moduleBean:categoryBean.getModule()){
                                    if (i<8){
                                        i++;
                                        categoryLists.add(moduleBean);
                                    }
                                }

                            }
                        });
                    }
                }
            }
        });
    }

    private void dealWithTheData(List<NewHomePageBean.ModuleBean.PicListBean> picList) {
        guideArray.clear();
        indicatorContainer.removeAllViews();
        bannerCount=picList.size();
        //设配数据
        mViewPager.setAdapter(new AutoRecyclePagerAdapter2(mActivity,picList));
        enqueueBannerLoopMessage();
        initGuide();
        initViewPagerListener();
        changeViewPagerSpeed(mViewPager,700);
    }


    /**
     *  初始化导航圆点
     */
    private void initGuide() {
        if (bannerCount<2){
            return;
        }
        for (int i = 0; i < bannerCount; i++) {
            View view=  mInflate.inflate(R.layout.viewpager_guide_circle,null);
            ImageView guide= (ImageView) view.findViewById(R.id.image_guide);
            guideArray.add(guide);
            indicatorContainer.addView(view);
        }
        guideArray.get(0).setEnabled(false);
    }

    /**
     *  为轮播图添加监听
     */
    private void initViewPagerListener() {
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                position%=bannerCount;
                for (int i = 0; i < bannerCount; i++) {
                    if (i==position){
                        guideArray.get(i).setEnabled(false);
                    }else {
                        guideArray.get(i).setEnabled(true);
                    }
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                switch (state){
                    case ViewPager.SCROLL_STATE_DRAGGING:
                        removeBannerLoopMessage();
                        FIRST=true;
                        break;
                    case ViewPager.SCROLL_STATE_IDLE:
                        if(FIRST){
                            enqueueBannerLoopMessage();
                            FIRST=false;
                        }
                }
            }
        });
    }

    /**
     * 通过反射改变轮播图的滚动速度
     */
    private void changeViewPagerSpeed(ViewPager mViewPager, int i) {
        try {
            Field mScroller;
            mScroller = ViewPager.class.getDeclaredField("mScroller");
            mScroller.setAccessible(true);
            FixedSpeedScroller scroller = new FixedSpeedScroller(mActivity);
            scroller.setmDuration(i);   //改变滑动速度
            mScroller.set(mViewPager,scroller);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // 添加Banner循环消息到队列
    public void enqueueBannerLoopMessage() {
        if (bannerCount <= 1) return;
        mHandler.sendEmptyMessageDelayed(KEEP_SCROLL, DELAY_TIME);
    }

    // 移除Banner循环的消息
    public void removeBannerLoopMessage() {
        if (mHandler.hasMessages(KEEP_SCROLL)) {
            mHandler.removeMessages(KEEP_SCROLL);
        }
    }
}
