package com.xianzaishi.normandie.home_page;

import android.content.Intent;
import android.os.CountDownTimer;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.umeng.analytics.MobclickAgent;
import com.xianzaishi.normandie.GoodsDetailsActivity;
import com.xianzaishi.normandie.HuoDongActivity;
import com.xianzaishi.normandie.MainActivity;
import com.xianzaishi.normandie.R;
import com.xianzaishi.normandie.adapter.TimeDownGoodsAdapter;
import com.xianzaishi.normandie.bean.NewHomePageBean;
import com.xianzaishi.normandie.global.Contants;
import com.xianzaishi.normandie.utils.GlideUtils;
import com.xianzaishi.normandie.utils.NetUtils;

import java.util.Locale;

/**
 * Created by ShenLang on 2016/12/27.
 * 首页倒计时抢购模块
 */

public class HomeTimeCountDownView extends HomeBaseView<NewHomePageBean.ModuleBean> implements View.OnClickListener{
    private RecyclerView timeDownGoods;
    private ImageView timeDownBanner;
    private TextView day,hour,minute,second;
    private LinearLayout timeDownContainer;
    private NewHomePageBean.ModuleBean moduleBean;
    private long currentTime;
    private static final long DAY_MILLINS=86400000;

    public HomeTimeCountDownView(MainActivity activity) {
        super(activity);
    }

    @Override
    protected void getView(NewHomePageBean.ModuleBean moduleBean, LinearLayout linearLayout, long currentTime,String color) {
        this.moduleBean=moduleBean;
        this.currentTime=currentTime;
        View view=mInflate.inflate(R.layout.home_time_count_down,null);
        timeDownGoods= (RecyclerView) view.findViewById(R.id.home_time_countDown_goods);
        timeDownBanner= (ImageView) view.findViewById(R.id.home_time_countDown_banner);
        timeDownContainer= (LinearLayout) view.findViewById(R.id.home_time_countDown_container);
        day= (TextView) view.findViewById(R.id.home_time_countDown_day);
        hour= (TextView) view.findViewById(R.id.home_time_countDown_hour);
        minute= (TextView) view.findViewById(R.id.home_time_countDown_minute);
        second= (TextView) view.findViewById(R.id.home_time_countDown_secont);
        linearLayout.addView(view);

        dealWithTheData(moduleBean);
    }

    private void dealWithTheData(NewHomePageBean.ModuleBean moduleBean) {
        if(moduleBean.getPicList()!=null){
            GlideUtils.LoadImage(mActivity,moduleBean.getPicList().get(0).getPicUrl(),timeDownBanner);
        }
        timeDownBanner.setOnClickListener(this);
        //倒计时相关
        long countDownMillins=moduleBean.getEnd()-currentTime;
        if (countDownMillins>0){
            initCountDown(countDownMillins);
        }else {
            day.setVisibility(View.VISIBLE);
            timeDownContainer.setVisibility(View.GONE);
            day.setText("00:00:00");
        }
        //限时抢购的商品
        if (moduleBean.getItems()==null){
            return;
        }
        TimeDownGoodsAdapter adapter=new TimeDownGoodsAdapter(moduleBean.getItems(),mActivity);
        timeDownGoods.setLayoutManager(new LinearLayoutManager(mActivity,LinearLayoutManager.HORIZONTAL,false));
        timeDownGoods.setAdapter(adapter);
    }

    private void initCountDown(long countDownMillins) {
        CountDownTimer countDownTimer=new CountDownTimer(countDownMillins,1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                if (millisUntilFinished<DAY_MILLINS) {
                    day.setVisibility(View.GONE);
                    timeDownContainer.setVisibility(View.VISIBLE);

                    int hours = (int) (millisUntilFinished / 3600000);
                    hour.setText(String.format(Locale.CHINESE, "%02d", hours));
                    int minutes = (int) ((millisUntilFinished % 3600000) / 60000);
                    minute.setText(String.format(Locale.CHINESE, "%02d", minutes));
                    int seconds = (int) (((millisUntilFinished % 3600000) % 60000) / 1000);
                    second.setText(String.format(Locale.CHINESE, "%02d", seconds));
                }else {
                    day.setVisibility(View.VISIBLE);
                    timeDownContainer.setVisibility(View.GONE);
                    int dayCount= (int) (millisUntilFinished/DAY_MILLINS);

                    day.setText(dayCount+"天");
                }
            }

            @Override
            public void onFinish() {
                second.setText("00");
            }
        };
        countDownTimer.start();
    }

    @Override
    public void onClick(View view) {
        switch (moduleBean.getPicList().get(0).getTargetType()){
            case 0://不能点击
                break;
            case 1://活动页
                Intent intent = new Intent(mActivity, HuoDongActivity.class);
                String targetId=moduleBean.getPicList().get(0).getTargetId();
                intent.putExtra("targetId", targetId);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                mActivity.startActivity(intent);
                break;
            case 2://商品详情页
                MobclickAgent.onEvent(mActivity,"tapItem");
                Intent intent1 = new Intent(mActivity, GoodsDetailsActivity.class);
                String targetId1=moduleBean.getPicList().get(0).getTargetId();
                intent1.putExtra(Contants.ITEM_ID, Integer.valueOf(targetId1));
                intent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                mActivity.startActivity(intent1);
                break;
            case 3://领券
                NetUtils.obtainCoupons(mActivity,moduleBean.getPicList().get(0).getTargetId());
                break;
            case 4://
                NetUtils.toHTML5(mActivity,moduleBean.getPicList().get(0).getTargetId(),moduleBean.getPicList().get(0).getTitleInfo());
                break;
        }
    }
}
