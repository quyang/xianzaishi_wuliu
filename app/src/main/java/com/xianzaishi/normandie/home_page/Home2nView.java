package com.xianzaishi.normandie.home_page;

import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.GridLayout;
import android.widget.LinearLayout;

import com.xianzaishi.normandie.MainActivity;
import com.xianzaishi.normandie.R;
import com.xianzaishi.normandie.adapter.MorePictrueAdapter;
import com.xianzaishi.normandie.bean.NewHomePageBean;

import java.util.List;

/**
 * Created by ShenLang on 2016/12/26.
 * 首页2n图片楼层
 */

public class Home2nView extends HomeBaseView<List<NewHomePageBean.ModuleBean.PicListBean>> {

    private RecyclerView recyclerView;
    private static final int COLUMNS=2;
    public Home2nView(MainActivity activity) {
        super(activity);
    }

    @Override
    protected void getView(List<NewHomePageBean.ModuleBean.PicListBean> picList, LinearLayout linearLayout, long currentTime,String color) {
        View view = mInflate.inflate(R.layout.four_pics, null);
        recyclerView= (RecyclerView) view.findViewById(R.id.four_pics_recyclerView);
        linearLayout.addView(view);

        dealWithTheData(picList);
    }

    private void dealWithTheData(List<NewHomePageBean.ModuleBean.PicListBean> picList) {
        recyclerView.setLayoutManager(new GridLayoutManager(mActivity,COLUMNS, GridLayout.VERTICAL,false){
            @Override
            public boolean canScrollVertically() {
                return false;
            }
        });
        recyclerView.setAdapter(new MorePictrueAdapter(picList,mActivity));
    }
}
