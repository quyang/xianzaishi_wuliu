package com.xianzaishi.normandie.home_page;

import android.view.LayoutInflater;
import android.widget.LinearLayout;

import com.xianzaishi.normandie.MainActivity;

import java.util.List;

/**
 * Created by ShenLang on 2016/12/20.
 *  首页模块基类
 */

public abstract class HomeBaseView<T> {
    protected MainActivity mActivity;
    protected LayoutInflater mInflate;

    public HomeBaseView(MainActivity activity){
        mActivity=activity;
        mInflate=LayoutInflater.from(activity);
    }
    public boolean fillView(T t, LinearLayout linearLayout,long currentTime,String color) {
        if (t == null) {
            return false;
        }
        if ((t instanceof List) && ((List) t).size() == 0) {
            return false;
        }
        getView(t, linearLayout,currentTime,color);
        return true;
    }

    protected abstract void getView(T t, LinearLayout linearLayout,long currentTime,String color);
}
