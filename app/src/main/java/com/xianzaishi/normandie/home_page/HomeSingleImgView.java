package com.xianzaishi.normandie.home_page;

import android.content.Intent;
import android.graphics.Bitmap;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.target.Target;
import com.umeng.analytics.MobclickAgent;
import com.xianzaishi.normandie.GoodsDetailsActivity;
import com.xianzaishi.normandie.HuoDongActivity;
import com.xianzaishi.normandie.MainActivity;
import com.xianzaishi.normandie.R;
import com.xianzaishi.normandie.bean.Home2DownBean;
import com.xianzaishi.normandie.bean.NewHomePageBean;
import com.xianzaishi.normandie.global.Contants;
import com.xianzaishi.normandie.utils.GlideUtils;
import com.xianzaishi.normandie.utils.NetUtils;

import java.util.List;

/**
 * Created by ShenLang on 2016/12/21.
 * 单图楼层
 */

public class HomeSingleImgView extends HomeBaseView<List<NewHomePageBean.ModuleBean.PicListBean>>implements View.OnClickListener{
    private ImageView imageView;
    private List<NewHomePageBean.ModuleBean.PicListBean> picList;
    public HomeSingleImgView(MainActivity activity) {
        super(activity);
    }

    @Override
    protected void getView(List<NewHomePageBean.ModuleBean.PicListBean> picList, LinearLayout linearLayout,long c,String color) {
        this.picList=picList;
        View view=mInflate.inflate(R.layout.home_single_img_view,null);
        imageView= (ImageView) view.findViewById(R.id.home_single_img_view);
        imageView.setOnClickListener(this);
        linearLayout.addView(view);

        dealWithTheData(picList);
    }

    private void dealWithTheData(final List<NewHomePageBean.ModuleBean.PicListBean> picList) {
        /**
         *  让图片等比例缩放
         */
        Glide.with(mActivity).load( picList.get(0).getPicUrl()).asBitmap().into(new SimpleTarget<Bitmap>(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL) {
            @Override
            public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                int imageWidth = resource.getWidth();
                int imageHeight = resource.getHeight();
                int height = mActivity.getResources().getDisplayMetrics().widthPixels * imageHeight / imageWidth;
                ViewGroup.LayoutParams para = imageView.getLayoutParams();
                para.height = height;
                imageView.setLayoutParams(para);
                //计算好宽高后正式加载图片
                GlideUtils.LoadImage(mActivity,picList.get(0).getPicUrl(), imageView);
            }
        });

    }

    @Override
    public void onClick(View view) {
        switch (picList.get(0).getTargetType()){
            case 0://不能点击
                break;
            case 1://活动页
                Intent intent = new Intent(mActivity, HuoDongActivity.class);
                String targetId=picList.get(0).getTargetId();
                intent.putExtra("targetId", targetId);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                mActivity.startActivity(intent);
                break;
            case 2://商品详情页
                MobclickAgent.onEvent(mActivity,"tapItem");
                Intent intent1 = new Intent(mActivity, GoodsDetailsActivity.class);
                String targetId1=picList.get(0).getTargetId();
                intent1.putExtra(Contants.ITEM_ID, Integer.valueOf(targetId1));
                intent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                mActivity.startActivity(intent1);
                break;
            case 3://领券
                NetUtils.obtainCoupons(mActivity,picList.get(0).getTargetId());
                break;
            case 4://
                NetUtils.toHTML5(mActivity,picList.get(0).getTargetId(),picList.get(0).getTitleInfo());
                break;
        }
    }
}
