package com.xianzaishi.normandie.home_page;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.LinearLayout;

import com.xianzaishi.normandie.MainActivity;
import com.xianzaishi.normandie.R;
import com.xianzaishi.normandie.adapter.TasteNewViewPagerAdapter;
import com.xianzaishi.normandie.bean.NewHomePageBean;
import com.xianzaishi.normandie.fragment.TasteNewFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ShenLang on 2016/12/21.
 *  5n商品模块
 */

public class Home5nView  extends HomeBaseView<List<NewHomePageBean.ModuleBean.ItemsBean>> {
    private ArrayList<NewHomePageBean.ModuleBean.ItemsBean> list;
    private ArrayList<TasteNewFragment> fragments=new ArrayList<>();
    private LinearLayout guideParent;
    private ViewPager viewPager;
    private int pageCount;
    private FragmentManager fragmentManager;

    public Home5nView(MainActivity activity, FragmentManager fragmentManager) {
        super(activity);
        this.fragmentManager=fragmentManager;
    }

    @Override
    protected void getView(List<NewHomePageBean.ModuleBean.ItemsBean> itemsList, LinearLayout linearLayout, long currentTime,String color) {
        View theNewView=mInflate.inflate(R.layout.taste_new_view,null);
        linearLayout.addView(theNewView);
        guideParent= (LinearLayout) theNewView.findViewById(R.id.home_the_new_indicator);
        viewPager= (ViewPager) theNewView.findViewById(R.id.home_the_new_food);
        dealWithTheData(itemsList);
    }

    private void dealWithTheData(List<NewHomePageBean.ModuleBean.ItemsBean> itemsList) {
        pageCount=itemsList.size()/5;
        for (int i = 0; i < pageCount; i++) {
            LinearLayout guide= (LinearLayout) mInflate.inflate(R.layout.viewpager_guide_circle_yellow,null);
            guideParent.addView(guide);
            Bundle bundle=new Bundle();
            list=new ArrayList<>();
            int start=i*5;
            int end=(i+1)*5;
            for (int j=start;j<end;j++){
                list.add(itemsList.get(j));
            }
            TasteNewFragment fragment=new TasteNewFragment();
            bundle.putSerializable("tasteList",list);
            fragment.setArguments(bundle);
            fragments.add(fragment);
        }

        initViewPager();
    }

    private void initViewPager() {
        TasteNewViewPagerAdapter<TasteNewFragment> viewPagerAdapter=new TasteNewViewPagerAdapter<TasteNewFragment>(fragmentManager,fragments);
        viewPager.setAdapter(viewPagerAdapter);
        guideParent.getChildAt(0).findViewById(R.id.image_guide).setEnabled(false);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }
            @Override
            public void onPageSelected(int position) {
                for (int i = 0; i < pageCount; i++) {
                    if (i==position){
                        (guideParent.getChildAt(i).findViewById(R.id.image_guide)).setEnabled(false);
                    }else {
                        (guideParent.getChildAt(i).findViewById(R.id.image_guide)).setEnabled(true);
                    }
                }
            }
            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

}
