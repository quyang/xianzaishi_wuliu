package com.xianzaishi.normandie.home_page;

import android.content.Intent;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ViewFlipper;

import com.umeng.analytics.MobclickAgent;
import com.xianzaishi.normandie.GoodsDetailsActivity;
import com.xianzaishi.normandie.HuoDongActivity;
import com.xianzaishi.normandie.MainActivity;
import com.xianzaishi.normandie.NewLoginActivity;
import com.xianzaishi.normandie.R;
import com.xianzaishi.normandie.ShareForCouponsActivity;
import com.xianzaishi.normandie.bean.NewHomePageBean;
import com.xianzaishi.normandie.global.Contants;
import com.xianzaishi.normandie.pay.PayResult;
import com.xianzaishi.normandie.utils.NetUtils;
import com.xianzaishi.normandie.utils.SPUtils;
import com.xianzaishi.normandie.utils.ToastUtils;

import java.util.List;

/**
 * Created by ShenLang on 2016/12/29.
 * 首页鲜在时快报
 */

public class HomeNewsView extends HomeBaseView<List<NewHomePageBean.ModuleBean.ContentListBean>>  {

    private ViewFlipper viewFlipper;
    List<NewHomePageBean.ModuleBean.ContentListBean> contentList;
    public HomeNewsView(MainActivity activity) {
        super(activity);
    }

    @Override
    protected void getView(List<NewHomePageBean.ModuleBean.ContentListBean> contentList, LinearLayout linearLayout, long currentTime,String color) {
        this.contentList=contentList;
        View view=mInflate.inflate(R.layout.home_news_view,null);
        viewFlipper= (ViewFlipper) view.findViewById(R.id.home_news_viewFlipper);
        linearLayout.addView(view);

        dealWithTheData(contentList);
    }

    private void dealWithTheData(List<NewHomePageBean.ModuleBean.ContentListBean> contentList) {
        for(NewHomePageBean.ModuleBean.ContentListBean bean:contentList){
            TextView textView=new TextView(mActivity);
            textView.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.MATCH_PARENT));
            textView.setGravity(Gravity.CENTER_VERTICAL);
            textView.setLines(1);
            textView.setEllipsize(TextUtils.TruncateAt.END);
            textView.setText(bean.getContent());
            textView.setOnClickListener(new Click(bean));
            viewFlipper.addView(textView);
        }
        viewFlipper.startFlipping();
    }



    private  class Click implements View.OnClickListener{
        private NewHomePageBean.ModuleBean.ContentListBean contentBean;

         Click(NewHomePageBean.ModuleBean.ContentListBean contentBean) {
            this.contentBean = contentBean;
        }

        @Override
        public void onClick(View view) {
            switch (contentBean.getTargetType()){
                case 1://活动页
                    Intent intent = new Intent(mActivity, HuoDongActivity.class);
                    String targetId=contentBean.getTargetId();
                    intent.putExtra("targetId", targetId);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    mActivity.startActivity(intent);
                    break;
                case 2://详情页
                    MobclickAgent.onEvent(mActivity,"tapItem");
                    Intent intent1 = new Intent(mActivity, GoodsDetailsActivity.class);
                    String targetId1=contentBean.getTargetId();
                    intent1.putExtra(Contants.ITEM_ID, Integer.valueOf(targetId1));
                    intent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    mActivity.startActivity(intent1);
                    break;
                case 4://H5
                    NetUtils.toHTML5(mActivity,contentBean.getTargetId(),contentBean.getTitleInfo());
                    break;
                case 3://链接
                    NetUtils.obtainCoupons(mActivity,contentBean.getTargetId());
                    break;
                case 5://跳转老带新页面
                    String uid= SPUtils.getStringValue(mActivity,Contants.SP_NAME,Contants.UID,null);
                    if (uid!=null) {
                        mActivity.startActivity(new Intent(mActivity, ShareForCouponsActivity.class));
                    }else {
                        mActivity.startActivity(new Intent(mActivity, NewLoginActivity.class));
                    }
                    break;
            }
        }
    }
}
