package com.xianzaishi.normandie.home_page;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Path;
import android.graphics.PathMeasure;
import android.support.v7.widget.GridLayoutManager;
import android.util.Log;
import android.view.View;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.umeng.analytics.MobclickAgent;
import com.xianzaishi.normandie.GoodsDetailsActivity;
import com.xianzaishi.normandie.MainActivity;
import com.xianzaishi.normandie.MyApplication;
import com.xianzaishi.normandie.NewLoginActivity;
import com.xianzaishi.normandie.R;
import com.xianzaishi.normandie.adapter.OpenCrazyListAdapter;
import com.xianzaishi.normandie.bean.GeneralBean;
import com.xianzaishi.normandie.bean.Home2DownBean;
import com.xianzaishi.normandie.bean.NewHomePageBean;
import com.xianzaishi.normandie.common.Urls;
import com.xianzaishi.normandie.customs.CircleImageView;
import com.xianzaishi.normandie.customs.MyRecyclerView;
import com.xianzaishi.normandie.global.Contants;
import com.xianzaishi.normandie.utils.GetBeanClass;
import com.xianzaishi.normandie.utils.GlideUtils;
import com.xianzaishi.normandie.utils.SPUtils;
import com.xianzaishi.normandie.utils.ToastUtils;
import com.xianzaishi.normandie.utils.UiUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by ShenLang on 2016/12/21.
 * 首页1+3n模块
 */

public class Home1_3nView extends HomeBaseView<List<NewHomePageBean.ModuleBean.ItemsBean>> {
    private List<NewHomePageBean.ModuleBean.ItemsBean> itemList;
    private TextView shoppingCount;//购物车商品数量指示器
    private int[] parentLocation,endLocation,startLocation;
    private RelativeLayout rootView;
    private PathMeasure pathMeasure;
    private OkHttpClient client= MyApplication.getOkHttpClient();

    public Home1_3nView(MainActivity activity) {
        super(activity);
    }

    @Override
    protected void getView(List<NewHomePageBean.ModuleBean.ItemsBean> itemsList, LinearLayout linearLayout,long c,String color) {
        itemList=itemsList;
        View view=mInflate.inflate(R.layout.shopping_crazy_view,null);
        LinearLayout colorbg= (LinearLayout) view.findViewById(R.id.open_container);
        colorbg.setBackgroundColor(Color.parseColor("#"+color));
        linearLayout.addView(view);
        initAnimation();
        dealWithTheData(view);
    }

    private void dealWithTheData(View view) {
        /*final ImageView icon= (ImageView) view.findViewById(R.id.iv_icon);//商品图
        TextView title= (TextView) view.findViewById(R.id.tv_name);//商品名称
        TextView subTitle= (TextView) view.findViewById(R.id.tv_subtitle);//商品说明
        TextView standard= (TextView) view.findViewById(R.id.tv_quality);//商品规格
        TextView price= (TextView) view.findViewById(R.id.tv_discount_price);//商品价格
        ImageView car= (ImageView) view.findViewById(R.id.iv_shopping);//购物车按钮
        TextView redSign= (TextView) view.findViewById(R.id.red_sign);//标
        RelativeLayout root= (RelativeLayout) view.findViewById(R.id.root);
        root.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MobclickAgent.onEvent(mActivity,"secondSectionGood");//友盟第二分区商品点击
                MobclickAgent.onEvent(mActivity,"tapItem");

                Intent intent = new Intent(mActivity, GoodsDetailsActivity.class);
                intent.putExtra(Contants.ITEM_ID, itemList.get(0).getItemId());
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                mActivity.startActivity(intent);
            }
        });*/

        MyRecyclerView myRecyclerView= (MyRecyclerView) view.findViewById(R.id.open_recycler);//商品列表
        myRecyclerView.setLayoutManager(new GridLayoutManager(mActivity,3,GridLayoutManager.VERTICAL,false){
            @Override
            public boolean canScrollVertically() {
                return false;
            }
        });
        /*NewHomePageBean.ModuleBean.ItemsBean itemsBean=itemList.get(0);
        NewHomePageBean.ModuleBean.ItemsBean.ItemSkuVOsBean.TagDetailBean tagDetailBean=itemsBean.getItemSkuVOs().get(0).getTagDetail();
        GlideUtils.LoadImage(mActivity,itemsBean.getPicUrl(),icon);
        car.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addIntoCar(0,icon);
            }
        });
        title.setText(itemsBean.getTitle());
        subTitle.setText(itemsBean.getSubtitle());
        standard.setText(itemsBean.getItemSkuVOs().get(0).getSaleDetailInfo());
        price.setText(itemsBean.getDiscountPriceYuanString());
        //打标
        if (itemsBean.getItemSkuVOs().get(0).getInventory()<=0){
            redSign.setVisibility(View.VISIBLE);
            redSign.setBackgroundResource(R.drawable.grey_sign);
            redSign.setText("卖光啦");

            car.setBackgroundResource(R.mipmap.shoppingcargrey2x);
            car.setEnabled(false);
        }else if(tagDetailBean.getChannel()!=null&&!tagDetailBean.isMayPlus()&&tagDetailBean.getTagContent()!=null){
            redSign.setVisibility(View.VISIBLE);
            redSign.setBackgroundResource(R.drawable.grey_sign);
            redSign.setText(tagDetailBean.getTagContent());

            car.setBackgroundResource(R.mipmap.shoppingcargrey2x);
            car.setEnabled(false);
        }else {
            car.setBackgroundResource(R.mipmap.gouwuche2x);
            car.setEnabled(true);

            if (tagDetailBean.getChannel()==null||tagDetailBean.getTagContent()==null||tagDetailBean.getChannel().equals("2")){
                redSign.setVisibility(View.GONE);
            }else {
                redSign.setVisibility(View.VISIBLE);
                redSign.setBackgroundResource(R.drawable.red_sign);
                redSign.setText(tagDetailBean.getTagContent());
            }
        }
*/

        /*ArrayList<NewHomePageBean.ModuleBean.ItemsBean> itemsList=new ArrayList<>();
        int size=itemList.size();
        for (int i = 0; i < size; i++) {
            if (i!=0){
                itemsList.add(itemList.get(i));
            }
        }*/
        OpenCrazyListAdapter adapter=new OpenCrazyListAdapter(itemList,mActivity);
        myRecyclerView.setAdapter(adapter);
        adapter.setAddIntoShoppingCarListener(new OpenCrazyListAdapter.AddIntoShoppingCarListener() {
            @Override
            public void addIntoShoppingCar(View view, int position, ImageView imageView) {
                addIntoCar(position,imageView);
            }
        });
    }

    /**
     *  初始化加购动画所需的条件
     */
    private void initAnimation() {
        shoppingCount= (TextView) mActivity.findViewById(R.id.shopping_trolley_count);
        rootView= (RelativeLayout) mActivity.findViewById(R.id.main_root_view);
        RadioButton shoppingCar= (RadioButton) mActivity.findViewById(R.id.shoppingTrolley_button);
        parentLocation=new int[2];
        endLocation=new int[2];
        rootView.getLocationInWindow(parentLocation);
        shoppingCar.getLocationInWindow(endLocation);
    }

    /**
     *  添加加购的动画
     */
    private void addTrolleyAnimator(ImageView imageView){
        startLocation=new int[2];
        imageView.getLocationInWindow(startLocation);
        /**
         *  构造一个用来执行动画的商品图
         */
        final CircleImageView goods=new CircleImageView(mActivity);
        goods.setImageDrawable(imageView.getDrawable());
        LinearLayout.LayoutParams params=new LinearLayout.LayoutParams(150,150);
        rootView.addView(goods,params);
        /**
         * 开始掉落的商品的起始点：商品起始点-父布局起始点+该商品图片的一半
         */
        float fromX=startLocation[0]-parentLocation[0]+imageView.getWidth()/2;
        float fromY=startLocation[1]-parentLocation[1]+imageView.getHeight()/2;
        /**
         * 商品掉落后的终点坐标：购物车起始点-父布局起始点
         */
        float toX=endLocation[0]-parentLocation[0];
        float toY=endLocation[1]-parentLocation[1];
        final float[] currentLocation=new float[2];
        Path path=new Path();//开始绘制贝塞尔曲线
        path.moveTo(fromX,fromY);//移动到起始点
        path.quadTo((fromX+toX)/2,fromY,toX,toY);//使用贝塞尔曲线
        pathMeasure=new PathMeasure(path,false);//用来计算贝塞尔曲线的曲线长度和贝塞尔曲线中间插值的坐标，
        // 如果是true，path会形成一个闭环
        ValueAnimator valueAnimator=ValueAnimator.ofFloat(0,pathMeasure.getLength());
        valueAnimator.setDuration(500);//设置属性动画
        valueAnimator.setInterpolator(new LinearInterpolator());//匀速线性插值器
        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                float value= (float) valueAnimator.getAnimatedValue();
                pathMeasure.getPosTan(value,currentLocation,null);
                goods.setTranslationX(currentLocation[0]);
                goods.setTranslationY(currentLocation[1]);
            }
        });
        valueAnimator.start();
        valueAnimator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }
            @Override
            public void onAnimationEnd(Animator animator) {
                rootView.removeView(goods);
            }
            @Override
            public void onAnimationCancel(Animator animator) {
            }
            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
    }

    private void addIntoCar(int position, final ImageView imageView) {
        MobclickAgent.onEvent(mActivity,"homeSecondSectionCart");//首页第二分区购物车按钮
        MobclickAgent.onEvent(mActivity,"addToCart");

        String uid= SPUtils.getStringValue(MyApplication.getContext(), Contants.SP_NAME,Contants.UID,null);
        if(uid==null){
            mActivity.startActivity(new Intent(mActivity, NewLoginActivity.class));
        }else {
            String itemId = String.valueOf(itemList.get(position).getItemId());
            RequestBody formBody = new FormBody.Builder()
                    .add("uid", uid)
                    .add("itemId", itemId)
                    .add("itemCount", "1")
                    .add("skuId", itemList.get(position).getSku())
                    .build();
            Request request = new Request.Builder()
                    .url(Urls.ADD_TO_TROLLEY)
                    .post(formBody)
                    .build();
            client.newCall(request).enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    UiUtils.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(mActivity, "网络请求失败！", Toast.LENGTH_SHORT).show();
                        }
                    });
                }

                @Override
                public void onResponse(Call call, final Response response) throws IOException {
                    if (!response.isSuccessful()){return;}
                    final GeneralBean generalBean = (GeneralBean) GetBeanClass.getBean(response, GeneralBean.class);
                    UiUtils.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (generalBean!=null&&generalBean.success) {
                                SPUtils.putBooleanValue(MyApplication.getContext(), Contants.SP_NAME, "countChanged", true);
                                String countText = shoppingCount.getText().toString().equals("") ? "0" : shoppingCount.getText().toString();
                                int count = Integer.valueOf(countText);
                                if (count == 0) {
                                    shoppingCount.setVisibility(View.VISIBLE);
                                }
                                count = ++count;
                                shoppingCount.setText(String.valueOf(count));
                                addTrolleyAnimator(imageView);
                            }else {
                                ToastUtils.showToast("添加购物车失败");
                            }
                        }
                    });
                }
            });
        }
    }
}
