package com.xianzaishi.normandie.home_page;

import android.support.v7.widget.GridLayoutManager;
import android.view.View;
import android.widget.LinearLayout;

import com.xianzaishi.normandie.MainActivity;
import com.xianzaishi.normandie.R;
import com.xianzaishi.normandie.adapter.HomeCategoryAdapter;
import com.xianzaishi.normandie.bean.CategoryBean;
import com.xianzaishi.normandie.bean.NewHomePageBean;
import com.xianzaishi.normandie.customs.MyRecyclerView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ShenLang on 2016/12/24.
 * 首页分类模块
 */

public class HomeCategoryView extends HomeBaseView<List<NewHomePageBean.ModuleBean.CatListBean>> {
    private MyRecyclerView categoryView;
    private ArrayList<CategoryBean.ModuleBean> list=new ArrayList<>();
    public HomeCategoryView(MainActivity activity) {
        super(activity);
    }

    @Override
    protected void getView(List<NewHomePageBean.ModuleBean.CatListBean> catList, LinearLayout linearLayout,long c,String color) {
        /**
         *  首页分类
         */
        View categoryView=mInflate.inflate(R.layout.home_category_view,null);
        this.categoryView= (MyRecyclerView) categoryView.findViewById(R.id.home_category_view);
        this.categoryView.setLayoutManager(new GridLayoutManager(mActivity,4,GridLayoutManager.VERTICAL,false){
            @Override
            public boolean canScrollVertically() {
                return false;
            }
        });
        linearLayout.addView(categoryView);
        for (NewHomePageBean.ModuleBean.CatListBean catListBean:catList){
            CategoryBean.ModuleBean moduleBean=new CategoryBean.ModuleBean();
            moduleBean.setCatId(catListBean.getCatId());
            moduleBean.setName(catListBean.getCatName());
            moduleBean.setPic(catListBean.getPicUrl());
            list.add(moduleBean);
        }
        this.categoryView.setAdapter(new HomeCategoryAdapter(mActivity, list));
    }
}
