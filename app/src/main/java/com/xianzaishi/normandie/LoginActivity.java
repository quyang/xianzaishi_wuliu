package com.xianzaishi.normandie;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.xianzaishi.normandie.bean.LoginAndRegister;
import com.xianzaishi.normandie.bean.NewLoginBean;
import com.xianzaishi.normandie.common.Urls;
import com.xianzaishi.normandie.global.Contants;
import com.xianzaishi.normandie.utils.GetBeanClass;
import com.xianzaishi.normandie.utils.JudgePhoneNumber;
import com.xianzaishi.normandie.utils.SPUtils;
import com.xianzaishi.normandie.utils.UiUtils;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener{
    private EditText phone,pwd;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        getWindow().setBackgroundDrawableResource(R.mipmap.login_bg);
        initView();
    }

    private void initView() {
        phone= (EditText) findViewById(R.id.login_phone_number);
        pwd= (EditText) findViewById(R.id.login_pass_word);

        Button registerButton= (Button) findViewById(R.id.login_register_button);
        registerButton.setOnClickListener(this);
        TextView forgetPWD= (TextView) findViewById(R.id.login_forget_pwd);
        forgetPWD.setOnClickListener(this);
        Button loginButton= (Button) findViewById(R.id.login_button);
        loginButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.login_register_button:
                this.startActivity(new Intent(LoginActivity.this,RegisterActivity.class));
                finish();
                break;
            case R.id.login_forget_pwd:
                this.startActivity(new Intent(LoginActivity.this,ForgetPWDActivity.class));
                break;
            case R.id.login_button:
                OkHttpClient client=MyApplication.getOkHttpClient();
                String phones=phone.getText().toString();
                String passWord=pwd.getText().toString();
                TelephonyManager tm= (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
                String IMEI=tm.getDeviceId();//硬件设备码
                if (JudgePhoneNumber.judgeMobileNumber(this,phones)){
                    if(TextUtils.isEmpty(passWord)){
                        Toast.makeText(LoginActivity.this,"密码不能为空！",Toast.LENGTH_SHORT).show();}
                    Request request=new Request.Builder()
                            .url(String.format(Urls.MINE_LOGIN,phones,passWord,IMEI))
                            .build();
                    client.newCall(request).enqueue(new Callback() {
                        @Override
                        public void onFailure(Call call, IOException e) {
                            UiUtils.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(LoginActivity.this,"网络访问失败",Toast.LENGTH_SHORT).show();
                                }
                            });
                        }
                        @Override
                        public void onResponse(Call call, Response response) throws IOException {
                            if(!response.isSuccessful()) throw new IOException("Unexpected code"+response);
                            NewLoginBean loginAndRegister= (NewLoginBean) GetBeanClass.getBean(response,NewLoginBean.class);
                            response.close();//响应使用之后关闭
                            switch (loginAndRegister.getResultCode()){
                                case 1:
                                    String token=loginAndRegister.getModule().getToken();
                                    SPUtils.putStringValue(MyApplication.getContext(), Contants.SP_NAME,"token",token);
                                    finish();
                                    break;
                            }
                        }
                    });
                }
                break;
        }
    }
}
