package com.xianzaishi.normandie;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;

import com.google.gson.Gson;
import com.xianzaishi.normandie.bean.JPushBean;
import com.xianzaishi.normandie.global.Contants;

import cn.jpush.android.api.JPushInterface;

/**
 * Created by ShenLang on 2016/11/17.
 * 自定义的极光推送广播接收者
 */

public class JPushReceiver extends BroadcastReceiver {
    private static final String TAG = "JPush";

    @Override
    public void onReceive(Context context, Intent intent) {
        Bundle bundle = intent.getExtras();

        if (JPushInterface.ACTION_REGISTRATION_ID.equals(intent.getAction())) {
            String regId = bundle.getString(JPushInterface.EXTRA_REGISTRATION_ID);
            //send the Registration Id to your server...

        } else if (JPushInterface.ACTION_MESSAGE_RECEIVED.equals(intent.getAction())) {
            Log.d(TAG, "[MyReceiver] 接收到推送下来的自定义消息: " + bundle.getString(JPushInterface.EXTRA_MESSAGE));
            //processCustomMessage(context, bundle);

        } else if (JPushInterface.ACTION_NOTIFICATION_RECEIVED.equals(intent.getAction())) {
            //Log.d(TAG, "[MyReceiver] 接收到推送下来的通知");
            int notifactionId = bundle.getInt(JPushInterface.EXTRA_NOTIFICATION_ID);
            //Log.d(TAG, "[MyReceiver] 接收到推送下来的通知的ID: " + notifactionId);

        } else if (JPushInterface.ACTION_NOTIFICATION_OPENED.equals(intent.getAction())) {
            //Log.d(TAG, "[MyReceiver] 用户点击打开了通知");

            //打开自定义的Activity
            String json=bundle.getString(JPushInterface.EXTRA_EXTRA);
            Gson gson=new Gson();
            JPushBean jPushBean =gson.fromJson(json,JPushBean.class);
            String targetId=jPushBean.getTargetId();
            String titleInfo=jPushBean.getTitleInfo();
            String itemId=jPushBean.getItemId();
            if (itemId==null&&targetId!=null){
                Intent i = new Intent(context, HTML5Activity.class);
                i.putExtra("url",targetId)
                        .putExtra("title",titleInfo);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(i);
            }else if(itemId!=null&&targetId==null){
                Intent i = new Intent(context, GoodsDetailsActivity.class);
                i.putExtra(Contants.ITEM_ID, Integer.valueOf(itemId));
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(i);
            }else {
                PackageManager packageManager = context.getPackageManager();
                Intent intent1;
                intent1 = packageManager.getLaunchIntentForPackage("com.xianzaishi.normandie");
                context.startActivity(intent1);
            }

        } else if (JPushInterface.ACTION_RICHPUSH_CALLBACK.equals(intent.getAction())) {
            //Log.d(TAG, "[MyReceiver] 用户收到到RICH PUSH CALLBACK: " + bundle.getString(JPushInterface.EXTRA_EXTRA));
            //在这里根据 JPushInterface.EXTRA_EXTRA 的内容处理代码，比如打开新的Activity， 打开一个网页等..

        } else if(JPushInterface.ACTION_CONNECTION_CHANGE.equals(intent.getAction())) {
            boolean connected = intent.getBooleanExtra(JPushInterface.EXTRA_CONNECTION_CHANGE, false);
            //Log.w(TAG, "[MyReceiver]" + intent.getAction() +" connected state change to "+connected);
        } else {
            //Log.d(TAG, "[MyReceiver] Unhandled intent - " + intent.getAction());
        }
    }
}
