package com.xianzaishi.normandie;

import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;

import com.xianzaishi.normandie.utils.MyThreadPoolManager;
import com.xianzaishi.normandie.utils.UiUtils;

/**
 *
 * quyang
 *
 * 加载中的状态   --- mLoadingView
 * 加载失败的状态  --- mErrorView
 * 加载成功的状态 --- mSuccessView
 * 空数据的状态 --- mEmptyView
 *
 */
public abstract class LoadingPage extends FrameLayout {
	
	public static final int STATE_LOADING = 0;//正在加载
	public static final int STATE_ERROR = 1;//加载错误
	public static final int STATE_SUCCESS = 2;//加载成功
	public static final int STATE_EMPTY = 3;//没有数据
	
	private  int mCurrentState = STATE_LOADING;
	
	
	private View mErrorView;
	private View mLoadingView;
	private View mEmptyView;
	private View mSuccessView;

	public LoadingPage(Context context) {
		super(context);
		init();
	}
	
	private void init() {
		//初始化mErrorView
		if (mErrorView == null) {
			mErrorView = createErrorView();
			addView(mErrorView);
		}

		if (mLoadingView == null) {
			mLoadingView = createLoadingView();
			addView(mLoadingView);
		}

		if (mEmptyView == null) {
			mEmptyView = createEmptyView();
			addView(mEmptyView);
		}

		//显示该显示的view
		showRightPage();
	}

	/**
	 * 根据当前的状态，显示正确的View
	 */
	private void showRightPage() {

		mLoadingView.setVisibility(( mCurrentState == STATE_LOADING) ? View.VISIBLE:View.GONE);
		mErrorView.setVisibility(mCurrentState==STATE_ERROR?View.VISIBLE:View.GONE);
		mEmptyView.setVisibility(mCurrentState==STATE_EMPTY?View.VISIBLE:View.GONE);
		
		
		if(mSuccessView == null && mCurrentState == STATE_SUCCESS) {
			mSuccessView = createSuccessView();
			if(mSuccessView != null) {
				addView(mSuccessView);
			}
		}
		if(mSuccessView != null) {
			mSuccessView.setVisibility(mCurrentState==STATE_SUCCESS?View.VISIBLE:View.GONE);
		}
		
		
		
	}
	
	//加载数据
	public void loadData() {
		
		if(mCurrentState == STATE_LOADING) {
			return;
		}
		
		//先把mCurrentState的值进行初始化
		mCurrentState = STATE_LOADING;
		showRightPage();
		
		//访问网络数据
		MyThreadPoolManager.getInstance().execute(new Runnable(){

			@Override
			public void run() {
//				mCurrentState = STATE_LOADING;

				ResultState resultState = onLoad();

				if(resultState != null) {
					mCurrentState = resultState.state;
					UiUtils.runOnUiThread(new Runnable(){

						@Override
						public void run() {
							showRightPage();
							
						}});
					
				}
				
			}});
	}

	//由于每个节目的数据结构都不一样，所以有子类自己实现
	public abstract ResultState onLoad();

	//每个子类的布局都不一样，所以由自己实现
	public abstract View createSuccessView() ;


	private View createEmptyView() {
		View view = UiUtils.inflateView(R.layout.layout_empty);
		return view;
	}

	private View createLoadingView() {
		View view = UiUtils.inflateView(R.layout.layout_loading);
		return view;
	}

	private View createErrorView() {
		View view = UiUtils.inflateView(R.layout.layout_error);
		Button btn_retry = (Button) view.findViewById(R.id.btn_retry);
		
		btn_retry.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				loadData();
			}});
		return view;
	}


	public enum ResultState {
		LOADING(STATE_LOADING),ERROR(STATE_ERROR),SUCCESS(STATE_SUCCESS),EMPTY(STATE_EMPTY);
		
		int state;
		
		private ResultState(int state) {
			this.state = state;
		}

	}

}
