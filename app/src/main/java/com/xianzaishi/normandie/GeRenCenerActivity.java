package com.xianzaishi.normandie;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.xianzaishi.normandie.common.Urls;
import com.xianzaishi.normandie.global.Contants;
import com.xianzaishi.normandie.utils.SPUtils;
import com.xianzaishi.normandie.utils.ToastUtils;
import com.xianzaishi.normandie.view.OptionsPickerView;
import com.xianzaishi.normandie.view.TimePickerView;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * 个人中心
 * ShenLang
 */
public class GeRenCenerActivity extends AppCompatActivity implements View.OnClickListener {

    private RelativeLayout gender,birthday;
    private EditText nameEdit;
    private TextView tvGender,tvBirthday;
    private Button saveButton;
    private OptionsPickerView pvOptions;
    private TimePickerView timePicker;
    private ArrayList<String> genderList=new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ge_ren_cener);
        initView();
    }

    private void initView() {
        pvOptions=new OptionsPickerView(this);
        timePicker=new TimePickerView(this, TimePickerView.Type.YEAR_MONTH_DAY);
        genderList.add("帅哥");
        genderList.add("美女");
        genderList.add("保密");

        gender= (RelativeLayout) findViewById(R.id.rl_03);
        gender.setOnClickListener(this);
        birthday= (RelativeLayout) findViewById(R.id.rl_04);
        birthday.setOnClickListener(this);
        saveButton= (Button) findViewById(R.id.btn_save);
        saveButton.setOnClickListener(this);
        nameEdit= (EditText) findViewById(R.id.tv_name);
        nameEdit.setSelection(nameEdit.getText().length());
        tvGender= (TextView) findViewById(R.id.tv_sex);
        tvBirthday= (TextView) findViewById(R.id.tv_birthday);

        View view=findViewById(R.id.geren_title);
        TextView title= (TextView) view.findViewById(R.id.text_title);
        title.setText("个人中心");
        ImageView back= (ImageView) view.findViewById(R.id.address_go_back);
        back.setOnClickListener(this);

        initContent();
    }

    private void initContent() {
        Intent intent=getIntent();
        int sex=intent.getIntExtra("sex",0);
        String bir=intent.getStringExtra("birthday");
        nameEdit.setText(intent.getStringExtra("name"));
        switch (sex){
            case 0:
                tvGender.setText("帅哥");
                break;
            case 1:
                tvGender.setText("美女");
                break;
            case 2:
                tvGender.setText("保密");
                break;
        }
        if(bir!=null){
            tvBirthday.setText(getStringDate(Long.valueOf(bir)*1000));
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.rl_03://gender
                chooseGender();
                break;
            case R.id.rl_04:
                chooseBirthday();
                break;
            case R.id.btn_save:
                commit();
                break;
            case R.id.address_go_back:
                finish();
                break;
        }
    }

    private void commit() {
        String uid= SPUtils.getStringValue(this, Contants.SP_NAME,Contants.UID,null);
        String name=nameEdit.getText().toString().trim();
        int gender=getGender(tvGender.getText().toString().trim());
        long birthday=getMillions(tvBirthday.getText().toString().trim());
        OkHttpClient client=MyApplication.getOkHttpClient();
        MediaType MEDIA_TYPE_MARKDOWN= MediaType.parse("application/json; charset=utf-8");
        String postBody="{\"userId\":%s,\"name\":\"%s\",\"sex\":\"%d\",\"birthday\":\"%d\"}";
        String body=String.format(postBody,uid,name,gender,birthday);

        Request request=new Request.Builder()
                .url(Urls.UPDATE_USER_INFO)
                .post(RequestBody.create(MEDIA_TYPE_MARKDOWN,body))
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                ToastUtils.showToast("提交失败");
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if(response.isSuccessful()){
                    ToastUtils.showToast("个人信息修改成功！");
                    TimerTask timerTask=new TimerTask() {
                        @Override
                        public void run() {
                            finish();
                        }
                    };
                    Timer timer=new Timer();
                    timer.schedule(timerTask,3500);

                }else {
                    ToastUtils.showToast("服务器请求失败！");
                }
            }
        });

    }

    private void chooseBirthday() {
        timePicker.setRange(1900,2100);
        String[] array=tvBirthday.getText().toString().trim().split("-");
        int year=Integer.valueOf(array[0]);
        int Month=Integer.valueOf(array[1])-1;
        int day=Integer.valueOf(array[2]);
        timePicker.setTime(year,Month,day,0,0);
        timePicker.setCyclic(false,false,false,false,false);
        timePicker.setOnTimeSelectListener(new TimePickerView.OnTimeSelectListener() {
            @Override
            public void onTimeSelect(Date date) {
                tvBirthday.setText(getTime(date));
            }
        });
        timePicker.show();
    }
    public static String getTime(Date date) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.CHINA);
        return format.format(date);
    }
    /**
     * 将长时间格式字符串转换为时间 yyyy-MM-dd
     *
     */
    public static String getStringDate(Long date) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd", Locale.CHINA);
        return formatter.format(date);
    }
    //将yyyy-MM-dd转换成long型时间戳
    public static long getMillions(String time){
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.CHINA);
        try {
            return format.parse(time).getTime()/1000;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return -1;
    }
    public static int getGender(String gender){
        switch (gender){
            case "帅哥":
                return 0;
            case "美女":
                return 1;
            case "保密":
                return 2;
            default:
                return 0;
        }
    }
    private void chooseGender() {
        pvOptions.setPicker(genderList);
        pvOptions.setCyclic(false);
        pvOptions.setSelectOptions(0);
        pvOptions.setOnoptionsSelectListener(new OptionsPickerView.OnOptionsSelectListener() {
            @Override
            public void onOptionsSelect(int options1, int option2, int options3) {
                tvGender.setText(genderList.get(options1));
            }
        });
        pvOptions.show();
    }
}
