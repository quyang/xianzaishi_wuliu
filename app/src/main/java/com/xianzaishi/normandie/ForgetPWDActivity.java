package com.xianzaishi.normandie;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.xianzaishi.normandie.bean.LoginAndRegister;
import com.xianzaishi.normandie.common.Urls;
import com.xianzaishi.normandie.utils.GetBeanClass;
import com.xianzaishi.normandie.utils.JudgePhoneNumber;
import com.xianzaishi.normandie.utils.ValidateCountDownTimer;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class ForgetPWDActivity extends AppCompatActivity implements View.OnClickListener{
    private TextView tile;
    private EditText phone,validate,passWord,pwdAgain;
    private ImageView isValidateRight,isPWDRight;
    private Button getValidate,sureButton;
    private OkHttpClient client=MyApplication.getOkHttpClient();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_pwd);
        initView();
    }

    private void initView() {
        /**
         * 设置标题
         */
        View view=findViewById(R.id.forget_title);
        tile= (TextView) view.findViewById(R.id.text_title);
        tile.setText("忘记密码");

        phone= (EditText) findViewById(R.id.forget_phone);
        validate= (EditText) findViewById(R.id.forget_validate);
        passWord= (EditText) findViewById(R.id.forget_setPassWord);
        pwdAgain= (EditText) findViewById(R.id.forget_setPWD_again);

        isValidateRight= (ImageView) findViewById(R.id.forget_isValidateRight);
        isPWDRight= (ImageView) findViewById(R.id.forget_isPWDRight);

        getValidate= (Button) findViewById(R.id.forget_getValidate);
        getValidate.setOnClickListener(this);
        sureButton= (Button) findViewById(R.id.forget_sure_button);
        sureButton.setOnClickListener(this);


        /**
         * 判断验证码输入是否正确
         */
        validate.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }
            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (!"".equals(charSequence.toString())) {
                    isValidateRight.setVisibility(View.VISIBLE);
                    isValidateRight.setImageResource(R.mipmap.iv_delete_bg);
                    if(charSequence.toString().equals("123456")){
                        isValidateRight.setImageResource(R.mipmap.yanzheng2x);
                        /**
                         * 如果验证码输入正确 密码框恢复可编辑状态
                         */
                        passWord.setEnabled(true);
                        pwdAgain.setEnabled(true);
                    }
                } else {
                    isValidateRight.setVisibility(View.INVISIBLE);
                }
            }
            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        pwdAgain.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            /**
             *  判断第一次设置的密码是否符合规则
             * @param view
             * @param hasFocus
             */
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if(hasFocus){
                    String firstpwd = passWord.getText().toString();
                    if ("".equals(firstpwd)) {
                        Toast.makeText(ForgetPWDActivity.this, "密码不能为空", Toast.LENGTH_SHORT).show();
                    } else if (firstpwd.getBytes().length < 6) {
                        Toast.makeText(ForgetPWDActivity.this, "密码长度不能小于6位", Toast.LENGTH_SHORT).show();
                    }

                    /**
                     * 第二次输入密码时 验证是否与第一次设置的密码相同
                     */
                    pwdAgain.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                        }

                        @Override
                        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                            if (!"".equals(charSequence.toString())) {
                                isPWDRight.setVisibility(View.VISIBLE);
                                isPWDRight.setImageResource(R.mipmap.iv_delete_bg);
                                String firstpwd = passWord.getText().toString();
                                if (charSequence.toString().equals(firstpwd)) {
                                    isPWDRight.setVisibility(View.VISIBLE);
                                    isPWDRight.setImageResource(R.mipmap.yanzheng2x);
                                    sureButton.setEnabled(true);
                                }else {
                                    sureButton.setEnabled(false);
                                }
                            } else {
                                isPWDRight.setVisibility(View.INVISIBLE);
                            }
                        }
                        @Override
                        public void afterTextChanged(Editable editable) {
                        }
                    });
                }
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.forget_getValidate:
                String mobileNumber=phone.getText().toString();
                if(JudgePhoneNumber.judgeMobileNumber(this,mobileNumber)){
                    new ValidateCountDownTimer(60000,1000,getValidate).start();//点击获取验证码后开始倒计时
                    Request request=new Request.Builder()
                            .url(String.format(Urls.MINE_SENDCHECKCODE,mobileNumber,2))
                            .build();
                    client.newCall(request).enqueue(new Callback() {
                        @Override
                        public void onFailure(Call call, IOException e) {
                        }
                        @Override
                        public void onResponse(Call call, Response response) throws IOException {
                            if(!response.isSuccessful()) throw new IOException("Unexpected code"+response);
                            response.close();
                        }
                    });
                }
                break;
            case R.id.forget_sure_button:
                String mobileNumber1=phone.getText().toString();
                String pwd=passWord.getText().toString();
                String verification=validate.getText().toString();
                Request request=new Request.Builder()
                        .url(String.format(Urls.MINE_MODIFIEDPWD,mobileNumber1,pwd,verification))
                        .build();
                client.newCall(request).enqueue(new Callback() {
                    @Override
                    public void onFailure(Call call, IOException e) {

                    }

                    @Override
                    public void onResponse(Call call, Response response) throws IOException {
                        if(!response.isSuccessful()) throw new IOException("Unexpected code " + response);
                        LoginAndRegister loginAndRegister= (LoginAndRegister) GetBeanClass.getBean(response,LoginAndRegister.class);
                        if(loginAndRegister.getResultCode()==1){
                            finish();
                        }else {
                            Toast.makeText(ForgetPWDActivity.this,"修改密码失败！",Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                break;
        }
    }
}
