package com.xianzaishi.normandie;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.xianzaishi.normandie.adapter.HistorySearchRecyclerAdapter;
import com.xianzaishi.normandie.adapter.SuggestSearchRecyclerAdapter;
import com.xianzaishi.normandie.bean.CategoryListViewBean;
import com.xianzaishi.normandie.bean.HotSearchBean;
import com.xianzaishi.normandie.common.Urls;
import com.xianzaishi.normandie.customs.SearchView;
import com.xianzaishi.normandie.utils.GetBeanClass;
import com.xianzaishi.normandie.utils.UiUtils;

import java.io.IOException;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class SearchActivity extends AppCompatActivity implements View.OnClickListener,HistorySearchRecyclerAdapter.OnHistoryItemClickListener{

    private SearchView searchView;
    private TextView cancel;
    private RecyclerView hotSearchGrid;
    private RecyclerView historySearch;
    private Button clearButton;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    private String[] historyArray;
    private LinearLayoutManager linearLayoutManager;
    private OkHttpClient client=MyApplication.getOkHttpClient();
    private HistorySearchRecyclerAdapter adapter;
    private ImageView scanner;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        initView();
    }

    private void initView() {
        /**
         * 找到需要的控件
         */
        scanner= (ImageView) findViewById(R.id.search_scanner);
        scanner.setOnClickListener(this);
        searchView= (SearchView) findViewById(R.id.search_view);
        cancel= (TextView) findViewById(R.id.back_cancel);
        cancel.setOnClickListener(this);
        hotSearchGrid= (RecyclerView) findViewById(R.id.hot_search);
        historySearch= (RecyclerView) findViewById(R.id.history_search);
        clearButton= (Button) findViewById(R.id.clear_history);
        sharedPreferences=getSharedPreferences("history", Context.MODE_PRIVATE);
        editor=sharedPreferences.edit();
        linearLayoutManager=new LinearLayoutManager(this);
        searchView.setHint("搜索鲜在时商品");

        clearButton.setOnClickListener(this);

        GridLayoutManager gridLayoutManager=new GridLayoutManager(this,3);
        hotSearchGrid.setLayoutManager(gridLayoutManager);
        initHotSearch();
    }

    @Override
    protected void onResume() {
        super.onResume();
        initHistorySearch();//历史搜索开始记录
    }

    private void initHistorySearch() {
        /**
         * 历史搜索开始记录
         */
        historyArray=sharedPreferences.getString("searched","").split(",");
        adapter=new HistorySearchRecyclerAdapter(this,historyArray);
        historySearch.setLayoutManager(linearLayoutManager);
        historySearch.setAdapter(adapter);
        adapter.setOnHistoryItemClickListener(this);
    }

    /**
     *  访问服务器获取热搜词
     */
    private void initHotSearch() {
        Request request=new Request.Builder()
                .url(Urls.HOT_SEARCH)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

            }
            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if(!response.isSuccessful()) throw new IOException("Unexpected code"+response);
                HotSearchBean hotBean=(HotSearchBean) GetBeanClass.getBean(response,HotSearchBean.class);
                response.close();
                if(hotBean.getResultCode()==1){
                    final List<String> list=hotBean.getModule();
                    UiUtils.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            SuggestSearchRecyclerAdapter adapter=new SuggestSearchRecyclerAdapter(SearchActivity.this,list);
                            hotSearchGrid.setAdapter(adapter);
                            adapter.setOnHotItemClickListener(new SuggestSearchRecyclerAdapter.OnHotItemClickListener() {
                                @Override
                                public void onClick(View view, int position) {
                                    String commodity=list.get(position);
                                    searchView.saveSearchHistory(commodity);
                                    Intent intent=new Intent(SearchActivity.this,SearchResultActivity.class);
                                    intent.putExtra("search",commodity);
                                    startActivity(intent);
                                }
                            });
                        }
                    });

                }
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.clear_history:
                editor.clear();
                editor.commit();
                String[] historyArray=sharedPreferences.getString("searched","").split(",");
                historySearch.setLayoutManager(linearLayoutManager);
                historySearch.setAdapter(new HistorySearchRecyclerAdapter(this,historyArray));
                break;
            case R.id.search_scanner:
                Intent intent1 = new Intent(SearchActivity.this, CaptureActivity.class);
                startActivity(intent1);
                break;
            case R.id.back_cancel:
                finish();
                break;
        }
    }

    @Override
    public void onClick(View view, int position) {
        String commodity=historyArray[position];
        searchView.saveSearchHistory(commodity);
        Intent intent=new Intent(SearchActivity.this,SearchResultActivity.class);
        intent.putExtra("search",commodity);
        startActivity(intent);
    }
}
