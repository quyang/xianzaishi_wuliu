package com.xianzaishi.normandie;

import android.content.ClipboardManager;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;

import com.umeng.socialize.utils.ShareBoardlistener;
import com.xianzaishi.normandie.adapter.TasteNewViewPagerAdapter;
import com.xianzaishi.normandie.bean.AchievementBean;
import com.xianzaishi.normandie.common.Urls;
import com.xianzaishi.normandie.fragment.DetailAchievementFragment;
import com.xianzaishi.normandie.global.Contants;
import com.xianzaishi.normandie.utils.GetBeanClass;
import com.xianzaishi.normandie.utils.SPUtils;
import com.xianzaishi.normandie.utils.UiUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class MyAchievementActivity extends AppCompatActivity implements View.OnClickListener {
    private TextView achievement;
    private String code;
    private ShareBoardlistener shareBoardlistener;
    private ClipboardManager myClipboard;
    private LinearLayout guideContainer;
    private ViewPager viewPager;
    private ArrayList<DetailAchievementFragment> fragments = new ArrayList<>();
    //private TextView five, five1, ten, ten1, twentyfive, twentyfive1, fifty, fifty1;
    private LayoutInflater inflater;
    private ArrayList<AchievementBean.ModuleBean.AchievementDetailBean> list;
    private int pageCount;
    private int eachHeight;

    private OkHttpClient client = MyApplication.getOkHttpClient();
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_achievement);
        initView();
        netWork();
        //initAhievementRules();
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client2 = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    private void initAhievementRules() {
        Request request = new Request.Builder()
                .url(Urls.ACHIEVEMENT_RULE)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (!response.isSuccessful()) {
                    return;
                }
                final String jsonStr=response.body().string();
                UiUtils.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //dealWithTheJson(jsonStr);
                    }
                });

            }
        });
    }

    /*private void dealWithTheJson(String jsonStr) {
        ArrayList<String> keys=new ArrayList<String>();
        try {
            JSONObject jsonObject=new JSONObject(jsonStr);
            Iterator<String> iterator=jsonObject.keys();
            while (iterator.hasNext()){
                keys.add(iterator.next());
            }
            five.setText(keys.get(0));
            five1.setText(jsonObject.getString(keys.get(0)));
            ten.setText(keys.get(1));
            ten1.setText(jsonObject.getString(keys.get(1)));
            twentyfive.setText(keys.get(2));
            twentyfive1.setText(jsonObject.getString(keys.get(2)));
            fifty.setText(keys.get(3));
            fifty1.setText(jsonObject.getString(keys.get(3)));

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }*/

    private void netWork() {
        String token = SPUtils.getStringValue(MyApplication.getContext(), Contants.SP_NAME, Contants.TOKEN, null);

        FormBody formBody = new FormBody.Builder()
                .add("token", token)
                .build();
        Request request = new Request.Builder()
                .url(Urls.GET_ACHIEVEMENT)
                .post(formBody)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (response.isSuccessful()) {
                    final AchievementBean bean = (AchievementBean) GetBeanClass.getBean(response, AchievementBean.class);
                    if (bean != null && bean.isSuccess() && bean.getModule() != null) {
                        UiUtils.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                dealWithTheData(bean);
                            }
                        });

                    }
                }
            }
        });
    }

    private void dealWithTheData(AchievementBean bean) {
        AchievementBean.ModuleBean moduleBean = bean.getModule();
        List<String> redWordList = moduleBean.getRedFont();
        String sw = moduleBean.getCw();
        SpannableString spstr = new SpannableString(sw);
        ForegroundColorSpan colorSpan = new ForegroundColorSpan(getResources().getColor(R.color.colorYellow));
        for (String str : redWordList) {
            int len = str.length();
            int start = sw.indexOf(str);
            spstr.setSpan(colorSpan, start, start + len, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        }
        achievement.setText(spstr);

        ArrayList<AchievementBean.ModuleBean.AchievementDetailBean> achievementDetail = (ArrayList<AchievementBean.ModuleBean.AchievementDetailBean>) moduleBean.getAchievementDetail();
        if (achievementDetail == null || achievementDetail.size() == 0) {
            return;
        }

        initAchievementDetail(achievementDetail);
    }

    private void initAchievementDetail(ArrayList<AchievementBean.ModuleBean.AchievementDetailBean> achievementDetail) {
        int detailCount = achievementDetail.size();

        int totalHeight = viewPager.getMeasuredHeight();
        eachHeight = totalHeight / 8;

        pageCount = detailCount / 8 + (detailCount % 8 == 0 ? 0 : 1);
        for (int i = 0; i < pageCount; i++) {
            LinearLayout guide = (LinearLayout) inflater.inflate(R.layout.viewpager_guide_circle_brownness, null);
            guideContainer.addView(guide);
            list = new ArrayList<>();
            int start = 8 * i;
            int end = 8 * (i + 1) > detailCount ? detailCount : 8 * (i + 1);

            for (int i1 = start; i1 < end; i1++) {
                list.add(achievementDetail.get(i1));
            }

            DetailAchievementFragment fragment = new DetailAchievementFragment();
            Bundle bundle = new Bundle();
            bundle.putInt("height", eachHeight);
            bundle.putSerializable("list", list);
            fragment.setArguments(bundle);
            fragments.add(fragment);
        }
        setViewPager(fragments);
    }

    private void setViewPager(ArrayList<DetailAchievementFragment> fragments) {
        TasteNewViewPagerAdapter<DetailAchievementFragment> viewPagerAdapter = new TasteNewViewPagerAdapter<DetailAchievementFragment>(getSupportFragmentManager(), fragments);
        viewPager.setAdapter(viewPagerAdapter);
        guideContainer.getChildAt(0).findViewById(R.id.image_guide).setEnabled(false);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                for (int i = 0; i < pageCount; i++) {
                    if (i == position) {
                        (guideContainer.getChildAt(i).findViewById(R.id.image_guide)).setEnabled(false);
                    } else {
                        (guideContainer.getChildAt(i).findViewById(R.id.image_guide)).setEnabled(true);
                    }
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private void initView() {
        inflater = LayoutInflater.from(this);
        //设置标题和返回的点击事件
        TextView title = (TextView) findViewById(R.id.text_title);
        title.setText("我的成就");
        ImageView back = (ImageView) findViewById(R.id.address_go_back);
        back.setOnClickListener(this);
        code = getIntent().getStringExtra("invitationCode");
        achievement = (TextView) findViewById(R.id.display_achievement);
        guideContainer = (LinearLayout) findViewById(R.id.guide_container);
        viewPager = (ViewPager) findViewById(R.id.detail_achievement);
        /*five = (TextView) findViewById(R.id.five);
        five1 = (TextView) findViewById(R.id.five1);
        ten = (TextView) findViewById(R.id.ten);
        ten1 = (TextView) findViewById(R.id.ten1);
        twentyfive = (TextView) findViewById(R.id.twenty_five);
        twentyfive1 = (TextView) findViewById(R.id.twenty_five1);
        fifty = (TextView) findViewById(R.id.fifty);
        fifty1 = (TextView) findViewById(R.id.fifty1);*/
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.address_go_back:
                finish();
                break;
        }
    }


}
