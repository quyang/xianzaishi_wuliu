package com.xianzaishi.normandie;

import android.content.Intent;
import android.graphics.Color;
import android.os.Handler;
import android.os.Message;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.alipay.sdk.app.PayTask;
import com.baoyachi.stepview.VerticalStepView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.gson.Gson;
import com.umeng.analytics.MobclickAgent;
import com.xianzaishi.normandie.bean.CouponsByIdBean;
import com.xianzaishi.normandie.bean.GetAddressByIDBean;
import com.xianzaishi.normandie.bean.GoPayBean;
import com.xianzaishi.normandie.bean.OrderDetailsBean;
import com.xianzaishi.normandie.bean.SystemTimeBean;
import com.xianzaishi.normandie.common.Urls;
import com.xianzaishi.normandie.global.Contants;
import com.xianzaishi.normandie.http.PostProtocol;
import com.xianzaishi.normandie.interfaces.OnDataFromPostServerListener;
import com.xianzaishi.normandie.pay.PayResult;
import com.xianzaishi.normandie.utils.AliPayResultMap;
import com.xianzaishi.normandie.utils.GetBeanClass;
import com.xianzaishi.normandie.utils.SPUtils;
import com.xianzaishi.normandie.utils.ToastUtils;
import com.xianzaishi.normandie.utils.UiUtils;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class OrderDetailsActivity extends BaseActivity01 implements View.OnClickListener {


    private OrderDetailsBean data;
    private TextView bianhao;
    private TextView liushuihao;
    private TextView chuangjian_time;
    private TextView pay_time;
    private TextView peisong_time;
    private TextView danhao_peisong;
    private TextView qianshou_time;
    private TextView name;
    private TextView haoma;
    private TextView address;
    private LinearLayout goodsContainer;
    private TextView zongjia;
    private TextView yunfei,beizhu;
    private TextView quan;
    private TextView heji,credit;
    private String mUserAddress,resultPay;
    private String gmtPay,gmtEnd,gmtDistribution;//支付时间,签收时间
    private Double totalPrice=0.00;
    private RadioGroup one,two;
    private Double coupons=0.00;
    private String oid;
    private OkHttpClient client=MyApplication.getOkHttpClient();
    private static final int SDK_PAY_FLAG=1;
    private MyHandler handler;
    private long payTime;//付款的毫秒时间
    private String uid,addrId;
    private long currentTime;
   // private VerticalStepView mMSetpview0;

    public View createSuccessView(long currentTime) {

        View view = UiUtils.inflateView(R.layout.order_details);
        OrderDetailsBean.DataBean dataBean = this.data.data;
        addrId=dataBean.userAddressId;
        if (dataBean.gmtPay!=null) {
            payTime = Long.valueOf(dataBean.gmtPay);
        }
        String id = dataBean.id;//订单编号
        String seq = dataBean.seq;//流水号
        //用户地址
        mUserAddress = dataBean.userAddress;
        String userAddressId = dataBean.userAddressId;//地址id
        String payAmount = dataBean.payAmount;//账单金额
        String effeAmount = dataBean.effeAmount;//实收金额

        resultPay=effeAmount;
        Double creditPrice=dataBean.credit/(double)100;
        String gmtCreate = getStringDate(Long.valueOf(dataBean.gmtCreate));//创建时间
        if (dataBean.gmtPay==null){
            gmtPay = "待支付";//支付时间
        }else {
            gmtPay=getStringDate(Long.valueOf(dataBean.gmtPay));
        }
        if (dataBean.gmtEnd==null){
            gmtEnd = "未签收";//签收时间
        }else {
            gmtEnd=getStringDate(Long.valueOf(dataBean.gmtEnd));
        }

        if (dataBean.gmtDistribution==null){
            gmtDistribution = "待配送";//配送时间
        }else {
            gmtDistribution=getStringDate(Long.valueOf(dataBean.gmtDistribution));
        }
        Object logisticalInfo = dataBean.logisticalInfo;//物流信息


        //找到组件
        RadioButton btnApplyBill = (RadioButton) view.findViewById(R.id.btn_apply_bill);//申请发票按钮
        RadioButton btnApplyService = (RadioButton) view.findViewById(R.id.btn_apply_service);//申请售后按钮
        RadioButton btnGoPay= (RadioButton) view.findViewById(R.id.btn_go_pay);//去支付

        one= (RadioGroup) view.findViewById(R.id.btn_for_one);
        two= (RadioGroup) view.findViewById(R.id.btn_for_two);
        String status=dataBean.status;
        if(status.equals("2")){
            one.setVisibility(View.VISIBLE);
            two.setVisibility(View.GONE);
        }else if (status.equals("7")){//可以申请售后和发票
            one.setVisibility(View.GONE);
            if((System.currentTimeMillis()-payTime)>48*60*60*1000){
                two.setVisibility(View.GONE);
            }else {
                two.setVisibility(View.VISIBLE);
            }
            //two.setVisibility(View.GONE);
        }else if(status.equals("3")||status.equals("5")||status.equals("6")||status.equals("4")){//可以申请发票
            one.setVisibility(View.GONE);
            two.setVisibility(View.GONE);
            //two.setVisibility(View.GONE);
            btnApplyService.setVisibility(View.GONE);
        }else {
            one.setVisibility(View.GONE);
            two.setVisibility(View.GONE);
        }
        TextView title1= (TextView) view.findViewById(R.id.text_title);
        title1.setText("订单详情");
        ImageView cancel= (ImageView) view.findViewById(R.id.address_go_back);
        cancel.setOnClickListener(this);


        bianhao = (TextView) view.findViewById(R.id.bianhao);
        bianhao.setText("订单编号 ：" + id + "");
        liushuihao = (TextView) view.findViewById(R.id.liushuihao);
        liushuihao.setText("流水号 : " + seq + "");
        chuangjian_time = (TextView) view.findViewById(R.id.chuangjian_time);
        chuangjian_time.setText("创建时间 : " + gmtCreate + "");
        pay_time = (TextView) view.findViewById(R.id.pay_time);
        pay_time.setText("付款时间 : " + gmtPay + "");
        peisong_time = (TextView) view.findViewById(R.id.peisong_time);
        peisong_time.setText("配送时间 : " + gmtDistribution);

        danhao_peisong = (TextView) view.findViewById(R.id.danhao_peisong);
        danhao_peisong.setText("配送单号 : " + id);
        qianshou_time = (TextView) view.findViewById(R.id.qianshou_time);
        qianshou_time.setText("签收时间 : " + gmtEnd);
        credit= (TextView) view.findViewById(R.id.tv_credit);
        credit.setText("-¥"+String.valueOf(creditPrice));
        beizhu= (TextView) view.findViewById(R.id.order_details_beizhu);
        beizhu.setText(dataBean.attribute);
        setNameAndPhone(view);
        address = (TextView) view.findViewById(R.id.address);
        address.setText(mUserAddress);

        goodsContainer = (LinearLayout) view.findViewById(R.id.ll_container);
        List<OrderDetailsBean.DataBean.ItemsBean> items = dataBean.items;
        for (int i = 0; i < items.size(); i++) {
            View child = UiUtils.inflateView(R.layout.order_one_commodity);

            ImageView icon = (ImageView) child.findViewById(R.id.ivOneIcon);
            TextView title = (TextView) child.findViewById(R.id.tvOneTitle);
            TextView price = (TextView) child.findViewById(R.id.tvOnePrice);
            TextView count = (TextView) child.findViewById(R.id.tvOneCount);

            OrderDetailsBean.DataBean.ItemsBean itemsBean = items.get(i);
            Glide.with(this).load(itemsBean.iconUrl).diskCacheStrategy(DiskCacheStrategy.ALL).override(122, 122).into(icon);
            title.setText(itemsBean.name);
            price.setText(itemsBean.effePrice);
            count.setText(itemsBean.count);
            count.setTextColor(Color.BLACK);

            goodsContainer.addView(child);
            totalPrice+=Double.valueOf(itemsBean.effePrice)*Double.valueOf(itemsBean.count);
        }

        zongjia = (TextView) view.findViewById(R.id.zongjia);
        zongjia.setText("¥"+totalPrice);
        yunfei = (TextView) view.findViewById(R.id.yunfei);
        if(dataBean.channelType==1){
            if (totalPrice>59){
                yunfei.setText("¥0.00");
            }else {
                yunfei.setText("¥6.00");
            }
        }else {
            yunfei.setText("¥0.00");
        }
        setCouponsText(view,dataBean.id,dataBean.userId);
        heji = (TextView) view.findViewById(R.id.heji);
        heji.setText("¥"+effeAmount);

        //设置点击事件
        btnApplyBill.setOnClickListener(this);
        btnApplyService.setOnClickListener(this);
        btnGoPay.setOnClickListener(this);
        return view;
    }

    private void setNameAndPhone(final View view) {
        RequestBody formBody=new FormBody.Builder()
                .add("uid",uid)
                .add("id",addrId)
                .build();
        Request request=new Request.Builder()
                .url(Urls.GET_ADDRESS_BY_ID)
                .post(formBody)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                UiUtils.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(OrderDetailsActivity.this,"网络请求失败！",Toast.LENGTH_SHORT).show();
                    }
                });
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if(!response.isSuccessful()) throw new IOException("Unexpected code"+response);
                final GetAddressByIDBean sa= (GetAddressByIDBean) GetBeanClass.getBean(response,GetAddressByIDBean.class);
                response.close();
                UiUtils.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (sa.getCode()==1){
                            GetAddressByIDBean.DataBean dataBean=sa.getData();
                            name = (TextView) view.findViewById(R.id.tianbi);
                            haoma = (TextView) view.findViewById(R.id.haoma);

                            haoma.setText(dataBean.getPhone());
                            name.setText(dataBean.getName());
                        }
                    }
                });
            }
        });
    }

    private void setCouponsText(View view, String oid,String uid) {
        quan = (TextView) view.findViewById(R.id.quan);
        FormBody body = new FormBody.Builder()
                .add("uid",uid)
                .add("oid",oid)
                .build();
        PostProtocol protocol1 = new PostProtocol();
        protocol1.setUrl(Urls.GET_COUPONS_BY_ID);
        protocol1.setRequestBody(body);
        protocol1.getDataByPOST();
        protocol1.setOnDataFromPostServerListener(new OnDataFromPostServerListener() {
            @Override
            public void onSuccess(String result) {
                Gson gson=new Gson();
                CouponsByIdBean bean=gson.fromJson(result,CouponsByIdBean.class);
                if(bean.isSuccess()){
                    for(CouponsByIdBean.DataBean dataBean:bean.getData()){
                        coupons+=dataBean.getAmount();
                    }
                    String couponPrice=String.format("%.2f",coupons);
                    quan.setText("-¥"+couponPrice);
                }else {
                    quan.setText("-¥0.00");
                }
            }
            @Override
            public void onFail() {

            }
        });
    }

    /**
     * 将长时间格式字符串转换为时间 yyyy-MM-dd kk:mm:ss
     *
     */
    public static String getStringDate(Long date) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss", Locale.CHINA);
        return formatter.format(date);
    }

    @Override
    public void loadDataFromNet() {
        uid=SPUtils.getStringValue(MyApplication.getContext(),Contants.SP_NAME,Contants.UID,null);
        handler=new MyHandler(OrderDetailsActivity.this);
        oid=getIntent().getStringExtra("oid");
        FormBody body = new FormBody.Builder()
                .add("oid",oid)
                .build();

        PostProtocol protocol1 = new PostProtocol();
        protocol1.setUrl(Contants.ORDER_DETAILS);
        protocol1.setRequestBody(body);
        protocol1.getDataByPOST();
        protocol1.setOnDataFromPostServerListener(new OnDataFromPostServerListener() {
            @Override
            public void onSuccess(String result) {
                //解析json
                parseJson(result);
            }
            @Override
            public void onFail() {
                mLoadingView.setVisibility(View.INVISIBLE);
                mErrorView.setVisibility(View.VISIBLE);
            }
        });
    }

    public void parseJson(String json) {
        Gson gson = new Gson();
        data = gson.fromJson(json, OrderDetailsBean.class);
        //获取服务器时间戳 用于判断Ui展示与否
        getTimeFromServer();


    }

    /**
     *  从服务器获取时间戳
     */
    private void getTimeFromServer() {
        Request request=new Request.Builder()
                .url(Urls.GET_SYSTEM_TIME)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

            }
            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (response.isSuccessful()){
                    final SystemTimeBean timeBean= (SystemTimeBean) GetBeanClass.getBean(response,SystemTimeBean.class);
                    if (timeBean!=null&&timeBean.isSuccess()){
                        UiUtils.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                currentTime=timeBean.getModule();
                                //隐藏其他界面
                                mLoadingView.setVisibility(View.INVISIBLE);
                                //显示成功界面
                                View view = createSuccessView(currentTime);
                                view.setVisibility(View.VISIBLE);
                                flContainer.addView(view);
                            }
                        });

                    }
                }
            }
        });
    }
    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.btn_apply_bill:
                Intent intent = new Intent(this, ApplyBillsActivity.class);
                intent.putExtra("userAddress", mUserAddress);
                startActivity(intent);
                break;
            case R.id.btn_apply_service:
                Intent intent1=new Intent(this, ApplyServiceActivity.class);
                intent1.putExtra("oid",oid);
                startActivity(intent1);
                break;
            case R.id.btn_go_pay:
                toAliPay(oid);
                break;
            case R.id.address_go_back:
                finish();
                break;
        }

    }

    /**
     *  支付宝支付
     * @param oid
     */
    private void toAliPay(String oid) {
        RequestBody formBody=new FormBody.Builder()
                .add("oid",oid)
                .build();
        Request request=new Request.Builder()
                .url(Urls.GET_ALIPAY_SIGN)
                .post(formBody)
                .build();
        client.newCall(request).enqueue(new okhttp3.Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

            }
            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if(!response.isSuccessful()) throw new IOException("Unexpected code"+response);
                final GoPayBean goPayBean= (GoPayBean) GetBeanClass.getBean(response,GoPayBean.class);
                response.close();
                if (goPayBean.getCode()==1){
                    String payInfo =goPayBean.getData();
                    // 构造PayTask 对象
                    PayTask alipay = new PayTask(OrderDetailsActivity.this);
                    // 调用支付接口，获取支付结果
                    String result = alipay.pay(payInfo, true);
                    Message msg = new Message();
                    msg.what = SDK_PAY_FLAG;
                    msg.obj = result;
                    handler.sendMessage(msg);
                }

            }
        });
    }

    /**
     *  通过handler保证异步请求服务器的结果可以同步
     */
    static class MyHandler extends Handler {
        WeakReference<OrderDetailsActivity> weakReference;

        public MyHandler(OrderDetailsActivity orderActivity) {
            weakReference = new WeakReference<OrderDetailsActivity>(orderActivity);
        }

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            OrderDetailsActivity orderActivity = weakReference.get();
            if (orderActivity != null) {
                switch (msg.what) {
                    case SDK_PAY_FLAG:
                        PayResult payResult = new PayResult((String) msg.obj);
                        /**
                         * 同步返回的结果必须放置到服务端进行验证（验证的规则请看https://doc.open.alipay.com/doc2/
                         * detail.htm?spm=0.0.0.0.xdvAU6&treeId=59&articleId=103665&
                         * docType=1) 建议商户依赖异步通知
                         */
                        String resultInfo = payResult.getResult();// 同步返回需要验证的信息
                        HashMap<String,String> map= AliPayResultMap.getResultMap(resultInfo);
                        String out_trade_no=map.get("out_trade_no");

                        String resultStatus = payResult.getResultStatus();
                        // 判断resultStatus 为“9000”则代表支付成功，具体状态码代表含义可参考接口文档
                        if (TextUtils.equals(resultStatus, "9000")&&map.get("success").equals("\"true\"")) {
                            Intent intent=new Intent(orderActivity,AliPayResultActivity.class);
                            String tradeNum=out_trade_no.substring(1,out_trade_no.length()-1);
                            intent.putExtra("aliResult",tradeNum);
                            intent.putExtra("pay",orderActivity.resultPay);
                            intent.putExtra("time","");
                            intent.putExtra("channel","支付宝");
                            orderActivity.startActivity(intent);
                        } else {
                            // 判断resultStatus 为非"9000"则代表可能支付失败
                            // "8000"代表支付结果因为支付渠道原因或者系统原因还在等待支付结果确认，最终交易是否成功以服务端异步通知为准（小概率状态）
                            if (TextUtils.equals(resultStatus, "8000")) {
                                Toast.makeText(orderActivity, "支付结果确认中", Toast.LENGTH_SHORT).show();

                            } else {
                                // 其他值就可以判断为支付失败，包括用户主动取消支付，或者系统返回的错误
                                Toast.makeText(orderActivity, "支付失败+" + resultStatus, Toast.LENGTH_SHORT).show();
                            }
                        }
                        break;
                }
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
    }
}
