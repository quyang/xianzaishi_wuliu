package com.xianzaishi.normandie;

import android.content.Intent;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.umeng.analytics.MobclickAgent;
import com.xianzaishi.normandie.adapter.DanTuActivityAdapter;
import com.xianzaishi.normandie.adapter.HuodongRecyclerAdapter;
import com.xianzaishi.normandie.bean.HuoDongBean;
import com.xianzaishi.normandie.customs.MyRecyclerView;
import com.xianzaishi.normandie.global.Contants;
import com.xianzaishi.normandie.utils.GridSpacingItemDecoration;
import com.xianzaishi.normandie.utils.StringUtils;
import com.xianzaishi.normandie.utils.ToastUtils;
import com.xianzaishi.normandie.utils.UiUtils;

import java.io.IOException;
import java.util.List;

/**
 * 活动页面
 * 2016年9月29日10:34:31
 */
public class HuoDongActivity extends BaseActivity01 implements View.OnClickListener {


    public static final MediaType JSON
            = MediaType.parse("application/json; charset=utf-8");

    private LinearLayout mLlContainer;

    @Override
    protected void onResume() {
        super.onResume();

        MobclickAgent.onResume(this);
    }


    @Override
    public void loadDataFromNet() {
        requestNet();
    }

    //request the net
    private void requestNet() {
        OkHttpClient okHttpClient = new OkHttpClient();

        Intent intent = getIntent();
        String extra = intent.getStringExtra(Contants.TARGET_ID);
        if (StringUtils.juege(extra)) {

            String str = "{\"pageId\":\""+extra+"\"}";
            RequestBody requestBody = RequestBody.create(JSON, str);

            Request request = new Request.Builder()
                    .post(requestBody)
                    .url(Contants.HOME_URL)
                    .build();
            okHttpClient.newCall(request).enqueue(new Callback() {
                @Override
                public void onFailure(final Request request, IOException e) {
                    UiUtils.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mErrorView.setVisibility(View.VISIBLE);
                            mEmptyView.setVisibility(View.INVISIBLE);
                            mLoadingView.setVisibility(View.INVISIBLE);
                        }
                    });
                }

                @Override
                public void onResponse(com.squareup.okhttp.Response response) throws IOException {

                    final String string = response.body().string();
                    UiUtils.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try{
                                parseJson(string);
                            }catch (Exception e){
                                ToastUtils.showToast("未知错误");
                            }
                        }
                    });
                }
            });
        }

    }

    //parse json
    private void parseJson(String string) {

        Gson gson = new Gson();
        HuoDongBean huoDongBean = gson.fromJson(string, HuoDongBean.class);
        if (huoDongBean.success) {
            List<HuoDongBean.ModuleBean> mModuleBeen = huoDongBean.module;
            if (mModuleBeen.size() != 0) {


                mLoadingView.setVisibility(View.INVISIBLE);
                mErrorView.setVisibility(View.INVISIBLE);
                mEmptyView.setVisibility(View.INVISIBLE);

                View view = getRightView();
                view.setVisibility(View.VISIBLE);

                flContainer.addView(view);

                ImageView back = (ImageView) view.findViewById(R.id.iv_back_base);
                back.setOnClickListener(this);
                TextView headTitle = (TextView) view.findViewById(R.id.tv_base_center_text);
                headTitle.setText(mModuleBeen.get(0).pageName);

                //适配数据
                deployData(view, mModuleBeen);

            } else {
                mLoadingView.setVisibility(View.INVISIBLE);
                mErrorView.setVisibility(View.INVISIBLE);
                mEmptyView.setVisibility(View.VISIBLE);
            }
        } else {
            mLoadingView.setVisibility(View.INVISIBLE);
            mErrorView.setVisibility(View.INVISIBLE);
            mEmptyView.setVisibility(View.VISIBLE);
        }
    }

    public View getRightView() {
        View view = View.inflate(HuoDongActivity.this,R.layout.activity_huo_dong,null);
        mLlContainer = (LinearLayout) view.findViewById(R.id.llContainr);
        return view;
    }

    //适配数据
    public void deployData(View view, List<HuoDongBean.ModuleBean> mModuleBeen) {
        addChildView(view, mModuleBeen);
    }


    //添加childview
    private void addChildView(View view, List<HuoDongBean.ModuleBean> mModuleBeen) {
        for (int i = 0; i < mModuleBeen.size(); i++) {
            HuoDongBean.ModuleBean moduleBean = mModuleBeen.get(i);
            int stepType = mModuleBeen.get(i).stepType;
            initDataOne(stepType, i, moduleBean);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_back_base://返回键
                finish();
                break;
        }

    }

    //dp->px
    public  int dip2px(float dpValue) {
        final float scale = HuoDongActivity.this.getResources().getDisplayMetrics().density;
        return (int) (dpValue * scale + 0.5f);
    }
    //获取steptype对应的view
    public void initDataOne(final int type, final int i, final HuoDongBean.ModuleBean moduleBean) {

       // Toast.makeText(HuoDongActivity.this, "initDataOne", Toast.LENGTH_LONG).show();

        UiUtils.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                switch (type) {//多图
                    case 12://单图
                        View view4 = UiUtils.getLayoutInflater().inflate(R.layout.huodong_one, null);
                        new DanTuActivityAdapter().set(view4, i, moduleBean, HuoDongActivity.this);
                        mLlContainer.addView(view4);
                        break;
                    case 16://文字+列表
                        View view2 = UiUtils.getLayoutInflater().inflate(R.layout.huodong_item, null);
                        MyRecyclerView recyclerView2= (MyRecyclerView) view2.findViewById(R.id.huodong_item_recyclerview);
                        recyclerView2.setLayoutManager(new GridLayoutManager(HuoDongActivity.this,2, GridLayout.VERTICAL,false){
                            @Override
                            public boolean canScrollVertically() {
                                return false;
                            }
                        });
                        recyclerView2.addItemDecoration(new GridSpacingItemDecoration(2,dip2px(8),false));
                        recyclerView2.setAdapter(new HuodongRecyclerAdapter(HuoDongActivity.this,moduleBean.items));
                        TextView itemTitle2= (TextView) view2.findViewById(R.id.huodong_item_title);
                        itemTitle2.setText(moduleBean.title);
                        mLlContainer.addView(view2);
                        break;
                    /*case 3://文字+列表
                        View view3 = UiUtils.getLayoutInflater().inflate(R.layout.huodong_item, null);
                        MyRecyclerView recyclerView= (MyRecyclerView) view3.findViewById(R.id.huodong_item_recyclerview);
                        recyclerView.setLayoutManager(new GridLayoutManager(HuoDongActivity.this,2, GridLayout.VERTICAL,false){
                            @Override
                            public boolean canScrollVertically() {
                                return false;
                            }
                        });
                        recyclerView.addItemDecoration(new GridSpacingItemDecoration(2,dip2px(8),false));
                        recyclerView.setAdapter(new HuodongRecyclerAdapter(HuoDongActivity.this,moduleBean.items));
                        TextView itemTitle= (TextView) view3.findViewById(R.id.huodong_item_title);
                        itemTitle.setText(moduleBean.title);
                        mLlContainer.addView(view3);
                        break;*/
                }

            }
        });
    }



    @Override
    protected void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
    }
}
