package com.xianzaishi.normandie;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;

import com.xianzaishi.normandie.common.Urls;

/**
 *  create by ShenLang 2016/12/30
 *  礼券奖励细则
 */
public class InvitationRulesActivity extends AppCompatActivity implements View.OnClickListener{
    private WebView mWebView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invitation_rules);
        initView();
    }

    private void initView() {
        //标题和返回上一页按钮
        ImageView back= (ImageView) findViewById(R.id.address_go_back);
        back.setOnClickListener(this);
        TextView title= (TextView) findViewById(R.id.text_title);
        title.setText("礼券奖励细则");
        //webView相关
        mWebView= (WebView) findViewById(R.id.invitation_rules_webView);
        WebSettings settings = mWebView.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setUseWideViewPort(true);
        settings.setLoadWithOverviewMode(true);
        settings.setBuiltInZoomControls(true);
        settings.setSupportZoom(true);
        settings.setDomStorageEnabled(true);
        mWebView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }
        });
        mWebView.loadUrl(Urls.INVITATION_RULES);
    }

    @Override
    public void onClick(View view) {
        finish();
    }
}
