package com.xianzaishi.normandie;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.umeng.analytics.MobclickAgent;
import com.xianzaishi.normandie.bean.AddAddressBean;
import com.xianzaishi.normandie.bean.SelectAddressBean1;
import com.xianzaishi.normandie.bean.SelectAddressBean;
import com.xianzaishi.normandie.common.Urls;
import com.xianzaishi.normandie.global.Contants;
import com.xianzaishi.normandie.utils.GetBeanClass;
import com.xianzaishi.normandie.utils.JudgePhoneNumber;
import com.xianzaishi.normandie.utils.SPUtils;
import com.xianzaishi.normandie.utils.UiUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class AddAddressActivity extends AppCompatActivity implements View.OnClickListener{
    private TextView title,city,subCity,area,street;
    private EditText detailAddr,name,phone;
    private Button saveButton;
    private LinearLayout hint;//地址不在配送范围的提示
    private OkHttpClient client=MyApplication.getOkHttpClient();
    private Intent intent;
    public static final int CITY_CODE=1;
    public static final int SUBCITY_CODE=2;
    public static final int AREA_CODE=3;
    public static final int STREET_CODE=4;
    private String subCityCode,areaCode,streetCode,lastCode;
    private SelectAddressBean1 saBean;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_address);
        initView();
        initData();//请求可配送的四级行政地址
    }

    private void initData() {
        RequestBody formBody=new FormBody.Builder()
                .add("shop_id","1")
                .build();
        Request request=new Request.Builder()
                .url(Urls.GET_ADDRESS)
                .post(formBody)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if(!response.isSuccessful()) throw new IOException("Unexpected code"+response);
                saBean= (SelectAddressBean1) GetBeanClass.getBean(response,SelectAddressBean1.class);
                response.close();
            }
        });
    }

    private void initView() {
        /**
         * 设置标题
         */
        View view=findViewById(R.id.title_bar_save_address);
        title= (TextView) view.findViewById(R.id.text_title);
        title.setText("新增收货地址");
        ImageView backIcon= (ImageView) view.findViewById(R.id.address_go_back);
        backIcon.setOnClickListener(this);

        intent=new Intent(AddAddressActivity.this,SelectAddressActivity.class);

        city= (TextView) findViewById(R.id.add_address_city);//选择省市
        city.setOnClickListener(this);
        subCity= (TextView) findViewById(R.id.add_address_subCity);//选择市区
        subCity.setOnClickListener(this);
        area= (TextView) findViewById(R.id.add_address_area);//选择区域
        area.setOnClickListener(this);
        street= (TextView) findViewById(R.id.add_address_street);//选择街道
        street.setOnClickListener(this);
        hint= (LinearLayout) findViewById(R.id.add_address_hint);

        detailAddr= (EditText) findViewById(R.id.add_address_detail);
        name= (EditText) findViewById(R.id.add_address_name);
        phone= (EditText) findViewById(R.id.add_address_phone);

        saveButton= (Button) findViewById(R.id.add_address_button1);
        saveButton.setOnClickListener(this);
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode==RESULT_OK){
            switch (requestCode){
                case CITY_CODE:
                    city.setText(data.getStringExtra("area"));
                    subCity.setText("");
                    area.setText("");
                    street.setText("");
                    subCityCode=data.getStringExtra("code");
                    break;
                case SUBCITY_CODE:
                    subCity.setText(data.getStringExtra("area"));
                    area.setText("");
                    street.setText("");
                    areaCode=data.getStringExtra("code");
                    break;
                case AREA_CODE:
                    area.setText(data.getStringExtra("area"));
                    street.setText("");
                    streetCode=data.getStringExtra("code");
                    break;
                case STREET_CODE:
                    street.setText(data.getStringExtra("area"));
                    lastCode=data.getStringExtra("code");
                    break;
            }
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.add_address_city:
                ArrayList<SelectAddressBean1.DataBean.Level1AreaBean> list1= (ArrayList<SelectAddressBean1.DataBean.Level1AreaBean>) saBean.getData().getLevel1Area();
                intent.putExtra("list",list1);
                intent.putExtra("code",1);
                startActivityForResult(intent,CITY_CODE);
                break;
            case R.id.add_address_subCity:
                if(!TextUtils.isEmpty(city.getText())){
                    ArrayList<SelectAddressBean1.DataBean.Level2AreaBean> list2=new ArrayList<>();
                    for (SelectAddressBean1.DataBean.Level2AreaBean bean:saBean.getData().getLevel2Area()){
                        if(bean.getParentCode().equals(subCityCode)){
                            list2.add(bean);
                        }
                    }
                    intent.putExtra("list",list2);
                    intent.putExtra("code",2);
                    startActivityForResult(intent,SUBCITY_CODE);
                }else {
                    Toast.makeText(AddAddressActivity.this,"请先选择城市",Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.add_address_area:
                if(!TextUtils.isEmpty(subCity.getText())){
                    ArrayList<SelectAddressBean1.DataBean.Level3AreaBean> list3=new ArrayList<>();
                    for (SelectAddressBean1.DataBean.Level3AreaBean bean:saBean.getData().getLevel3Area()){
                        if(bean.getParentCode().equals(areaCode)){
                            list3.add(bean);
                        }
                    }
                    intent.putExtra("list",list3);
                    intent.putExtra("code",3);
                    startActivityForResult(intent,AREA_CODE);
                }else {
                    Toast.makeText(AddAddressActivity.this,"请先选择市区",Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.add_address_street:
                if(!TextUtils.isEmpty(area.getText())) {
                    ArrayList<SelectAddressBean1.DataBean.Level4AreaBean> list4=new ArrayList<>();
                    for (SelectAddressBean1.DataBean.Level4AreaBean bean:saBean.getData().getLevel4Area()){
                        if(bean.getParentCode().equals(streetCode)){
                            list4.add(bean);
                        }
                    }
                    intent.putExtra("list",list4);
                    intent.putExtra("code",4);
                    startActivityForResult(intent, STREET_CODE);
                }else {
                    Toast.makeText(AddAddressActivity.this,"请先选择城市和区域",Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.address_go_back:
                finish();
                break;
            case R.id.add_address_button1:
                String streetName= street.getText().toString();
                String details=detailAddr.getText().toString();
                String uid= SPUtils.getStringValue(MyApplication.getContext(), Contants.SP_NAME,Contants.UID,null);
                final String names=name.getText().toString();
                final String phones=phone.getText().toString();
                final String wholeAddress=city.getText().toString()+subCity.getText().toString()+area.getText().toString()+street.getText().toString()+details;
                if(!TextUtils.isEmpty(details)&!TextUtils.isEmpty(streetName)&!TextUtils.isEmpty(names)){
                    if(JudgePhoneNumber.judgeMobileNumber(this,phones)){
                        RequestBody formBody=new FormBody.Builder()
                                .add("uid",uid)
                                .add("code",lastCode)
                                .add("address",details)
                                .add("phone",phones)
                                .add("name",names)
                                .build();
                        Request request=new Request.Builder()
                                .url(Urls.ADD_LINK_MAN)
                                .post(formBody)
                                .build();
                        client.newCall(request).enqueue(new Callback() {
                            @Override
                            public void onFailure(Call call, IOException e) {
                                UiUtils.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(AddAddressActivity.this,"网络请求失败！",Toast.LENGTH_SHORT).show();
                                    }
                                });
                            }

                            @Override
                            public void onResponse(Call call, Response response) throws IOException {
                                if(!response.isSuccessful()) throw new IOException("Unexpected code"+response);
                                final AddAddressBean addAddressBean = (AddAddressBean) GetBeanClass.getBean(response,AddAddressBean.class);
                                response.close();
                                UiUtils.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        if (addAddressBean.getCode()==1){
                                            if(getIntent().getIntExtra("callback",0)==1){
                                                Intent intent=new Intent();
                                                intent.putExtra("name",names);
                                                intent.putExtra("phone",phones);
                                                intent.putExtra("address",wholeAddress);
                                                //intent.putExtra("addressId",addAddressBean.getData());
                                                SPUtils.putStringValue(MyApplication.getContext(),Contants.SP_NAME,"addressId",addAddressBean.getData());
                                                setResult(RESULT_OK,intent);
                                            }
                                            finish();
                                        }else if(addAddressBean.getCode()==-1){
                                            hint.setVisibility(View.VISIBLE);
                                            TimerTask timerTask=new TimerTask() {
                                                @Override
                                                public void run() {
                                                    UiUtils.runOnUiThread(new Runnable() {
                                                        @Override
                                                        public void run() {
                                                            hint.setVisibility(View.GONE);
                                                        }
                                                    });
                                                }
                                            };
                                            Timer timer=new Timer();
                                            timer.schedule(timerTask,3000);
                                        }
                                    }
                                });
                            }
                        });
                    }
                }else {
                    Toast.makeText(this,"请完善您的信息",Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
    }
}
