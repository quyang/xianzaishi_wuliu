package com.xianzaishi.normandie;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.content.Intent;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PathMeasure;
import android.graphics.drawable.Drawable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Display;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.LinearInterpolator;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.gson.Gson;
import com.umeng.analytics.MobclickAgent;
import com.xianzaishi.normandie.bean.GeneralBean;
import com.xianzaishi.normandie.bean.GoodsIntroduceBean;
import com.xianzaishi.normandie.bean.ShoppingCarCountBean;
import com.xianzaishi.normandie.common.Urls;
import com.xianzaishi.normandie.customs.CircleImageView;
import com.xianzaishi.normandie.customs.McoyProductContentPage;
import com.xianzaishi.normandie.customs.McoyProductDetailInfoPage;
import com.xianzaishi.normandie.customs.McoySnapPageLayout;
import com.xianzaishi.normandie.customs.MyWebView;
import com.xianzaishi.normandie.global.Contants;
import com.xianzaishi.normandie.http.AllOrderProtocol;
import com.xianzaishi.normandie.http.BaseProtocol;
import com.xianzaishi.normandie.http.PostProtocol;
import com.xianzaishi.normandie.interfaces.OnDataFromPostServerListener;
import com.xianzaishi.normandie.utils.GetBeanClass;
import com.xianzaishi.normandie.utils.GlideUtils;
import com.xianzaishi.normandie.utils.SPUtils;
import com.xianzaishi.normandie.utils.ToastUtils;
import com.xianzaishi.normandie.utils.UiUtils;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


/**
 * quyang
 * 2016年8月19日10:46:56
 * edited by ShenLang 2016.10.15
 */
public class GoodsDetailsActivity extends BaseActivity01 {
    public int count = 0;
    public int tag = 1;
    public boolean isIntrouceSeclected = true;
    public boolean isShuxingSeclected = false;
    public boolean isSelected = false;

    //share   appkey 164b09560d63b
    int[] viewpagers = new int[]{R.mipmap.page01, R.mipmap.page01, R.mipmap.page01, R.mipmap.page01};
    private GestureDetector detector;
    private ViewPager viewPager;
    private List<ImageView> guideArray=new ArrayList<>();
    private LinearLayout guideParent;
    private ImageView ivPref;
    private ImageView ivShare;
    private TextView redSign;
    private RelativeLayout rl01;
    private TextView tvGoodName;
    private TextView tvDanzhong;
    private TextView textView3;
    private TextView tvPriceNow;
    private TextView tvPriceOld;
    private Button ivJian;
    private TextView tvCount;
    private Button ivZeng;
    private TextView tvGuige;
    private RadioButton tvSiJing;
    private TextView tvChandi;
    private TextView tvCountry;
    private ImageView ivShopping;
    private TextView tvAddShop;
    private GoodsIntroduceBean data;
    private GoodsIntroduceBean.ModuleBean module;
    private List<GoodsIntroduceBean.ModuleBean.ItemSkuVOsBean> itemSkuVOsBeanList;
    private List<String> picList;
    private View top;
    private View down;
    private MyClick listener;
    private TextView count1;
    private Button add;
    private TextView shoppingCount;
    private ImageView goToshoppingCar,placeHolderImage;
    private McoySnapPageLayout mcoySnapPageLayout = null;
    private McoyProductContentPage bottomPage = null;
    private McoyProductDetailInfoPage topPage = null;
    private int itemId;
    private int intValue;
    private int mIntExtra;
    private int mAnInt;
    private String uid;
    private View view;
    private MyWebView webview;
    private int[] start=new int[2];
    private int[] end=new int[2];
    private int[] assist=new int[2];
    private RelativeLayout rootView;

    @Override
    protected void onResume() {
        super.onResume();
        getShoppingCount();

        MobclickAgent.onResume(this);
    }

    public View createSuccessView() {

        View view = UiUtils.inflateView(R.layout.activity_goods_details02);
        goToshoppingCar = (ImageView) view.findViewById(R.id.iv_shoppingcar);
        //找到组件
        shoppingCount = (TextView) view.findViewById(R.id.tv_shopping_count);

        add = (Button) view.findViewById(R.id.btn_add_shopping);
        mcoySnapPageLayout = (McoySnapPageLayout) view.findViewById(R.id.flipLayout);
        rootView= (RelativeLayout) view.findViewById(R.id.root_view);
        top = View.inflate(this, R.layout.activity_goods_details, null);
        redSign= (TextView) top.findViewById(R.id.goods_detail_red_sign);
        placeHolderImage= (ImageView) top.findViewById(R.id.place_holder_image);
        down = View.inflate(this, R.layout.shopping_pic_details, null);
        topPage = new McoyProductDetailInfoPage(GoodsDetailsActivity.this, top);
        bottomPage = new McoyProductContentPage(GoodsDetailsActivity.this, down);
        mcoySnapPageLayout.setSnapPages(topPage, bottomPage);
        Display display=getWindowManager().getDefaultDisplay();
        int height=display.getHeight();
        int width=display.getWidth();

        start[0]=2*width/3;
        start[1]=height;
        end[0]=0;
        end[1]=height;
        assist[0]=width/3;
        assist[1]=height/3;
        return view;
    }


    @Override
    public void loadDataFromNet() {
        view = createSuccessView();
        AllOrderProtocol protocal = new AllOrderProtocol();
        mIntExtra = getIntent().getIntExtra(Contants.ITEM_ID, -1);
        if (mIntExtra != -1) {
            String url = Contants.GOODS_DETAILS_URL + "?itemId=" + mIntExtra;
            protocal.setUrl(url);
            protocal.getData();
            protocal.setOnDataFromNetListener(new BaseProtocol.OnDataFromNetListener() {
                @Override
                public void onSuccess(String json) {

                    //解析json
                    try{
                        parseJson(json);
                    }catch (Exception e){
                        ToastUtils.showToast("未知错误");
                    }
                }

                @Override
                public void onFail() {
                    mLoadingView.setVisibility(View.INVISIBLE);
                    mErrorView.setVisibility(View.VISIBLE);
                }
            });
        }


    }

    //解析json
    public void parseJson(String json) {

        Gson gson = new Gson();
        data = gson.fromJson(json, GoodsIntroduceBean.class);
        boolean succcess = data.succcess;

        if (succcess) {
            module = data.module;
            itemId = module.itemId;
            picList = module.picList==null?new ArrayList<String>():module.picList;
            itemSkuVOsBeanList = module.itemSkuVOs;
            GoodsIntroduceBean.ModuleBean.ItemSkuVOsBean.TagDetailBean tagDetailBean=itemSkuVOsBeanList.get(0).tagDetail;
            //隐藏其他界面
            mLoadingView.setVisibility(View.INVISIBLE);
            /**
             *  显示成功界面
             */
            //商品打标
            if (itemSkuVOsBeanList.get(0).inventory<=0){
                redSign.setVisibility(View.VISIBLE);
                redSign.setBackgroundResource(R.drawable.grey_sign);
                redSign.setText("卖光啦");
                add.setEnabled(false);
                add.setText("本商品已售罄");
                add.setBackgroundColor(getResources().getColor(R.color.colorLightGreyButton));
            }else if(!tagDetailBean.mayPlus){
                if(tagDetailBean.channel==null||tagDetailBean.tagContent==null){
                    redSign.setVisibility(View.GONE);
                }else {
                    redSign.setVisibility(View.VISIBLE);
                    redSign.setBackgroundResource(R.drawable.grey_sign);
                    redSign.setText(tagDetailBean.tagContent);
                }
                add.setEnabled(false);
                add.setText("本商品尚未开售");
                add.setBackgroundColor(getResources().getColor(R.color.colorLightGreyButton));
            }else {

                if (tagDetailBean.channel==null||tagDetailBean.tagContent==null||tagDetailBean.channel.equals("2")){
                    redSign.setVisibility(View.GONE);
                }else {
                    redSign.setVisibility(View.VISIBLE);
                    redSign.setBackgroundResource(R.drawable.red_sign);
                    redSign.setText(tagDetailBean.tagContent);
                }
            }

            //找到组件
            findComponent();
            //初始化viewpager
            initViewPager();
            //修改组件文字
            changeComponentText();
            //初始化btn
            initBtn();

            listener = new MyClick();
            ivZeng.setOnClickListener(listener);
            ivJian.setOnClickListener(listener);
            tvSiJing.setOnClickListener(listener);
            ivPref.setOnClickListener(listener);
            ivShare.setOnClickListener(listener);
            add.setOnClickListener(listener);
            goToshoppingCar.setOnClickListener(listener);

            view.setVisibility(View.VISIBLE);
            flContainer.addView(view);

        }
    }


    //找到组件
    private void findComponent() {

        viewPager = (ViewPager) top.findViewById(R.id.viewPager_goods);
        ivPref = (ImageView) top.findViewById(R.id.iv_pref);
        ivShare = (ImageView) top.findViewById(R.id.iv_share);

        TextView deliverHint= (TextView) top.findViewById(R.id.deliver_hint);//下单明日送达的提示
        SimpleDateFormat dateFormat=new SimpleDateFormat("kk", Locale.CHINA);
        int hour=Integer.valueOf(dateFormat.format(data.currentTime));
        if (19<=hour&&24>hour){
            deliverHint.setVisibility(View.VISIBLE);
            deliverHint.setText("下单后明日送达");
        }else {
            deliverHint.setVisibility(View.GONE);
        }

        tvGoodName = (TextView) top.findViewById(R.id.tv_goods_name);
        tvDanzhong = (TextView) top.findViewById(R.id.tv_danzhong);

        tvPriceNow = (TextView) top.findViewById(R.id.tv_price_now);
        tvPriceOld = (TextView) top.findViewById(R.id.tv_price_old);
        ivJian = (Button) top.findViewById(R.id.iv_jian);

        tvCount = (TextView) top.findViewById(R.id.tv_count);
        ivZeng = (Button) top.findViewById(R.id.iv_zeng);
        tvGuige = (TextView) top.findViewById(R.id.tv_guige);
        tvSiJing = (RadioButton) top.findViewById(R.id.rb03);
        tvChandi = (TextView) top.findViewById(R.id.tv_chandi);
        tvCountry = (TextView) top.findViewById(R.id.tv_country);


        //-----------------------------图文详情页组件---------------------------------

        ImageView ivBack = (ImageView) down.findViewById(R.id.iv_back_details);
        TextView tvCenter = (TextView) down.findViewById(R.id.tv_base_center_text);
        tvCenter.setText("图文详情");

        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        //初始化商品介绍
        initIntroduce();
    }


    private void initIntroduce() {
        webview = (MyWebView) down.findViewById(R.id.wv_webview);
        initWebView(webview);
    }


    //初始化webview
    private void initWebView(WebView webview) {
        WebSettings webSettings= webview.getSettings();
        webSettings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
        webSettings.setBlockNetworkImage(false);
        webSettings.setDomStorageEnabled(true);
        String url=String.format(Urls.WEBVIEW_URL,mIntExtra);
        webview.loadUrl(url);
    }



    //点击加入购物车的时候
    private void postData(String uid) {
        FormBody body = new FormBody.Builder()
                .add(Contants.UID, uid)
                .add(Contants.ITEM_ID, itemId + "")
                .add("itemCount", tvCount.getText().toString().trim())
                .add("skuId", itemSkuVOsBeanList.get(0).skuId + "")
                .build();
        PostProtocol protocol1 = new PostProtocol();
        protocol1.setUrl(Contants.ADD_SHOPPING_CAR);
        protocol1.setRequestBody(body);
        protocol1.getDataByPOST();
        protocol1.setOnDataFromPostServerListener(new OnDataFromPostServerListener() {
            @Override
            public void onSuccess(String result) {

                Gson gson = new Gson();
                GeneralBean generalBean = gson.fromJson(result, GeneralBean.class);
                if (generalBean.success) {
                    //{"code":1,"message":"成功","data":313,"success":true,"error":false}
                    //获取商品数量
                    int a = Integer.parseInt(tvCount.getText().toString().trim());
                    mAnInt = mAnInt + a;
                    //添加成功,修改购物车图标上的数字
                    if (mAnInt != 0) {
                        shoppingCount.setText(mAnInt + "");
                        shoppingCount.setVisibility(View.VISIBLE);
                    }
                    addTrolleyAnimator();
                }else {
                    ToastUtils.showToast("添加购物车失败");
                }
            }

            @Override
            public void onFail() {
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(webview!=null){
            webview.destroy();
        }
    }

    //添加商品到购物车
    private void addShoppingCar() {
        MobclickAgent.onEvent(this,"detailCart");//友盟统计加购按钮点击次数
        MobclickAgent.onEvent(this,"addToCart");

        String uid = SPUtils.getStringValue(GoodsDetailsActivity.this,
                Contants.SP_NAME, Contants.UID, null);
        if (!tvSiJing.isSelected()) {
            Toast.makeText(GoodsDetailsActivity.this, "您未选规格", Toast.LENGTH_LONG).show();
            return;
        }
        if (uid == null) {
            //登录界面
            Intent intent = new Intent(this, NewLoginActivity.class);
            startActivity(intent);
            return;
        } else {
            postData(uid);
        }
    }

    private void initBtn() {

        tvSiJing.setSelected(true);

        tvCountry.setText(itemSkuVOsBeanList.get(0).originplace);//产地
    }

    private void changeComponentText() {

        tvGoodName.setText(module.title);//标题
        tvDanzhong.setText(module.subtitle);//副标题
        String yuan=itemSkuVOsBeanList.get(0).priceYuanString;
        String discountYuan=itemSkuVOsBeanList.get(0).discountPriceYuanString;
        if (yuan.equals(discountYuan)){
            tvPriceOld.setVisibility(View.INVISIBLE);
        }
        tvPriceOld.setText("¥"+yuan);//原价
        tvPriceNow.setText(discountYuan);//优惠后的价格
        tvSiJing.setText(itemSkuVOsBeanList.get(0).saleDetailInfo);
    }

    public String getGoodsIntroduce() {
        return module.introductionUrl;
    }

    public String getGoodsShuxing() {
        return module.propertyUrl;
    }

    //初始化viewpager
    private void initViewPager() {

        //中划线
        tvPriceOld.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG); //中划线

        //设置适配器
        viewPager.setAdapter(new MyPagerAdapter());

        LayoutInflater inflater=LayoutInflater.from(this);
        guideParent= (LinearLayout) view.findViewById(R.id.goods_detail_guide_parent);
        guideArray.clear();
        guideParent.removeAllViews();

        final int size=picList==null?0:picList.size();
        if(size>0) {
            for (int i = 0; i < size; i++) {
                View view = inflater.inflate(R.layout.viewpager_guide_circle, null);
                ImageView guide = (ImageView) view.findViewById(R.id.image_guide);

                guideArray.add(guide);
                guideParent.addView(view);
            }
            guideArray.get(0).setEnabled(false);

            viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                }

                @Override
                public void onPageSelected(int position) {
                    for (int i = 0; i < size; i++) {
                        if (i == position) {
                            guideArray.get(i).setEnabled(false);
                        } else {
                            guideArray.get(i).setEnabled(true);
                        }
                    }
                }

                @Override
                public void onPageScrollStateChanged(int state) {
                }
            });
        }else {
            placeHolderImage.setVisibility(View.VISIBLE);
        }

    }

    //点击事件
    public class MyClick implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            switch (view.getId()) {

                case R.id.iv_zeng:
                    count++;
                    tvCount.setText(count + "");
                    break;


                case R.id.iv_jian:

                    count--;
                    if (count < 1) {
                        count = 1;
                    }

                    tvCount.setText(count + "");
                    break;

//                case R.id.rb02:
//                    Toast.makeText(getApplicationContext(), "二斤", Toast.LENGTH_LONG).show();
//
//                    tvTwoJing.setSelected(true);
//                    tvSiJing.setSelected(false);
//
//                    break;

                case R.id.rb03:

                    if (isSelected) {
                        tvSiJing.setSelected(false);
                        isSelected = false;

                    } else {
                        tvSiJing.setSelected(true);
                        isSelected = true;
                    }

                    break;
                case R.id.iv_pref:
                    finish();
                    break;
                case R.id.iv_share:

                    break;
                case R.id.btn_add_shopping:
                    addShoppingCar();
                    break;
                case R.id.iv_shoppingcar:
                    MobclickAgent.onEvent(GoodsDetailsActivity.this,"goToCart");

                    Intent intent = new Intent(GoodsDetailsActivity.this, MainActivity.class);
                    intent.putExtra("pageNo", 3);
                    startActivity(intent);
                    finish();
                    break;
            }
        }
    }

    //viewpager的适配器类
    private class MyPagerAdapter extends PagerAdapter {
        @Override
        public int getCount() {
            return picList==null?0:picList.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {


            ImageView imageView = new ImageView(GoodsDetailsActivity.this);

            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);

            Glide.with(GoodsDetailsActivity.this)
                    .load(picList.get(position))
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .placeholder(R.mipmap.zhanwei120)
                    .into(imageView);
            container.addView(imageView);

            return imageView;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }
    }



    @Override
    protected void onPause() {
        super.onPause();

        MobclickAgent.onPause(this);
    }

    /**
     * 获取购物车商品数量
     */
    private void getShoppingCount() {
        OkHttpClient client=MyApplication.getOkHttpClient();
        uid= SPUtils.getStringValue(MyApplication.getContext(), Contants.SP_NAME,Contants.UID,"");
        if (uid.equals("")){//未登录状态 隐藏指示器
            shoppingCount.setVisibility(View.GONE);
        }else {
            RequestBody requestForm=new FormBody.Builder()
                    .add("uid",uid)
                    .build();
            final Request request=new Request.Builder()
                    .url(Urls.GET_ALL_COUNT)
                    .post(requestForm)
                    .build();
            client.newCall(request).enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    if(!response.isSuccessful()) throw new IOException("Unexpected code"+response);
                    final ShoppingCarCountBean shoppingCarCountBean = (ShoppingCarCountBean) GetBeanClass.getBean(response,ShoppingCarCountBean.class);
                    response.close();
                    UiUtils.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (shoppingCarCountBean.getCode()==1){
                                intValue=Integer.valueOf(shoppingCarCountBean.getData());
                                if (intValue != 0) {
                                    shoppingCount.setText(intValue + "");
                                    mAnInt=intValue;
                                    shoppingCount.setVisibility(View.VISIBLE);
                                } else {
                                    //默认影藏
                                    shoppingCount.setVisibility(View.INVISIBLE);
                                }
                            }
                        }
                    });

                }
            });
        }
    }

    private void addTrolleyAnimator(){
        /**
         *  构造一个用来执行动画的商品图
         */
        /**
         *  加购动画所需的图片
         */
        Drawable drawable=((ImageView)viewPager.getChildAt(0)).getDrawable();
        final CircleImageView goods=new CircleImageView(this);
        goods.setImageDrawable(drawable);
        RelativeLayout.LayoutParams params=new RelativeLayout.LayoutParams(150,150);
        rootView.addView(goods,params);
        /**
         * 开始掉落的商品的起始点：商品起始点-父布局起始点+该商品图片的一半
         */
        float fromX=start[0];
        float fromY=start[1];
        /**
         * 商品掉落后的终点坐标：购物车起始点-父布局起始点
         */
        float toX=end[0];
        float toY=end[1];
        final float[] currentLocation=new float[2];
        Path path=new Path();//开始绘制贝塞尔曲线
        path.moveTo(fromX,fromY);//移动到起始点
        path.quadTo(assist[0],assist[1],toX,toY);//使用贝塞尔曲线
        final PathMeasure pathMeasure=new PathMeasure(path,false);//用来计算贝塞尔曲线的曲线长度和贝塞尔曲线中间插值的坐标，
        // 如果是true，path会形成一个闭环
        ValueAnimator valueAnimator=ValueAnimator.ofFloat(0,pathMeasure.getLength());
        valueAnimator.setDuration(1000);//设置属性动画
        valueAnimator.setInterpolator(new LinearInterpolator());//匀速线性插值器
        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                float value= (float) valueAnimator.getAnimatedValue();
                pathMeasure.getPosTan(value,currentLocation,null);
                goods.setTranslationX(currentLocation[0]);
                goods.setTranslationY(currentLocation[1]);
            }
        });
        valueAnimator.start();
        valueAnimator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                rootView.removeView(goods);
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
    }
}
