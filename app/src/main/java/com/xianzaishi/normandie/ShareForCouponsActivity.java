package com.xianzaishi.normandie;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.umeng.socialize.ShareAction;
import com.umeng.socialize.UMShareAPI;
import com.umeng.socialize.UMShareListener;
import com.umeng.socialize.bean.SHARE_MEDIA;
import com.umeng.socialize.media.UMImage;
import com.umeng.socialize.shareboard.ShareBoardConfig;
import com.umeng.socialize.shareboard.SnsPlatform;
import com.umeng.socialize.utils.ShareBoardlistener;
import com.xianzaishi.normandie.bean.InvitationBean;
import com.xianzaishi.normandie.bean.InvitationTopBean;
import com.xianzaishi.normandie.common.Urls;
import com.xianzaishi.normandie.global.Contants;
import com.xianzaishi.normandie.utils.GetBeanClass;
import com.xianzaishi.normandie.utils.SPUtils;
import com.xianzaishi.normandie.utils.ToastUtils;
import com.xianzaishi.normandie.utils.UiUtils;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class ShareForCouponsActivity extends AppCompatActivity implements View.OnClickListener {
    private TextView invitationCode, achievementBtn, invitationBtn,titlePrice,titleText,text1,text2;
    private TextView icon1,icon2;
    private Button ruleBtn;
    private ShareBoardlistener shareBoardlistener;
    private ClipboardManager myClipboard;
    private String code=null;
    private OkHttpClient client=MyApplication.getOkHttpClient();
    private String phone=null;
    private String shareTitle,shareContent,shareImg,shareUrl,targetUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_share_for_coupons);
        initView();
        netWork();
        initTop();
    }

    private void initTop() {
        Request request=new Request.Builder()
                .url(Urls.INVITATION)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (!response.isSuccessful()){
                    response.close();
                    return;
                }
                final InvitationTopBean topBean= (InvitationTopBean) GetBeanClass.getBean(response, InvitationTopBean.class);
                UiUtils.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        titlePrice.setText("¥"+topBean.getTitleprice());
                        titleText.setText(topBean.getSubtitle());
                        icon1.setText(topBean.getDes_1().getTitleprice()+"\n"+topBean.getDes_1().getSubtitle());
                        icon2.setText(topBean.getDes_2().getTitleprice()+"\n"+topBean.getDes_2().getSubtitle());

                        String[] font1=topBean.getDes_1().getFont().split("-");
                        String[] font2=topBean.getDes_2().getFont().split("-");
                        spanColorText(text1,font1,topBean.getDes_1().getContent());
                        spanColorText(text2,font2,topBean.getDes_2().getContent());

                        InvitationTopBean.WeixinDesBean weixinDesBean=topBean.getWeixin_des();
                        shareTitle=weixinDesBean.getTitle();
                        shareContent=weixinDesBean.getContent();
                        shareImg=weixinDesBean.getImgurl();
                        shareUrl=weixinDesBean.getTargeturl();
                        invitationBtn.setOnClickListener(ShareForCouponsActivity.this);
                    }
                });
            }
        });
    }

    private void spanColorText(TextView textView, String[] fonts, String content) {
        if (fonts.length<2){
            textView.setText(content);
        }else {
            int start = Integer.valueOf(fonts[0]);
            int end = Integer.valueOf(fonts[1]);
            SpannableString spannableString = new SpannableString(content);
            ForegroundColorSpan span = new ForegroundColorSpan(this.getResources().getColor(R.color.colorRed));
            //将这个Span应用于指定范围的字体
            spannableString.setSpan(span, start, start + end, Spannable.SPAN_EXCLUSIVE_INCLUSIVE);
            textView.setText(spannableString);
        }

    }


    private void netWork() {
        String uid= SPUtils.getStringValue(MyApplication.getContext(), Contants.SP_NAME,Contants.UID,null);
        FormBody formBody=new FormBody.Builder()
                .add("userId",uid)
                .build();
        Request request=new Request.Builder()
                .url(Urls.GET_INVITATION_CODE)
                .post(formBody)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (response.isSuccessful()){
                    InvitationBean bean= (InvitationBean) GetBeanClass.getBean(response,InvitationBean.class);
                    if (bean!=null&&bean.isSuccess()&&bean.getModule()!=null){
                        code=bean.getModule().getInviteCode();
                        phone=bean.getModule().getPhone();
                        UiUtils.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                invitationCode.setText(code);
                            }
                        });
                    }
                }
            }
        });
    }

    private void initView() {
        myClipboard = (ClipboardManager)getSystemService(CLIPBOARD_SERVICE);
        //设置标题和返回的点击事件
        TextView title = (TextView) findViewById(R.id.text_title);
        title.setText("分享赢礼券");
        ImageView back = (ImageView) findViewById(R.id.address_go_back);
        back.setOnClickListener(this);
        invitationCode = (TextView) findViewById(R.id.invitation_code);
        //活动规则
        ruleBtn = (Button) findViewById(R.id.activity_rule_btn);
        ruleBtn.setOnClickListener(this);
        //我的成就
        achievementBtn = (TextView) findViewById(R.id.btn_achievement);
        achievementBtn.setOnClickListener(this);
        //马上邀请
        invitationBtn = (TextView) findViewById(R.id.btn_invitation);



        titlePrice= (TextView) findViewById(R.id.title_price);
        titleText= (TextView) findViewById(R.id.title_text);
        text1= (TextView) findViewById(R.id.text_1);
        text2= (TextView) findViewById(R.id.text_2);
        icon1= (TextView) findViewById(R.id.icon_1);
        icon2= (TextView) findViewById(R.id.icon_2);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.address_go_back:
                finish();
                break;
            case R.id.activity_rule_btn:
                startActivity(new Intent(ShareForCouponsActivity.this,InvitationRulesActivity.class));
                break;
            case R.id.btn_achievement:
                if (code!=null){
                    startActivity(new Intent(ShareForCouponsActivity.this, MyAchievementActivity.class));
                }else {
                    ToastUtils.showToast("您没有生成正确的邀请码，请重试！");
                }

                break;
            case R.id.btn_invitation:
                if (code!=null){
                    share(code);
                }else {
                    ToastUtils.showToast("您没有生成正确的邀请码，请重试！");
                }
                break;
        }
    }



    private void share(final String code) {
        shareBoardlistener = new  ShareBoardlistener() {

            @Override
            public void onclick(SnsPlatform snsPlatform, SHARE_MEDIA share_media) {
                if (share_media==null){
                    if (snsPlatform.mKeyword.equals("umeng_sharebutton_custom")){
                        ClipData myClipData=ClipData.newPlainText("text",code);
                        myClipboard.setPrimaryClip(myClipData);
                        ToastUtils.showToast("复制邀请码成功！");
                    }
                }
                else {
                    new ShareAction(ShareForCouponsActivity.this).setPlatform(share_media).setCallback(umShareListener)
                            .withText(shareContent)
                            .withTitle(shareTitle)
                            .withTargetUrl(shareUrl+"?telnum="+phone+"&invitenum="+code)
                            .withMedia(new UMImage(ShareForCouponsActivity.this,shareImg))
                            .share();
                }
            }
        };

        ShareBoardConfig config = new ShareBoardConfig();
        config.setShareboardPostion(ShareBoardConfig.SHAREBOARD_POSITION_BOTTOM)
        .setMenuItemBackgroundShape(ShareBoardConfig.BG_SHAPE_CIRCULAR)
        .setCancelButtonVisibility(false)
        .setTitleVisibility(false);
        new ShareAction(ShareForCouponsActivity.this)
                .setDisplayList(SHARE_MEDIA.WEIXIN,SHARE_MEDIA.WEIXIN_CIRCLE)
                .addButton("umeng_sharebutton_custom","umeng_sharebutton_custom","copy_code","copy_code")
                .setShareboardclickCallback(shareBoardlistener)
                .open(config);

    }


    private UMShareListener umShareListener=new UMShareListener() {
        @Override
        public void onResult(SHARE_MEDIA share_media) {

        }
        @Override
        public void onError(SHARE_MEDIA share_media, Throwable throwable) {

        }
        @Override
        public void onCancel(SHARE_MEDIA share_media) {

        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        UMShareAPI.get(this).onActivityResult(requestCode, resultCode, data);
    }

}
