package com.xianzaishi.normandie;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.umeng.analytics.MobclickAgent;
import com.xianzaishi.normandie.adapter.AddressListAdapter;
import com.xianzaishi.normandie.bean.SelectAddressBean;
import com.xianzaishi.normandie.common.Urls;
import com.xianzaishi.normandie.global.Contants;
import com.xianzaishi.normandie.utils.GetBeanClass;
import com.xianzaishi.normandie.utils.SPUtils;
import com.xianzaishi.normandie.utils.ToastUtils;
import com.xianzaishi.normandie.utils.UiUtils;

import java.io.IOException;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class AddressActivity extends AppCompatActivity implements View.OnClickListener{
    private ListView addressListView;
    private List<SelectAddressBean.DataBean> list;
    private ImageView back,delete,edit;
    private TextView editAddress,title;
    private Button button_add,button_empty;
    private LinearLayout container;
    private AddressListAdapter adapter;
    private boolean toggle;
    private Intent intent;
    private View empty;
    private FrameLayout flContainer;
    private LinearLayout linear_layout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_address);
        flContainer = (FrameLayout) findViewById(R.id.rlContainer);
        linear_layout = (LinearLayout) findViewById(R.id.linear_layout);

        initView();
    }

    private void initView() {
        /**
         * 标题栏相关初始化
         */
        View view=findViewById(R.id.title_bar_select_address);
        back= (ImageView) view.findViewById(R.id.address_go_back);
        back.setOnClickListener(this);
        title= (TextView) view.findViewById(R.id.text_title);
        title.setText("选择收货地址");
        editAddress= (TextView) view.findViewById(R.id.control_address);
        editAddress.setText("管理");
        editAddress.setOnClickListener(this);
        LayoutInflater inflater=LayoutInflater.from(this);
        empty=inflater.inflate(R.layout.emptyview,null);
        button_empty= (Button) empty.findViewById(R.id.add_address_button_for_empty);
        button_empty.setOnClickListener(this);
        button_add= (Button) findViewById(R.id.add_address_button);
        button_add.setOnClickListener(this);
        intent=new Intent(AddressActivity.this,AddAddressActivity.class);
        addressListView= (ListView) findViewById(R.id.address_list);
        container= (LinearLayout) findViewById(R.id.button_container);
        container.setVisibility(View.GONE);
        editAddress.setVisibility(View.INVISIBLE);
    }


    private void initListView() {
        /**
         * 获取全部收货地址
         */
        OkHttpClient client=MyApplication.getOkHttpClient();
        String uid= SPUtils.getStringValue(MyApplication.getContext(), Contants.SP_NAME,Contants.UID,null);
        RequestBody formBody=new FormBody.Builder()
                .add("uid",uid)
                .build();
        Request request=new Request.Builder()
                .url(Urls.GET_ALL_ADDRESS)
                .post(formBody)
                .build();

        System.out.println("hehe = "+String.format(Urls.GET_ALL_ADDRESS,uid));

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                UiUtils.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(AddressActivity.this,"网络请求失败！",Toast.LENGTH_SHORT).show();
                    }
                });
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if(!response.isSuccessful()) throw new IOException("Unexpected code"+response);
                final SelectAddressBean selectAddressBean = (SelectAddressBean) GetBeanClass.getBean(response,SelectAddressBean.class);
                response.close();
                UiUtils.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if(selectAddressBean.getCode()==1){
                            list= selectAddressBean.getData();
                            adapter=new AddressListAdapter(AddressActivity.this,list,container,editAddress);
                            if("完成".equals(editAddress.getText())){
                                adapter.setShow(true);
                            }
                            addressListView.setAdapter(adapter);
                            /**
                             * 回调地址信息给调用的页面
                             */
                            addressListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                @Override
                                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                                    if(getIntent().getIntExtra("callback",0)==1){//判断本页面是否由startforresult打开的
                                        SelectAddressBean.DataBean dataBean=list.get(i);
                                        String name=dataBean.getName();
                                        String phone=dataBean.getPhone();
                                        String address=dataBean.getCodeAddress()+dataBean.getAddress();
                                        String addressId=String.valueOf(dataBean.getId());
                                        Intent intent=new Intent();
                                        //intent.putExtra("addressId",addressId);
                                        intent.putExtra("name",name);
                                        intent.putExtra("phone",phone);
                                        intent.putExtra("address",address);
                                        SPUtils.putStringValue(MyApplication.getContext(),Contants.SP_NAME,"addressId",addressId);
                                        setResult(RESULT_OK,intent);
                                        finish();
                                    }
                                }
                            });
                            ViewGroup vg= (ViewGroup) addressListView.getParent();
                            if(empty.getParent()==null){
                                vg.addView(empty);
                            }
                            addressListView.setEmptyView(empty);
                            if(adapter.isEmpty()){
                                container.setVisibility(View.GONE);
                                editAddress.setVisibility(View.INVISIBLE);
                                editAddress.setText("管理");
                            }else {
                                container.setVisibility(View.VISIBLE);
                                editAddress.setVisibility(View.VISIBLE);
                            }
                        }
                    }
                });
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        initListView();

        MobclickAgent.onResume(this);
    }
    @Override
    protected void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.address_go_back:
                finish();
                break;
            case R.id.control_address:
                if(!toggle){
                    adapter.setShow(true);
                    editAddress.setText("完成");
                    toggle=true;
                    adapter.notifyDataSetChanged();
                }else{
                    adapter.setShow(false);
                    editAddress.setText("管理");
                    toggle=false;
                    adapter.notifyDataSetChanged();
                }
                break;
            case R.id.add_address_button:
                this.startActivity(intent);
                break;
            case R.id.add_address_button_for_empty:
                this.startActivity(intent);
                break;
        }
    }
}
