package com.xianzaishi.normandie;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.gson.Gson;
import com.lzy.imagepicker.ImagePicker;
import com.lzy.imagepicker.bean.ImageItem;
import com.lzy.imagepicker.ui.ImageGridActivity;
import com.umeng.analytics.MobclickAgent;
import com.xianzaishi.normandie.bean.UpLoadImageBean;
import com.xianzaishi.normandie.common.Urls;
import com.xianzaishi.normandie.global.Contants;
import com.xianzaishi.normandie.utils.GlideImageLoader;
import com.xianzaishi.normandie.utils.SPUtils;
import com.xianzaishi.normandie.utils.ToastUtils;
import com.xianzaishi.normandie.utils.UiUtils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.Headers;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * quyang
 * <p/>
 * 申请售后
 * 2016年8月23日12:28:18
 */
public class ApplyServiceActivity extends BaseActivity {


    private Button btnCommit;

    private Button btnAdd;

    private EditText etDes;

    private LinearLayout llContainer;

    private MyClickListener listener;

    private ImagePicker imagePicker;


    private AlertDialog dialog;


    private ArrayList<ImageItem> mImageItems;


    private static final String TAG = "ApplyServiceActivity";

    private OkHttpClient client=MyApplication.getOkHttpClient();

    private String imgUrl="";

    private String oid;

    private String str;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_apply_service);


        btnCommit = (Button) childView.findViewById(R.id.btn_commit);
        btnAdd = (Button) childView.findViewById(R.id.btn_add);
        etDes = (EditText) childView.findViewById(R.id.et_des_service);
        llContainer = (LinearLayout) childView.findViewById(R.id.llContainr_service);
        oid=getIntent().getStringExtra("oid");
        listener = new MyClickListener();
        //设置点击事件
        btnCommit.setOnClickListener(listener);
        btnAdd.setOnClickListener(listener);
    }


    class MyClickListener implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            switch (view.getId()) {

                case R.id.btn_commit:
                    str=etDes.getText().toString().trim();
                   if (TextUtils.isEmpty(str)) {
                        Toast.makeText(getApplicationContext(), "申请内容不能为空!", Toast.LENGTH_LONG).show();
                        return;
                    }

                    int childCount = llContainer.getChildCount();
                    if (childCount == 0) {
                        Toast.makeText(getApplicationContext(), "没有添加商品图片", Toast.LENGTH_LONG).show();
                        return;
                    }

                    //将申请信息提交到服务器
                    upload();

                    break;
                case R.id.btn_add:
                    imagePicker = ImagePicker.getInstance();
                    imagePicker.setImageLoader(new GlideImageLoader());
                    imagePicker.setShowCamera(true);
                    imagePicker.setSelectLimit(4);
                    imagePicker.setCrop(false);
                    Intent intent = new Intent(getApplicationContext(), ImageGridActivity.class);
                    startActivityForResult(intent, 100);
                    break;
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode == ImagePicker.RESULT_CODE_ITEMS) {
            if (data != null && requestCode == 100) {
                mImageItems = (ArrayList<ImageItem>) data.getSerializableExtra(ImagePicker.EXTRA_RESULT_ITEMS);

                for (int i = 0; i < mImageItems.size(); i++) {
                    //获取路劲
                    System.out.println(mImageItems.get(i).path);

                    Bitmap btp = BitmapFactory.decodeFile(mImageItems.get(i).path);

                    //.显示位图
                    ImageView imageView = new ImageView(getApplicationContext());
                    imageView.setImageBitmap(btp);
                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(142, 142);
                    params.setMargins(0, 0, 16, 0);
                    imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
                    imageView.setLayoutParams(params);
                    imageView.setImageBitmap(btp);
                    llContainer.addView(imageView);

                }
            } else {
                Toast.makeText(getApplicationContext(), "没有数据", Toast.LENGTH_SHORT).show();
            }
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    private void upload(){
        String token = SPUtils.getStringValue(this, Contants.SP_NAME, Contants.TOKEN, null);

        MediaType MEDIA_TYPE_PNG = MediaType.parse("image/png");

        if (mImageItems != null) {
            for (int i = 0; i < mImageItems.size(); i++) {
                final int j=i;
                MultipartBody.Builder builder=  new MultipartBody.Builder().setType(MultipartBody.FORM);
                RequestBody body=builder.addPart(Headers.of("Content-Disposition", "form-data; name=\"img\";filename="+"\""+mImageItems.get(i).name+".jpg\""),
                        RequestBody.create(MEDIA_TYPE_PNG,new File(mImageItems.get(i).path)))
                        .addFormDataPart("token",token)
                        .addFormDataPart("type","1")
                        .addFormDataPart("bizid","4")
                        .build();
                Request request=new Request.Builder()
                        .url(Contants.IMG_COMMIT_URL)
                        .post(body)
                        .build();
                ToastUtils.showToast("正在上传……");
                client.newCall(request).enqueue(new Callback() {
                    @Override
                    public void onFailure(Call call, IOException e) {
                    }
                    @Override
                    public void onResponse(Call call, Response response) throws IOException {
                        Gson gson=new Gson();
                        UpLoadImageBean bean=gson.fromJson(response.body().string(),UpLoadImageBean.class);
                        response.close();
                        if (bean.isSucccess()){
                            String url=bean.getModule().replace("http://img.xianzaishi.com/","");
                            imgUrl+=String.format(Urls.DATA_PIC,j+2,url);
                            if (j==mImageItems.size()-1){
                                applyService(imgUrl);
                            }

                        }
                    }
                });
            }

        }
    }

    private void applyService(String imgUrl) {

        MediaType MEDIA_TYPE_MARKDOWN=MediaType.parse("text/x-markdown; charset=utf-8");
        String postBody=String.format(Urls.DATA,oid,str,imgUrl);

        FormBody body=new FormBody.Builder()
                .add("data",postBody)
                .build();
        Request request=new Request.Builder()
                .url(Urls.APPLY_SERVICE)
                .post(body)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
            }
            @Override
            public void onResponse(Call call, Response response) throws IOException {
                Gson gson=new Gson();
                final UpLoadImageBean bean=gson.fromJson(response.body().string(),UpLoadImageBean.class);
                response.close();
                UiUtils.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if(bean.isSuccess()){
                            ToastUtils.showToast("申请成功！");
                            finish();
                        }else {
                            ToastUtils.showToast("申请失败！");
                        }
                    }
                });
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
    }
}
