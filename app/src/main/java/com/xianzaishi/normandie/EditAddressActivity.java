package com.xianzaishi.normandie;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.xianzaishi.normandie.bean.GetAddressByIDBean;
import com.xianzaishi.normandie.bean.SelectAddressBean;
import com.xianzaishi.normandie.bean.SelectAddressBean1;
import com.xianzaishi.normandie.common.Urls;
import com.xianzaishi.normandie.global.Contants;
import com.xianzaishi.normandie.utils.GetBeanClass;
import com.xianzaishi.normandie.utils.JudgePhoneNumber;
import com.xianzaishi.normandie.utils.SPUtils;
import com.xianzaishi.normandie.utils.UiUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class EditAddressActivity extends AppCompatActivity implements View.OnClickListener{
    private TextView title,city,subCity,area,street;
    private EditText detailAddr,name,phone;
    private Button saveButton,deleteButton;
    private LinearLayout hint;//地址不在配送范围的提示
    private OkHttpClient client=MyApplication.getOkHttpClient();
    private Intent intent;
    public static final int CITY_CODE=1;
    public static final int SUBCITY_CODE=2;
    public static final int AREA_CODE=3;
    public static final int STREET_CODE=4;
    private String subCityCode,areaCode,streetCode,lastCode;
    private String addressID,uid;//地址ID，用户ID
    private SelectAddressBean1 saBean;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_address);

        initView();
        initData();//请求可配送的四级行政地址
    }

    private void initData() {
        RequestBody formBody=new FormBody.Builder()
                .add("shop_id","1")
                .build();
        Request request=new Request.Builder()
                .url(Urls.GET_ADDRESS)
                .post(formBody)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if(!response.isSuccessful()) throw new IOException("Unexpected code"+response);
                saBean= (SelectAddressBean1) GetBeanClass.getBean(response,SelectAddressBean1.class);
                response.close();
            }
        });
    }

    private void initView() {
        /**
         * 设置标题
         */
        View view=findViewById(R.id.title_bar_edit_address);
        title= (TextView) view.findViewById(R.id.text_title);
        title.setText("编辑收获地址");
        ImageView backIcon= (ImageView) view.findViewById(R.id.address_go_back);
        backIcon.setOnClickListener(this);

        city= (TextView) findViewById(R.id.edit_address_city);//选择省市
        city.setOnClickListener(this);
        subCity= (TextView) findViewById(R.id.edit_address_subCity);//选择市区
        subCity.setOnClickListener(this);
        area= (TextView) findViewById(R.id.edit_address_area);//选择区域
        area.setOnClickListener(this);
        street= (TextView) findViewById(R.id.edit_address_street);//选择街道
        street.setOnClickListener(this);
        hint= (LinearLayout) findViewById(R.id.edit_address_hint);

        detailAddr= (EditText) findViewById(R.id.edit_address_detail);
        name= (EditText) findViewById(R.id.edit_address_name);
        phone= (EditText) findViewById(R.id.edit_address_phone);

        saveButton= (Button) findViewById(R.id.edit_save_address_button);
        saveButton.setOnClickListener(this);
        deleteButton= (Button) findViewById(R.id.edit_delete_address_button);
        deleteButton.setOnClickListener(this);

        intent=new Intent();
        intent.setClass(EditAddressActivity.this,SelectAddressActivity.class);

        /**
         * 获取intent里的数据,并给控件赋值
         */
        Bundle bundle1=getIntent().getExtras();
        addressID=String.valueOf(bundle1.getInt("addressID"));
        uid=String.valueOf(bundle1.getInt("uid"));
        RequestBody formBody=new FormBody.Builder()
                .add("uid",uid)
                .add("id",addressID)
                .build();
        Request request=new Request.Builder()
                .url(Urls.GET_ADDRESS_BY_ID)
                .post(formBody)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                UiUtils.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(EditAddressActivity.this,"网络请求失败！",Toast.LENGTH_SHORT).show();
                    }
                });
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if(!response.isSuccessful()) throw new IOException("Unexpected code"+response);
                final GetAddressByIDBean sa= (GetAddressByIDBean) GetBeanClass.getBean(response,GetAddressByIDBean.class);
                response.close();
                UiUtils.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (sa.getCode()==1){
                            GetAddressByIDBean.DataBean dataBean=sa.getData();
                            city.setText(dataBean.getLevel1Name());
                            subCity.setText(dataBean.getLevel2Name());
                            area.setText(dataBean.getLevel3Name());
                            street.setText(dataBean.getLevel4Name());
                            detailAddr.setText(dataBean.getAddress());
                            phone.setText(dataBean.getPhone());
                            name.setText(dataBean.getName());
                            if(!TextUtils.isEmpty(dataBean.getCode())){
                                int areaCode1=Integer.valueOf(dataBean.getCode());
                                subCityCode=String.valueOf((areaCode1/1000000)*1000);
                                areaCode=String.valueOf((areaCode1/100000)*100);
                                streetCode=String.valueOf(areaCode1/1000);
                                lastCode=String.valueOf(areaCode1);
                            }
                        }
                    }
                });
            }
        });
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode==RESULT_OK){
            switch (requestCode){
                case CITY_CODE:
                    city.setText(data.getStringExtra("area"));
                    subCity.setText("");
                    area.setText("");
                    street.setText("");
                    subCityCode=data.getStringExtra("code");
                    break;
                case SUBCITY_CODE:
                    subCity.setText(data.getStringExtra("area"));
                    area.setText("");
                    street.setText("");
                    areaCode=data.getStringExtra("code");
                    break;
                case AREA_CODE:
                    area.setText(data.getStringExtra("area"));
                    street.setText("");
                    streetCode=data.getStringExtra("code");
                    break;
                case STREET_CODE:
                    street.setText(data.getStringExtra("area"));
                    lastCode=data.getStringExtra("code");
                    break;
            }
        }
    }
    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.edit_address_city:
                ArrayList<SelectAddressBean1.DataBean.Level1AreaBean> list1= (ArrayList<SelectAddressBean1.DataBean.Level1AreaBean>) saBean.getData().getLevel1Area();
                intent.putExtra("list",list1);
                intent.putExtra("code",1);
                startActivityForResult(intent,CITY_CODE);
                break;
            case R.id.edit_address_subCity:
                if(!TextUtils.isEmpty(city.getText())){
                    ArrayList<SelectAddressBean1.DataBean.Level2AreaBean> list2=new ArrayList<>();
                    for (SelectAddressBean1.DataBean.Level2AreaBean bean:saBean.getData().getLevel2Area()){
                        if(bean.getParentCode().equals(subCityCode)){
                            list2.add(bean);
                        }
                    }
                    intent.putExtra("list",list2);
                    intent.putExtra("code",2);
                    startActivityForResult(intent,SUBCITY_CODE);
                }else {
                    Toast.makeText(EditAddressActivity.this,"请先选择城市",Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.edit_address_area:
                if(!TextUtils.isEmpty(subCity.getText())){
                    ArrayList<SelectAddressBean1.DataBean.Level3AreaBean> list3=new ArrayList<>();
                    for (SelectAddressBean1.DataBean.Level3AreaBean bean:saBean.getData().getLevel3Area()){
                        if(bean.getParentCode().equals(areaCode)){
                            list3.add(bean);
                        }
                    }
                    intent.putExtra("list",list3);
                    intent.putExtra("code",3);
                    startActivityForResult(intent,AREA_CODE);
                }else {
                    Toast.makeText(EditAddressActivity.this,"请先选择市区",Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.edit_address_street:
                if(!TextUtils.isEmpty(area.getText())) {
                    ArrayList<SelectAddressBean1.DataBean.Level4AreaBean> list4=new ArrayList<>();
                    for (SelectAddressBean1.DataBean.Level4AreaBean bean:saBean.getData().getLevel4Area()){
                        if(bean.getParentCode().equals(streetCode)){
                            list4.add(bean);
                        }
                    }
                    intent.putExtra("list",list4);
                    intent.putExtra("code",4);
                    startActivityForResult(intent, STREET_CODE);
                }else {
                    Toast.makeText(EditAddressActivity.this,"请先选择城市和区域",Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.edit_save_address_button:
                String streetName= street.getText().toString();
                String details=detailAddr.getText().toString();
                String names=name.getText().toString();
                String phones=phone.getText().toString();
                if(!TextUtils.isEmpty(details)&!TextUtils.isEmpty(streetName)&!TextUtils.isEmpty(names)){
                    if(JudgePhoneNumber.judgeMobileNumber(this,phones)){
                        RequestBody formBody=new FormBody.Builder()
                                .add("uid",uid)
                                .add("id",addressID)
                                .add("code",lastCode)
                                .add("address",details)
                                .add("phone",phones)
                                .add("name",names)
                                .build();
                        Request request=new Request.Builder()
                                .url(Urls.MODIFY_LINK_MAN)
                                .post(formBody)
                                .build();
                        client.newCall(request).enqueue(new Callback() {
                            @Override
                            public void onFailure(Call call, IOException e) {
                                UiUtils.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(EditAddressActivity.this,"网络请求失败！",Toast.LENGTH_SHORT).show();
                                    }
                                });
                            }

                            @Override
                            public void onResponse(Call call, Response response) throws IOException {
                                if(!response.isSuccessful()) throw new IOException("Unexpected code"+response);
                                SelectAddressBean selectAddressBean = (SelectAddressBean) GetBeanClass.getBean(response,SelectAddressBean.class);
                                response.close();
                                if (selectAddressBean.getCode()==1){
                                    finish();
                                }else if(selectAddressBean.getCode()==-1){
                                    UiUtils.runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            hint.setVisibility(View.VISIBLE);
                                        }
                                    });
                                    TimerTask timerTask=new TimerTask() {
                                        @Override
                                        public void run() {
                                            UiUtils.runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    hint.setVisibility(View.GONE);
                                                }
                                            });
                                        }
                                    };
                                    Timer timer=new Timer();
                                    timer.schedule(timerTask,3000);
                                }
                            }
                        });
                    }
                }else {
                    Toast.makeText(this,"请完善您的信息",Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.edit_delete_address_button:
                dialog();//删除地址弹出确认对话框
                break;
            case R.id.address_go_back:
                finish();
                break;
        }
    }
    /**
     *  删除地址时弹出dialog
     */
    private void dialog(){

        LayoutInflater inflater= this.getLayoutInflater();
        View view=inflater.inflate(R.layout.commodity_delete_dialog,null);
        final Dialog dialog=new Dialog(this,R.style.Dialog);
        dialog.setCancelable(false);
        dialog.setContentView(view);
        TextView sure= (TextView) view.findViewById(R.id.commodity_dialog_sure);
        TextView cancel= (TextView) view.findViewById(R.id.commodity_dialog_cancel);
        TextView hint= (TextView) view.findViewById(R.id.dialog_hint);
        hint.setText("确认删除该地址么？");
        sure.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                RequestBody formBody=new FormBody.Builder()
                        .add("uid",uid)
                        .add("id",addressID)
                        .build();
                Request request=new Request.Builder()
                        .url(Urls.DEL_LINK_MAN)
                        .post(formBody)
                        .build();
                client.newCall(request).enqueue(new Callback() {
                    @Override
                    public void onFailure(Call call, IOException e) {
                        UiUtils.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(EditAddressActivity.this,"网络请求失败！",Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                    @Override
                    public void onResponse(Call call, Response response) throws IOException {
                        if(!response.isSuccessful()) throw new IOException("Unexpected code"+response);
                        SelectAddressBean selectAddressBean = (SelectAddressBean) GetBeanClass.getBean(response,SelectAddressBean.class);
                        response.close();
                        if (selectAddressBean.getCode()==1){
                            String addrid= SPUtils.getStringValue(MyApplication.getContext(), Contants.SP_NAME,"addressId","0");
                            if (addrid.equals(addressID)){
                                SPUtils.putStringValue(MyApplication.getContext(), Contants.SP_NAME,"addressId","0");
                            }
                            finish();
                        }
                    }
                });
                dialog.dismiss();
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.show();
        Window dialogWindow = dialog.getWindow();
        WindowManager m = this.getWindowManager();
        Display d = m.getDefaultDisplay(); // 获取屏幕宽、高度
        WindowManager.LayoutParams p = dialogWindow.getAttributes(); // 获取对话框当前的参数值
        //p.height = (int) (d.getHeight() * 0.5); // 高度设置为屏幕的0.6，根据实际情况调整
        p.width = (int) (d.getWidth() * 0.6); // 宽度设置为屏幕的0.6，根据实际情况调整
        dialogWindow.setAttributes(p);
    }
}
