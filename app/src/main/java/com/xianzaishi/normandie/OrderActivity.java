package com.xianzaishi.normandie;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.alipay.sdk.app.PayTask;
import com.google.gson.Gson;
import com.tencent.mm.sdk.modelpay.PayReq;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.WXAPIFactory;
import com.umeng.analytics.MobclickAgent;
import com.xianzaishi.normandie.bean.CanBuyBean;
import com.xianzaishi.normandie.bean.CouponsBean;
import com.xianzaishi.normandie.bean.CreditBean;
import com.xianzaishi.normandie.bean.DefaultAddress;
import com.xianzaishi.normandie.bean.GoPayBean;
import com.xianzaishi.normandie.bean.OrderActivityBean;
import com.xianzaishi.normandie.bean.SelectAddressBean;
import com.xianzaishi.normandie.bean.SystemTimeBean;
import com.xianzaishi.normandie.bean.WXPayBean;
import com.xianzaishi.normandie.common.Urls;
import com.xianzaishi.normandie.global.Contants;
import com.xianzaishi.normandie.pay.PayResult;
import com.xianzaishi.normandie.utils.AliPayResultMap;
import com.xianzaishi.normandie.utils.DataCalcUtils;
import com.xianzaishi.normandie.utils.GetBeanClass;
import com.xianzaishi.normandie.utils.GetDistributionTime;
import com.xianzaishi.normandie.utils.GlideUtils;
import com.xianzaishi.normandie.utils.SPUtils;
import com.xianzaishi.normandie.utils.ToastUtils;
import com.xianzaishi.normandie.utils.UiUtils;
import com.xianzaishi.normandie.view.OptionsPickerView;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;



/**
 * author linghuchong 2016-8-18
 * editor shenlang 2016-9-1
 */
public class OrderActivity extends Activity {

    private ImageView ivFinish;
    private LinearLayout rlInputAddress;
    private LinearLayout rlUpdateAddress;
    private ImageView ivAddressNext;
    private TextView tvOrderName;
    private TextView tvPhoneNamber;
    private TextView tvOrderAddress;
    private RelativeLayout rlOne;
    private ImageView ivOneIcon;
    private TextView tvOneTitle;
    private TextView tvOnePrice;
    private TextView tvOneCount;
    private TextView tvOrderCount;
    private RelativeLayout rlCommdityMore;
    private ImageView ivCommNext;
    private RelativeLayout rlAgio;
    private RelativeLayout rlHopeTime;
    private EditText etRemark;
    private TextView tvAllPrice;
    private TextView tvMailPrice;
    private TextView tvToken,tvTokenText;
    private TextView tvShouldPay;
    private TextView tvOrderGotoPay;
    private RelativeLayout rlAliPay;
    private RelativeLayout rlWeChat;
    private ImageView ivAliPayState;
    private ImageView ivWeiPayState;
    private ImageView ivOrder2;
    private ImageView ivOrder1;
    private ImageView ivOrder3;
    private ImageView ivOrder4;
    private final int UPDATE_ADDRESS_CODE = 1;
    private final int ADD_ADDRESS_CODE = 2;
    private final int GET_COUPONS_CODE = 3;
    private double shouldPay;
    private Double allPrice;
    private Double mailPrice;
    private Double coupon;
    private Double creditPrice=0.0;
    private OkHttpClient client=MyApplication.getOkHttpClient();
    private boolean isHaveAddress;
    private String skuIds;
    private Intent intent;
    private MyHandler handler;
    private String sciId,uid,oid;
    private static final int SDK_PAY_FLAG = 2;//支付宝支付后返回结果的标识
    private IWXAPI api;//微信支付
    private AlertDialog dialog;
    private NumberPicker day,hour;
    private TextView receiptTime;
    private LinearLayout creditContainer;//显示抵用积分的容器
    private RelativeLayout couponsContainer;//显示优惠券的容器
    private TextView creditText,credit;//积分
    private CheckBox creditBox;//是否使用积分的checkBox
    private int credits;
    private String addressId;//收货地址ID
    private String couponId="0";//优惠券ID
    private String resultPay;
    private TextView couponsCount;
    private String distributionTime="";
    private ArrayList<String> dayArray = new ArrayList<>();//时间选择天
    private ArrayList<ArrayList<String>> hoursArray = new ArrayList<>();//时间选择小时段
    private OptionsPickerView pvOptions;
    private String buySkuDetailJson;
    private OrderActivityBean.DataBean dataBean;
    private boolean isNetWorkOver;
    private ArrayList<OrderActivityBean.DataBean.ObjectsBean> dataList;
    private LinearLayout discountContainer;
    private TextView discountText;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order);
        initWXapi();//初始化微信支付
        initView();
        //获取服务器时间
        getTimeFromServer();
        //判断用户是否留有收货地址
        isNeedInputAddress();
    }
    /**
     *  从服务器获取时间戳
     */
    private void getTimeFromServer() {
        Request request=new Request.Builder()
                .url(Urls.GET_SYSTEM_TIME)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

            }
            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (response.isSuccessful()){
                    SystemTimeBean timeBean= (SystemTimeBean) GetBeanClass.getBean(response,SystemTimeBean.class);
                    if (timeBean!=null&&timeBean.isSuccess()){
                        long currentTime=timeBean.getModule();
                        dayArray= GetDistributionTime.getTimeRange(9,20,currentTime, GetDistributionTime.TYPE.DAY);
                        hoursArray=GetDistributionTime.getTimeRange(9,20,currentTime, GetDistributionTime.TYPE.HOURS);
                        UiUtils.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if(dayArray.get(0).equals("今天")){
                                    if (hoursArray.get(0).get(0).equals("尽快送达")){
                                        receiptTime.setText("尽快送达");
                                    }else {
                                        receiptTime.setText("预计"+dayArray.get(0)+hoursArray.get(0).get(0)+"送达");
                                    }
                                }else{
                                    receiptTime.setText("预计"+dayArray.get(0)+hoursArray.get(0).get(0)+"送达");
                                }

                                //期望收货时间
                                rlHopeTime.setOnClickListener( new MyClickLinstener());
                                distributionTime=getDistributionTime();//获取期望送达时间戳
                            }
                        });
                    }
                }
            }
        });
    }
    /**
     *  初始化可用优惠券数量
     * @param commodityPrice
     */
    private void initCouponsCount(String commodityPrice) {
        String token= SPUtils.getStringValue(MyApplication.getContext(), Contants.SP_NAME,Contants.TOKEN,"");
        couponsCount.setVisibility(View.INVISIBLE);
        FormBody formBody=new FormBody.Builder()
                .add("token",token)
                .add("queryType","1")
                .add("payType","1")
                .add("buySkuDetailJson",buySkuDetailJson)
                .build();
        Request request=new Request.Builder()
                .url(Urls.GET_COUPONS)
                .post(formBody)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
            }
            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if(!response.isSuccessful()) throw new IOException("Unexpected code"+response);
                final CouponsBean couponsBean= (CouponsBean) GetBeanClass.getBean(response,CouponsBean.class);
                response.close();
                if (couponsBean!=null&&couponsBean.getCode()==1&&couponsBean.getModule()!=null){
                    UiUtils.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            String count=String.valueOf(couponsBean.getModule().size());
                            couponsCount.setVisibility(View.VISIBLE);
                            couponsCount.setText(count);
                        }
                    });
                }else {
                    UiUtils.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            couponsCount.setVisibility(View.VISIBLE);
                            couponsCount.setText("0");
                        }
                    });
                }
            }
        });
    }


    private void initWXapi() {
        api= WXAPIFactory.createWXAPI(this, Contants.APP_ID);
        api.registerApp(Contants.APP_ID);//注册APP到微信
    }

    //更新应该付多少钱
    private void updateShouldPayPrice(){
        shouldPay = DataCalcUtils.sub(dataBean.getTotalPrice(),(DataCalcUtils.add(coupon, creditPrice)));
        shouldPay=shouldPay<=0?0.00:shouldPay;
        //只显示小数点后2位
        resultPay = String.format("%.2f", shouldPay);
        String commodityPrice=String.valueOf(allPrice);
        initCouponsCount(commodityPrice);//初始化优惠券的信息
        tvShouldPay.setText(resultPay);
    }

    //根据本地存储的一些配置信息初始化控件状态
    private void initViewState() {//
        boolean isAliPay = SPUtils.getBooleanValue(MyApplication.getContext(),Contants.SP_NAME,"isAliPay",true);
        if (isAliPay) {
            ivAliPayState.setImageResource(R.mipmap.checked2x);
            ivWeiPayState.setImageResource(R.mipmap.check2x);
        } else {
            ivAliPayState.setImageResource(R.mipmap.check2x);
            ivWeiPayState.setImageResource(R.mipmap.checked2x);
        }
        //商品总价
        allPrice = dataBean.getCurrPrice();
        String allPrices = String.valueOf(allPrice);

        //总价
        tvAllPrice.setText(allPrices);

        mailPrice=dataBean.getFreight();
        //运费
        tvMailPrice.setText(String.valueOf(mailPrice));
        //代金券
        coupon = Double.valueOf(tvToken.getText().toString().trim());
        /**
         *  验证积分该用多少
         */
        Double shouldPay1 = DataCalcUtils.sub(dataBean.getTotalPrice(),coupon);
        shouldPay1=shouldPay1<=0?0.00:shouldPay1;
        //只显示小数点后2位
        String result = String.format("%.2f", shouldPay1);
        if(shouldPay1<((double)credits)/((double) 100)){
            creditText.setText("可用"+shouldPay1*100+"积分抵¥"+result);
            credits= (int) (shouldPay1*100);
        }else {
            creditText.setText("可用"+credits+"积分抵¥"+String.format("%.2f",(((double)credits))/((double) 100)));
        }
        if (creditBox.isChecked()){
            creditContainer.setVisibility(View.VISIBLE);
            credit.setText(String.format("%.2f",(((double)credits))/((double) 100)));
            //积分折现值
            creditPrice=Double.valueOf(credit.getText().toString());
            }else {
                creditContainer.setVisibility(View.GONE);
                creditPrice=0.0;
            }

        updateShouldPayPrice();
    }

    @Override
    protected void onResume() {
        super.onResume();
        //初始化支付状态
        if (isNetWorkOver){
            initViewState();
        }
        addressId=SPUtils.getStringValue(MyApplication.getContext(),Contants.SP_NAME,"addressId","0");
        if("0".equals(addressId)){
            isNeedInputAddress();
        }
        MobclickAgent.onResume(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
    }

    //判断用户是否存有收货信息
    public void isNeedInputAddress() {
        //获取本地是否存有收货人地址信息
        RequestBody formBody=new FormBody.Builder()
                .add("uid",uid)
                .build();
        Request request=new Request.Builder()
                .url(Urls.GET_DEFAULT_ADDRESS)
                .post(formBody)
                .build();//默认收货地址
        final Request request1=new Request.Builder()
                .url(Urls.GET_ALL_ADDRESS)
                .post(formBody)
                .build();//全部收货地址
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                UiUtils.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(OrderActivity.this,"网络请求失败！",Toast.LENGTH_SHORT).show();
                    }
                });
            }
            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if(!response.isSuccessful()) throw new IOException("Unexpected code"+response);
                final DefaultAddress defaultAddress = (DefaultAddress) GetBeanClass.getBean(response,DefaultAddress.class);
                response.close();
                UiUtils.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (defaultAddress.getData()==null){
                            client.newCall(request1).enqueue(new Callback() {
                                @Override
                                public void onFailure(Call call, IOException e) {
                                    UiUtils.runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(OrderActivity.this,"网络请求失败！",Toast.LENGTH_SHORT).show();
                                        }
                                    });
                                }
                                @Override
                                public void onResponse(Call call, Response response) throws IOException {
                                    if(!response.isSuccessful()) throw new IOException("Unexpected code"+response);
                                    final SelectAddressBean selectAddressBean1 = (SelectAddressBean) GetBeanClass.getBean(response,SelectAddressBean.class);
                                    response.close();
                                    UiUtils.runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            if (selectAddressBean1.getData()==null){
                                                //没有收货人信息提示用户编辑收货人信息
                                                rlInputAddress.setVisibility(View.VISIBLE);
                                                rlUpdateAddress.setVisibility(View.GONE);
                                                isHaveAddress=false;
                                            }else {
                                                //没有默认地址但有收货人信息提示用户编辑收货人信息
                                                rlInputAddress.setVisibility(View.VISIBLE);
                                                rlUpdateAddress.setVisibility(View.GONE);
                                                isHaveAddress=true;
                                            }
                                        }
                                    });
                                }
                            });
                        }else{
                            //有收货人信息显示收货人信息,隐藏编辑收货人信息
                            DefaultAddress.DataBean dataBean=defaultAddress.getData();
                            tvOrderName.setText(dataBean.getName());
                            tvPhoneNamber.setText(dataBean.getPhone());
                            tvOrderAddress.setText(dataBean.getLevel2Name()+dataBean.getLevel3Name()
                            +dataBean.getLevel4Name()+dataBean.getAddress());
                            SPUtils.putStringValue(MyApplication.getContext(),Contants.SP_NAME,"addressId",String.valueOf(dataBean.getId()));
                            addressId=String.valueOf(dataBean.getId());
                            rlUpdateAddress.setVisibility(View.VISIBLE);
                            rlInputAddress.setVisibility(View.GONE);
                        }
                    }
                });

            }
        });
    }

    //判断购物车商品列表
    private void commIsOne() {
        //获取购物车商品总类别
        long shoppingCarCount = dataList.size();
        //此处要做数据校验，不能小于1，需谨慎
        if (shoppingCarCount == 1) {
            //只有一种类别
            rlOne.setVisibility(View.VISIBLE);
            rlCommdityMore.setVisibility(View.GONE);
            OrderActivityBean.DataBean.ObjectsBean dataBean=dataList.get(0);
            GlideUtils.LoadImage(this,dataBean.getItemIconUrl(),ivOneIcon);
            tvOneTitle.setText(dataBean.getItemName());
            tvOnePrice.setText(dataBean.getItemEffePrice()+"");
            tvOneCount.setText(dataBean.getItemCount()+"");
        } else {
            //商品有多种，
            rlCommdityMore.setVisibility(View.VISIBLE);
            rlOne.setVisibility(View.GONE);
            tvOrderCount.setText("共"+dataList.size()+"种");
            //判断显示几个图标
            if (shoppingCarCount == 2) {
                //有两种类型，显示这两种类型的商品图标
                GlideUtils.LoadImage(this,dataList.get(0).getItemIconUrl(),ivOrder1);
                GlideUtils.LoadImage(this,dataList.get(1).getItemIconUrl(),ivOrder2);
                ivOrder3.setVisibility(View.GONE);
                ivOrder4.setVisibility(View.GONE);
            } else if (shoppingCarCount == 3) {
                //有三种类型，显示这三种类型的商品图标
                ivOrder4.setVisibility(View.GONE);
                GlideUtils.LoadImage(this,dataList.get(0).getItemIconUrl(),ivOrder1);
                GlideUtils.LoadImage(this,dataList.get(1).getItemIconUrl(),ivOrder2);
                GlideUtils.LoadImage(this,dataList.get(2).getItemIconUrl(),ivOrder3);
            } else if (shoppingCarCount > 3) {
                //三种类型以上就只显示四个，默认显示前四种类型的商品图图表
                GlideUtils.LoadImage(this,dataList.get(0).getItemIconUrl(),ivOrder1);
                GlideUtils.LoadImage(this,dataList.get(1).getItemIconUrl(),ivOrder2);
                GlideUtils.LoadImage(this,dataList.get(2).getItemIconUrl(),ivOrder3);
                GlideUtils.LoadImage(this,dataList.get(3).getItemIconUrl(),ivOrder4);
            }
        }

    }

    private void initView() {
        uid=SPUtils.getStringValue(MyApplication.getContext(), Contants.SP_NAME,Contants.UID,null);//获取uid
        View view=findViewById(R.id.order_head);
        TextView title= (TextView) view.findViewById(R.id.text_title);
        title.setText("订单结算");
        pvOptions=new OptionsPickerView(this);
        creditContainer= (LinearLayout) findViewById(R.id.order_credit_container);//抵用积分的容器
        couponsContainer= (RelativeLayout) findViewById(R.id.order_coupons_container);
        creditText= (TextView) findViewById(R.id.order_credit_text);//显示抵用积分有几多可用
        creditBox= (CheckBox) findViewById(R.id.order_credit_box);//是否选用积分抵现
        credit= (TextView) findViewById(R.id.order_credit);//选择的抵用积分textView
        receiptTime= (TextView) findViewById(R.id.tv_hope_time);//期望送达时间

        discountContainer= (LinearLayout) findViewById(R.id.discount_container);//优惠信息的容器 用于整体隐藏
        discountText= (TextView) findViewById(R.id.discount_text);//优惠详情
        couponsCount= (TextView) findViewById(R.id.iv_agio);//优惠券数量
        ivFinish = (ImageView)view.findViewById(R.id.address_go_back);
        rlInputAddress = (LinearLayout) findViewById(R.id.rl_input_address);//没有收货人信息的时候显示
        rlUpdateAddress = (LinearLayout) findViewById(R.id.rlUpdateAddress);//有收货人信息的时候显示
        ivAddressNext = (ImageView) findViewById(R.id.ivAddressNext);
        tvOrderName = (TextView) findViewById(R.id.tvOrderName);//有收货人地址时-->收货人姓名
        tvPhoneNamber = (TextView) findViewById(R.id.tvPhoneNamber); //收货人电话
        tvOrderAddress = (TextView) findViewById(R.id.tvOrderAddress);//收货地址
        rlOne = (RelativeLayout) findViewById(R.id.rlOne); //购物车只有一种类型的时候显示
        ivOneIcon = (ImageView) findViewById(R.id.ivOneIcon);//只有一种商品   商品 图标
        tvOneTitle = (TextView) findViewById(R.id.tvOneTitle);//商品名称
        tvOnePrice = (TextView) findViewById(R.id.tvOnePrice);//商品价格
        tvOneCount = (TextView) findViewById(R.id.tvOneCount);//商品个数
        rlCommdityMore = (RelativeLayout) findViewById(R.id.rlCommdityMore);//订单中有多种商品时候显示
        //多种商品时第一个商品图标
        ivOrder1 = (ImageView) findViewById(R.id.ivOrder1);
        //多种商品时第二个商品图标
        ivOrder2 = (ImageView) findViewById(R.id.ivOrder2);
        //多种商品时第三个商品图标
        ivOrder3 = (ImageView) findViewById(R.id.ivOrder3);
        //多种商品时第四个商品图标
        ivOrder4 = (ImageView) findViewById(R.id.ivOrder4);
        //多种商品时显示商品数量
        tvOrderCount= (TextView) findViewById(R.id.tv_more_count);
        ivCommNext = (ImageView) findViewById(R.id.ivCommNext);
        rlAgio = (RelativeLayout) findViewById(R.id.rlAgio);//选择折扣优惠
        //期望送货时间
        rlHopeTime = (RelativeLayout) findViewById(R.id.rlHopeTime);
        //用户反馈、、备注
        etRemark = (EditText) findViewById(R.id.etRemark);
        //商品总价
        tvAllPrice = (TextView) findViewById(R.id.tvAllPrice);
        //运费
        tvMailPrice = (TextView) findViewById(R.id.tvMailPrice);
        //代金券
        tvToken = (TextView) findViewById(R.id.tvToken);
        //代金券
        tvTokenText= (TextView) findViewById(R.id.tvTokenText);
        //应付多少钱
        tvShouldPay = (TextView) findViewById(R.id.tvShouldPay);
        //去支付
        tvOrderGotoPay = (TextView) findViewById(R.id.tvOrderGotoPay);
        //支付宝支付
        rlAliPay = (RelativeLayout) findViewById(R.id.rlAliPay);
        ivAliPayState = (ImageView) findViewById(R.id.ivAliPayState);
        //微信支付
        rlWeChat = (RelativeLayout) findViewById(R.id.rlWeChat);
        ivWeiPayState = (ImageView) findViewById(R.id.ivWeiPayState);

        //获取购物车选中的商品对象集合以及总价
        intent = getIntent();
        credits=intent.getIntExtra("credit",-1);
        skuIds = intent.getStringExtra("skuIds");

        /**
         *  通过skuIds请求网络获取商品详情及优惠信息
         */
        getGoodsBySkuId(skuIds);

        /**
         *  实例化handler
         */
        handler=new MyHandler(OrderActivity.this);
    }
    /**
     *  通过skuIds请求网络获取商品详情及优惠信息
     */
    private void getGoodsBySkuId(String skuIds) {
        String token=SPUtils.getStringValue(MyApplication.getContext(),Contants.SP_NAME,Contants.TOKEN,null);
        FormBody form=new FormBody.Builder()
                .add("token",token)
                .add("skuIds",skuIds)
                .build();
        Request request=new Request.Builder()
                .post(form)
                .url(Urls.GET_GOODS_BY_SKUIDS)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

            }
            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if(!response.isSuccessful()){
                    response.close();
                    return;
                }
                OrderActivityBean oBean= (OrderActivityBean) GetBeanClass.getBean(response,OrderActivityBean.class);
                if (oBean==null||oBean.getCode()!=1){
                    return;
                }
                dataBean=oBean.getData();
                dataList= (ArrayList<OrderActivityBean.DataBean.ObjectsBean>) dataBean.getObjects();
                UiUtils.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        isNetWorkOver=true;
                        dealWithTheData();
                    }
                });
            }
        });
    }

    /**
     *  对详细的商品信息做操作
     */
    private void dealWithTheData() {
        /**
         *  获取商品条目ID字符串
         */
        StringBuilder skuDetail=new StringBuilder("{");
        int size=dataBean.getObjects().size();
        long[] sciIdArray=new long[size];
        int j=0;
        for(OrderActivityBean.DataBean.ObjectsBean bean:dataBean.getObjects()){
            sciIdArray[j++]=bean.getId();
            skuDetail.append("\"").append(bean.getSkuId()).append("\":\"").append((int)(bean.getItemCount())).append("\",");
        }
        skuDetail.replace(skuDetail.length()-1,skuDetail.length(),"}");
        buySkuDetailJson=skuDetail.toString().trim();

        String sciId1= Arrays.toString(sciIdArray);
        sciId=sciId1.substring(1,sciId1.length()-1);
        /**
         * 初始化优惠展示信息
         */
        List<OrderActivityBean.DataBean.DiscountInfosBean> discList=dataBean.getDiscountInfos();
        StringBuilder discStr=new StringBuilder();
        if (discList.size()==0){
            discountContainer.setVisibility(View.GONE);
        }else {
            for (OrderActivityBean.DataBean.DiscountInfosBean bean:discList){
                if (bean.getDiscountDesc()!=null) {
                    discStr.append(bean.getDiscountDesc()).append("\n");
                }
            }
            String dis=discStr.substring(0,discStr.length()-1);
            discountText.setText(dis);
        }

        initViewState();//根据订单信息初始化控件状态
        commIsOne();//判断购物车商品是否只有一类
        setOnClick();//初始化点击事件
    }

    private void setOnClick() {
        MyClickLinstener linstener = new MyClickLinstener();
        //左上角关闭
        ivFinish.setOnClickListener(linstener);
        //点击此处添加收货信息
        rlInputAddress.setOnClickListener(linstener);
        //点击修改地址
        rlUpdateAddress.setOnClickListener(linstener);
        //点击修改收货人
        ivAddressNext.setOnClickListener(linstener);
        //点击跳转到选择折扣
        rlAgio.setOnClickListener(linstener);
        //去结算
        tvOrderGotoPay.setOnClickListener(linstener);

        //跳到订单商品列表
        //ivCommNext.setOnClickListener(linstener);
        //一种商品类别跳转到商品列表
        rlOne.setOnClickListener(linstener);
        //商品类别多于一种，跳转至商品列表
        rlCommdityMore.setOnClickListener(linstener);
        //阿里支付，点击取消微信支付并把结果写到sp
        rlAliPay.setOnClickListener(linstener);
        //微信支付，点击取消阿里支付并把结果写到sp
        rlWeChat.setOnClickListener(linstener);
        creditBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b){
                    creditContainer.setVisibility(View.VISIBLE);
                    credit.setText(String.format("%.2f",(((double)credits))/((double) 100)));
                    //积分折现值
                    creditPrice=Double.valueOf(credit.getText().toString());
                    updateShouldPayPrice();
                }else {
                    creditContainer.setVisibility(View.GONE);
                    creditPrice=0.0;
                    updateShouldPayPrice();
                }
            }
        });
    }



    class MyClickLinstener implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.address_go_back:
                    finish();
                    break;
                case R.id.rlUpdateAddress://用户有收货地址，点击跳转到选择收货地址页面
                    Intent intent=new Intent(MyApplication.getContext(), AddressActivity.class);
                    intent.putExtra("callback",1);//传递一个code过去目标页面，标识目标页面是需要回调结果过来的
                    startActivityForResult(intent, UPDATE_ADDRESS_CODE);
                    break;
                case R.id.rl_input_address://用户没有有效的收货地址
                    if(!isHaveAddress){
                        Intent intent1=new Intent(MyApplication.getContext(), AddAddressActivity.class);
                        intent1.putExtra("callback",1);
                        startActivityForResult(intent1, ADD_ADDRESS_CODE);
                    }else {
                        Intent intent2=new Intent(MyApplication.getContext(), AddressActivity.class);
                        intent2.putExtra("callback",1);
                        startActivityForResult(intent2, UPDATE_ADDRESS_CODE);
                    }
                    break;

                case R.id.ivAddressNext://跳转到选择收货人界面
                    startActivityForResult(new Intent(MyApplication.getContext(), AddAddressActivity.class), UPDATE_ADDRESS_CODE);
                    break;
                /*case R.id.ivCommNext://跳转到订单详情列表
                    startActivity(new Intent(MyApplication.getContext(), OrderCommdityActivity.class));
                    break;*/
                case R.id.rlAgio://跳转到 打折界面
                    Intent intent2=new Intent(MyApplication.getContext(), CouponsActivity.class);
                    intent2.putExtra("callback",1);
                    intent2.putExtra("buySkuDetailJson",buySkuDetailJson);
                    startActivityForResult(intent2,GET_COUPONS_CODE);
                    break;
                case R.id.rlHopeTime://选收货时间
                    dialog();
                    break;
                case R.id.tvOrderGotoPay:
                    /**
                     * 点击订单结算页的去结算，需要做的判断有
                     *  1、用户是否写了收货地址
                     *  2、用户是否选了付款方式
                     *  3、是否选择期望收货时间
                     *  4、是微信支付还是阿里支付
                     *  未完成
                     */
                    MobclickAgent.onEvent(OrderActivity.this,"upOrder");

                    if("0".equals(addressId)){
                        Toast.makeText(OrderActivity.this,"请填写收货地址",Toast.LENGTH_SHORT).show();
                    }else {
                        canBuy();
                        tvOrderGotoPay.setEnabled(false);
                    }
                    break;
                case R.id.rlOne://购物车只有一种类型，点击跳到商品详情
                    break;
                case R.id.rlCommdityMore://购物车有多种类型，点击跳到订单列表
                    Intent intent1=new Intent(MyApplication.getContext(), OrderCommdityActivity.class);
                    intent1.putExtra("list",dataList);
                    startActivity(intent1);
                    break;
                case R.id.rlAliPay://阿里支付
                    //默认选中阿里支付，不选中微信支付，点击效果，取消微信选中，并把结果存在本地
                    SPUtils.putBooleanValue(MyApplication.getContext(),Contants.SP_NAME,"isAliPay",true);
                    //更新组件状态
                    initViewState();
                    break;
                case R.id.rlWeChat:
                    //点击微信支付，取消阿里支付选中状态
                    SPUtils.putBooleanValue(MyApplication.getContext(),Contants.SP_NAME,"isAliPay",false);
                    initViewState();
                    break;
            }
        }
    }

    private void canBuy() {
        Request request=new Request.Builder()
                .url(Urls.CAN_BUY)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String json=response.body().string().replace("\"","");
                Gson gson=new Gson();
                final CanBuyBean canbean=gson.fromJson(json,CanBuyBean.class);
                UiUtils.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (canbean.isClose()){
                            ToastUtils.showToast("即将启幕，敬请期待！");
                            tvOrderGotoPay.setEnabled(true);
                        }else {
                            getOrderInfo();
                        }
                    }
                });

            }
        });
    }

    /**
     * 期望送达时间
     */
    private void dialog() {
        /*dayArray= GetDistributionTime.getTimeRange(8,20, GetDistributionTime.TYPE.DAY);
        hoursArray=GetDistributionTime.getTimeRange(8,20, GetDistributionTime.TYPE.HOURS);*/
        //二级联动效果
        pvOptions.setPicker(dayArray, hoursArray, null, true);
        pvOptions.setCyclic(false, false,true);
        pvOptions.setSelectOptions(0,0);
        pvOptions.setOnoptionsSelectListener(new OptionsPickerView.OnOptionsSelectListener() {
            @Override
            public void onOptionsSelect(int options1, int option2, int options3) {
                if(dayArray.get(options1).equals("今天")){
                    if (hoursArray.get(options1).get(option2).equals("尽快送达")){
                        receiptTime.setText("尽快送达");
                    }else {
                        receiptTime.setText("预计"+dayArray.get(options1)+hoursArray.get(options1).get(option2)+"送达");
                    }
                }else{
                    receiptTime.setText("预计"+dayArray.get(options1)+hoursArray.get(options1).get(option2)+"送达");
                }
                distributionTime=getDistributionTime();//获取期望送达时间戳
            }
        });
        pvOptions.show();
    }


    /**
     *  为提交订单生成时间参数
     */
    private String getDistributionTime(){
        Calendar cal=Calendar.getInstance();
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.MILLISECOND, 0);
        long millis=cal.getTimeInMillis();
        String isToday=receiptTime.getText().toString().substring(2,4);
        if (isToday.equals("送达")){
            return "";
        }else {
            String time=receiptTime.getText().toString().substring(4,9).replace(":","");
            if(isToday.equals("今天")){
                String date=getStringDate(millis).replace("-","");
                return date+time+"00";
            }else {
                String date=getStringDate(millis+24*60*60*1000).replace("-","");
                return date+time+"00";
            }
        }
    }

    /**
     * 将长时间格式字符串转换为时间 yyyy-MM-dd
     *
     */
    public static String getStringDate(Long date) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd", Locale.CHINA);
        return formatter.format(date);
    }
    /**
     * 将长时间格式字符串转换为时间 yyyyMMddkkmmss
     *
     */
    public static String getDistributionDate(Long date) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddkkmmss", Locale.CHINA);
        return formatter.format(date);
    }
    //访问服务器获取订单号
    private void getOrderInfo() {
        //uid=%s";//提交订单去结算&sciId=150&sciId=149&sciId=148&couponId=1&adderssId=87"

        String remark=etRemark.getText().toString().trim();
        String credit=String.valueOf((int)(creditPrice*100));
        RequestBody formBody=new FormBody.Builder()
                .add("uid",uid)
                .add("sciId",sciId)
                .add("distribution",distributionTime)
                .add("credit",credit)
                .add("couponId",couponId)
                .add("adderssId",addressId)
                .add("description",remark)
                .build();
        Request request=new Request.Builder()
                .url(Urls.TO_ORDER)
                .post(formBody)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
            }
            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if(!response.isSuccessful()) throw new IOException("Unexpected code"+response);

                final GoPayBean goPayBean= (GoPayBean) GetBeanClass.getBean(response,GoPayBean.class);
                response.close();
                if(goPayBean.getCode()==1){
                    oid=goPayBean.getData();
                    getOrderStatus(oid);//获取订单支付状态
                }else {
                    UiUtils.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            tvOrderGotoPay.setEnabled(true);
                            ToastUtils.showToast(goPayBean.getMessage());
                        }
                    });
                }
            }
        });
    }

    /**
     *  获取订单支付状态
     */
    private void getOrderStatus(final String oid) {
        RequestBody formBody=new FormBody.Builder()
                .add("uid",uid)
                .add("oid",oid)
                .build();
        Request request=new Request.Builder()
                .url(Urls.GET_ORDER_STATUS)
                .post(formBody)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if(!response.isSuccessful()) throw new IOException("Unexpected code"+response);
                final CreditBean creditBean= (CreditBean) GetBeanClass.getBean(response,CreditBean.class);
                response.close();
                if(creditBean.isSuccess()){
                    if (creditBean.getData()==2){
                        handler.sendEmptyMessage(1);
                    }else{
                        Intent intent=new Intent(OrderActivity.this,AliPayResultActivity.class);
                        intent.putExtra("aliResult",oid);
                        //Log.e("sss","=="+oid);
                        intent.putExtra("pay","0.00");
                        intent.putExtra("time",OrderActivity.this.distributionTime);
                        intent.putExtra("channel","积分支付");
                        OrderActivity.this.startActivity(intent);
                        finish();
                    }
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        //super.onActivityResult(requestCode, resultCode, data);
        if(data!=null) {
            switch (requestCode) {
                case ADD_ADDRESS_CODE://添加收货地址界面，需要拿到收货地址信息
                    rlUpdateAddress.setVisibility(View.VISIBLE);
                    rlInputAddress.setVisibility(View.GONE);
                    tvOrderName.setText(data.getStringExtra("name"));
                    tvPhoneNamber.setText(data.getStringExtra("phone"));
                    tvOrderAddress.setText(data.getStringExtra("address"));
                    //addressId=data.getStringExtra("addressId");
                    break;
                case UPDATE_ADDRESS_CODE://选择收货地址界面，需要拿到收货地质信息
                    rlUpdateAddress.setVisibility(View.VISIBLE);
                    rlInputAddress.setVisibility(View.GONE);
                    tvOrderName.setText(data.getStringExtra("name"));
                    tvPhoneNamber.setText(data.getStringExtra("phone"));
                    tvOrderAddress.setText(data.getStringExtra("address"));
                    //addressId=data.getStringExtra("addressId");
                    break;
                case GET_COUPONS_CODE://优惠券页面，需要拿到优惠卷信息
                    couponId=data.getStringExtra("couponsId").trim();
                    double couponsPrice=data.getDoubleExtra("couponsPrice",0.0);
                    String couponsTitle=data.getStringExtra("couponsTitle");
                    String result = String.format("%.2f", couponsPrice);
                    //代金券
                    tvToken.setText(result);
                    tvTokenText.setText(couponsTitle);
                    initViewState();
                    couponsContainer.setVisibility(View.VISIBLE);
                    break;
            }
        }
    }

    /**
     *  通过handler保证异步请求服务器的结果可以同步
     */
    static class MyHandler extends Handler{
        WeakReference<OrderActivity> weakReference;
        public MyHandler(OrderActivity orderActivity){
            weakReference=new WeakReference<OrderActivity>(orderActivity);
        }

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            OrderActivity orderActivity=weakReference.get();
            if(orderActivity!=null){
                switch (msg.what){
                    case 1:
                        boolean isAliPay=SPUtils.getBooleanValue(MyApplication.getContext(),Contants.SP_NAME,"isAliPay",true);
                        if(isAliPay){
                            //使用阿里支付
                            toAliPay(orderActivity);
                        }else{
                            //使用微信支付
                            toWXPay(orderActivity);
                        }
                        break;
                    case SDK_PAY_FLAG:
                        PayResult payResult = new PayResult((String) msg.obj);
                        /**
                         * 同步返回的结果必须放置到服务端进行验证（验证的规则请看https://doc.open.alipay.com/doc2/
                         * detail.htm?spm=0.0.0.0.xdvAU6&treeId=59&articleId=103665&
                         * docType=1) 建议商户依赖异步通知
                         */
                        String resultInfo = payResult.getResult();// 同步返回需要验证的信息
                        HashMap<String,String> map=AliPayResultMap.getResultMap(resultInfo);
                        String out_trade_no=map.get("out_trade_no");

                        String resultStatus = payResult.getResultStatus();
                        // 判断resultStatus 为“9000”则代表支付成功，具体状态码代表含义可参考接口文档
                        if (TextUtils.equals(resultStatus, "9000")&&map.get("success").equals("\"true\"")) {
                            String tradeNum=out_trade_no.substring(1,out_trade_no.length()-1);//截取的可能有问题 待定
                            Intent intent=new Intent(orderActivity,AliPayResultActivity.class);
                            intent.putExtra("aliResult",orderActivity.oid);
                            intent.putExtra("pay",orderActivity.resultPay);
                            intent.putExtra("time",orderActivity.distributionTime);
                            intent.putExtra("channel","支付宝");
                            orderActivity.startActivity(intent);
                            orderActivity.finish();
                        } else {
                            // 判断resultStatus 为非"9000"则代表可能支付失败
                            // "8000"代表支付结果因为支付渠道原因或者系统原因还在等待支付结果确认，最终交易是否成功以服务端异步通知为准（小概率状态）
                            if (TextUtils.equals(resultStatus, "8000")) {
                                Toast toast=Toast.makeText(orderActivity, "支付结果确认中", Toast.LENGTH_SHORT);
                                toast.setGravity(Gravity.CENTER,0,0);
                                toast.show();
                                orderActivity.finish();
                            } else {
                                // 其他值就可以判断为支付失败，包括用户主动取消支付，或者系统返回的错误
                                Toast toast=Toast.makeText(orderActivity, "支付失败", Toast.LENGTH_SHORT);
                                toast.setGravity(Gravity.CENTER,0,0);
                                toast.show();
                                orderActivity.finish();
                            }
                        }
                        break;
                }
            }
        }

        /**
         *   微信支付
         */
        private void toWXPay(final OrderActivity orderActivity) {
            RequestBody formBody=new FormBody.Builder()
                    .add("oid",orderActivity.oid)
                    .build();
            Request request=new Request.Builder()
                    .url(Urls.WX_PAY)
                    .post(formBody)
                    .build();
            Toast.makeText(MyApplication.getContext(), "获取订单中...", Toast.LENGTH_SHORT).show();
            orderActivity.client.newCall(request).enqueue(new okhttp3.Callback() {
                @Override
                public void onFailure(Call call, IOException e) {

                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    if(!response.isSuccessful()) throw new IOException("Unexpected code"+response);
                    final WXPayBean wxPayBean= (WXPayBean) GetBeanClass.getBean(response,WXPayBean.class);
                    response.close();
                    if(wxPayBean.getCode()==1){
                        WXPayBean.DataBean dataBean=wxPayBean.getData();
                        PayReq req = new PayReq();
                        req.appId			= dataBean.getAppid();
                        req.partnerId		= dataBean.getPartnerid();
                        req.prepayId		= dataBean.getPrepayid();
                        req.nonceStr		= dataBean.getNoncestr();
                        req.timeStamp		= dataBean.getTimestamp()+"";
                        req.packageValue	= dataBean.getPack();
                        req.sign			= dataBean.getSign();
                        req.extData			= "app data"; // optional

                        orderActivity.api.sendReq(req);

                        SPUtils.putStringValue(MyApplication.getContext(),Contants.SP_NAME,"aliResult",orderActivity.oid);
                        SPUtils.putStringValue(MyApplication.getContext(),Contants.SP_NAME,"pay",orderActivity.resultPay);
                        SPUtils.putStringValue(MyApplication.getContext(),Contants.SP_NAME,"time",orderActivity.distributionTime);

                        orderActivity.finish();
                    }
                    UiUtils.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            orderActivity.tvOrderGotoPay.setEnabled(true);
                        }
                    });
                }
            });
        }

        private void toAliPay(final OrderActivity orderActivity) {
            RequestBody formBody=new FormBody.Builder()
                    .add("oid",orderActivity.oid)
                    .build();
            Request request=new Request.Builder()
                    .url(Urls.GET_ALIPAY_SIGN)
                    .post(formBody)
                    .build();
            orderActivity.client.newCall(request).enqueue(new okhttp3.Callback() {
                @Override
                public void onFailure(Call call, IOException e) {

                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    if(!response.isSuccessful()) throw new IOException("Unexpected code"+response);
                    final GoPayBean goPayBean= (GoPayBean) GetBeanClass.getBean(response,GoPayBean.class);
                    response.close();
                    if (goPayBean.getCode()==1){
                        String payInfo =goPayBean.getData();
                        // 构造PayTask 对象
                        PayTask alipay = new PayTask(orderActivity);
                        // 调用支付接口，获取支付结果
                        String result = alipay.pay(payInfo, true);
                        Message msg = new Message();
                        msg.what = SDK_PAY_FLAG;
                        msg.obj = result;
                        orderActivity.handler.sendMessage(msg);
                    }
                    UiUtils.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            orderActivity.tvOrderGotoPay.setEnabled(true);
                        }
                    });

                }
            });
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        client.dispatcher().cancelAll();
    }
}
