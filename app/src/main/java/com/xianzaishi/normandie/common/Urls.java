package com.xianzaishi.normandie.common;

/**
 * shenLang
 * Created by Administrator on 2016/8/23.
 */
public class Urls {
    private static final String TEST="http://item.xianzaishi.net/wapcenter/";
    private static final String OFFICIAL="http://item.xianzaishi.com/";
    private static final String TEST_TRADE="http://trade.xianzaishi.net/";
    private static final String OFFICIAL_TRADE="http://trade.xianzaishi.com/";

    private static final String LOCAL_HOST=OFFICIAL;
    private static final String LOCAL_HOST_TRADE=OFFICIAL_TRADE;
//____________________________________________________________________________________________________
    //获取邀请码
    public static final String GET_INVITATION_CODE=LOCAL_HOST+"requestuser/queryuserinfobyid";
    //礼券奖励细则接口（H5页面）
    public static final String INVITATION_RULES="http://m.xianzaishi.com/app/huodongxinxi.html";
    //商品打标接口 (参数skuIds=1,2,3,4,5,6,7)
    public static final String MAKE_SIGN=LOCAL_HOST+"requestcommodies/queryitemtagsdetail";
    //获取邀请成就
    public static final String GET_ACHIEVEMENT=LOCAL_HOST+"oldwithnew/getachievementlist";
    //上传服务器用户个性化数据参数
    public static final String UPLOADING_MESSAGE=LOCAL_HOST+"usermsg/updatemsgid";
    //获取首页所有楼层ID (静态接口)
    public static final String GET_ALL_STEP="http://www.xianzaishi.com/mobile/homepage/index20161223.html";
    //获取首页广告图 （静态接口）
    public static final String GET_AD_PIC="http://www.xianzaishi.com/mobile/homepage/resource20161224.html";
    //成就额外奖励的规则  (静态接口)
    public static final String ACHIEVEMENT_RULE="http://www.xianzaishi.com/mobile/usercenter/newrule20161220.html";
    //邀请好友规则展示 （静态接口）
    public static final String INVITATION="http://www.xianzaishi.com/mobile/usercenter/newcenter20161219.html";
    //邀请码兑换
    public static final String EXCHANGE_INVITATION_CODE=LOCAL_HOST+"oldwithnew/invitecodeexchange";
    //获取服务器当前时间
    public static final String GET_SYSTEM_TIME="http://purchaseop.xianzaishi.com/system/gettime";
    //检查更新
    public static final String CHECK_VERSION="http://static.xianzaishi.com/erp/mobile/config.html";
    //下载更新包
    public static final String DOWN_LOAD_APK="http://www.xianzaishi.com/download/xianzaishi.apk";
    //领取优惠券的入口
    public static final String OBTAIN_COUPONS=LOCAL_HOST+"promotion/addDiscountCoupon";
    //购物车提示用户最接近使用哪个优惠券
    public static final String COUPONS_HINT=LOCAL_HOST+"promotion/selectMathCoupon";
    //根据楼层获取信息
    public static final String HOME_DATA_BY_STEPID=LOCAL_HOST+"requesthome/homepageoptimization";

    public static final String MINE_SENDCHECKCODE = LOCAL_HOST+"requestuser/" +
            "sendcheckcode";//发送验证码
    public static final String MINE_REGISTER = LOCAL_HOST+"requestuser/" +
            "regist?phone=%s&pwd=%s&verificationCode=%s";//注册-->弃用
    public static final String MINE_LOGIN = LOCAL_HOST+"requestuser/login";//登录
    public static final String MINE_DATA = LOCAL_HOST+"requestuser/" +
            "mine";//我的页面
    public static final String GET_UID = LOCAL_HOST+"requestuser/requestuserid";//根据token获取uid
    public static final String MINE_MODIFIEDPWD = LOCAL_HOST+"requestuser/" +
            "modifiedpwd?phone=%s&pwd=%s&verificationCode=%s";//修改密码--》弃用
    public static final String MINE_LOGOUT = LOCAL_HOST+"requestuser/" +
            "logout?token=%s";//退出登录
    public static final String UPDATE_USER_INFO=LOCAL_HOST+"requestuser/updateuserinfo";//修改个人信息
    public static final String SELECT_ADDR_ROOT = LOCAL_HOST_TRADE+"da/getRootArea.json";//添加收货地址，选择省市
    public static final String SELECT_ADDR_CHILD = LOCAL_HOST_TRADE+"da/getChildArea.json" +
            "?code=%s";//获取省市以下地区
    public static final String ADD_LINK_MAN = LOCAL_HOST_TRADE+"da/addLinkman.json";//添加收货人
    public static final String GET_ALL_ADDRESS = LOCAL_HOST_TRADE+"da/getAllAddress.json";//获取所有地址
    public static final String GET_ADDRESS_BY_ID = LOCAL_HOST_TRADE+"da/getAddressById.json";//根据地址id获取地址信息
    public static final String MODIFY_LINK_MAN = LOCAL_HOST_TRADE+"da/modifyLinkman.json";//修改收货地址
    public static final String DEL_LINK_MAN = LOCAL_HOST_TRADE+"da/delLinkman.json";//删除地址
    public static final String GET_DEFAULT_ADDRESS = LOCAL_HOST_TRADE+"da/getDefaultAddress" +
            ".json";//获取默认收货地址
    public static final String CATEGORY = LOCAL_HOST+"requestcategory/category" +
            "?homePageCategory=false";//一级分类
    public static final String LEAF_CATEGORY = LOCAL_HOST+"requestcategory/leafcategory" +
            "?parentId=%d&pageSize=10&pageNum=%d";//二级分类
    public static final String INDICATOR_CATEGORY = LOCAL_HOST+"requestcategory/commodies";
    //二级分类页面通过导航展示相应的内容
    public static final String ADD_TO_TROLLEY = LOCAL_HOST_TRADE+"sc/appendItem.json";//添加到购物车
    public static final String SHOPPING_CAR_URL = LOCAL_HOST_TRADE+"sc/getCarts.json";//购物车
    public static final String GET_ALL_COUNT = LOCAL_HOST_TRADE+"sc/getSumItemCount.json";//获取购物车所有商品数量，post uid
    //购物车选中接口
    public static final String SHOPPING_CAR_SELECT=LOCAL_HOST_TRADE+"sc/select.json";
    //订单结算页获取商品及优惠信息的接口
    public static final String GET_GOODS_BY_SKUIDS=LOCAL_HOST_TRADE+"sc/settle.json";
    public static final String CHANGE_ITEM_COUNT = LOCAL_HOST_TRADE+"sc/changeCartCount.json";//改变购物车中商品数量
    public static final String TO_ORDER = LOCAL_HOST_TRADE+"sc/toOrder.json";
    //提交订单去结算&sciId=150&sciId=149&sciId=148&couponId=1&adderssId=87"
    public static final String GET_ORDER_STATUS = LOCAL_HOST_TRADE+"order/getOrderStatus.json";//获取订单支付状态
    public static final String GET_ALIPAY_SIGN = LOCAL_HOST_TRADE+"order/getAlipaySign.json";
    //支付宝的完整签名数据
    public static final String GET_COUPONS = LOCAL_HOST+"promotion/selectUserCouponList";//获取用户所有的未使用的优惠券,包括已经过期的优惠券  参数:uid - 用户ID
    public static final String GET_COUPONS_QR=LOCAL_HOST_TRADE+"coupon/getCouponSignature.json";//获取用户优惠券生成二维码的信息
    public static final String GET_COUPONS_BY_ID = LOCAL_HOST_TRADE+"user/getCouponDetail.json";//通过优惠券ID获取优惠券信息 参数为 token 和优惠券id
    public static final String WX_PAY = LOCAL_HOST_TRADE+"order/getWeixinpaySign.json";//微信支付
    public static final String GET_ADDRESS = LOCAL_HOST_TRADE+"da/getShopServiceAreaDetail." +
            "json";//获取配送范围内的四级行政地址
    public static final String GET_CREDIT = LOCAL_HOST_TRADE+"user/getCredit.json";
    //获取可抵用积分，post "uid"
    public static final String GET_CREDIT_LOG = LOCAL_HOST_TRADE+"user/getCreditLogs.json";
    //获取积分记录，post "uid"
    public static final String SEARCH = LOCAL_HOST+"search/queryitems" +
            "?itemKeyWord=%s&pageSize=10&pageNum=%d";//搜索接口
    public static final String HOT_SEARCH = LOCAL_HOST+"search/hotsearchword";//热搜接口
    public static final String CANCEL_TRADE = LOCAL_HOST_TRADE+"order/cancelTrade.json";//取消订单
    public static final String REFUND_TRADE = LOCAL_HOST_TRADE+"order/refund.json";//退款
    public static final String WEBVIEW_URL = "http://www.xianzaishi.com/mobile/detail.html?id=%d";//商品详情webview
    public static final String CREAT_QR_FOR_COUPON="{\"security\":\"%s\",\"sign\":\"%s\"}";//为优惠券生成二维码
    public static final String CAN_BUY="http://static.xianzaishi.com/erp/switch/switch.html";//是否可以购物
    public static final String GET_ID_FROM_BAR_CODE="http://purchaseop.xianzaishi.com/item/queryskudetail?skuCode=%s";//扫码69码获得id
    public static final String APPLY_SERVICE="http://purchaseop.xianzaishi.com/customservice/applyrefund";//申请客服售后
    public static final String DATA="{\"bizId\":\"%s\",\"customerProof\":[{\"id\":1,\"source\":\"%s\",\"type\":1}%s]}";//,{"id":2,"source":"1/1474351778620.jpg","type":2}
    public static final String DATA_PIC=",{\"id\":%d,\"source\":\"%s\",\"type\":2}";

}
