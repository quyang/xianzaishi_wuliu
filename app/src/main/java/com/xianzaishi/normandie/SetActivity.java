package com.xianzaishi.normandie;

import android.app.Dialog;
import android.content.ComponentName;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.umeng.analytics.MobclickAgent;
import com.xianzaishi.normandie.common.Urls;
import com.xianzaishi.normandie.global.Contants;
import com.xianzaishi.normandie.utils.DataClean1Manager;
import com.xianzaishi.normandie.utils.SPUtils;
import com.xianzaishi.normandie.utils.ToastUtils;
import com.xianzaishi.normandie.utils.UiUtils;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * 设置页面
 */
public class SetActivity extends AppCompatActivity implements View.OnClickListener{
    private TextView cache,grade,version;
    private LinearLayout clearCache;
    private Button logout;
    private ImageView back;
    private MediaType MEDIA_TYPE_MARKDOWN;
    private Dialog dialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set);
        initView();
    }

    private void initView() {
        dialog=new Dialog(this,R.style.Dialog);
        String uid=SPUtils.getStringValue(MyApplication.getContext(),Contants.SP_NAME,Contants.UID,null);
        View view=findViewById(R.id.set_title);
        TextView title= (TextView) view.findViewById(R.id.text_title);
        title.setText("设置");
        version= (TextView) findViewById(R.id.set_version);
        String versionName=SPUtils.getVersionName(this);
        if (versionName!=null){
            version.setText("版本号："+versionName);
        }else {
            version.setVisibility(View.GONE);
        }
        /**
         *  展示当前缓存情况
         */
        cache= (TextView) findViewById(R.id.set_cache);
        try {
            cache.setText(DataClean1Manager.getTotalCacheSize(this));
        } catch (Exception e) {
            e.printStackTrace();
        }
        /**
         * 添加点击事件
         */
        grade= (TextView) findViewById(R.id.set_grade);
        grade.setOnClickListener(this);
        clearCache= (LinearLayout) findViewById(R.id.set_clear_cache);
        clearCache.setOnClickListener(this);
        logout= (Button) findViewById(R.id.set_logout);
        if (uid==null){
            logout.setVisibility(View.INVISIBLE);
        }
        logout.setOnClickListener(this);
        back= (ImageView) view.findViewById(R.id.address_go_back);
        back.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.set_grade://评分
                Uri uri = Uri.parse("market://details?id=" + this.getPackageName());
                Intent intentpf = new Intent(Intent.ACTION_VIEW,uri);
                intentpf.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                try {
                    startActivity(intentpf);
                }catch (Exception e){
                    ToastUtils.showToast("您尚未安装应用市场");
                }

                break;
            case R.id.set_clear_cache://清除缓存
                dialog("2");
                break;
            case R.id.set_logout://退出登录
                dialog("1");
                break;
            case R.id.address_go_back://返回，关闭此界面
                finish();
                break;
        }
    }
    /**
     *  退出账号时弹出dialog
     */
    private void dialog(final String type){
        LayoutInflater inflater=this.getLayoutInflater();
        View view=inflater.inflate(R.layout.commodity_delete_dialog,null);
        dialog.setCancelable(false);
        dialog.setContentView(view);
        TextView sure= (TextView) view.findViewById(R.id.commodity_dialog_sure);
        TextView cancel= (TextView) view.findViewById(R.id.commodity_dialog_cancel);
        TextView title= (TextView) view.findViewById(R.id.dialog_title);
        TextView hint= (TextView) view.findViewById(R.id.dialog_hint);
        switch (type){
            case "1":
                title.setVisibility(View.GONE);
                hint.setText("确认退出登录吗？");
                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.WRAP_CONTENT);
                lp.setMargins(0,80,0,25);
                hint.setLayoutParams(lp);
                hint.setGravity(Gravity.CENTER);
                break;
            case "2":
                title.setText("清除缓存");
                hint.setText("确认清除缓存？");
                break;
        }

        sure.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch (type){
                    case "1":
                        OkHttpClient client=MyApplication.getOkHttpClient();
                        String token= SPUtils.getStringValue(MyApplication.getContext(), Contants.SP_NAME,"token","");
                        MEDIA_TYPE_MARKDOWN=MediaType.parse("text/x-markdown;charset=utf-8");
                        String postBody="{\"token\":\"%s\"}";
                        Request request=new Request.Builder()
                                .url(Urls.MINE_LOGOUT)
                                .post(RequestBody.create(MEDIA_TYPE_MARKDOWN,String.format(postBody,token)))
                                .build();
                        client.newCall(request).enqueue(new Callback() {
                            @Override
                            public void onFailure(Call call, IOException e) {
                            }
                            @Override
                            public void onResponse(Call call, Response response) throws IOException {
                                if(!response.isSuccessful()) throw new IOException("Unexpected code"+response);
                                UiUtils.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(SetActivity.this,"退出登录成功！",Toast.LENGTH_SHORT).show();
                                        SPUtils.removeData(getApplicationContext(), Contants.SP_NAME,"uid");
                                        SPUtils.removeData(MyApplication.getContext(),Contants.SP_NAME,Contants.TOKEN);
                                        SPUtils.removeData(MyApplication.getContext(),Contants.SP_NAME,"isNewUser");
                                        SPUtils.removeData(MyApplication.getContext(),Contants.SP_NAME,"LoginChecked");
                                        SPUtils.putBooleanValue(MyApplication.getContext(),Contants.SP_NAME,"isCreatQRSuccess",false);
                                        finish();
                                    }
                                });
                            }
                        });
                        dialog.dismiss();
                        break;
                    case "2":
                        DataClean1Manager.clearAllCache(MyApplication.getContext());
                        try {
                            cache.setText(DataClean1Manager.getTotalCacheSize(SetActivity.this));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        dialog.dismiss();
                        break;
                }


            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.show();
        Window dialogWindow = dialog.getWindow();
        WindowManager m = this.getWindowManager();
        Display d = m.getDefaultDisplay(); // 获取屏幕宽、高度
        WindowManager.LayoutParams p = dialogWindow.getAttributes(); // 获取对话框当前的参数值
        //p.height = (int) (d.getHeight() * 0.5); // 高度设置为屏幕的0.6，根据实际情况调整
        p.width = (int) (d.getWidth() * 0.6); // 宽度设置为屏幕的0.6，根据实际情况调整
        dialogWindow.setAttributes(p);
    }

    @Override
    protected void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
    }

}
