package com.xianzaishi.normandie;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.xianzaishi.normandie.utils.StringUtils;

/**
 * quyang
 * <p>
 * 申请发票
 */
public class ApplyBillsActivity extends BaseActivity {

    private ImageView mIvLeft;
    private ImageView mIvRight;
    private EditText mEtDes;
    private TextView mTvAddress;
    private Button mBtnSub;
    private String mDes;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_apply_bills);

        //find the components
        mIvLeft = (ImageView) findViewById(R.id.iv_left);
        mIvRight = (ImageView) findViewById(R.id.iv_right);
        mEtDes = (EditText) findViewById(R.id.et_des);
        mTvAddress = (TextView) findViewById(R.id.tv_address);
        mBtnSub = (Button) findViewById(R.id.btn_sub);

        MyClick click = new MyClick();
        mIvLeft.setOnClickListener(click);
        mIvRight.setOnClickListener(click);
        mBtnSub.setOnClickListener(click);
        mIvLeft.setPressed(true);

        mDes = mEtDes.getText().toString().trim();

        String userAddress = getIntent().getStringExtra("userAddress");

        if (StringUtils.juege(userAddress)) {
            mTvAddress.setText(userAddress);
        } else {
            Toast.makeText(ApplyBillsActivity.this, "地址为空", Toast.LENGTH_LONG).show();
        }
    }

    public class MyClick implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.iv_left:
                    mIvLeft.setPressed(true);
                    mIvRight.setPressed(false);


                    break;
                case R.id.iv_right:
                    mIvLeft.setPressed(false);
                    mIvRight.setPressed(true);

                    break;
                case R.id.btn_sub://提交
                    sendMsg();

                    break;
            }
        }
    }

    //send msg to the server
    private void sendMsg() {

        Toast.makeText(ApplyBillsActivity.this, "功能暂不确定", Toast.LENGTH_LONG).show();
        return;

        //-----------------------------------------------------------------------------

//        PostProtocol protocol = new PostProtocol();
//        FormBody body = new FormBody
//                .Builder()
//                .add("","")
//                .add("","")
//                .build();
//        protocol.setUrl("");
//        protocol.setRequestBody(body);
//        protocol.getDataByPOST();
//        protocol.setOnDataFromPostServerListener(new OnDataFromPostServerListener() {
//            @Override
//            public void onSuccess(String result) {
//                try {
//                    JSONObject object = new JSONObject(result);
//                    if (object.getBoolean("success")) {
//                        Toast.makeText(ApplyBillsActivity.this,"提交成功",Toast.LENGTH_LONG).show();
//                    }
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//
//            }
//
//            @Override
//            public void onFail() {
//
//            }
//        });
    }

}
