package com.xianzaishi.normandie;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.alipay.sdk.app.PayTask;
import com.google.gson.Gson;
import com.umeng.analytics.MobclickAgent;
import com.xianzaishi.normandie.adapter.MyOrderAdapter;
import com.xianzaishi.normandie.bean.GoPayBean;
import com.xianzaishi.normandie.bean.LeafCategoryBean;
import com.xianzaishi.normandie.bean.OrdersListBean01;
import com.xianzaishi.normandie.common.Urls;
import com.xianzaishi.normandie.customs.MyRefreshHeader;
import com.xianzaishi.normandie.global.Contants;
import com.xianzaishi.normandie.pay.PayResult;
import com.xianzaishi.normandie.utils.AliPayResultMap;
import com.xianzaishi.normandie.utils.DataCleanManager;
import com.xianzaishi.normandie.utils.GetBeanClass;
import com.xianzaishi.normandie.utils.SPUtils;
import com.xianzaishi.normandie.utils.ToastUtils;
import com.xianzaishi.normandie.utils.UiUtils;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import in.srain.cube.views.ptr.PtrDefaultHandler;
import in.srain.cube.views.ptr.PtrFrameLayout;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class MyOrderNewActivity extends AppCompatActivity implements View.OnClickListener,MyOrderAdapter.OnOrderClickListener{
    private ImageView goBack;
    private List<TextView> tabs,indicators;
    private RecyclerView recyclerView;
    private TextView tab1,tab2,tab3,tab4,tab5,indicator1,indicator2,indicator3,indicator4,indicator5;
    private OkHttpClient client=MyApplication.getOkHttpClient();
    private OrdersListBean01 orderBean;
    private List<OrdersListBean01.DataBean.ObjectsBean> list;
    private static final int SDK_PAY_FLAG = 2;//支付宝支付后返回结果的标识
    private MyHandler handler;
    private int refreshTag=1;
    private String resultPay;
    private int PageNumber=2;
    private int visibleLastIndex = 0;   //最后的可视项索引
    private String uid;
    private PtrFrameLayout mPtrFrameLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_order_new);
        initView();
    }

    private void initView() {
        handler=new MyHandler(MyOrderNewActivity.this);
        /**
         *  标题栏相关  title 以及 返回按钮
         */
        View view=findViewById(R.id.my_order_head);
        TextView title= (TextView) view.findViewById(R.id.text_title);
        title.setText("我的订单");
        goBack= (ImageView) view.findViewById(R.id.address_go_back);
        goBack.setOnClickListener(this);

        /**
         *  导航栏
         */
        tabs=new ArrayList<>();
        indicators=new ArrayList<>();
        tab1= (TextView) findViewById(R.id.my_order_tab1);
        tab1.setOnClickListener(this);
        tab2= (TextView) findViewById(R.id.my_order_tab2);
        tab2.setOnClickListener(this);
        tab3= (TextView) findViewById(R.id.my_order_tab3);
        tab3.setOnClickListener(this);
        tab4= (TextView) findViewById(R.id.my_order_tab4);
        tab4.setOnClickListener(this);
        tab5= (TextView) findViewById(R.id.my_order_tab5);
        tab5.setOnClickListener(this);
        tabs.add(tab1);tabs.add(tab2);tabs.add(tab3);tabs.add(tab4);tabs.add(tab5);
        indicator1= (TextView) findViewById(R.id.my_order_indicator1);
        indicator2= (TextView) findViewById(R.id.my_order_indicator2);
        indicator3= (TextView) findViewById(R.id.my_order_indicator3);
        indicator4= (TextView) findViewById(R.id.my_order_indicator4);
        indicator5= (TextView) findViewById(R.id.my_order_indicator5);
        indicators.add(indicator1);indicators.add(indicator2);indicators.add(indicator3);
        indicators.add(indicator4);indicators.add(indicator5);
        /**
         *  初始化recyclerView 相关
         */
        GridLayoutManager lm=new GridLayoutManager(this,1,GridLayoutManager.VERTICAL,false);
        recyclerView= (RecyclerView) findViewById(R.id.my_order_recyclerView);
        recyclerView.setLayoutManager(lm);
        /**
         *  初始化刷新相关
         */
        mPtrFrameLayout= (PtrFrameLayout) findViewById(R.id.material_style_ptr_frame);
        MyRefreshHeader header=new MyRefreshHeader(MyOrderNewActivity.this);
        mPtrFrameLayout.setHeaderView(header);
        mPtrFrameLayout.addPtrUIHandler(header);
        mPtrFrameLayout.setPtrHandler(new PtrDefaultHandler() {
            @Override
            public void onRefreshBegin(PtrFrameLayout frame) {
                loadDataFromNet(getStatus(refreshTag));
            }
        });
        mPtrFrameLayout.autoRefresh(true);
    }


    private void loadDataFromNet(final String status){
        uid = SPUtils.getStringValue(this, Contants.SP_NAME, Contants.UID, null);
        RequestBody formbody = new FormBody.Builder()
                .add("uid", uid)
                .add("pageSize", "20")
                .add("curPage", "1")
                .add("status", status)
                .build();
        Request request=new Request.Builder()
                .url(Contants.ORDER_LIST)
                .post(formbody)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                mPtrFrameLayout.refreshComplete();
            }
            @Override
            public void onResponse(Call call, Response response) throws IOException {
                final String json=response.body().string();
                response.close();
                mPtrFrameLayout.refreshComplete();
                UiUtils.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            parseJson(json,status);
                        }catch (Exception e){
                            ToastUtils.showToast("未知错误，请稍后再试");
                        }
                    }
                });
            }
        });
    }

    private void parseJson(String json, final String status) {
        Gson gson = new Gson();
        orderBean = gson.fromJson(json, OrdersListBean01.class);
        if (orderBean.data!=null){
            list=orderBean.data.objects;
            final MyOrderAdapter adapter=new MyOrderAdapter(list,this);
            recyclerView.setAdapter(adapter);
            adapter.setOnOrderClickListener(this);
            /**
             *  listView分页加载
             */
           adapter.setLoadMoreData(new MyOrderAdapter.LoadMoreData() {
               @Override
               public void loadMoreData() {
                   if (orderBean.data.pagination.totalPage>=PageNumber){
                       String page=String.valueOf(PageNumber);
                       PageNumber++;
                       RequestBody formbody = new FormBody.Builder()
                               .add("uid", uid)
                               .add("pageSize", "20")
                               .add("curPage", page)
                               .add("status", status)
                               .build();
                       Request request=new Request.Builder()
                               .url(Contants.ORDER_LIST)
                               .post(formbody)
                               .build();
                       client.newCall(request).enqueue(new Callback() {
                           @Override
                           public void onFailure(Call call, IOException e) {

                           }

                           @Override
                           public void onResponse(Call call, Response response) throws IOException {
                               if (!response.isSuccessful())throw new IOException("Unexpected code" + response);
                               final OrdersListBean01 orderBean = (OrdersListBean01) GetBeanClass.getBean(response, OrdersListBean01.class);
                               response.close();
                               UiUtils.runOnUiThread(new Runnable() {
                                   @Override
                                   public void run() {
                                       if (orderBean.data!=null){
                                           List<OrdersListBean01.DataBean.ObjectsBean> list1=orderBean.data.objects;
                                           if (list1!=null&adapter!=null){
                                               list.addAll(list1);
                                               adapter.notifyDataSetChanged();
                                           }else {
                                               UiUtils.runOnUiThread(new Runnable() {
                                                   @Override
                                                   public void run() {
                                                       Toast.makeText(MyOrderNewActivity.this,"没有更多数据了！",Toast.LENGTH_SHORT).show();
                                                       PageNumber--;
                                                   }
                                               });
                                           }
                                       }
                                   }
                               });
                           }
                       });
                   }else {
                       UiUtils.runOnUiThread(new Runnable() {
                           @Override
                           public void run() {
                               Toast.makeText(MyOrderNewActivity.this,"没有更多数据了！",Toast.LENGTH_SHORT).show();
                           }
                       });
                   }
               }
           });

        }else {
            list=null;
            MyOrderAdapter adapter=new MyOrderAdapter(list,this);
            recyclerView.setAdapter(adapter);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.my_order_tab1://每一个tab选中后更改状态  做相应的数据请求(全部订单 status：0)
                refreshTag=1;
                for (int i = 0; i < 5; i++) {
                    if (i==0){
                        tabs.get(i).setSelected(true);
                        indicators.get(i).setVisibility(View.VISIBLE);
                    }else {
                        tabs.get(i).setSelected(false);
                        indicators.get(i).setVisibility(View.INVISIBLE);
                    }
                }
                mPtrFrameLayout.autoRefresh(true);
                break;
            case R.id.my_order_tab2://待付款 status：2
                refreshTag=2;
                for (int i = 0; i < 5; i++) {
                    if (i==1){
                        tabs.get(i).setSelected(true);
                        indicators.get(i).setVisibility(View.VISIBLE);
                    }else {
                        tabs.get(i).setSelected(false);
                        indicators.get(i).setVisibility(View.INVISIBLE);
                    }
                }
                mPtrFrameLayout.autoRefresh(true);
                break;
            case R.id.my_order_tab3://待配送 status：5
                refreshTag=3;
                for (int i = 0; i < 5; i++) {
                    if (i==2){
                        tabs.get(i).setSelected(true);
                        indicators.get(i).setVisibility(View.VISIBLE);
                    }else {
                        tabs.get(i).setSelected(false);
                        indicators.get(i).setVisibility(View.INVISIBLE);
                    }
                }
                mPtrFrameLayout.autoRefresh(true);
                break;
            case R.id.my_order_tab4://配送中 status：6
                refreshTag=4;
                for (int i = 0; i < 5; i++) {
                    if (i==3){
                        tabs.get(i).setSelected(true);
                        indicators.get(i).setVisibility(View.VISIBLE);
                    }else {
                        tabs.get(i).setSelected(false);
                        indicators.get(i).setVisibility(View.INVISIBLE);
                    }
                }
                mPtrFrameLayout.autoRefresh(true);
                break;
            case R.id.my_order_tab5://退款中
                refreshTag=5;
                for (int i = 0; i < 5; i++) {
                    if (i==4){
                        tabs.get(i).setSelected(true);
                        indicators.get(i).setVisibility(View.VISIBLE);
                    }else {
                        tabs.get(i).setSelected(false);
                        indicators.get(i).setVisibility(View.INVISIBLE);
                    }
                }
                mPtrFrameLayout.autoRefresh(true);
                break;
            case R.id.address_go_back:
                finish();
                break;
        }
    }

    /**
     *  根据刷新标记码获取相应的status
     * @param refreshTag 刷新标记码 用来记录当前在哪个导航条目下刷新所得的标识
     * @return
     */
    private String getStatus(int refreshTag){
        switch (refreshTag){
            case 1:
                return "0";
            case 2:
                return "2";
            case 3:
                return "3,4,5";
            case 4:
                return "6";
            case 5:
                return "8,9,10,11";
        }
        return null;
    }
    /**
     *  此方法是MyOrderAdapter里自定义的点击监听
     * @param view
     * @param position
     */
    @Override
    public void onClickListener(View view, int position) {
        switch (view.getId()){
            case R.id.btn_zhifu:
                resultPay=list.get(position).effeAmount;
                toAliPay(list.get(position).id);
                break;
            case R.id.btn_cancel:
                dialog(position);
                break;
        }
    }
    /**
     *  取消订单时弹出dialog
     */
    private void dialog(final int position){
        LayoutInflater inflater=this.getLayoutInflater();
        View view=inflater.inflate(R.layout.commodity_delete_dialog,null);
        final Dialog dialog=new Dialog(this,R.style.Dialog);
        dialog.setCancelable(false);
        dialog.setContentView(view);
        TextView sure= (TextView) view.findViewById(R.id.commodity_dialog_sure);
        TextView cancel= (TextView) view.findViewById(R.id.commodity_dialog_cancel);
        TextView title= (TextView) view.findViewById(R.id.dialog_title);
        TextView hint= (TextView) view.findViewById(R.id.dialog_hint);
        title.setText("取消订单");
        hint.setText("确认取消订单？");

        sure.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(list.get(position).status.equals("2")) {
                    cancelTrade(list.get(position).id);
                }else {
                    OrdersListBean01.DataBean.ObjectsBean bean=list.get(position);
                    refundTrade(bean.userId,bean.id,bean.effeAmount);
                }
            dialog.dismiss();
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.show();
        Window dialogWindow = dialog.getWindow();
        WindowManager m = this.getWindowManager();
        Display d = m.getDefaultDisplay(); // 获取屏幕宽、高度
        WindowManager.LayoutParams p = dialogWindow.getAttributes(); // 获取对话框当前的参数值
        //p.height = (int) (d.getHeight() * 0.5); // 高度设置为屏幕的0.6，根据实际情况调整
        p.width = (int) (d.getWidth() * 0.6); // 宽度设置为屏幕的0.6，根据实际情况调整
        dialogWindow.setAttributes(p);
    }
    /**
     *  取消订单后的提示
     */
    /**
     *  退款成功后弹出dialog
     */
    private void dialogCancel(){

        LayoutInflater inflater=this.getLayoutInflater();
        View view=inflater.inflate(R.layout.cancel_order_message,null);
        final Dialog dialog=new Dialog(this,R.style.Dialog);
        dialog.setCancelable(false);
        dialog.setContentView(view);
        Button sure= (Button) view.findViewById(R.id.cancel_sure_btn);
        sure.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.show();
        Window dialogWindow = dialog.getWindow();
        WindowManager m = this.getWindowManager();
        Display d = m.getDefaultDisplay(); // 获取屏幕宽、高度
        WindowManager.LayoutParams p = dialogWindow.getAttributes(); // 获取对话框当前的参数值
        //p.height = (int) (d.getHeight() * 0.5); // 高度设置为屏幕的0.6，根据实际情况调整
        p.width = (int) (d.getWidth() * 0.6); // 宽度设置为屏幕的0.6，根据实际情况调整
        dialogWindow.setAttributes(p);
    }
    /**
     * 对订单发起退款
     */
    private void refundTrade(String uid, String oid, String amount) {
        RequestBody formBody=new FormBody.Builder()
                .add("oid",oid)
                .add("uid",uid)
                .add("amount",amount)
                .add("cause","退款")
                .build();
        Request request=new Request.Builder()
                .url(Urls.REFUND_TRADE)
                .post(formBody)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                UiUtils.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ToastUtils.showToast("出现未知错误，请稍后再试");
                    }
                });
            }
            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if(!response.isSuccessful()) throw new IOException("Unexpected code"+response);
                final OrdersListBean01 bean= (OrdersListBean01) GetBeanClass.getBean(response,OrdersListBean01.class);
                response.close();
                UiUtils.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (bean.success){
                            mPtrFrameLayout.autoRefresh(true);
                            dialogCancel();
                        }else {
                            ToastUtils.showToast("系统繁忙，请稍后再试");
                        }
                    }
                });
            }
        });

    }


    /**
     * 取消订单
     */
    private void cancelTrade(String oid){
        RequestBody formBody=new FormBody.Builder()
                .add("oid",oid)
                .build();
        Request request=new Request.Builder()
                .url(Urls.CANCEL_TRADE)
                .post(formBody)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
            }
            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if(!response.isSuccessful()) throw new IOException("Unexpected code"+response);
                OrdersListBean01 bean= (OrdersListBean01) GetBeanClass.getBean(response,OrdersListBean01.class);
                response.close();
                if (bean.success){
                    mPtrFrameLayout.autoRefresh(true);
                }
            }
        });
    }
    /**
     *  支付宝支付
     * @param oid
     */
    private void toAliPay(String oid) {
        RequestBody formBody=new FormBody.Builder()
                .add("oid",oid)
                .build();
        Request request=new Request.Builder()
                .url(Urls.GET_ALIPAY_SIGN)
                .post(formBody)
                .build();
        client.newCall(request).enqueue(new okhttp3.Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

            }
            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if(!response.isSuccessful()) throw new IOException("Unexpected code"+response);
                final GoPayBean goPayBean= (GoPayBean) GetBeanClass.getBean(response,GoPayBean.class);
                response.close();
                if (goPayBean.getCode()==1){
                    String payInfo =goPayBean.getData();
                    // 构造PayTask 对象
                    PayTask alipay = new PayTask(MyOrderNewActivity.this);
                    // 调用支付接口，获取支付结果
                    String result = alipay.pay(payInfo, true);
                    Message msg = new Message();
                    msg.what = SDK_PAY_FLAG;
                    msg.obj = result;
                    handler.sendMessage(msg);
                }

            }
        });
    }

    /**
     *  通过handler保证异步请求服务器的结果可以同步
     */
    static class MyHandler extends Handler {
        WeakReference<MyOrderNewActivity> weakReference;

        public MyHandler(MyOrderNewActivity orderActivity) {
            weakReference = new WeakReference<MyOrderNewActivity>(orderActivity);
        }

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            MyOrderNewActivity orderActivity = weakReference.get();
            if (orderActivity != null) {
                switch (msg.what) {
                    case SDK_PAY_FLAG:
                        PayResult payResult = new PayResult((String) msg.obj);
                        /**
                         * 同步返回的结果必须放置到服务端进行验证（验证的规则请看https://doc.open.alipay.com/doc2/
                         * detail.htm?spm=0.0.0.0.xdvAU6&treeId=59&articleId=103665&
                         * docType=1) 建议商户依赖异步通知
                         */
                        String resultInfo = payResult.getResult();// 同步返回需要验证的信息


                        HashMap<String, String> map = AliPayResultMap.getResultMap(resultInfo);
                        String out_trade_no = map.get("out_trade_no");


                        String resultStatus = payResult.getResultStatus();

                        // 判断resultStatus 为“9000”则代表支付成功，具体状态码代表含义可参考接口文档
                        if (TextUtils.equals(resultStatus, "9000")&&map.get("success").equals("\"true\"")) {
                            orderActivity.mPtrFrameLayout.autoRefresh(true);
                            String tradeNum = out_trade_no.substring(1, out_trade_no.length() - 1);
                            Intent intent=new Intent(orderActivity,AliPayResultActivity.class);
                            intent.putExtra("aliResult",tradeNum);
                            intent.putExtra("pay",orderActivity.resultPay);
                            intent.putExtra("time","");
                            intent.putExtra("channel","支付宝");
                            orderActivity.startActivity(intent);
                        } else {
                            // 判断resultStatus 为非"9000"则代表可能支付失败
                            // "8000"代表支付结果因为支付渠道原因或者系统原因还在等待支付结果确认，最终交易是否成功以服务端异步通知为准（小概率状态）
                            if (TextUtils.equals(resultStatus, "8000")) {
                                Toast.makeText(orderActivity, "支付结果确认中", Toast.LENGTH_SHORT).show();

                            } else {
                                // 其他值就可以判断为支付失败，包括用户主动取消支付，或者系统返回的错误
                                Toast.makeText(orderActivity, "支付失败+" + resultStatus, Toast.LENGTH_SHORT).show();
                            }
                        }
                        break;
                }
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
    }
}
