package com.xianzaishi.normandie;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.xianzaishi.normandie.bean.ExchangeBean;
import com.xianzaishi.normandie.bean.InvitationBean;
import com.xianzaishi.normandie.common.Urls;
import com.xianzaishi.normandie.global.Contants;
import com.xianzaishi.normandie.utils.GetBeanClass;
import com.xianzaishi.normandie.utils.SPUtils;
import com.xianzaishi.normandie.utils.ToastUtils;
import com.xianzaishi.normandie.utils.UiUtils;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class ExchangeInvitationActivity extends AppCompatActivity implements View.OnClickListener{
    private TextView sureBtn,hint;
    private EditText inputInvitation;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exchange_invitation);
        initView();
    }

    private void initView() {
        //设置标题
        TextView title= (TextView) findViewById(R.id.text_title);
        title.setText("兑换优惠券");
        ImageView back= (ImageView) findViewById(R.id.address_go_back);
        back.setOnClickListener(this);
        //输入优惠券兑换码
        inputInvitation= (EditText) findViewById(R.id.exchange_invitation_input);
        //确认兑换按钮
        sureBtn= (TextView) findViewById(R.id.exchange_invitation_sure);
        //兑换码错误的提示
        hint= (TextView) findViewById(R.id.exchange_invitation_hint);
        sureBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.address_go_back:
                finish();
                break;
            case R.id.exchange_invitation_sure:
                exchange();
                break;
        }
    }

    private void exchange() {
        String code=inputInvitation.getText().toString().trim();
        if (code.equals("")){
            ToastUtils.showToast("邀请码不能为空！");
            return;
        }
        OkHttpClient client=MyApplication.getOkHttpClient();
        String token= SPUtils.getStringValue(MyApplication.getContext(), Contants.SP_NAME,Contants.TOKEN,null);
        FormBody formBody=new FormBody.Builder()
                .add("token",token)
                .add("inviteCode",code)
                .build();
        Request request=new Request.Builder()
                .url(Urls.EXCHANGE_INVITATION_CODE)
                .post(formBody)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (response.isSuccessful()){
                    final ExchangeBean bean= (ExchangeBean) GetBeanClass.getBean(response,ExchangeBean.class);
                    UiUtils.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (bean!=null&&bean.isSuccess()){
                                ToastUtils.showToast("领取成功！");
                            }else if(bean!=null){
                                hint.setVisibility(View.VISIBLE);
                                ToastUtils.showToast(bean.getErrorMsg());
                            }
                        }
                    });

                }
            }
        });
    }
}
