package com.xianzaishi.normandie.adapter;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.xianzaishi.normandie.ApplyServiceActivity;
import com.xianzaishi.normandie.MyOrderActivity;
import com.xianzaishi.normandie.OrderDetailsActivity;
import com.xianzaishi.normandie.R;
import com.xianzaishi.normandie.bean.OrdersListBean01;
import com.xianzaishi.normandie.global.Contants;
import com.xianzaishi.normandie.http.PostProtocol;
import com.xianzaishi.normandie.interfaces.OnDataFromPostServerListener;
import com.xianzaishi.normandie.pay.GoToPay;
import com.xianzaishi.normandie.utils.UiUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

import okhttp3.FormBody;

import static com.xianzaishi.normandie.utils.UiUtils.getLayoutInflater;

/**
 * quyang
 * 我的订单的recyclerview的adapter
 * Created by Administrator on 2016/9/3.
 */
public class OrderAdapter extends RecyclerView.Adapter<OrderAdapter.MyViewHoldr> {

    public List<OrdersListBean01.DataBean.ObjectsBean> mAllOrderInfo;
    public MyOrderActivity mActivity;
    private String mId;

    public OrderAdapter(List<OrdersListBean01.DataBean.ObjectsBean> allOrderInfo, MyOrderActivity activity) {
        mAllOrderInfo = allOrderInfo;
        mActivity = activity;
    }

    @Override
    public MyViewHoldr onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MyViewHoldr(View.inflate(UiUtils.getContext(), R.layout.listview_item_order, null));
    }

    @Override
    public void onBindViewHolder(MyViewHoldr holder, final int position) {

        OrdersListBean01.DataBean.ObjectsBean bean = mAllOrderInfo.get(position);

        //订单id
        mId = bean.id;
        String date=getStringDate(Long.valueOf(bean.gmtCreate));
        holder.tvData.setText(date);
        String time=getStringTime(Long.valueOf(bean.gmtCreate));
        holder.tvTime.setText(time);
        holder.tvDengMoney.setText(bean.statusString);
        holder.tvPrice.setText(bean.payAmount);

        List<OrdersListBean01.DataBean.ObjectsBean.ItemsBean> items = bean.items;
        for (int i = 0; i < items.size(); i++) {
            ImageView imageView = new ImageView(UiUtils.getContext());
            imageView.setScaleType(ImageView.ScaleType.FIT_XY);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.MATCH_PARENT);
            params.rightMargin = 24;

            imageView.setLayoutParams(params);

            Glide.with(UiUtils.getContext()).load(items.get(i).iconUrl).override(118, 118).diskCacheStrategy(DiskCacheStrategy.ALL).into(imageView);

            holder.llContainer.addView(imageView);
        }

        //设置点击事件
        MyOnClickListener listener = new MyOnClickListener(position, mId);

        Button zhifu = holder.zhifu;
        Button cancel = holder.cancel;
        zhifu.setText("去支付");
        cancel.setText("取消订单");

        if ("待付款".equals(bean.statusString)) {
            zhifu.setVisibility(View.VISIBLE);
            cancel.setVisibility(View.VISIBLE);
            //设置点击事件
            pay(zhifu, position);
            cancel(cancel, position);
            holder.llFather.setOnClickListener(listener);
            holder.llContainer.setOnClickListener(listener);
        }

        if ("待拣货".equals(bean.statusString) || "待配送".equals(bean.statusString)) {
            zhifu.setVisibility(View.INVISIBLE);
            cancel.setVisibility(View.VISIBLE);
            cancel(cancel, position);

            holder.llFather.setOnClickListener(listener);
            holder.llContainer.setOnClickListener(listener);
        }

        if ("交易成功".equals(bean.statusString)) {
            zhifu.setVisibility(View.INVISIBLE);
            cancel.setVisibility(View.VISIBLE);

            cancel.setText("申请售后");
            applayService(cancel);
        }

        if ("等待退款".equals(bean.statusString) || "等待客服处理".equals(bean.statusString)) {
            zhifu.setVisibility(View.INVISIBLE);
            cancel.setVisibility(View.INVISIBLE);

            holder.llFather.setOnClickListener(listener);
            holder.llContainer.setOnClickListener(listener);
        }

        if ("退款成功".equals(bean.statusString)) {
            zhifu.setVisibility(View.INVISIBLE);
            cancel.setVisibility(View.INVISIBLE);

        }

        if ("交易关闭".equals(bean.statusString)) {
            zhifu.setVisibility(View.INVISIBLE);
            cancel.setVisibility(View.INVISIBLE);

        }
    }
    /**
     * 将长时间格式字符串转换为时间 yyyy-MM-dd
     *
     */
    public static String getStringDate(Long date) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd", Locale.CHINA);
        return formatter.format(date);
    }
    /**
     * 将长时间格式字符串转换为时间 yyyy-MM-dd
     *
     */
    public static String getStringTime(Long date) {
        SimpleDateFormat formatter = new SimpleDateFormat("hh:mm:ss", Locale.CHINA);
        return formatter.format(date);
    }
    //申请售后
    private void applayService(Button cancel) {
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //申请售后
                Intent intent = new Intent(mActivity, ApplyServiceActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                UiUtils.getContext().startActivity(intent);
            }
        });
    }

    //去支付
    private void pay(Button zhifu, int position) {
        zhifu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(UiUtils.getContext(), "去支付,支付宝", Toast.LENGTH_LONG).show();
                Intent intent = new Intent(mActivity, GoToPay.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                UiUtils.getContext().startActivity(intent);
            }
        });
    }

    //取消订单
    private void cancel(Button cancel, final int position) {
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                newDialog(position);
            }
        });
    }


    @Override
    public int getItemCount() {
        return mAllOrderInfo == null ? 0 : mAllOrderInfo.size();
    }

    class MyViewHoldr extends RecyclerView.ViewHolder {

        TextView tvData;
        TextView tvTime;
        TextView tvDengMoney;
        LinearLayout llContainer;
        TextView tvPrice;
        Button zhifu;
        Button cancel;
        LinearLayout root;
        private final LinearLayout llFather;
        private final RecyclerView scrllview;

        public MyViewHoldr(View itemView) {
            super(itemView);

            scrllview = (RecyclerView) itemView.findViewById(R.id.scrollView_order);
            llFather = (LinearLayout) itemView.findViewById(R.id.ll_father);
            tvData = (TextView) itemView.findViewById(R.id.tv_data);
            tvTime = (TextView) itemView.findViewById(R.id.tv_time);
            tvDengMoney = (TextView) itemView.findViewById(R.id.tv_deng_money);
            llContainer = (LinearLayout) itemView.findViewById(R.id.llContainr);
            tvPrice = (TextView) itemView.findViewById(R.id.tv_price);
            zhifu = (Button) itemView.findViewById(R.id.btn_zhifu);
            cancel = (Button) itemView.findViewById(R.id.btn_cancel);
            root = (LinearLayout) itemView.findViewById(R.id.root);
        }
    }


    public class MyOnClickListener implements View.OnClickListener {

        public int position;
        public String id;

        public MyOnClickListener(int position, String id) {
            this.position = position;
            this.id = id;
        }

        @Override
        public void onClick(View view) {

            Intent intent = new Intent(mActivity, OrderDetailsActivity.class);
            intent.putExtra("oid", id);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            UiUtils.getContext().startActivity(intent);
        }
    }


    public void newDialog(final int position) {

        final int a = position;


        final Dialog dialog = new Dialog(mActivity, R.style.MyDialog);
        View view = getLayoutInflater().inflate(R.layout.alertdialog, null);
        TextView ok = (TextView) view.findViewById(R.id.tv_ok);
        TextView cancel = (TextView) view.findViewById(R.id.tv_condider);
        final TextView consider = (TextView) view.findViewById(R.id.tv_condider);
        dialog.setCancelable(false);
        dialog.setContentView(view);

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        //后面给确定和取消按钮设置监听,onclicklistener;
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                //更新订单状态

                FormBody body = new FormBody.Builder()
                        .add("oid", mId + "")
                        .build();

                PostProtocol protocol = new PostProtocol();
                protocol.setUrl(Contants.ORDER_STATUS);
                protocol.setRequestBody(body);
                protocol.getDataByPOST();
                protocol.setOnDataFromPostServerListener(new OnDataFromPostServerListener() {
                    @Override
                    public void onSuccess(String result) {
                        try {
                            JSONObject object = new JSONObject(result);
                            if (object.getBoolean("success")) {

                                newAnotherDialog();


                                mAllOrderInfo.remove(position);

                                //删除recyclerview中选中的条目
                                notifyItemRemoved(position);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFail() {
                        Toast.makeText(mActivity, "更新失败", Toast.LENGTH_LONG).show();
                    }
                });


            }
        });

        consider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        //设置弹窗宽高
        dialog.getWindow().setLayout(UiUtils.getScreenHeight(mActivity).get(0) - 100, WindowManager.LayoutParams.WRAP_CONTENT);
        dialog.show();

    }

    private void newAnotherDialog() {

        final Dialog dialog = new Dialog(mActivity, R.style.MyDialog);
        View view = getLayoutInflater().inflate(R.layout.dialog_order_cancled, null);
        Button ok = (Button) view.findViewById(R.id.btn_ok);

        dialog.setCancelable(false);
        dialog.setContentView(view);

        changeTextColor(view);

        //后面给确定和取消按钮设置监听,onclicklistener;
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });


        //设置弹窗宽高
        dialog.getWindow().setLayout(UiUtils.getScreenHeight(mActivity).get(0) - 100, WindowManager.LayoutParams.WRAP_CONTENT);
        dialog.show();

    }


    public void changeTextColor(View view) {
        String str = "退款在3~5个工作日";
        int bstart = str.indexOf("3~5个工作日");
        int bend = bstart + "3~5个工作日".length();
        int fstart = str.indexOf("3~5个工作日");
        int fend = fstart + "3~5个工作日".length();
        SpannableStringBuilder style = new SpannableStringBuilder(str);
        style.setSpan(new ForegroundColorSpan(Color.RED), fstart, fend, Spannable.SPAN_EXCLUSIVE_INCLUSIVE);
        TextView tvColor = (TextView) view.findViewById(R.id.tv_tuiqian);
        tvColor.setText(style);
    }


}
