package com.xianzaishi.normandie.adapter;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.Intent;
import android.graphics.Path;
import android.graphics.PathMeasure;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.umeng.analytics.MobclickAgent;
import com.xianzaishi.normandie.MyApplication;
import com.xianzaishi.normandie.NewLoginActivity;
import com.xianzaishi.normandie.R;
import com.xianzaishi.normandie.bean.CategoryListViewBean;
import com.xianzaishi.normandie.bean.GeneralBean;
import com.xianzaishi.normandie.bean.LeafCategoryBean;
import com.xianzaishi.normandie.common.Urls;
import com.xianzaishi.normandie.customs.CircleImageView;
import com.xianzaishi.normandie.global.Contants;
import com.xianzaishi.normandie.utils.GetBeanClass;
import com.xianzaishi.normandie.utils.GlideUtils;
import com.xianzaishi.normandie.utils.SPUtils;
import com.xianzaishi.normandie.utils.ToastUtils;
import com.xianzaishi.normandie.utils.UiUtils;

import java.io.IOException;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by Administrator on 2016/8/31.
 */
public class CategoryIndicatorListViewAdapter extends ListBaseAdapter<LeafCategoryBean.ModuleBean.ItemsBean> {
    private List<CategoryListViewBean.ModuleBean.ItemsBean> list;
    private Context context;
    private OkHttpClient client= MyApplication.getOkHttpClient();
    private String uid;
    private int[] parentLocation,endLocation,startLocation;
    private RelativeLayout root;
    private TextView shoppingCount;//购物车商品数量指示器
    private int mType;//区分哪个页面调用的
    public CategoryIndicatorListViewAdapter(Context context,int mType, List list,int[] parentLocation,int[] endLocation,RelativeLayout root) {
        super(context, list);
        this.list=list;
        this.context=context;
        this.parentLocation=parentLocation;
        this.endLocation=endLocation;
        this.root=root;
        this.shoppingCount= (TextView) root.findViewById(R.id.shopping_trolley_count);
        this.mType=mType;
    }

    @Override
    public View getItemView(int i, View convertView, ViewGroup viewGroup) {
        ViewHolder viewHolder;
        if (convertView==null){
            convertView=getInflater().inflate(R.layout.category_listview_item,null);
            viewHolder=new ViewHolder(convertView);

            convertView.setTag(viewHolder);
        }else {
            viewHolder= (ViewHolder) convertView.getTag();
        }
        CategoryListViewBean.ModuleBean.ItemsBean bean=list.get(i);
        CategoryListViewBean.ModuleBean.ItemsBean.ItemSkuVOsBean.TagDetailBean tagDetailBean=bean.getItemSkuVOs().get(0).getTagDetail();

        //打标
        if (bean.getItemSkuVOs().get(0).getInventory()<=0){
            viewHolder.redSign.setVisibility(View.VISIBLE);
            viewHolder.redSign.setBackgroundResource(R.drawable.grey_sign);
            viewHolder.redSign.setText("卖光啦");

            viewHolder.shoppinTrolley.setEnabled(false);
            viewHolder.shoppinTrolley.setImageResource(R.mipmap.shoppingcargrey2x);
        }else if(!tagDetailBean.isMayPlus()){
            if (tagDetailBean.getChannel()==null||tagDetailBean.getTagContent()==null){
                viewHolder.redSign.setVisibility(View.GONE);
            }else {
                viewHolder.redSign.setVisibility(View.VISIBLE);
                viewHolder.redSign.setBackgroundResource(R.drawable.grey_sign);
                viewHolder.redSign.setText(tagDetailBean.getTagContent());
            }
            viewHolder.shoppinTrolley.setImageResource(R.mipmap.shoppingcargrey2x);
            viewHolder.shoppinTrolley.setEnabled(false);
        }else {
            viewHolder.shoppinTrolley.setImageResource(R.mipmap.gouwuche2x);
            viewHolder.shoppinTrolley.setEnabled(true);

            if (tagDetailBean.getChannel()==null||tagDetailBean.getTagContent()==null||tagDetailBean.getChannel().equals("2")){
                viewHolder.redSign.setVisibility(View.GONE);
            }else {
                viewHolder.redSign.setVisibility(View.VISIBLE);
                viewHolder.redSign.setBackgroundResource(R.drawable.red_sign);
                viewHolder.redSign.setText(tagDetailBean.getTagContent());
            }
        }
        GlideUtils.LoadImage(context,bean.getPicUrl(),viewHolder.imageView);
        viewHolder.title.setText(bean.getTitle());
        viewHolder.subtitle.setText(bean.getSubtitle());
        CategoryListViewBean.ModuleBean.ItemsBean.ItemSkuVOsBean itemBean=bean.getItemSkuVOs().get(0);
        viewHolder.standard.setText(itemBean.getSaleDetailInfo());
        String price="¥"+itemBean.getDiscountPriceYuanString();
        viewHolder.price.setText(price);
        viewHolder.discount.setText((CharSequence) itemBean.getPromotionInfo());
        //添加购物车按钮添加点击事件
        viewHolder.shoppinTrolley.setOnClickListener(new AdapterClick(i,viewHolder.imageView));
        return convertView;
    }

    private class ViewHolder{
        private ImageView imageView,shoppinTrolley;
        private TextView title,subtitle,standard,price,discount,redSign;
        public ViewHolder(View view){
            imageView= (ImageView) view.findViewById(R.id.categoryListView_picture);
            redSign= (TextView) view.findViewById(R.id.red_sign);
            shoppinTrolley= (ImageView) view.findViewById(R.id.categoryListView_shoppingTrolley);
            title= (TextView) view.findViewById(R.id.categoryListView_title);
            subtitle= (TextView) view.findViewById(R.id.categoryListView_subTitle);
            standard= (TextView) view.findViewById(R.id.categoryListView_standard);
            price= (TextView) view.findViewById(R.id.categoryListView_price);
            discount= (TextView) view.findViewById(R.id.categoryListView_discount);
        }
    }
    private void addTrolleyAnimator(ImageView imageView){
        /**
         *  添加加购的动画
         */
        startLocation=new int[2];
        imageView.getLocationInWindow(startLocation);
        /**
         *  构造一个用来执行动画的商品图
         */
        final CircleImageView goods=new CircleImageView(context);
        goods.setImageDrawable(imageView.getDrawable());
        RelativeLayout.LayoutParams params=new RelativeLayout.LayoutParams(150,150);
        root.addView(goods,params);
        /**
         * 开始掉落的商品的起始点：商品起始点-父布局起始点+该商品图片的一半
         */
        float fromX=startLocation[0]-parentLocation[0]+imageView.getWidth()/2;
        float fromY=startLocation[1]-parentLocation[1]+imageView.getHeight()/2;
        /**
         * 商品掉落后的终点坐标：购物车起始点-父布局起始点
         */
        float toX=endLocation[0]-parentLocation[0];
        float toY=endLocation[1]-parentLocation[1];
        final float[] currentLocation=new float[2];
        Path path=new Path();//开始绘制贝塞尔曲线
        path.moveTo(fromX,fromY);//移动到起始点
        path.quadTo((fromX+toX)/2,fromY,toX,toY);//使用贝塞尔曲线
        final PathMeasure pathMeasure=new PathMeasure(path,false);//用来计算贝塞尔曲线的曲线长度和贝塞尔曲线中间插值的坐标，
        // 如果是true，path会形成一个闭环
        ValueAnimator valueAnimator=ValueAnimator.ofFloat(0,pathMeasure.getLength());
        valueAnimator.setDuration(700);//设置属性动画
        valueAnimator.setInterpolator(new LinearInterpolator());//匀速线性插值器
        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                float value= (float) valueAnimator.getAnimatedValue();
                pathMeasure.getPosTan(value,currentLocation,null);
                goods.setTranslationX(currentLocation[0]);
                goods.setTranslationY(currentLocation[1]);
            }
        });
        valueAnimator.start();
        valueAnimator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                root.removeView(goods);
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
    }
    private class AdapterClick implements View.OnClickListener {
        private int position;
        private ImageView imageView;
        public AdapterClick(int position,ImageView imageView) {
            this.position = position;
            this.imageView=imageView;
        }

        @Override
        public void onClick(View view) {
            MobclickAgent.onEvent(context,"addToCart");
            if(mType==1){
                MobclickAgent.onEvent(context,"searchResultCart");//搜索结果页加购按钮的点击
            }else {
                MobclickAgent.onEvent(context,"sortCart");//分类二级加购按钮的点击
            }

            uid= SPUtils.getStringValue(MyApplication.getContext(), Contants.SP_NAME,Contants.UID,null);
            if (uid==null){
                context.startActivity(new Intent(context, NewLoginActivity.class));
            }else {
                //String url=String.format(Urls.ADD_TO_TROLLEY, uid,list.get(position).getItemId(),list.get(position).getSku());
                String itemId=String.valueOf(list.get(position).getItemId());
                RequestBody formBody=new FormBody.Builder()
                        .add("uid",uid)
                        .add("itemId",itemId)
                        .add("itemCount","1")
                        .add("skuId",list.get(position).getSku())
                        .build();
                Request request = new Request.Builder()
                        .url(Urls.ADD_TO_TROLLEY)
                        .post(formBody)
                        .build();
                client.newCall(request).enqueue(new Callback() {
                    @Override
                    public void onFailure(Call call, IOException e) {
                        UiUtils.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(context,"网络请求失败！",Toast.LENGTH_SHORT).show();
                            }
                        });
                    }

                    @Override
                    public void onResponse(Call call, Response response) throws IOException {
                        if (!response.isSuccessful()){return;}
                        final GeneralBean generalBean = (GeneralBean) GetBeanClass.getBean(response, GeneralBean.class);
                        UiUtils.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (generalBean!=null&&generalBean.success) {
                                    String countText=shoppingCount.getText().toString().equals("")?"0":shoppingCount.getText().toString();
                                    int count=Integer.valueOf(countText);
                                    if(count==0){
                                        shoppingCount.setVisibility(View.VISIBLE);
                                    }
                                    count=++count;
                                    shoppingCount.setText(String.valueOf(count));
                                    addTrolleyAnimator(imageView);
                                }else {
                                    ToastUtils.showToast("添加购物车失败");
                                }
                            }
                        });

                    }
                });
            }
        }
    }
}
