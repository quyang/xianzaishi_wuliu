package com.xianzaishi.normandie.adapter;

import android.app.Activity;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.TypedValue;
import android.widget.Toast;

import com.google.gson.Gson;
import com.xianzaishi.normandie.R;
import com.xianzaishi.normandie.bean.OrdersListBean01;
import com.xianzaishi.normandie.http.PostProtocol;
import com.xianzaishi.normandie.interfaces.OnDataFromPostServerListener;
import com.xianzaishi.normandie.utils.UiUtils;

import java.util.List;

import okhttp3.FormBody;

/**
 * Created by Administrator on 2016/9/17.
 */
public class SwipeListener {

    public boolean isLoaidng = false;

    public SwipeRefreshLayout mSwipeRefreshWidget;

    public Activity activity;

    public OrderAdapter mAdapter;

    private FormBody mBody;

    private String mUrl;

    private List<OrdersListBean01.DataBean.ObjectsBean> mInfro;

    private int mCurPager;
    private int mTotal;

    /**
     * @param infro    数据集合
     * @param adapter  recyclerview
     * @param swipe    SwipeRefreshLayout
     * @param activity Activity
     * @param body     FormBody
     * @param url      String url
     * @param curPager
     */
    public SwipeListener(List<OrdersListBean01.DataBean.ObjectsBean> infro, OrderAdapter adapter, SwipeRefreshLayout swipe,
                         Activity activity,
                         FormBody body, String url, Integer curPager, int total) {
        mSwipeRefreshWidget = swipe;
        this.activity = activity;
        mAdapter = adapter;
        mBody = body;
        mUrl = url;
        mInfro = infro;
        mCurPager = curPager;
        mTotal = total;
    }


    public void inimSwipeRefreshWidget() {


        //设置颜色
        mSwipeRefreshWidget.setColorSchemeResources(R.color.colorRed, R.color.colorSwipe, R.color.colorSwipe01);

        //设置刷新监听
        mSwipeRefreshWidget.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                Toast.makeText(UiUtils.getContext(), "inimSwipeRefreshWidget", Toast.LENGTH_LONG).show();

                if (isLoaidng) {
                    Toast.makeText(activity, "正在请求网络请稍等", Toast.LENGTH_LONG).show();
                    mSwipeRefreshWidget.setRefreshing(false);
                    return;
                }

                if (mInfro.size() > mTotal) {
                    Toast.makeText(UiUtils.getContext(), "没有更多数据", Toast.LENGTH_LONG).show();
                    mSwipeRefreshWidget.setRefreshing(false);
                    return;
                }

                isLoaidng = true;

                PostProtocol protocol = new PostProtocol();
                protocol.setUrl(mUrl);
                protocol.setRequestBody(mBody);
                protocol.getDataByPOST();
                protocol.setOnDataFromPostServerListener(new OnDataFromPostServerListener() {
                    @Override
                    public void onSuccess(String result) {
                        Toast.makeText(UiUtils.getContext(), "成功", Toast.LENGTH_LONG).show();
                        Gson gson = new Gson();
                        OrdersListBean01 ordersListBean = gson.fromJson(result, OrdersListBean01.class);
                        if (ordersListBean.success) {
                            List<OrdersListBean01.DataBean.ObjectsBean> objectsBeanList = ordersListBean.data.objects;
                            resetData(objectsBeanList);
                            mSwipeRefreshWidget.setRefreshing(false);
                        }

                        isLoaidng = false;
                    }

                    @Override
                    public void onFail() {
                        Toast.makeText(UiUtils.getContext(), "失败", Toast.LENGTH_LONG).show();
                        mSwipeRefreshWidget.setRefreshing(false);
                        isLoaidng = false;
                    }
                });
            }
        });

        // 第一次进入页面的时候显示加载进度条
        mSwipeRefreshWidget.setProgressViewOffset(false, 0, (int) TypedValue
                .applyDimension(TypedValue.COMPLEX_UNIT_DIP, 24, activity.getResources()
                        .getDisplayMetrics()));
//        recycler.setHasFixedSize(true);
    }

    //重新设配数据
    private void resetData(List<OrdersListBean01.DataBean.ObjectsBean> bean) {
        if (bean.size() != 0) {
            mCurPager++;
        }

        if (mInfro.addAll(bean)) {
//                OrderAdapter orderAdapter = new OrderAdapter(mInfro, (MyOrderActivity) activity);
//                recycler.setAdapter(orderAdapter);
            mAdapter.notifyDataSetChanged();
        }


    }
}
