package com.xianzaishi.normandie.adapter;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.xianzaishi.normandie.MainActivity;
import com.xianzaishi.normandie.R;
import com.xianzaishi.normandie.bean.Home2DownBean;
import com.xianzaishi.normandie.fragment.TasteNewFragment;
import com.xianzaishi.normandie.utils.GlideUtils;

import java.util.ArrayList;

/**
 * 新品尝鲜
 * Created by Administrator on 2016/11/25.
 */

public class TasteNewAdapter {
    private Home2DownBean.ModuleBean downBean;
    private View theNewView;
    private MainActivity mMainActivity;
    private ArrayList<Home2DownBean.ModuleBean.ItemsBean> list;
    private ArrayList<TasteNewFragment> fragments=new ArrayList<>();
    private LayoutInflater inflater;

    public TasteNewAdapter(Context context, Home2DownBean.ModuleBean downBean, View theNewView, FragmentManager fragmentManager) {
        this.downBean = downBean;
        this.theNewView = theNewView;
        this.mMainActivity= (MainActivity) context;
        this.inflater=LayoutInflater.from(context);

        ImageView banner= (ImageView) theNewView.findViewById(R.id.home_the_new_banner);
        GlideUtils.LoadImage(context,downBean.getPicList().get(0).getPicUrl(),banner);
        ViewPager viewPager= (ViewPager) theNewView.findViewById(R.id.home_the_new_food);
        final LinearLayout guideParent= (LinearLayout) theNewView.findViewById(R.id.home_the_new_indicator);

        int size=downBean.getItems().size();
        final int pageCount=size/5;
        for (int i = 0; i < pageCount; i++) {
            LinearLayout guide= (LinearLayout) inflater.inflate(R.layout.viewpager_guide_circle_yellow,null);
            guideParent.addView(guide);
            Bundle bundle=new Bundle();
            list=new ArrayList<>();
            int start=5*i;
            int end=5*(i+1);
            for (int i1 =start ; i1 <end; i1++) {
                list.add(downBean.getItems().get(i1));
            }

            TasteNewFragment fragment=new TasteNewFragment();
            bundle.putSerializable("tasteList",list);
            fragment.setArguments(bundle);
            fragments.add(fragment);
        }

        TasteNewViewPagerAdapter viewPagerAdapter=new TasteNewViewPagerAdapter(fragmentManager,fragments);
        viewPager.setAdapter(viewPagerAdapter);
        guideParent.getChildAt(0).findViewById(R.id.image_guide).setEnabled(false);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                for (int i = 0; i < pageCount; i++) {
                    if (i==position){
                        (guideParent.getChildAt(i).findViewById(R.id.image_guide)).setEnabled(false);
                    }else {
                        (guideParent.getChildAt(i).findViewById(R.id.image_guide)).setEnabled(true);
                    }
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }
}
