package com.xianzaishi.normandie.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.xianzaishi.normandie.R;
import com.xianzaishi.normandie.bean.SelectAddressBean;
import com.xianzaishi.normandie.bean.SelectAddressBean1;

import java.util.List;

/**
 * Created by Administrator on 2016/8/26.
 */
public class SelectAddressListAdapter extends ListBaseAdapter {
    private List<SelectAddressBean1.DataBean.AreaBean> datas;
    public SelectAddressListAdapter(Context context, List datas) {
        super(context, datas);
        this.datas=datas;
    }

    @Override
    public View getItemView(int i, View convertView, ViewGroup viewGroup) {
        ViewHolder viewHolder = null;
        if (convertView==null){
            convertView=getInflater().inflate(R.layout.select_address_item,null);
            viewHolder= new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        }else {
            viewHolder= (ViewHolder) convertView.getTag();
        }
        SelectAddressBean1.DataBean.AreaBean dataBean=datas.get(i);
        viewHolder.textView.setText(dataBean.getName());
        return convertView;
    }
    class ViewHolder{
        private TextView textView;
        public ViewHolder(View view) {
            textView= (TextView) view.findViewById(R.id.select_address_text);
        }
    }
}
