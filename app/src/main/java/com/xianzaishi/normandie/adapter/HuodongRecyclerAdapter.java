package com.xianzaishi.normandie.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.umeng.analytics.MobclickAgent;
import com.xianzaishi.normandie.GoodsDetailsActivity;
import com.xianzaishi.normandie.MyApplication;
import com.xianzaishi.normandie.NewLoginActivity;
import com.xianzaishi.normandie.R;
import com.xianzaishi.normandie.bean.HuoDongBean;
import com.xianzaishi.normandie.common.Urls;
import com.xianzaishi.normandie.global.Contants;
import com.xianzaishi.normandie.utils.GlideUtils;
import com.xianzaishi.normandie.utils.SPUtils;
import com.xianzaishi.normandie.utils.ToastUtils;
import com.xianzaishi.normandie.utils.UiUtils;

import java.io.IOException;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by Administrator on 2016/10/27.
 */

public class HuodongRecyclerAdapter extends RecyclerView.Adapter<HuodongRecyclerAdapter.ViewHolder> {
    private Context context;
    private List<HuoDongBean.ModuleBean.ItemsBean> itemsBeans;
    private OkHttpClient client=MyApplication.getOkHttpClient();
    public HuodongRecyclerAdapter(Context context,List<HuoDongBean.ModuleBean.ItemsBean> itemsBeans){
        this.context=context;
        this.itemsBeans=itemsBeans;
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.huodong_recycler_view_item,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        //获取数据
        HuoDongBean.ModuleBean.ItemsBean bean1 = itemsBeans.get(position);
        HuoDongBean.ModuleBean.ItemsBean.ItemSkuVOsBean.TagDetailBean tagDetailBean=bean1.itemSkuVOs.get(0).tagDetail;

        final String itemId = bean1.itemId;//商品id
        final String sku=bean1.sku;
        String url = bean1.picUrl;
        String name = bean1.title;
        String subName = bean1.itemSkuVOs.get(0).saleDetailInfo;
        String priceYuanString = bean1.priceYuanString;
        String discountPriceYuanString = bean1.discountPriceYuanString;

        //打标
        if (bean1.itemSkuVOs.get(0).inventory<=0){
            holder.redSign.setVisibility(View.VISIBLE);
            holder.redSign.setBackgroundResource(R.drawable.grey_sign);
            holder.redSign.setText("卖光啦");

            holder.iv_car.setEnabled(false);
            holder.iv_car.setBackgroundResource(R.mipmap.shoppingcargrey2x);
        }else if(!tagDetailBean.mayPlus){
            if (tagDetailBean.channel==null||tagDetailBean.tagContent==null){
                holder.redSign.setVisibility(View.GONE);
            }else {
                holder.redSign.setVisibility(View.VISIBLE);
                holder.redSign.setBackgroundResource(R.drawable.grey_sign);
                holder.redSign.setText(tagDetailBean.tagContent);
            }
            holder.iv_car.setEnabled(false);
            holder.iv_car.setBackgroundResource(R.mipmap.shoppingcargrey2x);
        }else {
            holder.iv_car.setEnabled(true);
            holder.iv_car.setBackgroundResource(R.mipmap.gouwuche2x);

            if (tagDetailBean.channel==null||tagDetailBean.tagContent==null||tagDetailBean.channel.equals("2")){
                holder.redSign.setVisibility(View.GONE);
            }else {
                holder.redSign.setVisibility(View.VISIBLE);
                holder.redSign.setBackgroundResource(R.drawable.red_sign);
                holder.redSign.setText(tagDetailBean.tagContent);
            }
        }

        GlideUtils.LoadImage(context, url, holder.icon);
        holder.title.setText(name);
        holder.des.setText(subName);
        holder.priceNow.setText("¥"+discountPriceYuanString);
        if(priceYuanString.equals(discountPriceYuanString)){
            holder.priceOld.setVisibility(View.INVISIBLE);
        }
        holder.priceOld.setText("原价："+priceYuanString);
        holder.priceOld.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG);

        holder.iv_car.setOnClickListener(new Click(position,itemId,sku));
        holder.linearLayoutleft.setOnClickListener(new Click(position,itemId,sku));
    }

    @Override
    public int getItemCount() {
        return itemsBeans==null?0:itemsBeans.size();
    }

    private class Click implements View.OnClickListener{
        private int position;
        private String itemId,sku;
        public Click(int position,String itemId,String sku) {
            this.position = position;
            this.itemId=itemId;
            this.sku=sku;
        }
        @Override
        public void onClick(View view) {
            switch (view.getId()){
                case R.id.ll_left:
                    Intent intent1 = new Intent(context, GoodsDetailsActivity.class);
                    intent1.putExtra(Contants.ITEM_ID,Integer.valueOf(itemId));
                    intent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    UiUtils.getContext().startActivity(intent1);
                    break;
                case R.id.iv_car:
                    addIntoCar(itemId,sku);
                    break;
            }
        }
    }

    private void addIntoCar(String itemId,String sku) {
        MobclickAgent.onEvent(context,"eventCart");//活动页加购按钮点击次数统计

        String uid= SPUtils.getStringValue(MyApplication.getContext(), Contants.SP_NAME,Contants.UID,null);
        if(uid==null){
            context.startActivity(new Intent(context, NewLoginActivity.class));
        }else {
            RequestBody formBody = new FormBody.Builder()
                    .add("uid", uid)
                    .add("itemId", itemId)
                    .add("itemCount", "1")
                    .add("skuId", sku)
                    .build();
            Request request = new Request.Builder()
                    .url(Urls.ADD_TO_TROLLEY)
                    .post(formBody)
                    .build();
            client.newCall(request).enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    UiUtils.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(context, "网络请求失败！", Toast.LENGTH_SHORT).show();
                        }
                    });
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    UiUtils.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            ToastUtils.showToast("加购成功");
                        }
                    });
                }
            });
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView icon,iv_car;
        private TextView title,des,priceNow,priceOld,redSign;
        private LinearLayout linearLayoutleft;
        public ViewHolder(View itemView) {
            super(itemView);
            //找到组件
            icon = (ImageView) itemView.findViewById(R.id.iv_icon);
            redSign= (TextView) itemView.findViewById(R.id.red_sign);
            title = (TextView) itemView.findViewById(R.id.tv_name);
            des = (TextView) itemView.findViewById(R.id.tv_des);
            priceNow = (TextView) itemView.findViewById(R.id.tv_price_now);
            priceOld = (TextView) itemView.findViewById(R.id.tv_old_now);
            iv_car = (ImageView) itemView.findViewById(R.id.iv_car);
            linearLayoutleft= (LinearLayout) itemView.findViewById(R.id.ll_left);
        }
    }
}
