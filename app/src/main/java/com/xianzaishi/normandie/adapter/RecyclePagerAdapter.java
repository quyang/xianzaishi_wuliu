package com.xianzaishi.normandie.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.List;

/**
 * Created by Administrator on 2016/7/20.
 */
public class RecyclePagerAdapter extends PagerAdapter{

    private List<ImageView> imageViews;
    private Context context;
    public RecyclePagerAdapter(Context context,List<ImageView> imageViews) {
        this.context=context;
        this.imageViews=imageViews;
    }

    @Override
    public int getCount() {
        return Integer.MAX_VALUE;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view==object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        position%=imageViews.size();
        ImageView imageView=imageViews.get(position);
        final int finalPosition = position;
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(context,"我是第"+ (finalPosition+1) +"页",Toast.LENGTH_SHORT).show();
            }
        });
        container.addView(imageView);
        return imageViews.get(position);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        position%=imageViews.size();
        container.removeView(imageViews.get(position));
    }
}
