package com.xianzaishi.normandie.adapter;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.ArrayList;

/**
 * Created by ShenLang on 2016/11/23.
 * viewpager适配器
 */

public class TasteNewViewPagerAdapter<T> extends FragmentStatePagerAdapter {
    private ArrayList<T> fragments;
    public TasteNewViewPagerAdapter(FragmentManager fm, ArrayList<T> fragments) {
        super(fm);
        this.fragments=fragments;
    }

    @Override
    public Fragment getItem(int position) {
        return (Fragment) fragments.get(position);
    }

    @Override
    public int getCount() {
        return fragments==null?0:fragments.size();
    }


}
