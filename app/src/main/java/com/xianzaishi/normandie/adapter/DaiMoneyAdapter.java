package com.xianzaishi.normandie.adapter;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.xianzaishi.normandie.MyOrderActivity;
import com.xianzaishi.normandie.OrderDetailsActivity;
import com.xianzaishi.normandie.R;
import com.xianzaishi.normandie.bean.OrdersListBean;
import com.xianzaishi.normandie.utils.UiUtils;

import java.util.List;

/**
 * quyang
 * 我的订单的待配送的adapter
 * Created by Administrator on 2016/9/3.
 */
public class DaiMoneyAdapter extends RecyclerView.Adapter<DaiMoneyAdapter.MyViewHoldr>{

    public MyOrderActivity mActivity;
    public List<OrdersListBean.DataBean> mAllOrderInfo;


    public DaiMoneyAdapter(List<OrdersListBean.DataBean> allOrderInfo, MyOrderActivity activity) {
        mAllOrderInfo = allOrderInfo;
        mActivity = activity;
    }

    @Override
    public MyViewHoldr onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MyViewHoldr(View.inflate(UiUtils.getContext(), R.layout.listview_item_order, null));
    }

    @Override
    public void onBindViewHolder(MyViewHoldr holder, int position) {
        OrdersListBean.DataBean bean = mAllOrderInfo.get(position);


        holder.tvData.setText("2018-8-9");
        holder.tvTime.setText("19:23:09");
        holder.tvDengMoney.setText(bean.statusString);
        holder.tvPrice.setText(bean.payAmount);

        List<OrdersListBean.ItemsBean> items = bean.items;
        for (int i = 0; i < items.size(); i++) {

            ImageView imageView = new ImageView(UiUtils.getContext());
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(118, 118);
            params.rightMargin = 24;

            imageView.setLayoutParams(params);

            Glide.with(UiUtils.getContext()).load(items.get(i).iconUrl).override(118, 118).diskCacheStrategy(DiskCacheStrategy.ALL).into(imageView);

            holder.llContainer.addView(imageView);
        }


        holder.zhifu.setText("去支付");
        holder.cancel.setText("取消订单");


        holder.cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                newDialog();

            }

        });


    }

    @Override
    public int getItemCount() {
        return mAllOrderInfo.size();
    }

   public class MyViewHoldr extends RecyclerView.ViewHolder {

        TextView tvData;
        TextView tvTime;
        TextView tvDengMoney;
        LinearLayout llContainer;
        TextView tvPrice;
        Button cancel;
        Button zhifu;
        private final LinearLayout llFather;
        private final HorizontalScrollView scrllview;

        public MyViewHoldr(View itemView) {
            super(itemView);
            scrllview = (HorizontalScrollView) itemView.findViewById(R.id.scrollView_order);
            llFather = (LinearLayout) itemView.findViewById(R.id.ll_father);
            tvData = (TextView) itemView.findViewById(R.id.tv_data);
            tvTime = (TextView) itemView.findViewById(R.id.tv_time);
            tvDengMoney = (TextView) itemView.findViewById(R.id.tv_deng_money);
            llContainer = (LinearLayout) itemView.findViewById(R.id.llContainr);
            tvPrice = (TextView) itemView.findViewById(R.id.tv_price);
            cancel = (Button) itemView.findViewById(R.id.btn_cancel);
            zhifu = (Button) itemView.findViewById(R.id.btn_zhifu);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(mActivity, OrderDetailsActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    UiUtils.getContext().startActivity(intent);
                }
            });




        }
    }


//    public class MyOnClickListener implements View.OnClickListener {
//        @Override
//        public void onClick(View view) {
//            Toast.makeText(UiUtils.getContext(), "调到订单详情", Toast.LENGTH_LONG).show();
//
//        }
//    }


    private void newDialog() {
        // 获取构建器对象
        AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);

        // 给对话框设置自定义对话框
        final View view = View.inflate(UiUtils.getContext(), R.layout.alertdialog, null);   //填充布局, 这个password是一个布局文件


        TextView ok = (TextView) view.findViewById(R.id.tv_ok);
        TextView consider = (TextView) view.findViewById(R.id.tv_condider);


        // 设置布局
        builder.setView(view);  //将填充好的布局设置给构造器

        // 构建一个空的对话框
        final AlertDialog dialog = builder.create();  //创建对话框

        dialog.setCancelable(false);

        //后面给确定和取消按钮设置监听,利用一般的onclicklistener;
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                newAnotherDialog();
            }
        });

        consider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.show();


    }

    private void newAnotherDialog() {

        // 获取构建器对象
        AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);

        // 给对话框设置自定义对话框
        final View view = View.inflate(UiUtils.getContext(), R.layout.dialog_order_cancled, null);   //填充布局, 这个password是一个布局文件
        Button ok = (Button) view.findViewById(R.id.btn_ok);

        changeTextColor(view);


        // 设置布局
        builder.setView(view);  //将填充好的布局设置给构造器

        // 构建一个空的对话框
        final AlertDialog dialog = builder.create();  //创建对话框

        dialog.setCancelable(false);

        //后面给确定和取消按钮设置监听,利用一般的onclicklistener;
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });


        dialog.show();

    }


    public void changeTextColor(View view) {
        String str ="退款在3~5个工作日";
        int bstart=str.indexOf("3~5个工作日");
        int bend=bstart+"3~5个工作日".length();
        int fstart=str.indexOf("3~5个工作日");
        int fend=fstart+"3~5个工作日".length();
        SpannableStringBuilder style=new SpannableStringBuilder(str);
        style.setSpan(new ForegroundColorSpan(Color.RED),fstart,fend, Spannable.SPAN_EXCLUSIVE_INCLUSIVE);
        TextView tvColor=(TextView)view.findViewById(R.id.tv_tuiqian);
        tvColor.setText(style);
    }



}
