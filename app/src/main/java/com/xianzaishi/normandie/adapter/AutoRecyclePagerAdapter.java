package com.xianzaishi.normandie.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.umeng.analytics.MobclickAgent;
import com.xianzaishi.normandie.GoodsDetailsActivity;
import com.xianzaishi.normandie.HuoDongActivity;
import com.xianzaishi.normandie.bean.HomeTopBean;
import com.xianzaishi.normandie.global.Contants;
import com.xianzaishi.normandie.utils.GlideUtils;
import com.xianzaishi.normandie.utils.UiUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2016/7/20.
 */
public class AutoRecyclePagerAdapter extends PagerAdapter {

    private List<HomeTopBean.ModuleBean.PicListBean> picList ;
    private List<ImageView> imageViews=new ArrayList<>();
    private Context context;

    public AutoRecyclePagerAdapter(Context context, List<HomeTopBean.ModuleBean.PicListBean> picList) {
        this.context = context;
        this.picList = picList;
        Log.e("sss","=="+picList.size());
        for(HomeTopBean.ModuleBean.PicListBean url:picList){
            ImageView imageView=new ImageView(context);
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            ViewPager.LayoutParams params= new ViewPager.LayoutParams();
            params.width= ViewPager.LayoutParams.MATCH_PARENT;
            params.height=ViewPager.LayoutParams.MATCH_PARENT;
            imageView.setLayoutParams(params);
            GlideUtils.LoadImage(context,url.picUrl,imageView);
            imageViews.add(imageView);
        }

    }

    @Override
    public int getCount() {
        return Integer.MAX_VALUE;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        position %= picList.size();

        ImageView imageView=imageViews.get(position);
        if (imageView.getParent()!=null){

        }else {
            container.addView(imageView);
        }
        /*container.removeView(imageView);
        container.addView(imageView);*/
        imageView.setOnClickListener(new ViewPagerClickListener(position));
        return imageView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
//        position%=imageViews.size();
        if (picList.size()>3) {
            container.removeView((View) object);
        }
    }

    private class ViewPagerClickListener implements View.OnClickListener {
        private int position;
        public ViewPagerClickListener(int position){
            this.position=position;
        }
        @Override
        public void onClick(View view) {
            switch (picList.get(position).targetType){
                case 0://不能点击
                    break;
                case 1://活动页
                    Intent intent = new Intent(context, HuoDongActivity.class);
                    String targetId=picList.get(position).targetId;
                    intent.putExtra("targetId", targetId);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                    break;
                case 2://商品详情页
                    MobclickAgent.onEvent(context,"tapItem");

                    Intent intent1 = new Intent(context, GoodsDetailsActivity.class);
                    String targetId1=picList.get(position).targetId;
                    intent1.putExtra(Contants.ITEM_ID, Integer.valueOf(targetId1));
                    intent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent1);
                    break;
            }
        }
    }
}
