package com.xianzaishi.normandie.adapter;

import com.squareup.okhttp.Callback;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.xianzaishi.normandie.utils.ToastUtils;

import java.io.IOException;

/**
 * quyang
 * 提交zxing的字符串
 * Created by Administrator on 16/9/26.
 */

public class PostString {

    public static final MediaType JSON
            = MediaType.parse("application/json; charset=utf-8");


    /**
     * submit a string
     *
     * @param string String the string wo want to submit to the server
     */
    public static void submitString(String url, String string) {
        OkHttpClient okHttpClient = new OkHttpClient();
//        String str = "{\n" +
//                "    \"pageId\": \"1\",\n" +
//                "    \"hasAllFloor\": \"true\"\n" +
//                "}";

        RequestBody requestBody = RequestBody.create(JSON, string);

        Request request = new Request.Builder()
                .post(requestBody)
                .url(url)
                .build();

        okHttpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {
                ToastUtils.showToast("请求失败");
            }

            @Override
            public void onResponse(com.squareup.okhttp.Response response) throws IOException {
                String string = response.body().string();
            }
        });

    }
}
