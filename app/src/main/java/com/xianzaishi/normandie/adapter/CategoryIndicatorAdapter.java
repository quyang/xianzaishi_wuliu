package com.xianzaishi.normandie.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.xianzaishi.normandie.R;
import com.xianzaishi.normandie.bean.LeafCategoryBean;
import com.xianzaishi.normandie.utils.UiUtils;

import java.util.List;

/**
 * Created by Administrator on 2016/8/8.
 */
public class CategoryIndicatorAdapter extends RecyclerView.Adapter<CategoryIndicatorAdapter.ViewHolder> {
    private LayoutInflater inflater;
    private List<LeafCategoryBean.ModuleBean.CategoriesBean> list;
    public interface OnItemClickListener {
        void OnItemClickListener(View view, int position);
    }
    public CategoryIndicatorAdapter(Context context, List list) {
        inflater= UiUtils.getLayoutInflater();
        this.list=list;
    }

    private OnItemClickListener onItemClickListener;
    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }
    public List<LeafCategoryBean.ModuleBean.CategoriesBean> getList(){
        return list;
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view=inflater.inflate(R.layout.classification_indicator_item,parent,false);
        ViewHolder viewHolder=new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        LeafCategoryBean.ModuleBean.CategoriesBean categoriesBean=list.get(position);
        holder.tab.setSelected(categoriesBean.isSelect());
        holder.tab.setText(categoriesBean.getCatName());
        if (onItemClickListener!=null){
            holder.tab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int pos=holder.getLayoutPosition();
                    onItemClickListener.OnItemClickListener(view,pos);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return list==null?0:list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tab;
        public ViewHolder(View itemView) {
            super(itemView);
            tab= (TextView) itemView.findViewById(R.id.text_tab);
        }
    }
}
