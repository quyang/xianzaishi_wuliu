package com.xianzaishi.normandie.adapter;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.content.Intent;
import android.graphics.Path;
import android.graphics.PathMeasure;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.umeng.analytics.MobclickAgent;
import com.xianzaishi.normandie.MainActivity;
import com.xianzaishi.normandie.MyApplication;
import com.xianzaishi.normandie.NewLoginActivity;
import com.xianzaishi.normandie.R;
import com.xianzaishi.normandie.bean.HomeTopBean;
import com.xianzaishi.normandie.common.Urls;
import com.xianzaishi.normandie.customs.CircleImageView;
import com.xianzaishi.normandie.global.Contants;
import com.xianzaishi.normandie.utils.DividerLinearItemDecoration;
import com.xianzaishi.normandie.utils.SPUtils;
import com.xianzaishi.normandie.utils.UiUtils;

import java.io.IOException;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * 文字加列表adapter
 * quyang
 * Created by Administrator on 2016/9/19.
 */
public class TextLieBiaoAdapter {

    private TextView shoppingCount;//购物车商品数量指示器
    private List<HomeTopBean.ModuleBean.ItemsBean> items1;
    private OkHttpClient client= MyApplication.getOkHttpClient();
    private MainActivity activity;
    private int[] parentLocation,endLocation,startLocation;
    private RelativeLayout rootView;
    private PathMeasure pathMeasure;
    public void set(View view1, int i, List<HomeTopBean.ModuleBean> mModuleBeen, MainActivity activity) {

        this.activity=activity;
        //找到组件
        TextView tv = (TextView) view1.findViewById(R.id.tv_xianshi_buy);
        RecyclerView recyclerviewThree = (RecyclerView) view1.findViewById(R.id.receive_xianshi);
        shoppingCount= (TextView) activity.findViewById(R.id.shopping_trolley_count);
        tv.setText((CharSequence) mModuleBeen.get(i).title);

        rootView= (RelativeLayout) activity.findViewById(R.id.main_root_view);
        RadioButton shoppingCar= (RadioButton) activity.findViewById(R.id.shoppingTrolley_button);
        parentLocation=new int[2];
        endLocation=new int[2];
        rootView.getLocationInWindow(parentLocation);
        shoppingCar.getLocationInWindow(endLocation);

        HomeTopBean.ModuleBean bean1 = mModuleBeen.get(i);
        items1 = bean1.items;

        recyclerviewThree.setLayoutManager(new LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false));
        recyclerviewThree.addItemDecoration(new DividerLinearItemDecoration(activity, LinearLayoutManager.HORIZONTAL));
        FoorThreeAdapter adapter=new FoorThreeAdapter(items1,activity);
        recyclerviewThree.setAdapter(adapter);
        adapter.setAddIntoShoppingCarListener(new FoorThreeAdapter.AddIntoShoppingCarListener() {
            @Override
            public void addIntoShoppingCar(View view, int position,ImageView imageView) {
                addIntoCar(position,imageView);
            }
        });
    }

    private void addTrolleyAnimator(ImageView imageView){
        /**
         *  添加加购的动画
         */
        startLocation=new int[2];
        imageView.getLocationInWindow(startLocation);
        /**
         *  构造一个用来执行动画的商品图
         */
        final CircleImageView goods=new CircleImageView(activity);
        goods.setImageDrawable(imageView.getDrawable());
        LinearLayout.LayoutParams params=new LinearLayout.LayoutParams(150,150);
        rootView.addView(goods,params);
        /**
         * 开始掉落的商品的起始点：商品起始点-父布局起始点+该商品图片的一半
         */
        float fromX=startLocation[0]-parentLocation[0]+imageView.getWidth()/2;
        float fromY=startLocation[1]-parentLocation[1]+imageView.getHeight()/2;
        /**
         * 商品掉落后的终点坐标：购物车起始点-父布局起始点
         */
        float toX=endLocation[0]-parentLocation[0];
        float toY=endLocation[1]-parentLocation[1];
        final float[] currentLocation=new float[2];
        Path path=new Path();//开始绘制贝塞尔曲线
        path.moveTo(fromX,fromY);//移动到起始点
        path.quadTo((fromX+toX)/2,fromY,toX,toY);//使用贝塞尔曲线
        pathMeasure=new PathMeasure(path,false);//用来计算贝塞尔曲线的曲线长度和贝塞尔曲线中间插值的坐标，
        // 如果是true，path会形成一个闭环
        ValueAnimator valueAnimator=ValueAnimator.ofFloat(0,pathMeasure.getLength());
        valueAnimator.setDuration(500);//设置属性动画
        valueAnimator.setInterpolator(new LinearInterpolator());//匀速线性插值器
        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                float value= (float) valueAnimator.getAnimatedValue();
                pathMeasure.getPosTan(value,currentLocation,null);
                goods.setTranslationX(currentLocation[0]);
                goods.setTranslationY(currentLocation[1]);
            }
        });
        valueAnimator.start();
        valueAnimator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }
            @Override
            public void onAnimationEnd(Animator animator) {
                rootView.removeView(goods);
            }
            @Override
            public void onAnimationCancel(Animator animator) {
            }
            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
    }

    private void addIntoCar(int position, final ImageView imageView) {
        MobclickAgent.onEvent(activity,"homeSecondSectionCart");//首页第二分区购物车按钮
        MobclickAgent.onEvent(activity,"addToCart");

        String uid= SPUtils.getStringValue(MyApplication.getContext(), Contants.SP_NAME,Contants.UID,null);
        if(uid==null){
            activity.startActivity(new Intent(activity, NewLoginActivity.class));
        }else {
            String itemId = String.valueOf(items1.get(position).itemId);
            RequestBody formBody = new FormBody.Builder()
                    .add("uid", uid)
                    .add("itemId", itemId)
                    .add("itemCount", "1")
                    .add("skuId", items1.get(position).sku)
                    .build();
            Request request = new Request.Builder()
                    .url(Urls.ADD_TO_TROLLEY)
                    .post(formBody)
                    .build();
            client.newCall(request).enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    UiUtils.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(activity, "网络请求失败！", Toast.LENGTH_SHORT).show();
                        }
                    });
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    UiUtils.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            SPUtils.putBooleanValue(MyApplication.getContext(), Contants.SP_NAME, "countChanged", true);
                            String countText = shoppingCount.getText().toString().equals("") ? "0" : shoppingCount.getText().toString();
                            int count = Integer.valueOf(countText);
                            if (count == 0) {
                                shoppingCount.setVisibility(View.VISIBLE);
                            }
                            count = ++count;
                            shoppingCount.setText(String.valueOf(count));
                            addTrolleyAnimator(imageView);
                        }
                    });
                }
            });
        }
    }
}
