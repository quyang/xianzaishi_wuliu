package com.xianzaishi.normandie.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.xianzaishi.normandie.R;

/**
 * Created by Administrator on 2016/7/20.
 */
public class HistorySearchRecyclerAdapter extends RecyclerView.Adapter<HistorySearchRecyclerAdapter.ViewHolder> {
    private Context context;
    private LayoutInflater inflater;
    private String[] array;

    public HistorySearchRecyclerAdapter(Context context,String[] array) {
        this.context = context;
        this.array=array;
        inflater=LayoutInflater.from(context);
    }

    /**
     *  自定义点击item的接口
     */
    public interface OnHistoryItemClickListener{
        void onClick(View view,int position);
    }
    private OnHistoryItemClickListener onHistoryItemClickListener;

    public void setOnHistoryItemClickListener(OnHistoryItemClickListener onHistoryItemClickListener) {
        this.onHistoryItemClickListener = onHistoryItemClickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view=inflater.inflate(R.layout.history_search,parent,false);
        HistorySearchRecyclerAdapter.ViewHolder viewHolder=new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

            holder.textView.setText(array[position]);
        if (onHistoryItemClickListener!=null){
            final int p=position;
            holder.textView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onHistoryItemClickListener.onClick(view,p);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return array.length;
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView textView;
        public ViewHolder(View view) {
            super(view);
            textView= (TextView) view.findViewById(R.id.history_search_text);
        }
    }
}
