package com.xianzaishi.normandie.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.xianzaishi.normandie.R;
import com.xianzaishi.normandie.bean.AchievementBean;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2016/12/22.
 */

public class AchievementListViewAdapter extends BasicAdapter<AchievementBean.ModuleBean.AchievementDetailBean> {
    private int height;
    private LayoutInflater inflater;
    private List<AchievementBean.ModuleBean.AchievementDetailBean> datas;
    public AchievementListViewAdapter(List<AchievementBean.ModuleBean.AchievementDetailBean> datas, Context context,int height) {
        super(datas, context);
        inflater=LayoutInflater.from(context);
        this.height=height;
        this.datas=datas;
    }


    @Override
    public View getItemView(int position, View convertView, ViewGroup viewGroup) {
        ViewHolder viewHolder;
        if (convertView==null){
            convertView=inflater.inflate(R.layout.achievement_list_item,null);
            viewHolder=new ViewHolder(convertView);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        LinearLayout.LayoutParams params= (LinearLayout.LayoutParams) viewHolder.root.getLayoutParams();
        params.height=height;
        viewHolder.root.setLayoutParams(params);
        AchievementBean.ModuleBean.AchievementDetailBean detailBean=datas.get(position);
        String phone=detailBean.getPhone();
        String showPhone=phone.replace(phone.substring(3,8),"*****");
        viewHolder.phone.setText(showPhone);
        viewHolder.state.setText(detailBean.getStatuString());
        return convertView;
    }

    public static class ViewHolder {
        private TextView phone,state;
        private LinearLayout root;
        public ViewHolder(View view) {
            state= (TextView) view.findViewById(R.id.achievement_item_state);
            phone= (TextView) view.findViewById(R.id.achievement_item_phone);
            root= (LinearLayout) view.findViewById(R.id.achievement_item_root);
        }
    }
}
