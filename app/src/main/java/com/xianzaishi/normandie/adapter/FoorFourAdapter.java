package com.xianzaishi.normandie.adapter;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.xianzaishi.normandie.R;
import com.xianzaishi.normandie.bean.HomeTopBean;
import com.xianzaishi.normandie.utils.GlideUtils;
import com.xianzaishi.normandie.utils.UiUtils;

import java.util.List;

/**
 * quyang
 * <p/> 4lou 一图加列表
 * Created by Administrator on 2016/9/18.
 */
public class FoorFourAdapter extends RecyclerView.Adapter<FoorFourAdapter.MyViewHolder> {


    private List<HomeTopBean.ModuleBean.ItemsBean> itemsBeen4;

    //constructor
    public FoorFourAdapter(List<HomeTopBean.ModuleBean.ItemsBean> itemsBeen4) {
        this.itemsBeen4 = itemsBeen4;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MyViewHolder(UiUtils.getLayoutInflater().inflate(R.layout.new_foor_four_item, null));
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        HomeTopBean.ModuleBean.ItemsBean itemsBean = itemsBeen4.get(position);
        List<HomeTopBean.ModuleBean.ItemsBean.ItemSkuVOsBean> os = itemsBean.itemSkuVOs;
        HomeTopBean.ModuleBean.ItemsBean.ItemSkuVOsBean bean = os.get(0);
        String info = "¥"+itemsBean.discountPriceYuanString+"/"+bean.saleDetailInfo;

        GlideUtils.LoadImage(UiUtils.getContext(), itemsBean.picUrl, holder.icon);

        String title = itemsBean.title;
        if (title.length() > 10) {
            title = title.substring(0, 9);
        }

        holder.name.setText(title);

        String str = itemsBean.subtitle;

        String[] split = str.split("/");
        String pre = split[0];

        int bstart = str.indexOf(pre + "");
        int bend = bstart + pre.length();
        int fstart = str.indexOf(pre);
        int fend = fstart + pre.length();
        SpannableStringBuilder style = new SpannableStringBuilder(str);
        style.setSpan(new ForegroundColorSpan(Color.RED), fstart, fend, Spannable.SPAN_EXCLUSIVE_INCLUSIVE);

        holder.des.setText(info);


        holder.mRoot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });


    }

    @Override
    public int getItemCount() {
        return itemsBeen4 == null ? 0 : itemsBeen4.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private final ImageView icon;
        private final TextView name;
        private final TextView des;
        private final LinearLayout mRoot;

        public MyViewHolder(View itemView) {
            super(itemView);
            icon = (ImageView) itemView.findViewById(R.id.iv_icon);
            name = (TextView) itemView.findViewById(R.id.name);
            des = (TextView) itemView.findViewById(R.id.des);
            mRoot = (LinearLayout) itemView.findViewById(R.id.root);

        }
    }
}
