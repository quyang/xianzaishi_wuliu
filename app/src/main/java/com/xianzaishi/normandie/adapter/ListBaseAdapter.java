package com.xianzaishi.normandie.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.xianzaishi.normandie.utils.UiUtils;

import java.util.List;

/**
 * Created by Administrator on 2016/8/10. listView的基类Adapter
 */
public abstract class ListBaseAdapter<T> extends BaseAdapter {
    private Context context;
    private List<T> datas;
    private LayoutInflater inflater;
    public ListBaseAdapter(Context context, List<T> datas){
        this.context=context;
        this.datas=datas;
        inflater= UiUtils.getLayoutInflater();
    }
    @Override
    public int getCount() {
        return datas==null?0:datas.size();
    }

    @Override
    public Object getItem(int i) {
        return datas.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
        return getItemView(i,convertView,viewGroup);
    }

    public void addDatas(List<T> list){
        datas.addAll(list);
        notifyDataSetChanged();
    }

    public void clearDatas(){
        datas.clear();
        notifyDataSetChanged();
    }

    public LayoutInflater getInflater() {
        return inflater;
    }

    public abstract View getItemView(int i, View convertView, ViewGroup viewGroup);
}
