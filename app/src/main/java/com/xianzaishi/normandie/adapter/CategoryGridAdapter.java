package com.xianzaishi.normandie.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.xianzaishi.normandie.R;
import com.xianzaishi.normandie.bean.CategoryBean;
import com.xianzaishi.normandie.utils.GlideUtils;

import java.util.List;

/**
 * Created by Administrator on 2016/8/8.
 */
public class CategoryGridAdapter extends RecyclerView.Adapter<CategoryGridAdapter.ViewHolder> {
    private Context context;
    private List<CategoryBean.ModuleBean> list;
    public CategoryGridAdapter(Context context,List list) {
        this.context=context;
        this.list=list;
    }

    /**
     * 自定义点击回调接口
     */
    public interface OnItemClickListener {
        void OnItemClickListener(View view, int position);
    }

    private OnItemClickListener onItemClickListener;

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view=LayoutInflater.from(context).inflate(R.layout.classification_item,parent,false);
        ViewHolder viewHolder=new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder,int position) {
        CategoryBean.ModuleBean moduleBean=list.get(position);
        holder.title.setText(moduleBean.getName());
        holder.subtile.setText(moduleBean.getMemo());
        GlideUtils.LoadImage(context,moduleBean.getPic(),holder.imageView);
        if(onItemClickListener!=null) {
            holder.linearLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int pos=holder.getLayoutPosition();
                    onItemClickListener.OnItemClickListener(view,pos);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return list==null?0:list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView title,subtile;
        private ImageView imageView;
        private LinearLayout linearLayout;
        public ViewHolder(View itemView) {
            super(itemView);
            title= (TextView) itemView.findViewById(R.id.classification_title);
            subtile= (TextView) itemView.findViewById(R.id.classification_subTitle);
            imageView= (ImageView) itemView.findViewById(R.id.classification_image);
            linearLayout= (LinearLayout) itemView.findViewById(R.id.classification_item);
        }
    }
}
