package com.xianzaishi.normandie.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.xianzaishi.normandie.R;


/**
 * Created by Administrator on 2016/7/20.
 */
public class LazyoneRecyclerAdapter extends RecyclerView.Adapter<LazyoneRecyclerAdapter.ViewHolder> {
    private Context context;
    private LayoutInflater inflater;

    public LazyoneRecyclerAdapter(Context context) {
        this.context = context;
        inflater=LayoutInflater.from(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view=inflater.inflate(R.layout.recycler_view_item,parent,false);
        LazyoneRecyclerAdapter.ViewHolder viewHolder=new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.imageView.setImageResource(R.mipmap.ic_launcher);
        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(context,"我是第"+position+"项",Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return 10;
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private ImageView imageView;
        public ViewHolder(View view) {
            super(view);
            imageView= (ImageView) view.findViewById(R.id.recycler_view_item);
        }
    }
}
