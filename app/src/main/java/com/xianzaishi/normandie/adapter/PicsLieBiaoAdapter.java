package com.xianzaishi.normandie.adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.xianzaishi.normandie.GoodsDetailsActivity;
import com.xianzaishi.normandie.HuoDongActivity;
import com.xianzaishi.normandie.R;
import com.xianzaishi.normandie.bean.HomeTopBean;
import com.xianzaishi.normandie.global.Contants;
import com.xianzaishi.normandie.utils.GlideUtils;
import com.xianzaishi.normandie.utils.UiUtils;

import java.util.List;

/**
 * 一图加列表adapter
 * quyang
 * Created by Administrator on 2016/9/19.
 */
public class PicsLieBiaoAdapter {

    public void set(View view1, int i, List<HomeTopBean.ModuleBean> mModuleBeen, final Activity activity) {

        HomeTopBean.ModuleBean bean = mModuleBeen.get(i);
        HomeTopBean.ModuleBean.PicListBean picListBean = bean.picList.get(0);
        final String targetId = picListBean.targetId;


        //找到组件
        final ImageView iv = (ImageView) view1.findViewById(R.id.iv_icon4);
        final RecyclerView recyclerviewThree = (RecyclerView) view1.findViewById(R.id.viewPager4);

        final HomeTopBean.ModuleBean bean1 = mModuleBeen.get(i);
        final List<HomeTopBean.ModuleBean.ItemsBean> items1 = bean1.items;


        //设配数据
        GlideUtils.LoadImage(UiUtils.getContext(), bean1.picList.get(0).picUrl, iv);
        iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (bean1.picList.get(0).targetType) {
                    case 1://活动页面
                        Intent intent = new Intent(activity, HuoDongActivity.class);
                        intent.putExtra("targetId",targetId);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        UiUtils.getContext().startActivity(intent);
                        break;
                    case 2://商品详情
                        Intent intent1 = new Intent(activity, GoodsDetailsActivity.class);
                        intent1.putExtra(Contants.ITEM_ID,Integer.valueOf(targetId));
                        intent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        UiUtils.getContext().startActivity(intent1);
                        break;
                }
            }
        });

        //设置布局管理器
        recyclerviewThree.setLayoutManager(new LinearLayoutManager(UiUtils.getContext(), LinearLayoutManager.HORIZONTAL, false));

        //设配数据
        recyclerviewThree.setAdapter(new FoorFourAdapter(items1));//一图+列表
    }
}
