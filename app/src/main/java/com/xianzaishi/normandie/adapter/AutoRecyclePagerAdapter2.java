package com.xianzaishi.normandie.adapter;

import android.content.Intent;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.umeng.analytics.MobclickAgent;
import com.xianzaishi.normandie.GoodsDetailsActivity;
import com.xianzaishi.normandie.HuoDongActivity;
import com.xianzaishi.normandie.MainActivity;
import com.xianzaishi.normandie.R;
import com.xianzaishi.normandie.bean.NewHomePageBean;
import com.xianzaishi.normandie.global.Contants;
import com.xianzaishi.normandie.utils.GlideUtils;
import com.xianzaishi.normandie.utils.NetUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ShenLang on 2016/7/20.
 * 自滚动轮播图
 */
public class AutoRecyclePagerAdapter2 extends PagerAdapter {

    private List<NewHomePageBean.ModuleBean.PicListBean> picList ;
    private List<ImageView> imageViews=new ArrayList<>();
    private MainActivity context;


    public AutoRecyclePagerAdapter2(MainActivity context, List<NewHomePageBean.ModuleBean.PicListBean> picList) {
        this.context = context;
        this.picList = picList;

        if (picList.size()==2){
            picList.add(picList.get(0));
            picList.add(picList.get(1));
        }

        for(NewHomePageBean.ModuleBean.PicListBean url:picList){
            ImageView imageView=new ImageView(context);
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            imageView.setImageResource(R.mipmap.viewpagerzhanwei);
            ViewPager.LayoutParams params= new ViewPager.LayoutParams();
            params.width= ViewPager.LayoutParams.MATCH_PARENT;
            params.height=ViewPager.LayoutParams.MATCH_PARENT;
            imageView.setLayoutParams(params);
            GlideUtils.LoadImage(context,url.getPicUrl(),imageView);
            imageViews.add(imageView);
        }

    }

    @Override
    public int getCount() {
        return picList.size()==1?1:Integer.MAX_VALUE;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        position %= picList.size();

        ImageView imageView=imageViews.get(position);
        container.removeView(imageView);
        container.addView(imageView);
        imageView.setOnClickListener(new ViewPagerClickListener(position));
        return imageView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {

    }

    private class ViewPagerClickListener implements View.OnClickListener {
        private int position;
        public ViewPagerClickListener(int position){
            this.position=position;
        }
        @Override
        public void onClick(View view) {
            switch (picList.get(position).getTargetType()){
                case 1://活动页
                    Intent intent = new Intent(context, HuoDongActivity.class);
                    String targetId=picList.get(position).getTargetId();
                    intent.putExtra("targetId", targetId);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent);
                    break;
                case 2://商品详情页
                    MobclickAgent.onEvent(context,"tapItem");

                    Intent intent1 = new Intent(context, GoodsDetailsActivity.class);
                    String targetId1=picList.get(position).getTargetId();
                    intent1.putExtra(Contants.ITEM_ID, Integer.valueOf(targetId1));
                    intent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(intent1);
                    break;
                case 3://领券
                    NetUtils.obtainCoupons(context,picList.get(position).getTargetId());
                    break;
                case 4://
                    NetUtils.toHTML5(context,picList.get(position).getTargetId(),picList.get(position).getTitleInfo());
                    break;
            }
        }
    }
}
