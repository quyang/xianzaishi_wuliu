package com.xianzaishi.normandie.adapter;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.umeng.analytics.MobclickAgent;
import com.xianzaishi.normandie.GoodsDetailsActivity;
import com.xianzaishi.normandie.HuoDongActivity;
import com.xianzaishi.normandie.MainActivity;
import com.xianzaishi.normandie.R;
import com.xianzaishi.normandie.bean.HomeDownBean;
import com.xianzaishi.normandie.bean.NewHomePageBean;
import com.xianzaishi.normandie.global.Contants;
import com.xianzaishi.normandie.utils.GlideUtils;
import com.xianzaishi.normandie.utils.NetUtils;
import com.xianzaishi.normandie.utils.UiUtils;

import java.util.List;

/**
 * Created by ShenLang on 2016/10/27.
 *  首页2n模块适配器
 */

public class MorePictrueAdapter extends RecyclerView.Adapter<MorePictrueAdapter.ViewHolder> {
    private List<NewHomePageBean.ModuleBean.PicListBean> picList;
    private MainActivity activity;
    public MorePictrueAdapter(List<NewHomePageBean.ModuleBean.PicListBean> picList, MainActivity activity){
        this.picList=picList;
        this.activity=activity;
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(activity).inflate(R.layout.four_pic_recyclerview_imag,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        GlideUtils.LoadImage(activity,picList.get(position).getPicUrl(),holder.imageView);
        holder.imageView.setOnClickListener(new Click(position));
    }

    @Override
    public int getItemCount() {
        return picList==null?0:picList.size();
    }

    private class Click implements View.OnClickListener{
        private int position;
        Click(int position){
            this.position=position;
        }
        @Override
        public void onClick(View view) {
            switch (picList.get(position).getTargetType()) {
                case 1://活动页面
                    Intent intent = new Intent(activity, HuoDongActivity.class);
                    intent.putExtra("targetId", picList.get(position).getTargetId());
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    UiUtils.getContext().startActivity(intent);
                    break;
                case 2://商品详情
                    MobclickAgent.onEvent(activity,"fourthSectionGoods");//第四分区商品点击
                    MobclickAgent.onEvent(activity,"tapItem");
                    Intent intent1 = new Intent(activity, GoodsDetailsActivity.class);
                    intent1.putExtra(Contants.ITEM_ID, Integer.valueOf(picList.get(position).getTargetId()));
                    intent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    activity.startActivity(intent1);
                    break;
                case 3://领券
                    NetUtils.obtainCoupons(activity,picList.get(position).getTargetId());
                    break;
                case 4://
                    NetUtils.toHTML5(activity,picList.get(position).getTargetId(),picList.get(position).getTitleInfo());
                    break;
            }
        }
    }
    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView imageView;
        public ViewHolder(View itemView) {
            super(itemView);
            imageView= (ImageView) itemView.findViewById(R.id.four_pic_recycler_image);
        }
    }
}
