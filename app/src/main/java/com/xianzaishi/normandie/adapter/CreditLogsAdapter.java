package com.xianzaishi.normandie.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.xianzaishi.normandie.R;
import com.xianzaishi.normandie.bean.CreditLogsBean;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

/**
 * Created by Administrator on 2016/9/28.
 */

public class CreditLogsAdapter extends BasicAdapter<CreditLogsBean.DataBean> {
    private List<CreditLogsBean.DataBean> list;
    private LayoutInflater inflater;
    private Context context;
    public CreditLogsAdapter(List<CreditLogsBean.DataBean> datas, Context context) {
        super(datas, context);
        this.context=context;
        this.list=datas;
        this.inflater=LayoutInflater.from(context);
    }

    @Override
    public View getItemView(int position, View convertView, ViewGroup viewGroup) {
        ViewHolder viewHolder;
        if (convertView==null){
            convertView=inflater.inflate(R.layout.credit_log_item,null);
            viewHolder=new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        }else {
            viewHolder= (ViewHolder) convertView.getTag();
        }
        CreditLogsBean.DataBean dataBean=list.get(position);
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy.MM.dd", Locale.CHINA);
        String time=formatter.format(dataBean.getGmtCreate());
        String description=dataBean.getDescription().replace(":"," ");
        int amount=dataBean.getAmount();
        viewHolder.amount.setText(String.valueOf(amount));
        if (amount>0){
            viewHolder.amount.setTextColor(context.getResources().getColor(R.color.colorRed));
        }else {
            viewHolder.amount.setTextColor(context.getResources().getColor(R.color.colorBlack));
        }
        viewHolder.time.setText(time);
        viewHolder.description.setText(description);
        return convertView;
    }
    class ViewHolder{
        private TextView amount,time,description;
        public ViewHolder(View convertView){
            amount= (TextView) convertView.findViewById(R.id.credit_log_amount);
            time= (TextView) convertView.findViewById(R.id.credit_log_time);
            description= (TextView) convertView.findViewById(R.id.credit_log_description);
        }
    }
}
