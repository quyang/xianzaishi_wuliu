package com.xianzaishi.normandie.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.xianzaishi.normandie.R;
import com.xianzaishi.normandie.bean.CouponsBean;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by ShenLang on 2016/8/18.
 * 优惠券的适配器
 */
public class CouponsMultipleAdapter extends RecyclerView.Adapter<CouponsMultipleAdapter.ViewHolder> {
    private LayoutInflater inflater;
    private List<CouponsBean.ModuleBean> list;
    private SimpleDateFormat formatStart;
    private SimpleDateFormat formatEnd;
    private long currentTime;
    public CouponsMultipleAdapter(Context context,List<CouponsBean.ModuleBean> list,long currentTime) {
        inflater=LayoutInflater.from(context);
        this.list=list;
        formatStart=new SimpleDateFormat("yyyy.MM.dd", Locale.CHINA);
        formatEnd=new SimpleDateFormat("yyyy.MM.dd",Locale.CHINA);
        this.currentTime=currentTime;
    }

    /**
     *  自定义条目点击接口
     */
    public interface OnCouponsItemClickListener{
        void onClick(View view,int position);
    }
    private OnCouponsItemClickListener onCouponsItemClickListener;

    public void setOnCouponsItemClickListener(OnCouponsItemClickListener onCouponsItemClickListener) {
        this.onCouponsItemClickListener = onCouponsItemClickListener;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(inflater.inflate(R.layout.coupons_item,parent,false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        CouponsBean.ModuleBean dataBean=list.get(position);
        int couponType=dataBean.getCouponType();
        if (couponType==77){
            long endTime2=dataBean.getCouponEndTime();
            long currentTime2=System.currentTimeMillis();
            String timeLimit2="使用期限："+formatStart.format(dataBean.getCouponStartTime())
                    +"-"+formatEnd.format(dataBean.getCouponEndTime());
            String prices2="¥ "+String.format(Locale.CHINESE,"%.2f",dataBean.getAmountYuanDescription());
            if(currentTime2<endTime2){
                holder.type.setBackgroundResource(R.mipmap.zaocanquan2x);
                holder.foot.setBackgroundResource(R.drawable.coupons_blue);
                holder.state.setText("未使用");
            }else {
                holder.type.setBackgroundResource(R.mipmap.zaocanquanguoqi2x);
                holder.foot.setBackgroundResource(R.drawable.coupons_grey);
                holder.state.setText("已过期");
                holder.imageQR.setVisibility(View.GONE);
            }
            holder.date.setText(timeLimit2);
            holder.useLimit.setText("满"+dataBean.getAmountLimitYuanDescription()+"元使用");
            holder.price.setText(prices2);
            final int p=position;
            if (onCouponsItemClickListener!=null){
                holder.foot.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        onCouponsItemClickListener.onClick(view,p);
                    }
                });
            }
        }else {
            long endTime=dataBean.getCouponEndTime();
            String timeLimit="使用期限："+formatStart.format(dataBean.getCouponStartTime())
                    +"-"+formatEnd.format(dataBean.getCouponEndTime());
            String prices="¥ "+String.format(Locale.CHINESE,"%.2f",dataBean.getAmountYuanDescription());
            if(currentTime<endTime){
                holder.type.setBackgroundResource(R.mipmap.jianmianquan2x);
                holder.foot.setBackgroundResource(R.drawable.coupons_green);
                holder.state.setText("未使用");
            }else {
                holder.type.setBackgroundResource(R.mipmap.jianmianguoqi2x);
                holder.foot.setBackgroundResource(R.drawable.coupons_grey);
                holder.state.setText("已过期");
            }
            holder.date.setText(timeLimit);
            holder.useLimit.setText("满"+dataBean.getAmountLimitYuanDescription()+"元使用");
            holder.price.setText(prices);
            holder.imageQR.setVisibility(View.GONE);
            final int p=position;
            if (onCouponsItemClickListener!=null){
                holder.foot.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        onCouponsItemClickListener.onClick(view,p);
                    }
                });
            }
        }
    }


    @Override
    public int getItemCount() {
        return list==null?0:list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView price,useLimit,date,state;
        private ImageView type,imageQR;
        private LinearLayout foot;
        public ViewHolder(View itemView) {
            super(itemView);
            foot= (LinearLayout) itemView.findViewById(R.id.coupons_item_foot);
            price= (TextView) itemView.findViewById(R.id.coupons_item_price);
            useLimit= (TextView) itemView.findViewById(R.id.coupons_item_useLimit);
            date= (TextView) itemView.findViewById(R.id.coupons_item_date);
            state= (TextView) itemView.findViewById(R.id.coupons_item_state);
            type= (ImageView) itemView.findViewById(R.id.coupons_item_type);
            imageQR= (ImageView) itemView.findViewById(R.id.coupons_item_QR);
        }
    }
}
