package com.xianzaishi.normandie.adapter;

import android.content.Intent;
import android.graphics.Bitmap;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.target.Target;
import com.umeng.analytics.MobclickAgent;
import com.xianzaishi.normandie.GoodsDetailsActivity;
import com.xianzaishi.normandie.HTML5Activity;
import com.xianzaishi.normandie.HuoDongActivity;
import com.xianzaishi.normandie.MyApplication;
import com.xianzaishi.normandie.NewLoginActivity;
import com.xianzaishi.normandie.R;
import com.xianzaishi.normandie.bean.HuoDongBean;
import com.xianzaishi.normandie.common.Urls;
import com.xianzaishi.normandie.global.Contants;
import com.xianzaishi.normandie.utils.GlideUtils;
import com.xianzaishi.normandie.utils.NetUtils;
import com.xianzaishi.normandie.utils.SPUtils;
import com.xianzaishi.normandie.utils.ToastUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * <p>
 * Created by Administrator on 2016/9/30.
 */
public class DanTuActivityAdapter {

    private OkHttpClient client= MyApplication.getOkHttpClient();
    public void set(View view, int i, final HuoDongBean.ModuleBean bean, final HuoDongActivity activity) {

        final ImageView icon = (ImageView) view.findViewById(R.id.iv_healthy_down);
        //GlideUtils.LoadImage(activity, bean.picList.get(0).picUrl, icon);//图片
        /**
         *  让图片等比例缩放
         */
        Glide.with(activity).load( bean.picList.get(0).picUrl).asBitmap().into(new SimpleTarget<Bitmap>(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL) {
            @Override
            public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                int imageWidth = resource.getWidth();
                int imageHeight = resource.getHeight();
                int height = activity.getResources().getDisplayMetrics().widthPixels * imageHeight / imageWidth;
                ViewGroup.LayoutParams para = icon.getLayoutParams();
                para.height = height;
                icon.setLayoutParams(para);
                //计算好宽高后正式加载图片
                GlideUtils.LoadImage(activity, bean.picList.get(0).picUrl, icon);
            }
        });

        icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch (bean.picList.get(0).targetType){
                    case 0://不能点击
                        break;
                    case 2://商品详情页
                        MobclickAgent.onEvent(activity,"tapItem");
                        Intent intent1 = new Intent(activity, GoodsDetailsActivity.class);
                        String targetId1=bean.picList.get(0).targetId;
                        intent1.putExtra(Contants.ITEM_ID, Integer.valueOf(targetId1));
                        intent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        activity.startActivity(intent1);
                        break;
                    case 3://领券
                        obtainCoupons(bean.picList.get(0).targetId,activity);
                        break;
                    case 4://
                        Intent intent3=new Intent(activity, HTML5Activity.class);
                        intent3.putExtra("url",bean.picList.get(0).targetId)
                                .putExtra("title",bean.picList.get(0).titleInfo);
                        activity.startActivity(intent3);
                        break;
                }
            }
        });

    }
    /**
     *  领取优惠券
     */
    private void obtainCoupons(String url,HuoDongActivity huoDongActivity) {
        String token= SPUtils.getStringValue(huoDongActivity, Contants.SP_NAME,Contants.TOKEN,null);
        String obtainUrl=url+"&token="+token;
        if (url==null){
            return;
        }
        if (token!=null&&!url.equals("")){
            Request request=new Request.Builder()
                    .url(obtainUrl)
                    .build();
            client.newCall(request).enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                }
                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    if(response.isSuccessful()){
                        String jsonStr=response.body().string();
                        try {
                            JSONObject jsonObject=new JSONObject(jsonStr);
                            ToastUtils.showToast(jsonObject.getString("errorMsg"));
                            /*int resultCode=jsonObject.getInt("resultCode");
                            switch (resultCode){
                                case 1:
                                    ToastUtils.showToast("您已领取成功！");
                                    break;
                                case -30:
                                    ToastUtils.showToast("优惠券已领取成功，不可重复领取");
                                    break;
                                case -50:
                                    ToastUtils.showToast("优惠券已经领光了！");
                                    break;
                                default:
                                    ToastUtils.showToast("系统正忙，请稍后再试！");
                            }*/
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
        }else {
            if (!url.equals("")){
                huoDongActivity.startActivity(new Intent(huoDongActivity, NewLoginActivity.class));
            }
        }
    }
}
