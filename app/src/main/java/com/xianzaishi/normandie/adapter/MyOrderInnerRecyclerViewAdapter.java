package com.xianzaishi.normandie.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.xianzaishi.normandie.MyApplication;
import com.xianzaishi.normandie.OrderDetailsActivity;
import com.xianzaishi.normandie.R;
import com.xianzaishi.normandie.bean.OrdersListBean01;
import com.xianzaishi.normandie.utils.GlideUtils;

import java.util.List;

/**
 * Created by Administrator on 2016/10/19.
 */

public class MyOrderInnerRecyclerViewAdapter extends RecyclerView.Adapter<MyOrderInnerRecyclerViewAdapter.ImageViewHolder>{
    private List<OrdersListBean01.DataBean.ObjectsBean.ItemsBean> list;
    private String mId;
    private Context context;
    public MyOrderInnerRecyclerViewAdapter(Context context,List<OrdersListBean01.DataBean.ObjectsBean.ItemsBean> list,String mId){
        this.list=list;
        this.context=context;
        this.mId=mId;
    }


    @Override
    public ImageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(MyApplication.getContext()).inflate(R.layout.myorder_recyclerview_initem,null);
        return new ImageViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ImageViewHolder holder, int position) {
        GlideUtils.LoadImage(MyApplication.getContext(),list.get(position).iconUrl,holder.imageView);
        holder.imageView.setOnClickListener(new ClickToDetail());
    }

    private class ClickToDetail implements View.OnClickListener{
        public ClickToDetail() {
        }

        @Override
        public void onClick(View view) {
            Intent intent = new Intent(context, OrderDetailsActivity.class);
            intent.putExtra("oid", mId);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
        }
    }
    @Override
    public int getItemCount() {
        return list==null?0:list.size();
    }

    public class ImageViewHolder extends RecyclerView.ViewHolder {
        private ImageView imageView;
        private LinearLayout container;
        public ImageViewHolder(View itemView) {
            super(itemView);
            imageView= (ImageView) itemView.findViewById(R.id.inner_imageView);
        }
    }
}
