package com.xianzaishi.normandie.adapter;

import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.xianzaishi.normandie.bean.HomeTopBean;
import com.xianzaishi.normandie.utils.GlideUtils;
import com.xianzaishi.normandie.utils.UiUtils;

import java.util.List;

/**
 * 首页viewpager quyang
 * Created by Administrator on 2016/9/18.
 */
public class ViewPagerAdapter extends PagerAdapter {

    private List<HomeTopBean.ModuleBean.PicListBean> picList ;

    //constructor
    public ViewPagerAdapter(List<HomeTopBean.ModuleBean.PicListBean> picList ) {
        this.picList = picList;
    }

    @Override
    public int getCount() {
        return picList == null ? 0 : picList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }


    @Override
    public Object instantiateItem(ViewGroup container, final int position) {

        ImageView imageView = new ImageView(UiUtils.getContext());
        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        ViewPager.LayoutParams params = new ViewPager.LayoutParams();
        params.width = ViewPager.LayoutParams.MATCH_PARENT;
        params.height = 256;
        imageView.setLayoutParams(params);

        GlideUtils.LoadImage(UiUtils.getContext(),picList.get(position).picUrl,imageView);
        System.out.println(picList.get(position).picUrl);
        container.addView(imageView);


        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(UiUtils.getContext(),"viewpager,position =  " + position,Toast.LENGTH_LONG).show();
            }
        });



        return imageView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }
}
