package com.xianzaishi.normandie.adapter;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.xianzaishi.normandie.GoodsDetailsActivity;
import com.xianzaishi.normandie.HuoDongActivity;
import com.xianzaishi.normandie.MainActivity;
import com.xianzaishi.normandie.R;
import com.xianzaishi.normandie.bean.HomeDownBean;
import com.xianzaishi.normandie.bean.HomeTopBean;
import com.xianzaishi.normandie.global.Contants;
import com.xianzaishi.normandie.utils.GlideUtils;
import com.xianzaishi.normandie.utils.UiUtils;

import java.util.List;

/**
 * Created by Administrator on 2016/10/27.
 */

public class MorePictrueAdapterOne extends RecyclerView.Adapter<MorePictrueAdapterOne.ViewHolder> {
    private List<HomeTopBean.ModuleBean.PicListBean> picList;
    private MainActivity activity;
    public MorePictrueAdapterOne(List<HomeTopBean.ModuleBean.PicListBean> picList, MainActivity activity){
        this.picList=picList;
        this.activity=activity;
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(activity).inflate(R.layout.four_pic_recyclerview_imag,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        GlideUtils.LoadImage(activity,picList.get(position).picUrl,holder.imageView);
        holder.imageView.setOnClickListener(new Click(position));
    }

    @Override
    public int getItemCount() {
        return picList==null?0:picList.size();
    }
    private class Click implements View.OnClickListener{
        private int position;
        public Click(int position){
            this.position=position;
        }
        @Override
        public void onClick(View view) {
            switch (picList.get(position).targetType) {
                case 1://活动页面
                    Intent intent = new Intent(activity, HuoDongActivity.class);
                    intent.putExtra("targetId", picList.get(position).targetId);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    UiUtils.getContext().startActivity(intent);
                    break;
                case 2://商品详情
                    Intent intent1 = new Intent(activity, GoodsDetailsActivity.class);
                    intent1.putExtra(Contants.ITEM_ID, picList.get(position).targetId);
                    intent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    activity.startActivity(intent1);
                    break;
            }
        }
    }
    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView imageView;
        public ViewHolder(View itemView) {
            super(itemView);
            imageView= (ImageView) itemView.findViewById(R.id.four_pic_recycler_image);
        }
    }
}
