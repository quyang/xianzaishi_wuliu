package com.xianzaishi.normandie.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.umeng.analytics.MobclickAgent;
import com.xianzaishi.normandie.GoodsDetailsActivity;
import com.xianzaishi.normandie.R;
import com.xianzaishi.normandie.bean.Home2DownBean;
import com.xianzaishi.normandie.bean.NewHomePageBean;
import com.xianzaishi.normandie.global.Contants;
import com.xianzaishi.normandie.utils.GlideUtils;
import com.xianzaishi.normandie.utils.UiUtils;

import java.util.ArrayList;


/**
 * Created by Administrator on 2016/11/23.
 */

public class StaggeredTasteNewAdapter extends RecyclerView.Adapter{
    private Context context;
    private LayoutInflater inflate;
    private final static int TYPE_TEXT_IMAGE=0;
    private final static int TYPE_IMAGE=1;
    private ArrayList<NewHomePageBean.ModuleBean.ItemsBean> list;
    public StaggeredTasteNewAdapter(Context context,ArrayList<NewHomePageBean.ModuleBean.ItemsBean> list) {
        this.context = context;
        inflate=LayoutInflater.from(context);
        this.list=list;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType){
            case 0:
                return new ViewHolder0(inflate.inflate(R.layout.taste_new_text_image,parent,false));
            case 1:
                return new ViewHolder1(inflate.inflate(R.layout.taste_new_image,parent,false));
            default:
                return null;
        }

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if (getItemViewType(position)==TYPE_TEXT_IMAGE){
            ViewHolder0 viewHolder0= (ViewHolder0) holder;
            ViewGroup.LayoutParams params= viewHolder0.contain.getLayoutParams();
            params.height=dip2px(context,280);
            viewHolder0.contain.setLayoutParams(params);
            NewHomePageBean.ModuleBean.ItemsBean itemsBean=list.get(position);
            NewHomePageBean.ModuleBean.ItemsBean.ItemSkuVOsBean.TagDetailBean tagDetailBean=itemsBean.getItemSkuVOs().get(0).getTagDetail();

            GlideUtils.LoadImage(context,itemsBean.getPicUrl(),viewHolder0.imageView);
            viewHolder0.name.setText(itemsBean.getTitle());
            viewHolder0.standard.setText(itemsBean.getItemSkuVOs().get(0).getSaleDetailInfo());
            String price="¥"+itemsBean.getDiscountPriceYuanString();
            viewHolder0.price.setText(price);
            viewHolder0.contain.setOnClickListener(new ClickToDetail(position));
            //打标
            if (itemsBean.getItemSkuVOs().get(0).getInventory()<=0){
                viewHolder0.redSign.setVisibility(View.VISIBLE);
                viewHolder0.redSign.setBackgroundResource(R.drawable.grey_sign);
                viewHolder0.redSign.setText("卖光啦");
            }else if(tagDetailBean!=null&&tagDetailBean.getChannel()!=null&&!tagDetailBean.isMayPlus()&&tagDetailBean.getTagContent()!=null){
                viewHolder0.redSign.setVisibility(View.VISIBLE);
                viewHolder0.redSign.setBackgroundResource(R.drawable.grey_sign);
                viewHolder0.redSign.setText(tagDetailBean.getTagContent());
            }else if(tagDetailBean!=null){
                if (tagDetailBean.getChannel()==null||tagDetailBean.getTagContent()==null||tagDetailBean.getChannel().equals("2")){
                    viewHolder0.redSign.setVisibility(View.GONE);
                }else {
                    viewHolder0.redSign.setVisibility(View.VISIBLE);
                    viewHolder0.redSign.setBackgroundResource(R.drawable.red_sign);
                    viewHolder0.redSign.setText(tagDetailBean.getTagContent());
                }
            }
        }else if (getItemViewType(position)==TYPE_IMAGE){
            NewHomePageBean.ModuleBean.ItemsBean itemsBean=list.get(position);
            NewHomePageBean.ModuleBean.ItemsBean.ItemSkuVOsBean.TagDetailBean tagDetailBean=itemsBean.getItemSkuVOs().get(0).getTagDetail();

            ViewHolder1 viewHolder1= (ViewHolder1) holder;
            ViewGroup.LayoutParams params1= viewHolder1.itemView.getLayoutParams();
            params1.height=dip2px(context,140);
            viewHolder1.itemView.setLayoutParams(params1);

            GlideUtils.LoadImage(context,itemsBean.getPicUrl(),viewHolder1.imageView);
            viewHolder1.name.setText(itemsBean.getTitle());
            String price="¥"+itemsBean.getDiscountPriceYuanString();
            viewHolder1.price.setText(price);
            viewHolder1.contain.setOnClickListener(new ClickToDetail(position));
            //打标
            if (itemsBean.getItemSkuVOs().get(0).getInventory()<=0){
                viewHolder1.redSign.setVisibility(View.VISIBLE);
                viewHolder1.redSign.setBackgroundResource(R.drawable.grey_sign);
                viewHolder1.redSign.setText("卖光啦");
            }else if(tagDetailBean.getChannel()!=null&&!tagDetailBean.isMayPlus()&&tagDetailBean.getTagContent()!=null){
                viewHolder1.redSign.setVisibility(View.VISIBLE);
                viewHolder1.redSign.setBackgroundResource(R.drawable.grey_sign);
                viewHolder1.redSign.setText(tagDetailBean.getTagContent());
            }else {
                if (tagDetailBean.getChannel()==null||tagDetailBean.getTagContent()==null||tagDetailBean.getChannel().equals("2")){
                    viewHolder1.redSign.setVisibility(View.GONE);
                }else {
                    viewHolder1.redSign.setVisibility(View.VISIBLE);
                    viewHolder1.redSign.setBackgroundResource(R.drawable.red_sign);
                    viewHolder1.redSign.setText(tagDetailBean.getTagContent());
                }
            }
        }
    }

    @Override
    public int getItemCount() {
        return 5;
    }

    @Override
    public int getItemViewType(int position) {
        if (position==0){
            return TYPE_TEXT_IMAGE;
        }else {
            return TYPE_IMAGE;
        }
    }

   private class ClickToDetail implements View.OnClickListener{
        private int position;

       public ClickToDetail(int position) {
           this.position = position;
       }

       @Override
       public void onClick(View view) {
           MobclickAgent.onEvent(context,"secondSectionGood");//友盟第二分区商品点击
           MobclickAgent.onEvent(context,"tapItem");

           Intent intent = new Intent(context, GoodsDetailsActivity.class);
           intent.putExtra(Contants.ITEM_ID, list.get(position).getItemId());
           intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
           context.startActivity(intent);
       }
   }

    class ViewHolder0 extends RecyclerView.ViewHolder{
        private ImageView imageView;
        private TextView name,standard,price,redSign;
        private LinearLayout contain;
        public ViewHolder0(View itemView) {
            super(itemView);
            imageView= (ImageView) itemView.findViewById(R.id.text_image_imageView);
            name= (TextView) itemView.findViewById(R.id.text_image_name);
            standard= (TextView) itemView.findViewById(R.id.text_image_standard);
            price= (TextView) itemView.findViewById(R.id.text_image_price);
            contain= (LinearLayout) itemView.findViewById(R.id.text_image_foot_view);
            redSign= (TextView) itemView.findViewById(R.id.text_image_discount);
        }
    }
    class ViewHolder1 extends RecyclerView.ViewHolder{
        private ImageView imageView;
        private TextView name,price,redSign;
        private FrameLayout contain;
        public ViewHolder1(View itemView) {
            super(itemView);
            imageView= (ImageView) itemView.findViewById(R.id.image_imageView);
            name= (TextView) itemView.findViewById(R.id.image_name);
            price= (TextView) itemView.findViewById(R.id.image_price);
            contain= (FrameLayout) itemView.findViewById(R.id.image_foot_view);
            redSign= (TextView) itemView.findViewById(R.id.image_discount);
        }
    }
    /**
     * dp2px
     */
    private static int dip2px(Context context, float dpValue) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dpValue * scale + 0.5f);
    }
}
