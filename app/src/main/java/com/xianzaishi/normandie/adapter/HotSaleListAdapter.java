package com.xianzaishi.normandie.adapter;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.umeng.analytics.MobclickAgent;
import com.xianzaishi.normandie.GoodsDetailsActivity;
import com.xianzaishi.normandie.MainActivity;
import com.xianzaishi.normandie.R;
import com.xianzaishi.normandie.bean.Home2DownBean;
import com.xianzaishi.normandie.bean.HomeDownBean;
import com.xianzaishi.normandie.bean.NewHomePageBean;
import com.xianzaishi.normandie.global.Contants;
import com.xianzaishi.normandie.utils.GlideUtils;
import com.xianzaishi.normandie.utils.UiUtils;

import java.util.List;

/**
 * Created by ShenLang on 2016/9/18.
 *  垂直列表适配器
 */
public class HotSaleListAdapter extends RecyclerView.Adapter<HotSaleListAdapter.MyViewHolder> {


    private List<NewHomePageBean.ModuleBean.ItemsBean> items2;
    private MainActivity mActivity;
    public static int[] startLocation;
    //constructor
    public HotSaleListAdapter(List<NewHomePageBean.ModuleBean.ItemsBean> items2, MainActivity activity) {
        this.items2 = items2;
        mActivity=activity;
    }

    /**
     *  自定义一个加购的接口
     */
    public interface AddIntoShoppingCarListener{
        void addIntoShoppingCar(View view,int position,ImageView imageView);
    }

    private AddIntoShoppingCarListener addIntoShoppingCarListener;

    public void setAddIntoShoppingCarListener(AddIntoShoppingCarListener addIntoShoppingCarListener) {
        this.addIntoShoppingCarListener = addIntoShoppingCarListener;
    }

    @Override
    public HotSaleListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new HotSaleListAdapter.MyViewHolder(UiUtils.getLayoutInflater().inflate(R.layout.pager_tw0_itm, null));
    }

    @Override
    public void onBindViewHolder(final HotSaleListAdapter.MyViewHolder holder, int position) {

        NewHomePageBean.ModuleBean.ItemsBean itemsBean = items2.get(position);
        NewHomePageBean.ModuleBean.ItemsBean.ItemSkuVOsBean.TagDetailBean tagDetailBean=itemsBean.getItemSkuVOs().get(0).getTagDetail();
        final int itemId = itemsBean.getItemId();

        GlideUtils.LoadImage(UiUtils.getContext(), itemsBean.getPicUrl(), holder.icon);
        holder.name.setText(itemsBean.getTitle());
        String guiGe=itemsBean.getItemSkuVOs().get(0).getSaleDetailInfo();
        String subtitle = itemsBean.getSubtitle();

        holder.des.setText(subtitle);
        holder.guige.setText("规格："+guiGe);
        holder.priceNow.setText(itemsBean.getDiscountPriceYuanString());

        //打标
        if (itemsBean.getItemSkuVOs().get(0).getInventory()<=0){
            holder.redSign.setVisibility(View.VISIBLE);
            holder.redSign.setBackgroundResource(R.drawable.grey_sign);
            holder.redSign.setText("卖光啦");

            holder.car.setEnabled(false);
            holder.car.setBackgroundResource(R.mipmap.shoppingcargrey2x);
        }else if(!tagDetailBean.isMayPlus()){
            holder.car.setEnabled(false);
            holder.car.setBackgroundResource(R.mipmap.shoppingcargrey2x);
            if (tagDetailBean.getChannel()==null||tagDetailBean.getTagContent()==null){
                holder.redSign.setVisibility(View.GONE);
            }else {
                holder.redSign.setVisibility(View.VISIBLE);
                holder.redSign.setBackgroundResource(R.drawable.grey_sign);
                holder.redSign.setText(tagDetailBean.getTagContent());
            }
        }else {
            holder.car.setBackgroundResource(R.mipmap.gouwuche2x);
            holder.car.setEnabled(true);

            if (tagDetailBean.getChannel()==null||tagDetailBean.getTagContent()==null||tagDetailBean.getChannel().equals("2")){
                holder.redSign.setVisibility(View.GONE);
            }else {
                holder.redSign.setVisibility(View.VISIBLE);
                holder.redSign.setBackgroundResource(R.drawable.red_sign);
                holder.redSign.setText(tagDetailBean.getTagContent());
            }
        }
        //holder.priceOld.setText("原价 : " + itemsBean.discountPriceYuanString);
        //holder.priceOld.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG);
        final int p=position;
        if (addIntoShoppingCarListener!=null){
            holder.car.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    addIntoShoppingCarListener.addIntoShoppingCar(view,p,holder.icon);
                }
            });
        }
        holder.mRoot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MobclickAgent.onEvent(mActivity,"homeListGoods");//首页listView商品点击
                MobclickAgent.onEvent(mActivity,"tapItem");

                Intent intent = new Intent(mActivity, GoodsDetailsActivity.class);
                intent.putExtra(Contants.ITEM_ID, itemId);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                mActivity.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return items2 == null ? 0 : items2.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {


        private ImageView icon;
        private TextView name;
        private TextView des,guige;
        private TextView priceNow;
        private TextView priceOld;
        private TextView redSign;
        private ImageView car;
        private final RelativeLayout mRoot;

        public MyViewHolder(View itemView) {
            super(itemView);
            guige= (TextView) itemView.findViewById(R.id.tv_quality);
            icon = (ImageView) itemView.findViewById(R.id.iv_icon);
            redSign= (TextView) itemView.findViewById(R.id.red_sign);
            name = (TextView) itemView.findViewById(R.id.tv_name);
            des = (TextView) itemView.findViewById(R.id.tv_subtitle);
            priceNow = (TextView) itemView.findViewById(R.id.tv_discount_price);
            priceOld = (TextView) itemView.findViewById(R.id.tv_youhui);
            car = (ImageView) itemView.findViewById(R.id.iv_shopping);
            mRoot = (RelativeLayout) itemView.findViewById(R.id.root);
        }
    }
}
