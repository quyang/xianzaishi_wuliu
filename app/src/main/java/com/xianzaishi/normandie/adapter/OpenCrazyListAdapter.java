package com.xianzaishi.normandie.adapter;

import android.content.Intent;
import android.graphics.Paint;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.umeng.analytics.MobclickAgent;
import com.xianzaishi.normandie.GoodsDetailsActivity;
import com.xianzaishi.normandie.MainActivity;
import com.xianzaishi.normandie.R;
import com.xianzaishi.normandie.bean.Home2DownBean;
import com.xianzaishi.normandie.bean.HomeTopBean;
import com.xianzaishi.normandie.bean.NewHomePageBean;
import com.xianzaishi.normandie.global.Contants;
import com.xianzaishi.normandie.utils.GlideUtils;
import com.xianzaishi.normandie.utils.UiUtils;

import java.util.List;

/**
 * 狂欢购商品列表
 * Created by Administrator on 2016/9/18.
 */
public class OpenCrazyListAdapter extends RecyclerView.Adapter<OpenCrazyListAdapter.MyViewHolder> {


    private List<NewHomePageBean.ModuleBean.ItemsBean> items2;
    private MainActivity mActivity;
    public static int[] startLocation;
    //constructor
    public OpenCrazyListAdapter(List<NewHomePageBean.ModuleBean.ItemsBean> items2, MainActivity activity) {
        this.items2 = items2;
        mActivity=activity;
    }

    /**
     *  自定义一个加购的接口
     */
    public interface AddIntoShoppingCarListener{
        void addIntoShoppingCar(View view, int position, ImageView imageView);
    }

    private AddIntoShoppingCarListener addIntoShoppingCarListener;

    public void setAddIntoShoppingCarListener(AddIntoShoppingCarListener addIntoShoppingCarListener) {
        this.addIntoShoppingCarListener = addIntoShoppingCarListener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MyViewHolder(UiUtils.getLayoutInflater().inflate(R.layout.shopping_crazy_recycler_item, null));
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        NewHomePageBean.ModuleBean.ItemsBean itemsBean = items2.get(position);
        NewHomePageBean.ModuleBean.ItemsBean.ItemSkuVOsBean.TagDetailBean tagDetailBean=itemsBean.getItemSkuVOs().get(0).getTagDetail();
        final int itemId = itemsBean.getItemId();
        GlideUtils.LoadImage(UiUtils.getContext(), itemsBean.getPicUrl(), holder.icon);
        String title = itemsBean.getTitle();
        int length = title.length();
        holder.name.setText(title);
        String subtitle = itemsBean.getItemSkuVOs().get(0).getSaleDetailInfo();
        holder.des.setText(subtitle);
        holder.priceNow.setText("¥"+itemsBean.getDiscountPriceYuanString());

        if (addIntoShoppingCarListener!=null){
            holder.car.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    addIntoShoppingCarListener.addIntoShoppingCar(view,position,holder.icon);
                }
            });
        }

        //打标
        if (itemsBean.getItemSkuVOs().get(0).getInventory()<=0){
            holder.redSign.setVisibility(View.VISIBLE);
            holder.redSign.setBackgroundResource(R.drawable.grey_sign);
            holder.redSign.setText("卖光啦");

            holder.car.setEnabled(false);
            holder.car.setBackgroundResource(R.mipmap.shoppingcargrey2x);
        }else if(tagDetailBean!=null&&!tagDetailBean.isMayPlus()){
            holder.car.setEnabled(false);
            holder.car.setBackgroundResource(R.mipmap.shoppingcargrey2x);
            if (tagDetailBean.getChannel()==null||tagDetailBean.getTagContent()==null){
                holder.redSign.setVisibility(View.GONE);
            }else {
                holder.redSign.setVisibility(View.VISIBLE);
                holder.redSign.setBackgroundResource(R.drawable.grey_sign);
                holder.redSign.setText(tagDetailBean.getTagContent());
            }
        }else if(tagDetailBean!=null){
            holder.car.setEnabled(true);
            holder.car.setBackgroundResource(R.mipmap.gouwuche2x);
            if (tagDetailBean.getChannel()==null||tagDetailBean.getTagContent()==null||tagDetailBean.getChannel().equals("2")){
                holder.redSign.setVisibility(View.GONE);
            }else {
                holder.redSign.setVisibility(View.VISIBLE);
                holder.redSign.setBackgroundResource(R.drawable.red_sign);
                holder.redSign.setText(tagDetailBean.getTagContent());
            }
        }

        holder.mRoot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MobclickAgent.onEvent(mActivity,"secondSectionGood");//友盟第二分区商品点击
                MobclickAgent.onEvent(mActivity,"tapItem");

                Intent intent = new Intent(mActivity, GoodsDetailsActivity.class);
                intent.putExtra(Contants.ITEM_ID, itemId);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                mActivity.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return items2 == null ? 0 : items2.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private ImageView icon;
        private TextView name;
        private TextView des;
        private TextView priceNow;
        private TextView redSign;
        private ImageView car;
        private LinearLayout mRoot;

        public MyViewHolder(View itemView) {
            super(itemView);
            icon = (ImageView) itemView.findViewById(R.id.iv_icon);
            name = (TextView) itemView.findViewById(R.id.tv_name);
            des = (TextView) itemView.findViewById(R.id.tv_des);
            priceNow = (TextView) itemView.findViewById(R.id.tv_price_now);
            car = (ImageView) itemView.findViewById(R.id.iv_car);
            mRoot = (LinearLayout) itemView.findViewById(R.id.root);
            redSign= (TextView) itemView.findViewById(R.id.red_sign);
        }
    }
}
