package com.xianzaishi.normandie.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.xianzaishi.normandie.MainActivity;
import com.xianzaishi.normandie.R;
import com.xianzaishi.normandie.bean.Home2TopBean;
import com.xianzaishi.normandie.bean.NewHomePageBean;
import com.xianzaishi.normandie.utils.UiUtils;

import java.util.ArrayList;

/**
 * Created by ShenLang on 2016/11/24.
 * 限时抢购时间段适配器
 */

public class TimeLimitIndicatorAdapter extends RecyclerView.Adapter<TimeLimitIndicatorAdapter.ViewHolder> {
    private Context context;
    private LayoutInflater inflater;
    //private ArrayList<Home2TopBean.ModuleBean> list;
    private ArrayList<NewHomePageBean.ModuleBean.InnerStepInfoBean> list;
    public TimeLimitIndicatorAdapter(Context context, ArrayList<NewHomePageBean.ModuleBean.InnerStepInfoBean> list) {
        this.context = context;
        inflater=LayoutInflater.from(context);
        this.list=list;
    }

    @Override
    public TimeLimitIndicatorAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(inflater.inflate(R.layout.time_limit_indicator_item,parent,false));
    }

    /**
     *  自定义点击事件
     */
    public interface OnTimeLimitClickListener{
        void onTimeLimitClickListener(int position);
    }
    private OnTimeLimitClickListener onTimeLimitClickListener;

    public void setOnTimeLimitClickListener(OnTimeLimitClickListener onTimeLimitClickListener) {
        this.onTimeLimitClickListener = onTimeLimitClickListener;
    }

    @Override
    public void onBindViewHolder(TimeLimitIndicatorAdapter.ViewHolder holder, int position) {
        ViewGroup.LayoutParams params=holder.timeItem.getLayoutParams();
        if (getItemCount()>4) {
            params.width = 2 * UiUtils.getScreenWidth((MainActivity) context) / (getItemCount() * 2 - 1);
        }else {
            params.width = UiUtils.getScreenWidth((MainActivity) context) / getItemCount();
        }
        holder.timeItem.setLayoutParams(params);
        NewHomePageBean.ModuleBean.InnerStepInfoBean bean=list.get(position);
        String string=bean.getTitle();
        //先构造SpannableString
        SpannableString spanString = new SpannableString(string);
        //再构造一个改变字体颜色的Span
        ForegroundColorSpan span = new ForegroundColorSpan(context.getResources().getColor(R.color.colorTextWhite));
        //将这个Span应用于指定范围的字体
        spanString.setSpan(span, 0, 12, Spannable.SPAN_EXCLUSIVE_INCLUSIVE);
        holder.timeItem.setText(spanString);
        holder.timeItem.setSelected(bean.isChecked());
        final int p=position;
        if (onTimeLimitClickListener!=null){
            holder.timeItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onTimeLimitClickListener.onTimeLimitClickListener(p);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return list==null?0:list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView timeItem;
        public ViewHolder(View itemView) {
            super(itemView);
            timeItem= (TextView) itemView.findViewById(R.id.time_limit_indicator_item);
        }
    }
}
