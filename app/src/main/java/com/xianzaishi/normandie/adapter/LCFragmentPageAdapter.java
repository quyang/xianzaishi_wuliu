package com.xianzaishi.normandie.adapter;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.xianzaishi.normandie.fragment.LeafCategoryFragment;

import java.util.ArrayList;

/**
 * Created by Administrator on 2016/9/26.
 */

public class LCFragmentPageAdapter extends FragmentPagerAdapter {
    private ArrayList<LeafCategoryFragment> categoryFragmentList;
    private ArrayList<String> tabNameList;

    public LCFragmentPageAdapter(FragmentManager fm,
         ArrayList<LeafCategoryFragment> categoryFragmentArrayList, ArrayList<String> tabNameList) {
        super(fm);
        this.categoryFragmentList = categoryFragmentArrayList;
        this.tabNameList = tabNameList;
    }

    @Override
    public Fragment getItem(int position) {
        return categoryFragmentList.get(position);
    }

    @Override
    public int getCount() {
        return categoryFragmentList.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tabNameList.get(position);
    }
}
