package com.xianzaishi.normandie.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.List;

/**
 * Created by Administrator on 2016/8/10. listView的基类Adapter
 */
public abstract class BasicAdapter<T> extends BaseAdapter {
    private Context context;
    private List<T> datas;
    private LayoutInflater layoutInflater;

    public BasicAdapter(List<T> datas, Context context) {
        this.datas = datas;
        this.context = context;
    }

    public LayoutInflater getLayoutInflater() {
        layoutInflater=LayoutInflater.from(context);
        return layoutInflater;
    }

    @Override
    public int getCount() {
        return datas.size();
    }

    @Override
    public Object getItem(int i) {
        return datas.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        return getItemView(i,view,viewGroup);
    }
    public abstract View getItemView(int position,View convertView,ViewGroup viewGroup);
}
