package com.xianzaishi.normandie.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.xianzaishi.normandie.OrderDetailsActivity;
import com.xianzaishi.normandie.R;
import com.xianzaishi.normandie.bean.OrdersListBean01;
import com.xianzaishi.normandie.utils.UiUtils;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

/**
 * quyang
 * 我的订单的recyclerview的adapter
 * Created by Administrator on 2016/9/3.
 */
public class MyOrderAdapter extends RecyclerView.Adapter<MyOrderAdapter.MyViewHolder>{

    public List<OrdersListBean01.DataBean.ObjectsBean> list;
    public Context context;
    private String mId;
    public MyOrderAdapter(List<OrdersListBean01.DataBean.ObjectsBean> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MyViewHolder(View.inflate(UiUtils.getContext(), R.layout.listview_item_order, null));
    }



    public interface OnOrderClickListener{
        void onClickListener(View view,int position);
    }
    private OnOrderClickListener onOrderClickListener;

    public void setOnOrderClickListener(OnOrderClickListener onOrderClickListener) {
        this.onOrderClickListener = onOrderClickListener;
    }
    public interface LoadMoreData{
        void loadMoreData();
    }
    private LoadMoreData loadMoreData;

    public void setLoadMoreData(LoadMoreData loadMoreData) {
        this.loadMoreData = loadMoreData;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        if (position==getItemCount()-1&&loadMoreData!=null){
            loadMoreData.loadMoreData();
        }
        OrdersListBean01.DataBean.ObjectsBean bean = list.get(position);
        //订单id
        mId = bean.id;
        String date=getStringDate(Long.valueOf(bean.gmtCreate));
        holder.tvData.setText(date);
        String time=getStringTime(Long.valueOf(bean.gmtCreate));
        holder.tvTime.setText(time);
        if (bean.status.equals("3")||bean.status.equals("4")){
            holder.tvDengMoney.setText("待配送");
        }else {
            holder.tvDengMoney.setText(bean.statusString);
        }
        holder.tvPrice.setText("¥"+bean.effeAmount);

        //订单类型
        if (bean.channelType==1){
            holder.channel.setText("APP订单");
        }else if (bean.channelType==2){
            holder.channel.setText("线下订单");
        }else {
            holder.channel.setVisibility(View.GONE);
        }

        List<OrdersListBean01.DataBean.ObjectsBean.ItemsBean> items = bean.items;
        LinearLayoutManager linearLayoutManager=new LinearLayoutManager(context,LinearLayoutManager.HORIZONTAL,false);
        holder.recyclerView.setLayoutManager(linearLayoutManager);
        MyOrderInnerRecyclerViewAdapter innerAdapter=new MyOrderInnerRecyclerViewAdapter(context,items,mId);
        holder.recyclerView.setAdapter(innerAdapter);

        holder.llFather.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, OrderDetailsActivity.class);
                intent.putExtra("oid", list.get(position).id);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
            }
        });

        Button zhifu = holder.zhifu;
        Button cancel = holder.cancel;
        zhifu.setText("去支付");
        cancel.setText("取消订单");
        if(onOrderClickListener!=null){
            zhifu.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onOrderClickListener.onClickListener(view,position);
                }
            });
            cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onOrderClickListener.onClickListener(view,position);
                }
            });
        }

        if ("2".equals(bean.status)){
            zhifu.setVisibility(View.VISIBLE);
            cancel.setVisibility(View.VISIBLE);
        }else if ("3".equals(bean.status)||"4".equals(bean.status)){
            zhifu.setVisibility(View.INVISIBLE);
            cancel.setVisibility(View.VISIBLE);
        }else {
            zhifu.setVisibility(View.INVISIBLE);
            cancel.setVisibility(View.INVISIBLE);
        }
    }

    /**
     * 将长时间格式字符串转换为时间 yyyy-MM-dd
     *
     */
    public static String getStringDate(Long date) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd", Locale.CHINA);
        return formatter.format(date);
    }
    /**
     * 将长时间格式字符串转换为时间 kk:mm:ss
     *
     */
    public static String getStringTime(Long date) {
        SimpleDateFormat formatter = new SimpleDateFormat("kk:mm:ss", Locale.CHINA);
        return formatter.format(date);
    }

    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tvData;
        TextView tvTime;
        TextView channel;
        TextView tvDengMoney;
        TextView tvPrice;
        Button zhifu;
        Button cancel;
        LinearLayout root;
        private final LinearLayout llFather;
        private final RecyclerView recyclerView;

        public MyViewHolder(View itemView) {
            super(itemView);

            recyclerView = (RecyclerView) itemView.findViewById(R.id.scrollView_order);
            llFather = (LinearLayout) itemView.findViewById(R.id.ll_father);
            tvData = (TextView) itemView.findViewById(R.id.tv_data);
            tvTime = (TextView) itemView.findViewById(R.id.tv_time);
            channel= (TextView) itemView.findViewById(R.id.tv_channel);
            tvDengMoney = (TextView) itemView.findViewById(R.id.tv_deng_money);
            tvPrice = (TextView) itemView.findViewById(R.id.tv_price);
            zhifu = (Button) itemView.findViewById(R.id.btn_zhifu);
            cancel = (Button) itemView.findViewById(R.id.btn_cancel);
            root = (LinearLayout) itemView.findViewById(R.id.root);
        }
    }
}
