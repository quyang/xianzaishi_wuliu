package com.xianzaishi.normandie.adapter;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.umeng.analytics.MobclickAgent;
import com.xianzaishi.normandie.GoodsDetailsActivity;
import com.xianzaishi.normandie.R;
import com.xianzaishi.normandie.bean.HomeDownBean;
import com.xianzaishi.normandie.global.Contants;
import com.xianzaishi.normandie.utils.GlideUtils;
import com.xianzaishi.normandie.utils.UiUtils;

import java.util.List;

/**
 * quyang
 * <p/> 4lou 一图加列表  第二页
 * Created by Administrator on 2016/9/18.
 */
public class FoorFourAdapterTwo extends RecyclerView.Adapter<FoorFourAdapterTwo.MyViewHolder> {


    private List<HomeDownBean.ModuleBean.ItemsBean> itemsBeen4;
    private Activity mActivity;

    //constructor
    public FoorFourAdapterTwo(List<HomeDownBean.ModuleBean.ItemsBean> itemsBeen4, Activity activity) {
        this.itemsBeen4 = itemsBeen4;
        mActivity = activity;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MyViewHolder(UiUtils.getLayoutInflater().inflate(R.layout.new_foor_four_item, null));
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder,  int position) {

        final HomeDownBean.ModuleBean.ItemsBean itemsBean = itemsBeen4.get(position);
        List<HomeDownBean.ModuleBean.ItemsBean.ItemSkuVOsBean> os = itemsBean.itemSkuVOs;

        HomeDownBean.ModuleBean.ItemsBean.ItemSkuVOsBean bean = os.get(0);
        //规格
        String info = "¥"+itemsBean.discountPriceYuanString+"/"+bean.saleDetailInfo;
        SpannableString spannableString=new SpannableString(info);
        ForegroundColorSpan foregroundColorSpan=new ForegroundColorSpan(mActivity.getResources().getColor(R.color.colorRed));
        spannableString.setSpan(foregroundColorSpan,0,info.indexOf("/"),Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        GlideUtils.LoadImage(UiUtils.getContext(), itemsBean.picUrl, holder.icon);
        String title = itemsBean.title;
        if (title.length() > 6) {
            title = title.substring(0, 5);
        }

        holder.name.setText(title + "");

        holder.des.setText(spannableString);
        holder.des.setTextColor(UiUtils.getColor(R.color.colorLightBlack));


        holder.mRoot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MobclickAgent.onEvent(mActivity,"thirdSectionGoods");//第三分区商品点击
                MobclickAgent.onEvent(mActivity,"tapItem");

                Intent intent = new Intent(mActivity, GoodsDetailsActivity.class);
                intent.putExtra(Contants.ITEM_ID, itemsBean.itemId);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                UiUtils.getContext().startActivity(intent);
            }
        });


    }

    @Override
    public int getItemCount() {
        return itemsBeen4 == null ? 0 : itemsBeen4.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private final ImageView icon;
        private final TextView name;
        private final TextView des;
        private final LinearLayout mRoot;

        public MyViewHolder(View itemView) {
            super(itemView);
            icon = (ImageView) itemView.findViewById(R.id.iv_icon);
            name = (TextView) itemView.findViewById(R.id.name);
            des = (TextView) itemView.findViewById(R.id.des);
            mRoot = (LinearLayout) itemView.findViewById(R.id.root);

        }
    }
}
