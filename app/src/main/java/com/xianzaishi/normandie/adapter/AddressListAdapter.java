package com.xianzaishi.normandie.adapter;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.xianzaishi.normandie.AddressActivity;
import com.xianzaishi.normandie.EditAddressActivity;
import com.xianzaishi.normandie.MyApplication;
import com.xianzaishi.normandie.R;
import com.xianzaishi.normandie.bean.SelectAddressBean;
import com.xianzaishi.normandie.common.Urls;
import com.xianzaishi.normandie.global.Contants;
import com.xianzaishi.normandie.utils.GetBeanClass;
import com.xianzaishi.normandie.utils.SPUtils;
import com.xianzaishi.normandie.utils.UiUtils;

import java.io.IOException;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class AddressListAdapter extends BaseAdapter {
    private List<SelectAddressBean.DataBean> list;
    private LayoutInflater inflater;
    private boolean isShow;//点击标题栏的编辑选项时控制item里两个图片按钮的显示
    private LinearLayout container;
    private TextView editAdress;
    private Context context;
    private OkHttpClient client= MyApplication.getOkHttpClient();
    public AddressListAdapter(Context context, List list, LinearLayout container,TextView editAdress) {
        this.list = list;
        inflater = LayoutInflater.from(context);
        this.container = container;
        this.editAdress=editAdress;
        this.context=context;
    }

    public void setShow(boolean show) {
        isShow = show;
    }

    @Override
    public int getCount() {
        return list==null?0:list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.address_item, null);
            viewHolder = new ViewHolder(convertView);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        SelectAddressBean.DataBean dataBean=list.get(i);
        viewHolder.name.setText(dataBean.getName());
        viewHolder.phone.setText(dataBean.getPhone());
        String addr=dataBean.getCodeAddress()+dataBean.getAddress();
        viewHolder.address.setText(addr);
        ClickListener clickListener=new ClickListener(i);
        viewHolder.delete.setOnClickListener(clickListener);
        viewHolder.edit.setOnClickListener(clickListener);
        if (isShow) {
            viewHolder.delete.setVisibility(View.VISIBLE);
            viewHolder.edit.setVisibility(View.VISIBLE);
        } else {
            viewHolder.delete.setVisibility(View.INVISIBLE);
            viewHolder.edit.setVisibility(View.INVISIBLE);
        }

        return convertView;
    }

    public static class ViewHolder {
        private ImageView delete,edit;
        private TextView name,phone,address;

        public ViewHolder(View view) {
            name = (TextView) view.findViewById(R.id.name);
            delete = (ImageView) view.findViewById(R.id.delete);
            edit= (ImageView) view.findViewById(R.id.edit);
            address= (TextView) view.findViewById(R.id.address_detail);
            phone= (TextView) view.findViewById(R.id.phone);
        }
    }

    public class ClickListener implements View.OnClickListener {
        private int position;
        public ClickListener(int position) {
            this.position = position;
        }
        @Override
        public void onClick(View view) {
            switch (view.getId()){
                case R.id.delete:
                    dialog(position);
                    break;
                case R.id.edit:
                    Intent intent=new Intent(context, EditAddressActivity.class);
                    Bundle bundle=new Bundle();
                    SelectAddressBean.DataBean dataBean1=list.get(position);
                    int addrID1=dataBean1.getId();
                    int uid1=dataBean1.getUserId();
                    bundle.putInt("addressID",addrID1);
                    bundle.putInt("uid",uid1);
                    intent.putExtras(bundle);
                    context.startActivity(intent);
                    break;
            }
        }
    }
    /**
     *  删除地址时弹出dialog
     */
    private void dialog(final int position){

        LayoutInflater inflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view=inflater.inflate(R.layout.commodity_delete_dialog,null);
        final Dialog dialog=new Dialog(context,R.style.Dialog);
        dialog.setCancelable(false);
        dialog.setContentView(view);
        TextView sure= (TextView) view.findViewById(R.id.commodity_dialog_sure);
        TextView cancel= (TextView) view.findViewById(R.id.commodity_dialog_cancel);
        TextView hint= (TextView) view.findViewById(R.id.dialog_hint);
        hint.setText("确认删除该地址么？");
        sure.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SelectAddressBean.DataBean dataBean=list.get(position);
                final String addrID=String.valueOf(dataBean.getId());
                String uid=String.valueOf(dataBean.getUserId());
                RequestBody formBody=new FormBody.Builder()
                        .add("uid",uid)
                        .add("id",addrID)
                        .build();
                Request request=new Request.Builder()
                        .url(Urls.DEL_LINK_MAN)
                        .post(formBody)
                        .build();
                client.newCall(request).enqueue(new Callback() {
                    @Override
                    public void onFailure(Call call, IOException e) {
                        UiUtils.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(context,"网络请求失败！",Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                    @Override
                    public void onResponse(Call call, Response response) throws IOException {
                        if(!response.isSuccessful()) throw new IOException("Unexpected code"+response);
                        final SelectAddressBean selectAddressBean = (SelectAddressBean) GetBeanClass.getBean(response,SelectAddressBean.class);
                        response.close();
                        UiUtils.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (selectAddressBean.getCode()==1){
                                    String addrid= SPUtils.getStringValue(MyApplication.getContext(), Contants.SP_NAME,"addressId","0");
                                    if (addrid.equals(addrID)){
                                        SPUtils.putStringValue(MyApplication.getContext(), Contants.SP_NAME,"addressId","0");
                                    }
                                    list.remove(position);
                                    notifyDataSetChanged();
                                    /**
                                     * 当显示emptyView的时候隐藏按钮
                                     */
                                    if (getCount()==0) {
                                        container.setVisibility(View.GONE);
                                        editAdress.setVisibility(View.INVISIBLE);
                                    } else {
                                        container.setVisibility(View.VISIBLE);
                                        editAdress.setVisibility(View.VISIBLE);
                                    }
                                }
                            }
                        });

                    }
                });
                dialog.dismiss();
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.show();
        Window dialogWindow = dialog.getWindow();
        WindowManager m = ((AddressActivity)context).getWindowManager();
        Display d = m.getDefaultDisplay(); // 获取屏幕宽、高度
        WindowManager.LayoutParams p = dialogWindow.getAttributes(); // 获取对话框当前的参数值
        //p.height = (int) (d.getHeight() * 0.5); // 高度设置为屏幕的0.6，根据实际情况调整
        p.width = (int) (d.getWidth() * 0.6); // 宽度设置为屏幕的0.6，根据实际情况调整
        dialogWindow.setAttributes(p);
    }
}
