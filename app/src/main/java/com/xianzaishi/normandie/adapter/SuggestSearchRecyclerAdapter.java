package com.xianzaishi.normandie.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.xianzaishi.normandie.R;
import com.xianzaishi.normandie.SearchResultActivity;

import java.util.List;


/**
 * Created by Administrator on 2016/7/20.
 */
public class SuggestSearchRecyclerAdapter extends RecyclerView.Adapter<SuggestSearchRecyclerAdapter.ViewHolder> {
    private Context context;
    private LayoutInflater inflater;
    private List<String> list;

    public SuggestSearchRecyclerAdapter(Context context,List<String> list) {
        this.context = context;
        inflater=LayoutInflater.from(context);
        this.list=list;
    }
    public interface OnHotItemClickListener{
        void onClick(View view,int position);
    }
    private OnHotItemClickListener onHotItemClickListener;

    public void setOnHotItemClickListener(OnHotItemClickListener onHotItemClickListener) {
        this.onHotItemClickListener = onHotItemClickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view=inflater.inflate(R.layout.suggest_search,parent,false);
        SuggestSearchRecyclerAdapter.ViewHolder viewHolder=new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder,int position) {
        holder.textView.setText(list.get(position));
        if (onHotItemClickListener!=null){
            final int p=position;
            holder.textView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onHotItemClickListener.onClick(view,p);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return list==null?0:list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView textView;
        public ViewHolder(View view) {
            super(view);
            textView= (TextView) view.findViewById(R.id.suggest_search_text);
        }
    }
}
