package com.xianzaishi.normandie.adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.xianzaishi.normandie.LeafCategory01Activity;
import com.xianzaishi.normandie.R;
import com.xianzaishi.normandie.bean.CategoryBean;
import com.xianzaishi.normandie.utils.GlideUtils;
import com.xianzaishi.normandie.utils.UiUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * quyang  category
 * <p>
 * Created by Administrator on 2016/9/18.
 */
public class FoorTwoAdapter extends RecyclerView.Adapter<FoorTwoAdapter.MyViewHolder> {

    private ArrayList<CategoryBean.ModuleBean> categoryBeanModule;

    private Activity mActivity;

    //constructor
    public FoorTwoAdapter(ArrayList<CategoryBean.ModuleBean> categoryBeanModule, Activity activity) {
        this.categoryBeanModule = categoryBeanModule;
        this.mActivity = activity;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MyViewHolder(UiUtils.getLayoutInflater().inflate(R.layout.new_foor_two_item, null));
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        GlideUtils.LoadImage(UiUtils.getContext(), categoryBeanModule.get(position).getPic(), holder.icon);
        holder.des.setText(categoryBeanModule.get(position).getName());


        holder.mRoot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(mActivity, LeafCategory01Activity.class);
                intent.putExtra("categoryId",categoryBeanModule.get(position).getCatId());
                intent.putExtra("list",categoryBeanModule);
                intent.putExtra("position",position);
                mActivity.startActivity(intent);
            }
        });


    }


    @Override
    public int getItemCount() {
        return categoryBeanModule == null ? 0 : categoryBeanModule.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private ImageView icon;
        private TextView des;
        private LinearLayout mRoot;

        public MyViewHolder(View itemView) {
            super(itemView);

            icon = (ImageView) itemView.findViewById(R.id.iv_icon);
            des = (TextView) itemView.findViewById(R.id.tv_des);
            mRoot = (LinearLayout) itemView.findViewById(R.id.root);

        }
    }
}
