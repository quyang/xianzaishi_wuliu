package com.xianzaishi.normandie.adapter;

import android.content.Intent;
import android.graphics.Paint;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.umeng.analytics.MobclickAgent;
import com.xianzaishi.normandie.GoodsDetailsActivity;
import com.xianzaishi.normandie.MainActivity;
import com.xianzaishi.normandie.R;
import com.xianzaishi.normandie.bean.HomeTopBean;
import com.xianzaishi.normandie.global.Contants;
import com.xianzaishi.normandie.utils.GlideUtils;
import com.xianzaishi.normandie.utils.UiUtils;

import java.util.List;

/**
 * quyang  文字加列表
 * <p/> 3lou
 * Created by Administrator on 2016/9/18.
 */
public class FoorThreeAdapter extends RecyclerView.Adapter<FoorThreeAdapter.MyViewHolder> {


    private List<HomeTopBean.ModuleBean.ItemsBean> items2;
    private MainActivity mActivity;
    public static int[] startLocation;
    //constructor
    public FoorThreeAdapter(List<HomeTopBean.ModuleBean.ItemsBean> items2, MainActivity activity) {
        this.items2 = items2;
        mActivity=activity;
    }

    /**
     *  自定义一个加购的接口
     */
    public interface AddIntoShoppingCarListener{
        void addIntoShoppingCar(View view,int position,ImageView imageView);
    }

    private AddIntoShoppingCarListener addIntoShoppingCarListener;

    public void setAddIntoShoppingCarListener(AddIntoShoppingCarListener addIntoShoppingCarListener) {
        this.addIntoShoppingCarListener = addIntoShoppingCarListener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MyViewHolder(UiUtils.getLayoutInflater().inflate(R.layout.new_foor_three_item, null));
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        HomeTopBean.ModuleBean.ItemsBean itemsBean = items2.get(position);
        final int itemId = itemsBean.itemId;
        GlideUtils.LoadImage(UiUtils.getContext(), itemsBean.picUrl, holder.icon);
        String title = itemsBean.title;
        int length = title.length();
        holder.name.setText(title);
        String subtitle = itemsBean.itemSkuVOs.get(0).saleDetailInfo;
        holder.des.setText(subtitle);
        holder.priceNow.setText("¥"+itemsBean.discountPriceYuanString);
        holder.priceOld.setText("原价 : " + itemsBean.priceYuanString);
        holder.priceOld.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG);
        /*if (itemsBean.itemSkuVOs.get(0).inventory<=0){
            holder.soldOut.setVisibility(View.VISIBLE);
            holder.car.setEnabled(false);
            holder.car.setBackgroundResource(R.mipmap.shoppingcargrey2x);
        }else {
            holder.soldOut.setVisibility(View.GONE);
            holder.car.setEnabled(true);
            holder.car.setBackgroundResource(R.mipmap.gouwuche2x);
        }*/
        if (addIntoShoppingCarListener!=null){
            holder.car.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    addIntoShoppingCarListener.addIntoShoppingCar(view,position,holder.icon);
                }
            });
        }
        holder.mRoot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MobclickAgent.onEvent(mActivity,"secondSectionGood");//友盟第二分区商品点击
                MobclickAgent.onEvent(mActivity,"tapItem");

                Intent intent = new Intent(mActivity, GoodsDetailsActivity.class);
                intent.putExtra(Contants.ITEM_ID, itemId);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                mActivity.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return items2 == null ? 0 : items2.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {


        private ImageView icon,soldOut;
        private TextView name;
        private TextView des;
        private TextView priceNow;
        private TextView priceOld;
        private ImageView car;
        private LinearLayout mRoot;

        public MyViewHolder(View itemView) {
            super(itemView);
            icon = (ImageView) itemView.findViewById(R.id.iv_icon);
            name = (TextView) itemView.findViewById(R.id.tv_name);
            des = (TextView) itemView.findViewById(R.id.tv_des);
            priceNow = (TextView) itemView.findViewById(R.id.tv_price_now);
            priceOld = (TextView) itemView.findViewById(R.id.tv_old_now);
            car = (ImageView) itemView.findViewById(R.id.iv_car);
            mRoot = (LinearLayout) itemView.findViewById(R.id.root);

        }
    }
}
