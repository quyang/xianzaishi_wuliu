package com.xianzaishi.normandie.adapter;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.xianzaishi.normandie.GoodsDetailsActivity;
import com.xianzaishi.normandie.R;
import com.xianzaishi.normandie.bean.HomeDownBean;
import com.xianzaishi.normandie.utils.UiUtils;

import java.util.List;

/**
 * quyang
 * 无图,即最后一个模块
 * Created by Administrator on 2016/9/19.
 */
public class NoPicAdapterTwo {


    public void set(View view2, List<HomeDownBean.ModuleBean> mModuleBeen, int i, final Activity activity) {

        TextView title = (TextView) view2.findViewById(R.id.title);
        LinearLayout lineLast = (LinearLayout) view2.findViewById(R.id.nineContainer);


        final HomeDownBean.ModuleBean bean = mModuleBeen.get(i);
        List<HomeDownBean.ModuleBean.ItemsBean> items = bean.items;

        title.setText((String) bean.title);

        for (int a = 0; a < items.size(); a++) {

            final HomeDownBean.ModuleBean.ItemsBean item = items.get(a);
            List<HomeDownBean.ModuleBean.ItemsBean.ItemSkuVOsBean> skuVOs = item.itemSkuVOs;

            View child = UiUtils.getLayoutInflater().inflate(R.layout.pager_tw0_itm, null);

            ImageView iconh = (ImageView) child.findViewById(R.id.iv_icon);
            TextView name = (TextView) child.findViewById(R.id.tv_name);
            TextView subtitle = (TextView) child.findViewById(R.id.tv_subtitle);
            TextView quality = (TextView) child.findViewById(R.id.tv_quality);
            TextView discountPrice = (TextView) child.findViewById(R.id.tv_discount_price);
            TextView youhui = (TextView) child.findViewById(R.id.tv_youhui);
            ImageView ivShopping = (ImageView) child.findViewById(R.id.iv_shopping);
            RelativeLayout root = (RelativeLayout) child.findViewById(R.id.root);


            Glide.with(UiUtils.getContext()).load(item.picUrl).diskCacheStrategy(DiskCacheStrategy.ALL).override(246, 246).into(iconh);
            name.setText(item.title);
            subtitle.setText(item.subtitle);
            quality.setText(skuVOs.get(0).saleDetailInfo);
            discountPrice.setText(item.discountPriceYuanString);
            youhui.setText(skuVOs.get(0).promotionInfo + "");


            final int b = a;
            root.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Toast.makeText(UiUtils.getContext(), " position = " + b + ",itemId = " + item.itemId, Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(activity, GoodsDetailsActivity.class);
                    intent.putExtra("itemId", item.itemId);
                    UiUtils.getContext().startActivity(intent);
                }
            });


            lineLast.addView(child);

        }


    }
}
