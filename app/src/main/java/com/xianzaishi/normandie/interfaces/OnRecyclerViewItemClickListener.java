package com.xianzaishi.normandie.interfaces;

import android.view.View;

/**
 * quyang
 * Created by Administrator on 2016/8/31.
 */
public interface OnRecyclerViewItemClickListener {
    void onItemClick(View view, int position);
}
