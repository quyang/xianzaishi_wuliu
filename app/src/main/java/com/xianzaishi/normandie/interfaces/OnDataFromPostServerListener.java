package com.xianzaishi.normandie.interfaces;

/**
 * Created by Administrator on 2016/9/24.
 */

public interface OnDataFromPostServerListener {
    void onSuccess(String result);
    void onFail();
}
