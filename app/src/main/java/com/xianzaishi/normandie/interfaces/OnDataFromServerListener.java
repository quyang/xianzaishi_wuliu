package com.xianzaishi.normandie.interfaces;

/**
 * quyang
 * 请求网络的接口
 * Created by Administrator on 2016/9/3.
 */
public interface OnDataFromServerListener {

    void onSuccess(Object result);
    void onFail();

}
