package com.xianzaishi.normandie;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.xianzaishi.normandie.global.Contants;
import com.xianzaishi.normandie.utils.SPUtils;
import java.util.ArrayList;
import java.util.List;
/**
 * 向导界面
 */
public class GuideActivity extends AppCompatActivity {

    private ViewPager viewPager;
    private LinearLayout guideParent;
    private List<ImageView> guideIndicators=new ArrayList<>();
    private int[] pics = new int[]{R.mipmap.guide0,R.mipmap.guide1,R.mipmap.guide2};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guide_activity_one);
        initViewPager();
    }

    private void initViewPager() {
        viewPager = (ViewPager) findViewById(R.id.viewPager);
        guideParent= (LinearLayout) findViewById(R.id.guide_parent_flash);
        int length=pics.length;
        for (int i = 0; i < length; i++) {
            guideIndicators.add((ImageView) guideParent.getChildAt(i));
        }
        viewPager.setAdapter(new MyPagerAdapter());
        guideIndicators.get(0).setEnabled(false);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }
            @Override
            public void onPageSelected(int position) {
                for (int i = 0; i < 3; i++) {
                    if (i==position){
                        guideIndicators.get(i).setEnabled(false);
                    }else {
                        guideIndicators.get(i).setEnabled(true);
                    }
                }
            }
            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private class MyPagerAdapter extends PagerAdapter {
        @Override
        public int getCount() {
            return pics.length;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {

            ImageView imageView = new ImageView(GuideActivity.this);
            imageView.setImageResource(pics[position]);
            container.addView(imageView);

            if (position == getCount() -1) {
                imageView.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(GuideActivity.this,MainActivity.class);
                        startActivity(intent);
                        SPUtils.putBooleanValue(MyApplication.getContext(),Contants.SP_NAME,Contants.IS_FIRST,true);
                        finish();
                    }
                });
            }

            return imageView;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }
    }
}
