package com.xianzaishi.normandie;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.xianzaishi.normandie.bean.LoginAndRegister;
import com.xianzaishi.normandie.bean.NewLoginBean;
import com.xianzaishi.normandie.common.Urls;
import com.xianzaishi.normandie.global.Contants;
import com.xianzaishi.normandie.utils.GetBeanClass;
import com.xianzaishi.normandie.utils.JudgePhoneNumber;
import com.xianzaishi.normandie.utils.SPUtils;
import com.xianzaishi.normandie.utils.UiUtils;
import com.xianzaishi.normandie.utils.ValidateCountDownTimer;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class NewLoginActivity extends AppCompatActivity implements View.OnClickListener{
    private EditText phone,fillValidate;
    private Button getValidate,loginButton;
    private OkHttpClient client=MyApplication.getOkHttpClient();
    private MediaType MEDIA_TYPE_MARKDOWN;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_login);
        getWindow().setBackgroundDrawableResource(R.mipmap.login_background);//设置背景
        initView();
    }

    private void initView() {
        phone= (EditText) findViewById(R.id.newLogin_phone);
        fillValidate= (EditText) findViewById(R.id.newLogin_fillValidate);
        getValidate= (Button) findViewById(R.id.newLogin_getValidate);
        getValidate.setOnClickListener(this);
        loginButton= (Button) findViewById(R.id.newLogin_loginButton);
        loginButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.newLogin_getValidate:
                String mobileNumber=phone.getText().toString().trim();
                if(JudgePhoneNumber.judgeMobileNumber(this,mobileNumber)){
                    new ValidateCountDownTimer(100000,1000,getValidate).start();//点击获取验证码后开始倒计时
                    MEDIA_TYPE_MARKDOWN=MediaType.parse("text/x-markdown; charset=utf-8");
                    String postBody="{\"phone\":\"%s\"}";
                    Request request=new Request.Builder()
                            .url(Urls.MINE_SENDCHECKCODE)
                            .post(RequestBody.create(MEDIA_TYPE_MARKDOWN,String.format(postBody,mobileNumber)))
                            .build();
                    client.newCall(request).enqueue(new Callback() {
                        @Override
                        public void onFailure(Call call, IOException e) {
                        }
                        @Override
                        public void onResponse(Call call, Response response) throws IOException {
                            if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);
                            response.close();
                        }
                    });
                }
                break;
            case R.id.newLogin_loginButton:
                TelephonyManager tm= (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
                String IMEI=tm.getDeviceId();
                String phoneNumber=phone.getText().toString();
                String validate=fillValidate.getText().toString();
                if(TextUtils.isEmpty(phoneNumber)){
                    Toast toast=Toast.makeText(NewLoginActivity.this,"手机号和验证码不能为空",Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER,0,0);
                    toast.show();
                }else if(!JudgePhoneNumber.judgeMobileNumber(NewLoginActivity.this,phoneNumber)){
                }else if (TextUtils.isEmpty(validate)){
                    Toast toast=Toast.makeText(NewLoginActivity.this,"手机号和验证码不能为空",Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER,0,0);
                    toast.show();
                }else {
                    String postBody="{\"phone\":\"%s\",\"verificationCode\":\"%s\",\"hardwareCode\":\"%s\"}";
                    Request request=new Request.Builder()
                            .url(Urls.MINE_LOGIN)
                            .post(RequestBody.create(MEDIA_TYPE_MARKDOWN,String.format(postBody,phoneNumber,validate,IMEI)))
                            .build();
                    client.newCall(request).enqueue(new Callback() {
                        @Override
                        public void onFailure(Call call, IOException e) {
                            UiUtils.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast toast=Toast.makeText(NewLoginActivity.this,"网络访问失败",Toast.LENGTH_SHORT);
                                    toast.setGravity(Gravity.CENTER,0,0);
                                    toast.show();
                                }
                            });
                        }
                        @Override
                        public void onResponse(Call call, Response response) throws IOException {
                            if(!response.isSuccessful()) throw new IOException("Unexpected code"+response);
                            NewLoginBean loginAndRegister= (NewLoginBean) GetBeanClass.getBean(response,NewLoginBean.class);
                            response.close();//响应使用之后关闭
                            if(loginAndRegister.getResultCode()==1){
                                String token=loginAndRegister.getModule().getToken();
                                boolean isNewUser=loginAndRegister.getModule().isNewUser();
                                SPUtils.putBooleanValue(MyApplication.getContext(),Contants.SP_NAME,"isNewUser",isNewUser);
                                SPUtils.putStringValue(MyApplication.getContext(), Contants.SP_NAME,"token",token);
                                if (isNewUser){
                                    SPUtils.putBooleanValue(MyApplication.getContext(),Contants.SP_NAME,"LoginChecked",true);
                                }
                                getUID(token);
                            }else if(loginAndRegister.getResultCode()==-6){
                                UiUtils.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast toast=Toast.makeText(NewLoginActivity.this,"验证码过期",Toast.LENGTH_SHORT);
                                        toast.setGravity(Gravity.CENTER,0,0);
                                        toast.show();
                                    }
                                });
                            }else if (loginAndRegister.getResultCode()==-8){
                                UiUtils.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast toast=Toast.makeText(NewLoginActivity.this,"验证码错误",Toast.LENGTH_SHORT);
                                        toast.setGravity(Gravity.CENTER,0,0);
                                        toast.show();
                                    }
                                });
                            }else {
                                UiUtils.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast toast=Toast.makeText(NewLoginActivity.this,"未知错误",Toast.LENGTH_SHORT);
                                        toast.setGravity(Gravity.CENTER,0,0);
                                        toast.show();
                                    }
                                });
                            }
                        }
                    });
                }
                break;
        }
    }
    /**
     *  根据token获取uid  String tokenUrl = "http://139.196.92.240/wapcenter/requestuser/mine?"token=" + token;
     */
    private void getUID(String token){
        String requestBody="{\"token\":"+"\""+token+"\""+"}";
        Request request=new Request.Builder()
                .url(Urls.GET_UID)
                .post(RequestBody.create(MEDIA_TYPE_MARKDOWN,requestBody))
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if(!response.isSuccessful()) throw new IOException("Unexpected code"+response);
                LoginAndRegister loginAndRegister= (LoginAndRegister) GetBeanClass.getBean(response,LoginAndRegister.class);
                response.close();//响应使用之后关闭
                if (loginAndRegister.getResultCode()==1){
                    if(loginAndRegister.getModule()!=null) {
                        String uid = String.valueOf(loginAndRegister.getModule());
                        SPUtils.putStringValue(MyApplication.getContext(), Contants.SP_NAME,Contants.UID,uid);
                    }
                }
                finish();//登录成功后关闭当前页面
            }
        });

    }
}
