package com.xianzaishi.normandie;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.umeng.analytics.MobclickAgent;
import com.xianzaishi.normandie.adapter.LCFragmentPageAdapter;
import com.xianzaishi.normandie.bean.CategoryBean;
import com.xianzaishi.normandie.bean.ShoppingCarCountBean;
import com.xianzaishi.normandie.common.Urls;
import com.xianzaishi.normandie.fragment.LeafCategoryFragment;
import com.xianzaishi.normandie.global.Contants;
import com.xianzaishi.normandie.utils.GetBeanClass;
import com.xianzaishi.normandie.utils.SPUtils;
import com.xianzaishi.normandie.utils.UiUtils;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class LeafCategory01Activity extends AppCompatActivity implements View.OnClickListener{
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private RelativeLayout footView;//根布局
    private int[] parentLocation,endLocation;
    private ArrayList<CategoryBean.ModuleBean> list;//一级分类传递过来的集合
    private int viewPagerPosition;//viewpager应显示第几页内容
    private ArrayList<String> tabNameList=new ArrayList<>();//TabLayout数据源
    private ArrayList<Integer> catIdList=new ArrayList<>();//CatID集合
    private ArrayList<LeafCategoryFragment> categoryFragmentList=new ArrayList<>();//Fragment的集合
    private ImageView shoppingTrolley;
    private TextView shoppingCount;
    private boolean isFirstCreat=true;

    @Override
    protected void onResume() {
        super.onResume();
        getShoppingCount();
        MobclickAgent.onResume(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_leaf_category01);
        initView();
    }

    @Override
    protected void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        if(savedInstanceState!= null)

        {
            String FRAGMENTS_TAG = "Android:support:fragments";
            savedInstanceState.remove(FRAGMENTS_TAG);
        }

    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);

        footView= (RelativeLayout) findViewById(R.id.activity_leaf_category01);
        parentLocation=new int[2];
        footView.getLocationInWindow(parentLocation);
        endLocation=new int[2];
        shoppingTrolley.getLocationInWindow(endLocation);
        if (isFirstCreat){//让init（）方法只在此activity创建时调用一次，防止出现重复加载出错
            init();
            isFirstCreat=false;
        }

    }

    private void init() {
        /**
         *  为三个集合赋值
         */
        for (CategoryBean.ModuleBean bean:list){
            tabNameList.add(bean.getName());
            catIdList.add(bean.getCatId());
        }
        for(int catId:catIdList){
            LeafCategoryFragment fragment=new LeafCategoryFragment();
            Bundle bundle=new Bundle();
            bundle.putInt("catID",catId);
            bundle.putInt("position",viewPagerPosition);
            bundle.putIntArray("parentLocation",parentLocation);
            bundle.putIntArray("endLocation",endLocation);
            fragment.setArguments(bundle);
            categoryFragmentList.add(fragment);
        }
        FragmentManager fragmentManager=this.getSupportFragmentManager();
        tabLayout= (TabLayout) findViewById(R.id.leafCategory_tabLayout);//搜索下面的Tab
        viewPager= (ViewPager) findViewById(R.id.leafCategory_viewPager);
        viewPager.setAdapter(new LCFragmentPageAdapter(fragmentManager,categoryFragmentList,tabNameList));
        tabLayout.setupWithViewPager(viewPager);
        viewPager.setCurrentItem(viewPagerPosition);
    }
    private void initView() {
        initIntent();//获取一级分类传过来的数据

        View view=findViewById(R.id.classification_search_include);
        /**
         *  顶部返回
         */
        ImageView back= (ImageView) view.findViewById(R.id.go_back);
        back.setOnClickListener(this);
        /**
         *  搜索框，点击跳转到搜索页面
         */
        TextView seachText= (TextView) view.findViewById(R.id.search_text);
        seachText.setOnClickListener(this);
        /**
         *  购物车，点击跳转到购物车
         */
        shoppingTrolley= (ImageView) view.findViewById(R.id.shopping_trolley);
        shoppingTrolley.setOnClickListener(this);
        /**
         *  购物车商品数量指示器
         */
        shoppingCount= (TextView) view.findViewById(R.id.shopping_trolley_count);
        /**
         *  访问服务器获取购物车商品总数
         */

    }



    private void getShoppingCount() {
        OkHttpClient client=MyApplication.getOkHttpClient();
        String uid= SPUtils.getStringValue(MyApplication.getContext(), Contants.SP_NAME,Contants.UID,"");
        RequestBody requestForm=new FormBody.Builder()
                .add("uid",uid)
                .build();
        final Request request=new Request.Builder()
                .url(Urls.GET_ALL_COUNT)
                .post(requestForm)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if(!response.isSuccessful()) throw new IOException("Unexpected code"+response);
                final ShoppingCarCountBean shoppingCarCountBean = (ShoppingCarCountBean) GetBeanClass.getBean(response,ShoppingCarCountBean.class);
                response.close();
                if (shoppingCarCountBean.getCode()==1){
                    UiUtils.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if(!"0".equals(shoppingCarCountBean.getData())){
                                shoppingCount.setVisibility(View.VISIBLE);
                                shoppingCount.setText(shoppingCarCountBean.getData());
                            }
                        }
                    });
                }
            }
        });
    }

    private void initIntent() {
        Intent intent=getIntent();
        list= (ArrayList<CategoryBean.ModuleBean>) intent.getSerializableExtra("list");
        viewPagerPosition=intent.getIntExtra("position",0);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.go_back://返回上一级
                finish();
                break;
            case R.id.search_text://去到搜索页面
                startActivity(new Intent(LeafCategory01Activity.this,SearchActivity.class));
                break;
            case R.id.shopping_trolley:
                MobclickAgent.onEvent(LeafCategory01Activity.this,"goToCart");
                MobclickAgent.onEvent(LeafCategory01Activity.this,"clickSortCart");

                Intent intent=new Intent(LeafCategory01Activity.this,MainActivity.class);
                intent.putExtra("pageNo",3);
                startActivity(intent);
                break;
        }
    }
    @Override
    protected void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
    }
}
