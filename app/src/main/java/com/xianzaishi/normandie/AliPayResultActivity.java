package com.xianzaishi.normandie;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;

public class AliPayResultActivity extends AppCompatActivity implements View.OnClickListener{
    private TextView tradeNumber,payFor,date,time,payWay;
    private Button checkOrder,goBuy;
    private static long ONE_HOUR=3600000;
    private String oid;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ali_pay_result);
        initView();
    }

    private void initView() {
        Intent intent=getIntent();
        oid=intent.getStringExtra("aliResult");
        String pay=intent.getStringExtra("pay");
        String times=intent.getStringExtra("time");
        String channel=intent.getStringExtra("channel");
        /**
         *  找到控件
         */
        tradeNumber= (TextView) findViewById(R.id.ali_pay_result_number1);
        tradeNumber.setText(oid);
        payFor= (TextView) findViewById(R.id.ali_pay_result_pay1);
        payFor.setText(pay);

        payWay= (TextView) findViewById(R.id.ali_pay_result_channel1);
        payWay.setText(channel);
        checkOrder= (Button) findViewById(R.id.ali_pay_result_check);
        checkOrder.setOnClickListener(this);

        goBuy= (Button) findViewById(R.id.ali_pay_result_buy);
        goBuy.setOnClickListener(this);

        date= (TextView) findViewById(R.id.ali_pay_result_date);
        time= (TextView) findViewById(R.id.ali_pay_result_time);
        if(times.equals("")){
            long millions=System.currentTimeMillis();
            int hour=getHourTime(millions);
            date.setText(getStringDate(millions));
            if (hour>=8&&hour<21){
                time.setText(getStringTime(millions+20*60*1000)+"-"+getStringTime(millions+30*60*1000));
            }else {
                time.setText("08:00-09:00");
            }
        }else {
            long millions=getTimeMillions(times);
            date.setText(getStringDate(millions));
            time.setText(getStringTime(millions)+"-"+getStringTime(millions+ONE_HOUR));
        }
    }

    /**
     *  根据yyyyMMddhhmmss的时间参数获取对应的毫秒数
     */
    private long getTimeMillions(String time){
        SimpleDateFormat format=new SimpleDateFormat("yyyyMMddhhmmss", Locale.CHINA);
        try {
            return format.parse(time).getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return -1;
    }
    /**
     * 将长时间格式字符串转换为时间 yyyy-MM-dd
     *
     */
    public static String getStringDate(Long date) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd", Locale.CHINA);
        return formatter.format(date);
    }
    /**
     * 将长时间格式字符串转换为时间 kk:mm
     *
     */
    public static String getStringTime(Long date) {
        SimpleDateFormat formatter = new SimpleDateFormat("kk:mm", Locale.CHINA);
        return formatter.format(date);
    }
    /**
     * 将长时间格式字符串转换为时间 kk
     *
     */
    public static int getHourTime(Long date) {
        SimpleDateFormat formatter = new SimpleDateFormat("kk", Locale.CHINA);
        return Integer.valueOf(formatter.format(date));
    }
    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.ali_pay_result_check:
                Intent intent1 = new Intent(this, OrderDetailsActivity.class);
                intent1.putExtra("oid", oid);
                intent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent1);
                finish();
                break;
            case R.id.ali_pay_result_buy:
                Intent intent=new Intent(this,MainActivity.class);
                intent.putExtra("pageNo",0);
                startActivity(intent);
                finish();
                break;
        }
    }
}
