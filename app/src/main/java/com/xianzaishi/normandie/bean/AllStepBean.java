package com.xianzaishi.normandie.bean;

/**
 * Created by ShenLang on 2016/12/26.
 *  获取首页所有楼层id
 */

public class AllStepBean {

    /**
     * stepids : 134,160,135,10,136,143,144,145,146,147,148,157,158,159,149
     * color : E8333B
     */

    private String stepids;
    private String color;

    public String getStepids() {
        return stepids;
    }

    public void setStepids(String stepids) {
        this.stepids = stepids;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
}
