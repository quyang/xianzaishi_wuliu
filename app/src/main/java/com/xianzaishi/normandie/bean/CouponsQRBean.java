package com.xianzaishi.normandie.bean;

/**
 * Created by Administrator on 2016/10/24.
 */

public class CouponsQRBean {

    /**
     * code : 1
     * message : dajaa
     * data : {"couponTitle":"daaafffffffdf","couponId":2621,"couponStartTime":1477281111000,"couponEndTime":1479873111000,"security":"jhpDOFMU6nzfDT5y/Ehxou/ckJcopHt1gVG7ybeSX5QM7F9p+SdcsMgjNOuNqSJjjPh/4adKlUTfvjp6UNvIRg==","sign":"PASnXMKNxMMrCgpocIixORIQe4sveD4uqe0gWId08U6cOm58SVHhrqqWgWJ6x4VJz2ookMCp/AVIcTvulGtKOFpwGVvVga9Ha5+PbK80KNdiaXqY2tWnFDS57Pkc56wrRtwim1ZMhMldCPdAtlaM/QoJfRP25mBXtWQvMP1+5Fs="}
     * success : true
     * error : false
     */

    private int code;
    private String message;
    /**
     * couponTitle : daaafffffffdf
     * couponId : 2621
     * couponStartTime : 1477281111000
     * couponEndTime : 1479873111000
     * security : jhpDOFMU6nzfDT5y/Ehxou/ckJcopHt1gVG7ybeSX5QM7F9p+SdcsMgjNOuNqSJjjPh/4adKlUTfvjp6UNvIRg==
     * sign : PASnXMKNxMMrCgpocIixORIQe4sveD4uqe0gWId08U6cOm58SVHhrqqWgWJ6x4VJz2ookMCp/AVIcTvulGtKOFpwGVvVga9Ha5+PbK80KNdiaXqY2tWnFDS57Pkc56wrRtwim1ZMhMldCPdAtlaM/QoJfRP25mBXtWQvMP1+5Fs=
     */

    private DataBean data;
    private boolean success;
    private boolean error;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public static class DataBean {
        private String couponTitle;
        private int couponId;
        private long couponStartTime;
        private long couponEndTime;
        private String security;
        private String sign;

        public String getCouponTitle() {
            return couponTitle;
        }

        public void setCouponTitle(String couponTitle) {
            this.couponTitle = couponTitle;
        }

        public int getCouponId() {
            return couponId;
        }

        public void setCouponId(int couponId) {
            this.couponId = couponId;
        }

        public long getCouponStartTime() {
            return couponStartTime;
        }

        public void setCouponStartTime(long couponStartTime) {
            this.couponStartTime = couponStartTime;
        }

        public long getCouponEndTime() {
            return couponEndTime;
        }

        public void setCouponEndTime(long couponEndTime) {
            this.couponEndTime = couponEndTime;
        }

        public String getSecurity() {
            return security;
        }

        public void setSecurity(String security) {
            this.security = security;
        }

        public String getSign() {
            return sign;
        }

        public void setSign(String sign) {
            this.sign = sign;
        }
    }
}
