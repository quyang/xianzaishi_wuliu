package com.xianzaishi.normandie.bean;

import java.util.List;

/**
 * qyang
 * http://139.196.92.240/wapcenter/requesthome/homepage?pageId=1&hasAllFloor=true
 * Created by Administrator on 2016/8/30.
 */
public class HomeDownBean {


    public int resultCode;// 1,
    public boolean success;// true,
    public String errorMsg;// ,
    public List<ModuleBean> module;//
    public boolean succcess;// true


    public static class ModuleBean {

        public List<PicListBean> picList;//
        public String title;// null,
        public List<ItemsBean> items;// [
        public int stepType;// null


        public static class ItemsBean {

            public int itemId;// 10201656,
            public String picUrl;// http;////img.alicdn.com/imgextra/i3/496514980/TB2tdt5aVXXXXa0XXXXXXXXXXXX-496514980.jpg,
            public String title;// 活雪蟹 800-1200g/只,
            public String subtitle;// 活雪蟹,
            public String price;// 1,
            public String discountPrice;// 1,
            public String priceYuanString;// 0.01,
            public String discountPriceYuanString;// 0.01,
            public String sku;// 56,
            public Object feature;// null,
            public int inventory;// 1,
            public List<ItemSkuVOsBean> itemSkuVOs;//

            public class ItemSkuVOsBean {

                public String skuId;// 56,
                public String itemId;// 10201656,
                public int inventory;// 1,
                public String skuUnit;// 68,
                public String title;// 活雪蟹 800-1200g/只,
                public String quantity;// 0,
                public Object promotionInfo;// null,
                public String price;// 1,
                public String discountPrice;// 1,
                public String priceYuanString;// 0.01,
                public String discountPriceYuanString;// 0.01,
                public String saleDetailInfo;// 1只,
                public String originplace;// 朝鲜


                @Override
                public String toString() {
                    return "ItemSkuVOsBean{" +
                            "discountPrice='" + discountPrice + '\'' +
                            ", skuId='" + skuId + '\'' +
                            ", itemId='" + itemId + '\'' +
                            ", inventory='" + inventory + '\'' +
                            ", skuUnit='" + skuUnit + '\'' +
                            ", title='" + title + '\'' +
                            ", quantity='" + quantity + '\'' +
                            ", promotionInfo=" + promotionInfo +
                            ", price='" + price + '\'' +
                            ", priceYuanString='" + priceYuanString + '\'' +
                            ", discountPriceYuanString='" + discountPriceYuanString + '\'' +
                            ", saleDetailInfo='" + saleDetailInfo + '\'' +
                            ", originplace='" + originplace + '\'' +
                            '}';
                }
            }



            @Override
            public String toString() {
                return "ItemsBean{" +
                        "discountPrice='" + discountPrice + '\'' +
                        ", itemId=" + itemId +
                        ", picUrl='" + picUrl + '\'' +
                        ", title='" + title + '\'' +
                        ", subtitle='" + subtitle + '\'' +
                        ", price='" + price + '\'' +
                        ", priceYuanString='" + priceYuanString + '\'' +
                        ", discountPriceYuanString='" + discountPriceYuanString + '\'' +
                        ", sku='" + sku + '\'' +
                        ", feature='" + feature + '\'' +
                        ", inventory='" + inventory + '\'' +
                        ", itemSkuVOs=" + itemSkuVOs +
                        '}';
            }
        }


        public static class PicListBean {

            public String picUrl;// img.alicdn.com/imgextra/i3/496514980/TB2tdt5aVXXXXa0XXXXXXXXXXXX-496514980.jpg,
            public String targetId;// 1,
            public int targetType;// 1


            @Override
            public String toString() {
                return "ListBean{" +
                        "picUrl='" + picUrl + '\'' +
                        ", targetId='" + targetId + '\'' +
                        ", targetType=" + targetType +
                        '}';
            }
        }

    }


}





