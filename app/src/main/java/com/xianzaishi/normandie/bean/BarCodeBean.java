package com.xianzaishi.normandie.bean;

/**
 * Created by Administrator on 2016/11/7.
 */

public class BarCodeBean {

    /**
     * resultCode : 1
     * success : true
     * errorMsg :
     * module : {"id":10202025,"title":"朝日啤酒 超爽黑啤500ml/罐 日本进口","subTitle":"朝日啤酒 超爽黑啤500ml/罐 日本进口","catId":0,"picList":null,"introduction":null,"propertyVO":null,"supplierName":"杭州大热电子商务有限公司","purchasingAgentName":null,"status":0,"purchaseFlowStatus":0,"checkReason":null,"checkUserName":null,"titletags":null,"displayCheckInfo":false,"displayCheckButton":false,"skuId":20019,"sku69Id":4901004020062,"productId":0,"price":1490,"unitPrice":1490,"relatedItems":null,"elementList":null,"element":null,"displaySpecifications":null,"displaySpecificationList":null,"specification":"1罐","unStandardSpecification":"1罐","storageType":79,"saleUnitType":3,"saleRange":1,"saleUnitStr":"罐"}
     * succcess : true
     */

    private int resultCode;
    private boolean success;
    private String errorMsg;
    /**
     * id : 10202025
     * title : 朝日啤酒 超爽黑啤500ml/罐 日本进口
     * subTitle : 朝日啤酒 超爽黑啤500ml/罐 日本进口
     * catId : 0
     * picList : null
     * introduction : null
     * propertyVO : null
     * supplierName : 杭州大热电子商务有限公司
     * purchasingAgentName : null
     * status : 0
     * purchaseFlowStatus : 0
     * checkReason : null
     * checkUserName : null
     * titletags : null
     * displayCheckInfo : false
     * displayCheckButton : false
     * skuId : 20019
     * sku69Id : 4901004020062
     * productId : 0
     * price : 1490
     * unitPrice : 1490
     * relatedItems : null
     * elementList : null
     * element : null
     * displaySpecifications : null
     * displaySpecificationList : null
     * specification : 1罐
     * unStandardSpecification : 1罐
     * storageType : 79
     * saleUnitType : 3
     * saleRange : 1
     * saleUnitStr : 罐
     */

    private ModuleBean module;
    private boolean succcess;

    public int getResultCode() {
        return resultCode;
    }

    public void setResultCode(int resultCode) {
        this.resultCode = resultCode;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public ModuleBean getModule() {
        return module;
    }

    public void setModule(ModuleBean module) {
        this.module = module;
    }

    public boolean isSucccess() {
        return succcess;
    }

    public void setSucccess(boolean succcess) {
        this.succcess = succcess;
    }

    public static class ModuleBean {
        private int id;
        private String title;
        private String subTitle;
        private int catId;
        private Object picList;
        private Object introduction;
        private Object propertyVO;
        private String supplierName;
        private Object purchasingAgentName;
        private int status;
        private int purchaseFlowStatus;
        private Object checkReason;
        private Object checkUserName;
        private Object titletags;
        private boolean displayCheckInfo;
        private boolean displayCheckButton;
        private int skuId;
        private long sku69Id;
        private int productId;
        private int price;
        private int unitPrice;
        private Object relatedItems;
        private Object elementList;
        private Object element;
        private Object displaySpecifications;
        private Object displaySpecificationList;
        private String specification;
        private String unStandardSpecification;
        private int storageType;
        private int saleUnitType;
        private int saleRange;
        private String saleUnitStr;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getSubTitle() {
            return subTitle;
        }

        public void setSubTitle(String subTitle) {
            this.subTitle = subTitle;
        }

        public int getCatId() {
            return catId;
        }

        public void setCatId(int catId) {
            this.catId = catId;
        }

        public Object getPicList() {
            return picList;
        }

        public void setPicList(Object picList) {
            this.picList = picList;
        }

        public Object getIntroduction() {
            return introduction;
        }

        public void setIntroduction(Object introduction) {
            this.introduction = introduction;
        }

        public Object getPropertyVO() {
            return propertyVO;
        }

        public void setPropertyVO(Object propertyVO) {
            this.propertyVO = propertyVO;
        }

        public String getSupplierName() {
            return supplierName;
        }

        public void setSupplierName(String supplierName) {
            this.supplierName = supplierName;
        }

        public Object getPurchasingAgentName() {
            return purchasingAgentName;
        }

        public void setPurchasingAgentName(Object purchasingAgentName) {
            this.purchasingAgentName = purchasingAgentName;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public int getPurchaseFlowStatus() {
            return purchaseFlowStatus;
        }

        public void setPurchaseFlowStatus(int purchaseFlowStatus) {
            this.purchaseFlowStatus = purchaseFlowStatus;
        }

        public Object getCheckReason() {
            return checkReason;
        }

        public void setCheckReason(Object checkReason) {
            this.checkReason = checkReason;
        }

        public Object getCheckUserName() {
            return checkUserName;
        }

        public void setCheckUserName(Object checkUserName) {
            this.checkUserName = checkUserName;
        }

        public Object getTitletags() {
            return titletags;
        }

        public void setTitletags(Object titletags) {
            this.titletags = titletags;
        }

        public boolean isDisplayCheckInfo() {
            return displayCheckInfo;
        }

        public void setDisplayCheckInfo(boolean displayCheckInfo) {
            this.displayCheckInfo = displayCheckInfo;
        }

        public boolean isDisplayCheckButton() {
            return displayCheckButton;
        }

        public void setDisplayCheckButton(boolean displayCheckButton) {
            this.displayCheckButton = displayCheckButton;
        }

        public int getSkuId() {
            return skuId;
        }

        public void setSkuId(int skuId) {
            this.skuId = skuId;
        }

        public long getSku69Id() {
            return sku69Id;
        }

        public void setSku69Id(long sku69Id) {
            this.sku69Id = sku69Id;
        }

        public int getProductId() {
            return productId;
        }

        public void setProductId(int productId) {
            this.productId = productId;
        }

        public int getPrice() {
            return price;
        }

        public void setPrice(int price) {
            this.price = price;
        }

        public int getUnitPrice() {
            return unitPrice;
        }

        public void setUnitPrice(int unitPrice) {
            this.unitPrice = unitPrice;
        }

        public Object getRelatedItems() {
            return relatedItems;
        }

        public void setRelatedItems(Object relatedItems) {
            this.relatedItems = relatedItems;
        }

        public Object getElementList() {
            return elementList;
        }

        public void setElementList(Object elementList) {
            this.elementList = elementList;
        }

        public Object getElement() {
            return element;
        }

        public void setElement(Object element) {
            this.element = element;
        }

        public Object getDisplaySpecifications() {
            return displaySpecifications;
        }

        public void setDisplaySpecifications(Object displaySpecifications) {
            this.displaySpecifications = displaySpecifications;
        }

        public Object getDisplaySpecificationList() {
            return displaySpecificationList;
        }

        public void setDisplaySpecificationList(Object displaySpecificationList) {
            this.displaySpecificationList = displaySpecificationList;
        }

        public String getSpecification() {
            return specification;
        }

        public void setSpecification(String specification) {
            this.specification = specification;
        }

        public String getUnStandardSpecification() {
            return unStandardSpecification;
        }

        public void setUnStandardSpecification(String unStandardSpecification) {
            this.unStandardSpecification = unStandardSpecification;
        }

        public int getStorageType() {
            return storageType;
        }

        public void setStorageType(int storageType) {
            this.storageType = storageType;
        }

        public int getSaleUnitType() {
            return saleUnitType;
        }

        public void setSaleUnitType(int saleUnitType) {
            this.saleUnitType = saleUnitType;
        }

        public int getSaleRange() {
            return saleRange;
        }

        public void setSaleRange(int saleRange) {
            this.saleRange = saleRange;
        }

        public String getSaleUnitStr() {
            return saleUnitStr;
        }

        public void setSaleUnitStr(String saleUnitStr) {
            this.saleUnitStr = saleUnitStr;
        }
    }
}
