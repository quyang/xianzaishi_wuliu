package com.xianzaishi.normandie.bean;

import java.io.Serializable;
import java.util.List;

/**
 * Created by ShenLang on 2016/9/6.
 * 购物车实体类
 */
public class ShoppingTrolleyBean {

    private int code;
    private String message;


    private DataBean data;
    private boolean success;
    private boolean error;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public static class DataBean implements Serializable{
        /**
         * pageSize : 99
         * total : 7
         * currentPage : 1
         * totalPage : 1
         * limit : 7
         * offset : 0
         */

        private PaginationBean pagination;
        /**
         * discountDesc : 满99元立减30元专区，已为你立减30元
         * originPrice : 100
         * discountPrice : 10
         * currPrice : 99
         */

        private DiscountInfoBean discountInfo;
        private String freightDesc;
        /**
         * id : 2736
         * itemName : 红肉蜜柚1.5-2kg/个（散装）
         * itemIconUrl : http://img.xianzaishi.com/2/1477928696261.jpg
         * itemPayPrice : 15.96
         * itemCount : 1.00
         * skuInfo : {"spec":"散称1份/250g"}
         * skuId : 10682
         * itemId : 10203035
         * itemEffePrice : 15.96
         * payAmount : 15.96
         * effeAmount : 15.96
         * number : 70
         * itemTags : 10000000
         * discountInfo : {"discountDesc":"第二件5折,已为你立减15元","originPrice":0,"discountPrice":0,"currPrice":0}
         */

        private List<ObjectsBean> objects;

        public PaginationBean getPagination() {
            return pagination;
        }

        public void setPagination(PaginationBean pagination) {
            this.pagination = pagination;
        }

        public DiscountInfoBean getDiscountInfo() {
            return discountInfo;
        }

        public void setDiscountInfo(DiscountInfoBean discountInfo) {
            this.discountInfo = discountInfo;
        }

        public String getFreightDesc() {
            return freightDesc;
        }

        public void setFreightDesc(String freightDesc) {
            this.freightDesc = freightDesc;
        }

        public List<ObjectsBean> getObjects() {
            return objects;
        }

        public void setObjects(List<ObjectsBean> objects) {
            this.objects = objects;
        }

        public static class PaginationBean implements Serializable{
            private int pageSize;
            private int total;
            private int currentPage;
            private int totalPage;
            private int limit;
            private int offset;

            public int getPageSize() {
                return pageSize;
            }

            public void setPageSize(int pageSize) {
                this.pageSize = pageSize;
            }

            public int getTotal() {
                return total;
            }

            public void setTotal(int total) {
                this.total = total;
            }

            public int getCurrentPage() {
                return currentPage;
            }

            public void setCurrentPage(int currentPage) {
                this.currentPage = currentPage;
            }

            public int getTotalPage() {
                return totalPage;
            }

            public void setTotalPage(int totalPage) {
                this.totalPage = totalPage;
            }

            public int getLimit() {
                return limit;
            }

            public void setLimit(int limit) {
                this.limit = limit;
            }

            public int getOffset() {
                return offset;
            }

            public void setOffset(int offset) {
                this.offset = offset;
            }
        }

        public static class DiscountInfoBean implements Serializable{
            private String discountDesc;
            private String originPrice;
            private String discountPrice;
            private String currPrice;

            public String getDiscountDesc() {
                return discountDesc;
            }

            public void setDiscountDesc(String discountDesc) {
                this.discountDesc = discountDesc;
            }

            public String getOriginPrice() {
                return originPrice;
            }

            public void setOriginPrice(String originPrice) {
                this.originPrice = originPrice;
            }

            public String getDiscountPrice() {
                return discountPrice;
            }

            public void setDiscountPrice(String discountPrice) {
                this.discountPrice = discountPrice;
            }

            public String getCurrPrice() {
                return currPrice;
            }

            public void setCurrPrice(String currPrice) {
                this.currPrice = currPrice;
            }
        }

        public static class ObjectsBean implements Serializable{
            private int id;
            private String itemName;
            private String itemIconUrl;
            private String itemPayPrice;
            private double itemCount;
            /**
             * spec : 散称1份/250g
             */

            private SkuInfoBean skuInfo;
            private int skuId;
            private int itemId;
            private String itemEffePrice;
            private String payAmount;
            private String effeAmount;
            private int number;
            private String itemTags;
            private boolean selected;

            public boolean isSelected() {
                return selected;
            }

            public void setSelected(boolean selected) {
                this.selected = selected;
            }

            /**
             * discountDesc : 第二件5折,已为你立减15元
             * originPrice : 0
             * discountPrice : 0
             * currPrice : 0
             */

            private DiscountInfoBean discountInfo;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getItemName() {
                return itemName;
            }

            public void setItemName(String itemName) {
                this.itemName = itemName;
            }

            public String getItemIconUrl() {
                return itemIconUrl;
            }

            public void setItemIconUrl(String itemIconUrl) {
                this.itemIconUrl = itemIconUrl;
            }

            public String getItemPayPrice() {
                return itemPayPrice;
            }

            public void setItemPayPrice(String itemPayPrice) {
                this.itemPayPrice = itemPayPrice;
            }

            public double getItemCount() {
                return itemCount;
            }

            public void setItemCount(double itemCount) {
                this.itemCount = itemCount;
            }

            public SkuInfoBean getSkuInfo() {
                return skuInfo;
            }

            public void setSkuInfo(SkuInfoBean skuInfo) {
                this.skuInfo = skuInfo;
            }

            public int getSkuId() {
                return skuId;
            }

            public void setSkuId(int skuId) {
                this.skuId = skuId;
            }

            public int getItemId() {
                return itemId;
            }

            public void setItemId(int itemId) {
                this.itemId = itemId;
            }

            public String getItemEffePrice() {
                return itemEffePrice;
            }

            public void setItemEffePrice(String itemEffePrice) {
                this.itemEffePrice = itemEffePrice;
            }

            public String getPayAmount() {
                return payAmount;
            }

            public void setPayAmount(String payAmount) {
                this.payAmount = payAmount;
            }

            public String getEffeAmount() {
                return effeAmount;
            }

            public void setEffeAmount(String effeAmount) {
                this.effeAmount = effeAmount;
            }

            public int getNumber() {
                return number;
            }

            public void setNumber(int number) {
                this.number = number;
            }

            public String getItemTags() {
                return itemTags;
            }

            public void setItemTags(String itemTags) {
                this.itemTags = itemTags;
            }

            public DiscountInfoBean getDiscountInfo() {
                return discountInfo;
            }

            public void setDiscountInfo(DiscountInfoBean discountInfo) {
                this.discountInfo = discountInfo;
            }

            public static class SkuInfoBean {
                private String spec;

                public String getSpec() {
                    return spec;
                }

                public void setSpec(String spec) {
                    this.spec = spec;
                }
            }

            public static class DiscountInfoBean implements Serializable{
                private String discountDesc;
                private String originPrice;
                private String discountPrice;
                private String currPrice;

                public String getDiscountDesc() {
                    return discountDesc;
                }

                public void setDiscountDesc(String discountDesc) {
                    this.discountDesc = discountDesc;
                }

                public String getOriginPrice() {
                    return originPrice;
                }

                public void setOriginPrice(String originPrice) {
                    this.originPrice = originPrice;
                }

                public String getDiscountPrice() {
                    return discountPrice;
                }

                public void setDiscountPrice(String discountPrice) {
                    this.discountPrice = discountPrice;
                }

                public String getCurrPrice() {
                    return currPrice;
                }

                public void setCurrPrice(String currPrice) {
                    this.currPrice = currPrice;
                }
            }
        }
    }
}
