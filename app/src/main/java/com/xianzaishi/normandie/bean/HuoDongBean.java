package com.xianzaishi.normandie.bean;

import java.util.List;

/**
 * 活动页面
 * Created by Administrator on 2016/9/29.
 */

public class HuoDongBean {

    public boolean success;// true,
    public boolean succcess;// true,
    public int resultCode;
    public String errorMsg;
    public List<ModuleBean> module;


    public class ModuleBean {

        public String pageName;// 测试活动页面A,
        public List<PicListBean> picList;//
        public String title;// null,
        public List<ItemsBean> items;// null,
        public int stepType;// 4

        public class ItemsBean {
            public String itemId;// 10201604,
            public String picUrl;// http;////img.xianzaishi.com/1/1474617834416.jpg,
            public String title;// 浩烨气调崇明白山羊肉气调带皮羊前腿400g,
            public String subtitle;// 气调带皮羊前腿,
            public String price;// 1,
            public String discountPrice;// 1,
            public String priceYuanString;// 0.01,
            public String discountPriceYuanString;// 0.01,
            public String sku;// 10004,
            public Object feature;// null,
            public String inventory;// 1,
            public List<ItemSkuVOsBean> itemSkuVOs;// [

            public class ItemSkuVOsBean {
                public String skuId;// 10004,
                public String itemId;// 10201604,
                public int inventory;// 1,
                public String skuUnit;// 26,
                public String title;// 浩烨气调崇明白山羊肉气调带皮羊前腿400g,
                public String quantity;// 0,
                public Object promotionInfo;// null,
                public String price;// 1,
                public String discountPrice;// 1,
                public String priceYuanString;// 0.01,
                public String discountPriceYuanString;// 0.01,
                public String saleDetailInfo;// 1盒,
                public String originplace;// 上海
                public String tags;
                public TagDetailBean tagDetail;

                public  class TagDetailBean{
                    public int skuId;
                    public boolean mayPlus;
                    public boolean mayOrder;
                    public boolean maySale;
                    public boolean specialTag;
                    public String channel;
                    public String tagContent;
                }
            }

        }

        public class PicListBean {
            public String picUrl;// http;////img.xianzaishi.com/1/1474351778620.jpg,
            public String targetId;// 10201601,
            public int targetType;// 2
            public String titleInfo;
        }


    }


}
