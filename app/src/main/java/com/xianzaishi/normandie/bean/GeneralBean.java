package com.xianzaishi.normandie.bean;

/**
 * 简单的bean
 * quyang
 * Created by Administrator on 2016/9/12.
 */
public class GeneralBean {

    public int code;// 1,
    public String message;// 成功,
    public int data;// 175,
    public boolean success;// true,
    public boolean error;// false


    @Override
    public String toString() {
        return "GeneralBean{" +
                "code=" + code +
                ", message='" + message + '\'' +
                ", data=" + data +
                ", success=" + success +
                ", error=" + error +
                '}';
    }
}
