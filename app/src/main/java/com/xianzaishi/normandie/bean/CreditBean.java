package com.xianzaishi.normandie.bean;

/**
 * Created by Administrator on 2016/9/22.
 */
public class CreditBean {

    /**
     * code : 1
     * message : 成功
     * data : 0
     * success : true
     * error : false
     */

    private int code;
    private String message;
    private int data;
    private boolean success;
    private boolean error;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getData() {
        return data;
    }

    public void setData(int data) {
        this.data = data;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }
}
