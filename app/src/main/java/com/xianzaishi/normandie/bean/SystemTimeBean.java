package com.xianzaishi.normandie.bean;

/**
 * Created by Administrator on 2016/11/24.
 */

public class SystemTimeBean {

    /**
     * resultCode : 1
     * success : true
     * errorMsg :
     * module : 1479978496312
     * succcess : true
     */

    private int resultCode;
    private boolean success;
    private String errorMsg;
    private long module;
    private boolean succcess;

    public int getResultCode() {
        return resultCode;
    }

    public void setResultCode(int resultCode) {
        this.resultCode = resultCode;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public long getModule() {
        return module;
    }

    public void setModule(long module) {
        this.module = module;
    }

    public boolean isSucccess() {
        return succcess;
    }

    public void setSucccess(boolean succcess) {
        this.succcess = succcess;
    }
}
