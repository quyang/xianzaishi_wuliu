package com.xianzaishi.normandie.bean;

import java.io.Serializable;

/**
 * Created by Administrator on 2016/8/25.
 */
public class MineDataBean implements Serializable{

    /**
     * resultCode : 1
     * succcess : true
     * errorMsg :
     * module : {"userId":10001,"name":"13888888888","pic":"3.png","useType":1,"couponCount":0,"messageCount":0}
     */

    private int resultCode;
    private boolean succcess;
    private String errorMsg;
    /**
     * userId : 10001
     * name : 13888888888
     * pic : 3.png
     * useType : 1
     * couponCount : 0
     * messageCount : 0
     */

    private ModuleBean module;

    public int getResultCode() {
        return resultCode;
    }

    public void setResultCode(int resultCode) {
        this.resultCode = resultCode;
    }

    public boolean isSucccess() {
        return succcess;
    }

    public void setSucccess(boolean succcess) {
        this.succcess = succcess;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public ModuleBean getModule() {
        return module;
    }

    public void setModule(ModuleBean module) {
        this.module = module;
    }

    public static class ModuleBean {
        private int userId;
        private String name;
        private int sex;
        private String birthday;
        private String pic;
        private int useType;
        private int couponCount;
        private int messageCount;

        public int getSex() {
            return sex;
        }

        public String getBirthday() {
            return birthday;
        }

        public int getUserId() {
            return userId;
        }

        public void setUserId(int userId) {
            this.userId = userId;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getPic() {
            return pic;
        }

        public void setPic(String pic) {
            this.pic = pic;
        }

        public int getUseType() {
            return useType;
        }

        public void setUseType(int useType) {
            this.useType = useType;
        }

        public int getCouponCount() {
            return couponCount;
        }

        public void setCouponCount(int couponCount) {
            this.couponCount = couponCount;
        }

        public int getMessageCount() {
            return messageCount;
        }

        public void setMessageCount(int messageCount) {
            this.messageCount = messageCount;
        }
    }

}
