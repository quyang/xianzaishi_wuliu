package com.xianzaishi.normandie.bean;

import java.util.List;

/**
 * Created by Administrator on 2016/10/20.
 */

public class CouponsByIdBean {

    /**
     * code : 1
     * message : 成功
     * data : [{"id":930,"userId":10000,"couponId":4,"couponTitle":"无条件限制(6元)","couponStartTime":1474859262000,"couponEndTime":1506411873000,"orderId":11182,"couponRules":"6.0","gmtCreate":1475032065000,"gmtModify":1475032065000,"status":0,"amount":6,"couponType":1,"channelType":1,"amountLimit":0}]
     * success : true
     * error : false
     */

    private int code;
    private String message;
    private boolean success;
    private boolean error;
    /**
     * id : 930
     * userId : 10000
     * couponId : 4
     * couponTitle : 无条件限制(6元)
     * couponStartTime : 1474859262000
     * couponEndTime : 1506411873000
     * orderId : 11182
     * couponRules : 6.0
     * gmtCreate : 1475032065000
     * gmtModify : 1475032065000
     * status : 0
     * amount : 6
     * couponType : 1
     * channelType : 1
     * amountLimit : 0
     */

    private List<DataBean> data;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        private int id;
        private int userId;
        private int couponId;
        private String couponTitle;
        private long couponStartTime;
        private long couponEndTime;
        private int orderId;
        private String couponRules;
        private long gmtCreate;
        private long gmtModify;
        private int status;
        private double amount;
        private int couponType;
        private int channelType;
        private int amountLimit;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getUserId() {
            return userId;
        }

        public void setUserId(int userId) {
            this.userId = userId;
        }

        public int getCouponId() {
            return couponId;
        }

        public void setCouponId(int couponId) {
            this.couponId = couponId;
        }

        public String getCouponTitle() {
            return couponTitle;
        }

        public void setCouponTitle(String couponTitle) {
            this.couponTitle = couponTitle;
        }

        public long getCouponStartTime() {
            return couponStartTime;
        }

        public void setCouponStartTime(long couponStartTime) {
            this.couponStartTime = couponStartTime;
        }

        public long getCouponEndTime() {
            return couponEndTime;
        }

        public void setCouponEndTime(long couponEndTime) {
            this.couponEndTime = couponEndTime;
        }

        public int getOrderId() {
            return orderId;
        }

        public void setOrderId(int orderId) {
            this.orderId = orderId;
        }

        public String getCouponRules() {
            return couponRules;
        }

        public void setCouponRules(String couponRules) {
            this.couponRules = couponRules;
        }

        public long getGmtCreate() {
            return gmtCreate;
        }

        public void setGmtCreate(long gmtCreate) {
            this.gmtCreate = gmtCreate;
        }

        public long getGmtModify() {
            return gmtModify;
        }

        public void setGmtModify(long gmtModify) {
            this.gmtModify = gmtModify;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public double getAmount() {
            return amount;
        }

        public void setAmount(double amount) {
            this.amount = amount;
        }

        public int getCouponType() {
            return couponType;
        }

        public void setCouponType(int couponType) {
            this.couponType = couponType;
        }

        public int getChannelType() {
            return channelType;
        }

        public void setChannelType(int channelType) {
            this.channelType = channelType;
        }

        public int getAmountLimit() {
            return amountLimit;
        }

        public void setAmountLimit(int amountLimit) {
            this.amountLimit = amountLimit;
        }
    }
}
