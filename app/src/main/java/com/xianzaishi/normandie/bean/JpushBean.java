package com.xianzaishi.normandie.bean;

/**
 * Created by ShenLang on 2017/1/12.
 *  极光推送 键值接收
 */

public class JPushBean {

    /**
     * itemId : 43435
     * titleInfo : 活动页面
     * targetId : http://ofi7czwaa.bkt.clouddn.com/activity/act-20170103.html?pageId=32
     */

    private String itemId;
    private String titleInfo;
    private String targetId;

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getTitleInfo() {
        return titleInfo;
    }

    public void setTitleInfo(String titleInfo) {
        this.titleInfo = titleInfo;
    }

    public String getTargetId() {
        return targetId;
    }

    public void setTargetId(String targetId) {
        this.targetId = targetId;
    }
}
