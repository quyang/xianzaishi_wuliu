package com.xianzaishi.normandie.bean;

/**
 * Created by Administrator on 2016/9/16.
 */
public class WXPayBean {

    /**
     * code : 1
     * message : 成功
     * data : {"appid":"wxdf3989b2edb80672","partnerid":"1385693802","prepayid":"wx20160916201530d5ebcfbe470105463264","pack":"Sign=WXPay","noncestr":"909B8E993C08E204","timestamp":1474028130,"sign":"C20FA44072ADF3FBF7E02C5C8FC09C07"}
     * success : true
     * error : false
     */

    private int code;
    private String message;
    /**
     * appid : wxdf3989b2edb80672
     * partnerid : 1385693802
     * prepayid : wx20160916201530d5ebcfbe470105463264
     * pack : Sign=WXPay
     * noncestr : 909B8E993C08E204
     * timestamp : 1474028130
     * sign : C20FA44072ADF3FBF7E02C5C8FC09C07
     */

    private DataBean data;
    private boolean success;
    private boolean error;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public static class DataBean {
        private String appid;
        private String partnerid;
        private String prepayid;
        private String pack;
        private String noncestr;
        private int timestamp;
        private String sign;

        public String getAppid() {
            return appid;
        }

        public void setAppid(String appid) {
            this.appid = appid;
        }

        public String getPartnerid() {
            return partnerid;
        }

        public void setPartnerid(String partnerid) {
            this.partnerid = partnerid;
        }

        public String getPrepayid() {
            return prepayid;
        }

        public void setPrepayid(String prepayid) {
            this.prepayid = prepayid;
        }

        public String getPack() {
            return pack;
        }

        public void setPack(String pack) {
            this.pack = pack;
        }

        public String getNoncestr() {
            return noncestr;
        }

        public void setNoncestr(String noncestr) {
            this.noncestr = noncestr;
        }

        public int getTimestamp() {
            return timestamp;
        }

        public void setTimestamp(int timestamp) {
            this.timestamp = timestamp;
        }

        public String getSign() {
            return sign;
        }

        public void setSign(String sign) {
            this.sign = sign;
        }
    }
}
