package com.xianzaishi.normandie.bean;

/**
 * Created by Administrator on 2016/10/29.
 */

public class UpLoadImageBean {

    /**
     * resultCode : 1
     * success : true
     * errorMsg :
     * module : http://img.xianzaishi.com/4/1477726923973.jpg
     * succcess : true
     */

    private int resultCode;
    private boolean success;
    private String errorMsg;
    private String module;
    private boolean succcess;

    public int getResultCode() {
        return resultCode;
    }

    public void setResultCode(int resultCode) {
        this.resultCode = resultCode;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public String getModule() {
        return module;
    }

    public void setModule(String module) {
        this.module = module;
    }

    public boolean isSucccess() {
        return succcess;
    }

    public void setSucccess(boolean succcess) {
        this.succcess = succcess;
    }
}
