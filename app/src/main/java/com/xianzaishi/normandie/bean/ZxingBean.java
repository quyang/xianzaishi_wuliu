package com.xianzaishi.normandie.bean;

/**
 * quyang
 * <p>
 * Created by Administrator on 2016/10/10.
 */

public class ZxingBean {

    public String qrInfoType;
    public QueryDetailBean queryDetail;

    public class QueryDetailBean {

        public String queryType;
        public String data;
        public QueryParameter queryParameter;

        @Override
        public String toString() {
            return "QueryDetailBean{" +
                    "detail='" + data + '\'' +
                    ", queryType='" + queryType + '\'' +
                    ", queryParameter=" + queryParameter +
                    '}';
        }

        public class QueryParameter {

            public String id;

            @Override
            public String toString() {
                return "QueryParameter{" +
                        "id='" + id + '\'' +
                        '}';
            }
        }

    }


    @Override
    public String toString() {
        return "ZxingBean{" +
                "qrInfoType='" + qrInfoType + '\'' +
                ", queryDetail=" + queryDetail +
                '}';
    }
}
