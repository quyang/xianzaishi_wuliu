package com.xianzaishi.normandie.bean;

import java.io.Serializable;

/**
 * quang
 * Created by Administrator on 2016/9/16.
 */
public class TokenBean  implements Serializable{


    public int resultCode;// 1,
    public boolean success;// true,
    public String errorMsg;// ,
    public ModuleBean module;// 
    public boolean succcess;// true

    @Override
    public String toString() {
        return "TokenBean{" +
                "errorMsg='" + errorMsg + '\'' +
                ", resultCode=" + resultCode +
                ", success=" + success +
                ", module=" + module +
                ", succcess=" + succcess +
                '}';
    }


    public class ModuleBean implements Serializable{

        public int userId;//10025,
        public String name;//17742039272,
        public String pic;//17.png,
        public int userType;//1,
        public int sex;//0,
        public String birthday;//null,
        public int couponCount;//0,优惠券个数
        public int messageCount;//0,消息个数


        @Override
        public String toString() {
            return "ModuleBean{" +
                    "birthday='" + birthday + '\'' +
                    ", userId=" + userId +
                    ", name='" + name + '\'' +
                    ", pic='" + pic + '\'' +
                    ", userType=" + userType +
                    ", sex=" + sex +
                    ", couponCount=" + couponCount +
                    ", messageCount=" + messageCount +
                    '}';
        }
    }


}
