package com.xianzaishi.normandie.bean;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Administrator on 2016/11/25.
 */

public class Home2DownBean {

    private int resultCode;
    private boolean success;
    private String errorMsg;
    private boolean succcess;


    private List<ModuleBean> module;

    public int getResultCode() {
        return resultCode;
    }

    public void setResultCode(int resultCode) {
        this.resultCode = resultCode;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public boolean isSucccess() {
        return succcess;
    }

    public void setSucccess(boolean succcess) {
        this.succcess = succcess;
    }

    public List<ModuleBean> getModule() {
        return module;
    }

    public void setModule(List<ModuleBean> module) {
        this.module = module;
    }

    public static class ModuleBean {
        private String pageName;
        private int stepId;
        private String title;
        private int stepType;
        /**
         * picUrl : http://img.xianzaishi.com/1/1479953618773.jpeg
         * targetId : 0
         * targetType : 0
         */

        private List<PicListBean> picList;
        /**
         * itemId : 10202491
         * picUrl : http://img.xianzaishi.com/2/1477241113299.jpg
         * title : 蒙牛未来星儿童成长奶酪金装杯（原味）23g*4
         * subtitle : 蒙牛未来星儿
         * price : 1270
         * discountPrice : 1270
         * priceYuanString : 12.70
         * discountPriceYuanString : 12.70
         * sku : 10552
         * feature : null
         * inventory : 1
         * itemSkuVOs : [{"skuId":10552,"itemId":10202491,"inventory":10,"skuUnit":69,"title":"蒙牛未来星儿童成长奶酪金装杯（原味）23g*4","quantity":0,"promotionInfo":null,"price":1270,"discountPrice":1270,"priceYuanString":"12.70","discountPriceYuanString":"12.70","saleDetailInfo":"1组/4盒","originplace":"见包装","tags":null}]
         */

        private List<ItemsBean> items;

        public String getPageName() {
            return pageName;
        }

        public void setPageName(String pageName) {
            this.pageName = pageName;
        }

        public int getStepId() {
            return stepId;
        }

        public void setStepId(int stepId) {
            this.stepId = stepId;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public int getStepType() {
            return stepType;
        }

        public void setStepType(int stepType) {
            this.stepType = stepType;
        }

        public List<PicListBean> getPicList() {
            return picList;
        }

        public void setPicList(List<PicListBean> picList) {
            this.picList = picList;
        }

        public List<ItemsBean> getItems() {
            return items;
        }

        public void setItems(List<ItemsBean> items) {
            this.items = items;
        }

        public static class PicListBean {
            private String picUrl;
            private String targetId;
            private int targetType;

            public String getPicUrl() {
                return picUrl;
            }

            public void setPicUrl(String picUrl) {
                this.picUrl = picUrl;
            }

            public String getTargetId() {
                return targetId;
            }

            public void setTargetId(String targetId) {
                this.targetId = targetId;
            }

            public int getTargetType() {
                return targetType;
            }

            public void setTargetType(int targetType) {
                this.targetType = targetType;
            }
        }

        public static class ItemsBean implements Serializable{
            private int itemId;
            private String picUrl;
            private String title;
            private String subtitle;
            private int price;
            private int discountPrice;
            private String priceYuanString;
            private String discountPriceYuanString;
            private String sku;
            private Object feature;
            private int inventory;
            /**
             * skuId : 10552
             * itemId : 10202491
             * inventory : 10
             * skuUnit : 69
             * title : 蒙牛未来星儿童成长奶酪金装杯（原味）23g*4
             * quantity : 0
             * promotionInfo : null
             * price : 1270
             * discountPrice : 1270
             * priceYuanString : 12.70
             * discountPriceYuanString : 12.70
             * saleDetailInfo : 1组/4盒
             * originplace : 见包装
             * tags : null
             */

            private List<ItemSkuVOsBean> itemSkuVOs;

            public int getItemId() {
                return itemId;
            }

            public void setItemId(int itemId) {
                this.itemId = itemId;
            }

            public String getPicUrl() {
                return picUrl;
            }

            public void setPicUrl(String picUrl) {
                this.picUrl = picUrl;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getSubtitle() {
                return subtitle;
            }

            public void setSubtitle(String subtitle) {
                this.subtitle = subtitle;
            }

            public int getPrice() {
                return price;
            }

            public void setPrice(int price) {
                this.price = price;
            }

            public int getDiscountPrice() {
                return discountPrice;
            }

            public void setDiscountPrice(int discountPrice) {
                this.discountPrice = discountPrice;
            }

            public String getPriceYuanString() {
                return priceYuanString;
            }

            public void setPriceYuanString(String priceYuanString) {
                this.priceYuanString = priceYuanString;
            }

            public String getDiscountPriceYuanString() {
                return discountPriceYuanString;
            }

            public void setDiscountPriceYuanString(String discountPriceYuanString) {
                this.discountPriceYuanString = discountPriceYuanString;
            }

            public String getSku() {
                return sku;
            }

            public void setSku(String sku) {
                this.sku = sku;
            }

            public Object getFeature() {
                return feature;
            }

            public void setFeature(Object feature) {
                this.feature = feature;
            }

            public int getInventory() {
                return inventory;
            }

            public void setInventory(int inventory) {
                this.inventory = inventory;
            }

            public List<ItemSkuVOsBean> getItemSkuVOs() {
                return itemSkuVOs;
            }

            public void setItemSkuVOs(List<ItemSkuVOsBean> itemSkuVOs) {
                this.itemSkuVOs = itemSkuVOs;
            }

            public static class ItemSkuVOsBean implements Serializable{
                private int skuId;
                private int itemId;
                private int inventory;
                private int skuUnit;
                private String title;
                private int quantity;
                private Object promotionInfo;
                private int price;
                private int discountPrice;
                private String priceYuanString;
                private String discountPriceYuanString;
                private String saleDetailInfo;
                private String originplace;
                private String tags;

                public int getSkuId() {
                    return skuId;
                }

                public void setSkuId(int skuId) {
                    this.skuId = skuId;
                }

                public int getItemId() {
                    return itemId;
                }

                public void setItemId(int itemId) {
                    this.itemId = itemId;
                }

                public int getInventory() {
                    return inventory;
                }

                public void setInventory(int inventory) {
                    this.inventory = inventory;
                }

                public int getSkuUnit() {
                    return skuUnit;
                }

                public void setSkuUnit(int skuUnit) {
                    this.skuUnit = skuUnit;
                }

                public String getTitle() {
                    return title;
                }

                public void setTitle(String title) {
                    this.title = title;
                }

                public int getQuantity() {
                    return quantity;
                }

                public void setQuantity(int quantity) {
                    this.quantity = quantity;
                }

                public Object getPromotionInfo() {
                    return promotionInfo;
                }

                public void setPromotionInfo(Object promotionInfo) {
                    this.promotionInfo = promotionInfo;
                }

                public int getPrice() {
                    return price;
                }

                public void setPrice(int price) {
                    this.price = price;
                }

                public int getDiscountPrice() {
                    return discountPrice;
                }

                public void setDiscountPrice(int discountPrice) {
                    this.discountPrice = discountPrice;
                }

                public String getPriceYuanString() {
                    return priceYuanString;
                }

                public void setPriceYuanString(String priceYuanString) {
                    this.priceYuanString = priceYuanString;
                }

                public String getDiscountPriceYuanString() {
                    return discountPriceYuanString;
                }

                public void setDiscountPriceYuanString(String discountPriceYuanString) {
                    this.discountPriceYuanString = discountPriceYuanString;
                }

                public String getSaleDetailInfo() {
                    return saleDetailInfo;
                }

                public void setSaleDetailInfo(String saleDetailInfo) {
                    this.saleDetailInfo = saleDetailInfo;
                }

                public String getOriginplace() {
                    return originplace;
                }

                public void setOriginplace(String originplace) {
                    this.originplace = originplace;
                }

                public String getTags() {
                    return tags;
                }

                public void setTags(String tags) {
                    this.tags = tags;
                }
            }
        }
    }
}
