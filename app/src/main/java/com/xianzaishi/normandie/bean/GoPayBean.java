package com.xianzaishi.normandie.bean;

/**
 * Created by Administrator on 2016/9/8.
 */
public class GoPayBean {

    /**
     * code : 1
     * message : 成功
     * data : _input_charset="utf-8"&body="PAY"&it_b_pay="30m"&notify_url="http://trade.xianzaishi.net/order/alipayCallBack.htm"&out_trade_no="10000000100000000000000000010043"&partner="2088421284384567"&payment_type="1"&seller_id="pay@xianzaishi.com"&service="mobile.securitypay.pay"&show_url="m.alipay.com"&subject="XIANZAISHI_PAY"&total_fee="0.03"&sign="QhJkzZLXAKUqEHQa%2BD6%2Bcp1BalWNsj804%2Ftj%2FwdF4YPKWiw9ACUhgCKTkZmZsbs2EFAOhjNfAqwe3JR1xLuSaQCUbkZgKuJg7wKvP7WnxCkRU1IZg3p2VDy6aNrGUzh7PwuDxID4N3k5Kq1Bk5C79Nt7cu7QwSTYjbqC7ZTAgvI%3D"&sign_type="RSA"
     * success : true
     * error : false
     */

    private int code;
    private String message;
    private String data;
    private boolean success;
    private boolean error;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }
}
