package com.xianzaishi.normandie.bean;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Administrator on 2016/12/15.
 */

public class AchievementBean {

    /**
     * resultCode : 1
     * success : true
     * errorMsg :
     * module : {"redFont":["0元"],"cw":"您尚未邀请好友，获得0元礼券","achievementDetail":[{"userId":10097,"phone":"13334340003","statuString":"已注册"}],"type":1}
     * succcess : true
     */

    private int resultCode;
    private boolean success;
    private String errorMsg;
    /**
     * redFont : ["0元"]
     * cw : 您尚未邀请好友，获得0元礼券
     * achievementDetail : [{"userId":10097,"phone":"13334340003","statuString":"已注册"}]
     * type : 1
     */

    private ModuleBean module;
    private boolean succcess;

    public int getResultCode() {
        return resultCode;
    }

    public void setResultCode(int resultCode) {
        this.resultCode = resultCode;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public ModuleBean getModule() {
        return module;
    }

    public void setModule(ModuleBean module) {
        this.module = module;
    }

    public boolean isSucccess() {
        return succcess;
    }

    public void setSucccess(boolean succcess) {
        this.succcess = succcess;
    }

    public static class ModuleBean {
        private String cw;
        private int type;
        private List<String> redFont;
        /**
         * userId : 10097
         * phone : 13334340003
         * statuString : 已注册
         */

        private List<AchievementDetailBean> achievementDetail;

        public String getCw() {
            return cw;
        }

        public void setCw(String cw) {
            this.cw = cw;
        }

        public int getType() {
            return type;
        }

        public void setType(int type) {
            this.type = type;
        }

        public List<String> getRedFont() {
            return redFont;
        }

        public void setRedFont(List<String> redFont) {
            this.redFont = redFont;
        }

        public List<AchievementDetailBean> getAchievementDetail() {
            return achievementDetail;
        }

        public void setAchievementDetail(List<AchievementDetailBean> achievementDetail) {
            this.achievementDetail = achievementDetail;
        }

        public static class AchievementDetailBean implements Serializable{
            private int userId;
            private String phone;
            private String statuString;

            public int getUserId() {
                return userId;
            }

            public void setUserId(int userId) {
                this.userId = userId;
            }

            public String getPhone() {
                return phone;
            }

            public void setPhone(String phone) {
                this.phone = phone;
            }

            public String getStatuString() {
                return statuString;
            }

            public void setStatuString(String statuString) {
                this.statuString = statuString;
            }
        }
    }
}
