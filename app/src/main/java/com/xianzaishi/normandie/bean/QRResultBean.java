package com.xianzaishi.normandie.bean;

/**
 * Created by Administrator on 2016/10/23.
 */

public class QRResultBean {

    /**
     * qrInfoType : mobile
     * queryDetail : {"queryType":"get","queryParameter":{"id":"10201601"},"data":"user"}
     */

    private String qrInfoType;
    /**
     * queryType : get
     * queryParameter : {"id":"10201601"}
     * data : user
     */

    private QueryDetailBean queryDetail;

    public String getQrInfoType() {
        return qrInfoType;
    }

    public void setQrInfoType(String qrInfoType) {
        this.qrInfoType = qrInfoType;
    }

    public QueryDetailBean getQueryDetail() {
        return queryDetail;
    }

    public void setQueryDetail(QueryDetailBean queryDetail) {
        this.queryDetail = queryDetail;
    }

    public static class QueryDetailBean {
        private String queryType;
        /**
         * id : 10201601
         */

        private QueryParameterBean queryParameter;
        private String data;

        public String getQueryType() {
            return queryType;
        }

        public void setQueryType(String queryType) {
            this.queryType = queryType;
        }

        public QueryParameterBean getQueryParameter() {
            return queryParameter;
        }

        public void setQueryParameter(QueryParameterBean queryParameter) {
            this.queryParameter = queryParameter;
        }

        public String getData() {
            return data;
        }

        public void setData(String data) {
            this.data = data;
        }

        public static class QueryParameterBean {
            private String id;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }
        }
    }
}
