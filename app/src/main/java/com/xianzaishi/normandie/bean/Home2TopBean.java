package com.xianzaishi.normandie.bean;

import java.util.List;


public class Home2TopBean {

    private int resultCode;
    private boolean success;
    private String errorMsg;
    private boolean succcess;
    /**
     * pageName : new_home_page
     * stepId : 115
     * picList : [{"picUrl":"http://img.xianzaishi.com/1/1479951956528.jpeg","targetId":"0","targetType":0},{"picUrl":"http://img.xianzaishi.com/1/1479952245721.jpeg","targetId":"1","targetType":1}]
     * title :
     * items : null
     * stepType : 10
     */

    private List<ModuleBean> module;

    public int getResultCode() {
        return resultCode;
    }

    public void setResultCode(int resultCode) {
        this.resultCode = resultCode;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public boolean isSucccess() {
        return succcess;
    }

    public void setSucccess(boolean succcess) {
        this.succcess = succcess;
    }

    public List<ModuleBean> getModule() {
        return module;
    }

    public void setModule(List<ModuleBean> module) {
        this.module = module;
    }

    public static class ModuleBean {
        private String pageName;
        private int stepId;
        private String title;
        public List<ItemsBean> items;
        private int stepType;
        private boolean isChecked;

        public void setChecked(boolean checked) {
            isChecked = checked;
        }

        public boolean isChecked() {
            return isChecked;
        }

        /**
         * picUrl : http://img.xianzaishi.com/1/1479951956528.jpeg
         * targetId : 0
         * targetType : 0
         */

        private List<PicListBean> picList;

        public String getPageName() {
            return pageName;
        }

        public void setPageName(String pageName) {
            this.pageName = pageName;
        }

        public int getStepId() {
            return stepId;
        }

        public void setStepId(int stepId) {
            this.stepId = stepId;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }


        public int getStepType() {
            return stepType;
        }

        public void setStepType(int stepType) {
            this.stepType = stepType;
        }

        public List<PicListBean> getPicList() {
            return picList;
        }

        public void setPicList(List<PicListBean> picList) {
            this.picList = picList;
        }
        public class ItemsBean {
            public int itemId;// 10201659,
            public String picUrl;
            public String title;// Perrier 巴黎水含气天然青柠味 330ml/瓶,
            public String subtitle;// 巴黎水青柠味,
            public String price;// 1,
            public String discountPrice;// 1,
            public String priceYuanString;// 0.01,
            public String discountPriceYuanString;// 0.01,
            public String sku;// 59,
            public Object feature;// null,
            public int inventory;// 1,
            public List<Home2TopBean.ModuleBean.ItemsBean.ItemSkuVOsBean> itemSkuVOs;//


            public class ItemSkuVOsBean {
                public String skuId;// 59,
                public String itemId;// 10201659,
                public int inventory;// 1,
                public String skuUnit;// 69,
                public String title;// Perrier 巴黎水含气天然青柠味 330ml/瓶,
                public String quantity;// 0,
                public Object promotionInfo;// null,
                public String price;// 1,
                public String discountPrice;// 1,
                public String priceYuanString;// 0.01,
                public String discountPriceYuanString;// 0.01,
                public String saleDetailInfo;// 1组4瓶,
                public String originplace;// 法国
                public String tags;
            }
        }
        public static class PicListBean {
            private String picUrl;
            private String targetId;
            private int targetType;

            public String getPicUrl() {
                return picUrl;
            }

            public void setPicUrl(String picUrl) {
                this.picUrl = picUrl;
            }

            public String getTargetId() {
                return targetId;
            }

            public void setTargetId(String targetId) {
                this.targetId = targetId;
            }

            public int getTargetType() {
                return targetType;
            }

            public void setTargetType(int targetType) {
                this.targetType = targetType;
            }
        }
    }
}
