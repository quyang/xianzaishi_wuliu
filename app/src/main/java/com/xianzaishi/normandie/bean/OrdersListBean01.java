package com.xianzaishi.normandie.bean;

import java.util.List;

/**
 * quyang
 * <p/>
 * 订单列表
 * <p/>
 * Created by Administrator on 2016/8/25.
 */
public class OrdersListBean01 {

    public String code;
    public String message;
    public boolean success;
    public boolean error;
    public DataBean data;

    @Override
    public String toString() {
        return "OrdersListBean01{" +
                "code='" + code + '\'' +
                ", message='" + message + '\'' +
                ", success=" + success +
                ", error=" + error +
                ", data=" + data +
                '}';
    }

    public class DataBean {

        public PaginationBean pagination;

        public List<ObjectsBean> objects;

        @Override
        public String toString() {
            return "DataBean{" +
                    "objects=" + objects +
                    ", pagination=" + pagination +
                    '}';
        }

        public class PaginationBean {
            public int pageSize;// 99,
            public int total;// 11,
            public int currentPage;// 1,
            public int totalPage;// 1,
            public int limit;// 11,
            public int offset;// 0

            @Override
            public String toString() {
                return "PaginationBean{" +
                        "currentPage=" + currentPage +
                        ", pageSize=" + pageSize +
                        ", total=" + total +
                        ", totalPage=" + totalPage +
                        ", limit=" + limit +
                        ", offset=" + offset +
                        '}';
            }
        }

        public class ObjectsBean {
            public String payAmount;// 0,
            public String effeAmount;// 0.02,
            public String deviceId;//       ,
            public String cashierId;// 0,
            public String userAddressId;// 73,
            public String userAddress;// null,
            public String attribute;// null,
            public List<ItemsBean> items;//
            public String id;// 10020,
            public String seq;// 100000003,
            public String gmtCreate;// 1472816234000,
            public String gmtPay;// null,
            public String gmtEnd;// null,
            public Object logisticalInfo;// null,
            public String status;// 2,
            public String statusString;//    待付款
            public String gmtDistribution;
            public String shopId;
            public String userId;
            public int channelType;


            public class ItemsBean {
                @Override
                public String toString() {
                    return "ItemsBean{" +
                            "amount='" + amount + '\'' +
                            ", id='" + id + '\'' +
                            ", name='" + name + '\'' +
                            ", iconUrl='" + iconUrl + '\'' +
                            ", price='" + price + '\'' +
                            ", effePrice='" + effePrice + '\'' +
                            ", effeAmount='" + effeAmount + '\'' +
                            ", count='" + count + '\'' +
                            ", skuInfo=" + skuInfo +
                            ", categoryId=" + categoryId +
                            ", skuId='" + skuId + '\'' +
                            '}';
                }

                public String id;// 10201628,
                public String name;// 巴黎水青柠味,
                public String iconUrl;// http;////img.alicdn.com/imgextra/i3/496514980/TB2tdt5aVXXXXa0XXXXXXXXXXXX-496514980.jpg,
                public String price;// 0.01,
                public String effePrice;// 0.01,
                public String amount;// 0.02,
                public String effeAmount;// 0.02,
                public String count;// 2,
                public SkuBean skuInfo;// {\skuId\;//28,\itemId\;//10201628,\supplierId\;//1,\skuCode\;//10050020004004,\sku69code\;//\69121321312\,\skuPluCode\;//10021,\inventory\;//0,\price\;//1,\discountPrice\;//1,\property\;//\s=19&720;22&21600;17&0;24&499000;15&0;16&0;26&0;13&30;14&5;12&32;21&17;3&法国;20&17;2&Perrier;6&24;32&0;5&69121321312;4&法国;8&4;\\np=18&72;23&75;33&89;34&93;25&75;39&95;27&77;28&79;11&69;29&77;10&64;1&1;7&50;31&83;9&69;\,\status\;//1,\gmtCreate\;//1472742073000,\gmtModified\;//1472742073000,\skuUnit\;//69,\skuCount\;//6.0,\title\;//\Perrier 巴黎水含气天然青柠味 330ml/瓶\,\feature\;//\sp&1组4瓶\\nsu&2\,\tags\;//null,\checkedFailedReason\;//null,\checkerList\;//[],\propertyInfo\;//{},\priceYuan\;//0.01,\priceYuanString\;//\0.01\,\discountPriceYuan\;//0.01,\discountPriceYuanString\;//\0.01\,\saleStatus\;//1,\quantity\;//0,\cost\;//0,\skuType\;//1,\promotionInfo\;//null,\frontProperty\;//{}},
                public Object categoryId;// null,
                public String skuId;// 28


                public class SkuBean {
                    public String spec;
                }
            }

            @Override
            public String toString() {
                return "DataBean{" +
                        "attribute='" + attribute + '\'' +
                        ", payAmount='" + payAmount + '\'' +
                        ", effeAmount='" + effeAmount + '\'' +
                        ", deviceId='" + deviceId + '\'' +
                        ", cashierId='" + cashierId + '\'' +
                        ", userAddressId='" + userAddressId + '\'' +
                        ", userAddress='" + userAddress + '\'' +
                        ", items=" + items +
                        ", id='" + id + '\'' +
                        ", seq='" + seq + '\'' +
                        ", gmtCreate='" + gmtCreate + '\'' +
                        ", gmtPay='" + gmtPay + '\'' +
                        ", gmtEnd='" + gmtEnd + '\'' +
                        ", logisticalInfo='" + logisticalInfo + '\'' +
                        ", status='" + status + '\'' +
                        ", statusString='" + statusString + '\'' +
                        ", gmtDistribution=" + gmtDistribution +
                        ", shopId=" + shopId +
                        '}';
            }
        }


    }
}



