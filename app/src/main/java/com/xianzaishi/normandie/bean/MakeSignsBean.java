package com.xianzaishi.normandie.bean;

import java.util.List;

/**
 * Created by ShenLang on 2016/12/23.
 * 商品打标实体类
 */

public class MakeSignsBean {

    /**
     * resultCode : 1
     * success : true
     * errorMsg :
     * module : [{"skuId":1,"mayPlus":true,"mayOrder":true,"maySale":true,"specialTag":true,"channel":1,"tagContent":"热销"},{"skuId":2,"mayPlus":true,"mayOrder":true,"maySale":true,"specialTag":true,"channel":1,"tagContent":"爆款"},{"skuId":3,"mayPlus":true,"mayOrder":true,"maySale":true,"specialTag":true,"channel":1,"tagContent":"热销"},{"skuId":4,"mayPlus":true,"mayOrder":true,"maySale":true,"specialTag":true,"channel":1,"tagContent":"新品尝鲜"},{"skuId":5,"mayPlus":true,"mayOrder":true,"maySale":true,"specialTag":true,"channel":1,"tagContent":"店长推荐"},{"skuId":6,"mayPlus":true,"mayOrder":true,"maySale":true,"specialTag":true,"channel":1,"tagContent":"店长推荐"},{"skuId":7,"mayPlus":true,"mayOrder":true,"maySale":true,"specialTag":true,"channel":1,"tagContent":"店长推荐"}]
     * succcess : true
     */

    private int resultCode;
    private boolean success;
    private String errorMsg;
    private boolean succcess;
    /**
     * skuId : 1
     * mayPlus : true
     * mayOrder : true
     * maySale : true
     * specialTag : true
     * channel : 1
     * tagContent : 热销
     */

    private List<ModuleBean> module;

    public int getResultCode() {
        return resultCode;
    }

    public void setResultCode(int resultCode) {
        this.resultCode = resultCode;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public boolean isSucccess() {
        return succcess;
    }

    public void setSucccess(boolean succcess) {
        this.succcess = succcess;
    }

    public List<ModuleBean> getModule() {
        return module;
    }

    public void setModule(List<ModuleBean> module) {
        this.module = module;
    }

    public static class ModuleBean {
        private int skuId;
        private boolean mayPlus;
        private boolean mayOrder;
        private boolean maySale;
        private boolean specialTag;
        private String channel;
        private String tagContent;

        public int getSkuId() {
            return skuId;
        }

        public void setSkuId(int skuId) {
            this.skuId = skuId;
        }

        public boolean isMayPlus() {
            return mayPlus;
        }

        public void setMayPlus(boolean mayPlus) {
            this.mayPlus = mayPlus;
        }

        public boolean isMayOrder() {
            return mayOrder;
        }

        public void setMayOrder(boolean mayOrder) {
            this.mayOrder = mayOrder;
        }

        public boolean isMaySale() {
            return maySale;
        }

        public void setMaySale(boolean maySale) {
            this.maySale = maySale;
        }

        public boolean isSpecialTag() {
            return specialTag;
        }

        public void setSpecialTag(boolean specialTag) {
            this.specialTag = specialTag;
        }

        public String getChannel() {
            return channel;
        }

        public void setChannel(String channel) {
            this.channel = channel;
        }

        public String getTagContent() {
            return tagContent;
        }

        public void setTagContent(String tagContent) {
            this.tagContent = tagContent;
        }
    }
}
