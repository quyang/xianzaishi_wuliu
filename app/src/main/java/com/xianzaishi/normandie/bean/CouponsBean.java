package com.xianzaishi.normandie.bean;

import java.util.List;

/**
 * Created by Administrator on 2016/9/28.
 */

public class CouponsBean {

    /**
     * code : 1
     * message : 成功
     * data : [{"id":3,"userId":10015,"couponId":2,"couponTitle":"优惠券25元","couponStartTime":1474859396000,"couponEndTime":1506411873000,"orderId":0,"couponRules":"金额>50.0?25.0:0.0","gmtCreate":1475032084000,"gmtModify":1475032084000,"status":1},{"id":4,"userId":10015,"couponId":1,"couponTitle":"优惠券25元","couponStartTime":1474859262000,"couponEndTime":1506411873000,"orderId":0,"couponRules":"金额>50.0?25.0:0.0","gmtCreate":1475032097000,"gmtModify":1475032097000,"status":1}]
     * success : true
     * error : false
     */

    private int resultCode;
    private String message;
    private boolean success;
    private boolean error;
    /**
     * id : 3
     * userId : 10015
     * couponId : 2
     * couponTitle : 优惠券25元
     * couponStartTime : 1474859396000
     * couponEndTime : 1506411873000
     * orderId : 0
     * couponRules : 金额>50.0?25.0:0.0
     * gmtCreate : 1475032084000
     * gmtModify : 1475032084000
     * status : 1
     */

    private List<ModuleBean> module;

    public int getCode() {
        return resultCode;
    }

    public void setCode(int code) {
        this.resultCode = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public List<ModuleBean> getModule() {
        return module;
    }

    public void setModule(List<ModuleBean> module) {
        this.module = module;
    }

    public static class ModuleBean {
        private int id;
        private int userId;
        private int couponId;
        private String couponTitle;
        private long couponStartTime;
        private long couponEndTime;
        private int orderId;
        private String couponRules;
        private long gmtCreate;
        private long gmtModify;
        private int status;
        private double amount;
        private double amountCent;
        private double amountYuanDescription;
        private int couponType;
        private int channelType;
        private String amountLimit;
        private String amountLimitCent;
        private String amountLimitYuanDescription;
        private boolean fenPriceTypeUserCoupon;

        public double getAmountCent() {
            return amountCent;
        }

        public double getAmountYuanDescription() {
            return amountYuanDescription;
        }

        public String getAmountLimitCent() {
            return amountLimitCent;
        }

        public String getAmountLimitYuanDescription() {
            return amountLimitYuanDescription;
        }

        public boolean isFenPriceTypeUserCoupon() {
            return fenPriceTypeUserCoupon;
        }

        public void setAmount(double amount) {
            this.amount = amount;
        }

        public void setCouponType(int couponType) {
            this.couponType = couponType;
        }

        public void setChannelType(int channelType) {
            this.channelType = channelType;
        }

        public void setAmountLimit(String amountLimit) {
            this.amountLimit = amountLimit;
        }

        public double getAmount() {
            return amount;
        }

        public int getCouponType() {
            return couponType;
        }

        public int getChannelType() {
            return channelType;
        }

        public String getAmountLimit() {
            return amountLimit;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getUserId() {
            return userId;
        }

        public void setUserId(int userId) {
            this.userId = userId;
        }

        public int getCouponId() {
            return couponId;
        }

        public void setCouponId(int couponId) {
            this.couponId = couponId;
        }

        public String getCouponTitle() {
            return couponTitle;
        }

        public void setCouponTitle(String couponTitle) {
            this.couponTitle = couponTitle;
        }

        public long getCouponStartTime() {
            return couponStartTime;
        }

        public void setCouponStartTime(long couponStartTime) {
            this.couponStartTime = couponStartTime;
        }

        public long getCouponEndTime() {
            return couponEndTime;
        }

        public void setCouponEndTime(long couponEndTime) {
            this.couponEndTime = couponEndTime;
        }

        public int getOrderId() {
            return orderId;
        }

        public void setOrderId(int orderId) {
            this.orderId = orderId;
        }

        public String getCouponRules() {
            return couponRules;
        }

        public void setCouponRules(String couponRules) {
            this.couponRules = couponRules;
        }

        public long getGmtCreate() {
            return gmtCreate;
        }

        public void setGmtCreate(long gmtCreate) {
            this.gmtCreate = gmtCreate;
        }

        public long getGmtModify() {
            return gmtModify;
        }

        public void setGmtModify(long gmtModify) {
            this.gmtModify = gmtModify;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }
    }
}
