package com.xianzaishi.normandie.bean;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by linghuchong on 2016/8/26.
 */
public class ShoppingCarInfo {
    public int code;
    public ArrayList<CarBody> data;
    public String error;// false,
    public String message;// 成功,
    public String success;// true

    @Override
    public String toString() {
        return "ShoppingCarInfo{" +
                "error='" + error + '\'' +
                ", message='" + message + '\'' +
                ", success='" + success + '\'' +
                ", code='" + code + '\'' +
                '}';
    }

    public class CarBody implements Serializable{
        public boolean isChecked;
        public String effeAmount;// 3.0,
        public String id;// 1,
        public String itemCount;// 3,
        public String itemEffePrice;// 1.0,
        public String itemId;// 0,
        public String itemName;// 精选山东丰水梨1个 约480克 新鲜水果梨子,
        public String itemPayPrice;// 1.0,
        public String payAmount;// 3.0,
        public String skuId;// 0,


        @Override
        public String toString() {
            return "CarBody{" +
                    "id='" + id + '\'' +
                    ", isChecked=" + isChecked +
                    ", effeAmount='" + effeAmount + '\'' +
                    '}';
        }

        class skuInfo {
            public String skuId;//9,
            public String itemId;//10201609,
            public String supplierId;//1,
            public String skuCode;//10010010001009,
            public String sku69code;//6911006000052,
            public String skuPluCode;//10002,
            public String inventory;//0,
            public String price;//100,
            public String discountPrice;//100,
            public String property;//null,
            public String status;//1,
            public String gmtCreate;//1471946489000,
            public String gmtModified;//1471946489000,
            public String skuUnit;//1,
            public String skuCount;//100,
            public String title;//精选山东丰水梨1个 约480克 新鲜水果梨子,
            public String feature;//null,
            public String tags;//null,
            public String checkedFailedReason;//null,
            public String checkerList;//[],
            public String propertyInfo;//{},
            public String priceYuan;//1.00,
            public String priceYuanString;//1.00,
            public String discountPriceYuan;//1.00,
            public String discountPriceYuanString;//1.00,
            public String saleStatus;//1,
            public String quantity;//0,
            public String cost;//0,
            public String skuType;//1,
            public String promotionInfo;//null,
            public String specification;//null,
            public String saleUnitType;//1,
            public String frontProperty;//{}
        }
    }
}
