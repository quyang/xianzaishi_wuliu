package com.xianzaishi.normandie.bean;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Administrator on 2016/9/9.
 */
public class SelectAddressBean1 {

    /**
     * code : 1
     * message : 成功
     * data : {"level1Area":[{"id":10806,"code":"310000","parentCode":"0","name":"上海市","shortName":"上海","longitude":121.473,"latitude":31.2317,"level":1,"sort":10,"status":1,"gmtCreate":1462885832000,"gmtModify":1462885832000}],"level2Area":[{"id":10807,"code":"310100","parentCode":"310000","name":"上海市","shortName":"上海","longitude":121.473,"latitude":31.2317,"level":2,"sort":2,"status":1,"gmtCreate":1462885832000,"gmtModify":1462885832000}],"level3Area":[{"id":10880,"code":"310110","parentCode":"310100","name":"杨浦区","shortName":"杨浦","longitude":121.523,"latitude":31.2708,"level":3,"sort":17,"status":1,"gmtCreate":1462885832000,"gmtModify":1462885832000},{"id":10851,"code":"310107","parentCode":"310100","name":"普陀区","shortName":"普陀","longitude":121.393,"latitude":31.2417,"level":3,"sort":13,"status":1,"gmtCreate":1462885832000,"gmtModify":1462885832000}],"level4Area":[{"id":10883,"code":"310110008","parentCode":"310110","name":"江浦路街道","shortName":"江浦路街道","longitude":121.523,"latitude":31.2708,"level":4,"sort":4,"status":1,"gmtCreate":1462885832000,"gmtModify":1462885832000},{"id":10853,"code":"310107014","parentCode":"310107","name":"长风新村街道","shortName":"长风新村街道","longitude":121.4,"latitude":31.2325,"level":4,"sort":7,"status":1,"gmtCreate":1462885832000,"gmtModify":1462885832000},{"id":10854,"code":"310107015","parentCode":"310107","name":"长寿路街道","shortName":"长寿路街道","longitude":121.393,"latitude":31.2417,"level":4,"sort":8,"status":1,"gmtCreate":1462885832000,"gmtModify":1462885832000}]}
     * success : true
     * error : false
     */

    private int code;
    private String message;
    private DataBean data;
    private boolean success;
    private boolean error;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public static class DataBean {
        /**
         * id : 10806
         * code : 310000
         * parentCode : 0
         * name : 上海市
         * shortName : 上海
         * longitude : 121.473
         * latitude : 31.2317
         * level : 1
         * sort : 10
         * status : 1
         * gmtCreate : 1462885832000
         * gmtModify : 1462885832000
         */

        private List<Level1AreaBean> level1Area;
        /**
         * id : 10807
         * code : 310100
         * parentCode : 310000
         * name : 上海市
         * shortName : 上海
         * longitude : 121.473
         * latitude : 31.2317
         * level : 2
         * sort : 2
         * status : 1
         * gmtCreate : 1462885832000
         * gmtModify : 1462885832000
         */

        private List<Level2AreaBean> level2Area;
        /**
         * id : 10880
         * code : 310110
         * parentCode : 310100
         * name : 杨浦区
         * shortName : 杨浦
         * longitude : 121.523
         * latitude : 31.2708
         * level : 3
         * sort : 17
         * status : 1
         * gmtCreate : 1462885832000
         * gmtModify : 1462885832000
         */

        private List<Level3AreaBean> level3Area;
        /**
         * id : 10883
         * code : 310110008
         * parentCode : 310110
         * name : 江浦路街道
         * shortName : 江浦路街道
         * longitude : 121.523
         * latitude : 31.2708
         * level : 4
         * sort : 4
         * status : 1
         * gmtCreate : 1462885832000
         * gmtModify : 1462885832000
         */

        private List<Level4AreaBean> level4Area;

        public List<Level1AreaBean> getLevel1Area() {
            return level1Area;
        }

        public void setLevel1Area(List<Level1AreaBean> level1Area) {
            this.level1Area = level1Area;
        }

        public List<Level2AreaBean> getLevel2Area() {
            return level2Area;
        }

        public void setLevel2Area(List<Level2AreaBean> level2Area) {
            this.level2Area = level2Area;
        }

        public List<Level3AreaBean> getLevel3Area() {
            return level3Area;
        }

        public void setLevel3Area(List<Level3AreaBean> level3Area) {
            this.level3Area = level3Area;
        }

        public List<Level4AreaBean> getLevel4Area() {
            return level4Area;
        }

        public void setLevel4Area(List<Level4AreaBean> level4Area) {
            this.level4Area = level4Area;
        }
        public static class AreaBean {
            private int id;
            private String code;
            private String parentCode;
            private String name;
            private String shortName;
            private double longitude;
            private double latitude;
            private int level;
            private int sort;
            private int status;
            private long gmtCreate;
            private long gmtModify;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getCode() {
                return code;
            }

            public void setCode(String code) {
                this.code = code;
            }

            public String getParentCode() {
                return parentCode;
            }

            public void setParentCode(String parentCode) {
                this.parentCode = parentCode;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getShortName() {
                return shortName;
            }

            public void setShortName(String shortName) {
                this.shortName = shortName;
            }

            public double getLongitude() {
                return longitude;
            }

            public void setLongitude(double longitude) {
                this.longitude = longitude;
            }

            public double getLatitude() {
                return latitude;
            }

            public void setLatitude(double latitude) {
                this.latitude = latitude;
            }

            public int getLevel() {
                return level;
            }

            public void setLevel(int level) {
                this.level = level;
            }

            public int getSort() {
                return sort;
            }

            public void setSort(int sort) {
                this.sort = sort;
            }

            public int getStatus() {
                return status;
            }

            public void setStatus(int status) {
                this.status = status;
            }

            public long getGmtCreate() {
                return gmtCreate;
            }

            public void setGmtCreate(long gmtCreate) {
                this.gmtCreate = gmtCreate;
            }

            public long getGmtModify() {
                return gmtModify;
            }

            public void setGmtModify(long gmtModify) {
                this.gmtModify = gmtModify;
            }
        }
        public static class Level1AreaBean implements Serializable{
            private int id;
            private String code;
            private String parentCode;
            private String name;
            private String shortName;
            private double longitude;
            private double latitude;
            private int level;
            private int sort;
            private int status;
            private long gmtCreate;
            private long gmtModify;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getCode() {
                return code;
            }

            public void setCode(String code) {
                this.code = code;
            }

            public String getParentCode() {
                return parentCode;
            }

            public void setParentCode(String parentCode) {
                this.parentCode = parentCode;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getShortName() {
                return shortName;
            }

            public void setShortName(String shortName) {
                this.shortName = shortName;
            }

            public double getLongitude() {
                return longitude;
            }

            public void setLongitude(double longitude) {
                this.longitude = longitude;
            }

            public double getLatitude() {
                return latitude;
            }

            public void setLatitude(double latitude) {
                this.latitude = latitude;
            }

            public int getLevel() {
                return level;
            }

            public void setLevel(int level) {
                this.level = level;
            }

            public int getSort() {
                return sort;
            }

            public void setSort(int sort) {
                this.sort = sort;
            }

            public int getStatus() {
                return status;
            }

            public void setStatus(int status) {
                this.status = status;
            }

            public long getGmtCreate() {
                return gmtCreate;
            }

            public void setGmtCreate(long gmtCreate) {
                this.gmtCreate = gmtCreate;
            }

            public long getGmtModify() {
                return gmtModify;
            }

            public void setGmtModify(long gmtModify) {
                this.gmtModify = gmtModify;
            }
        }

        public static class Level2AreaBean implements Serializable{
            private int id;
            private String code;
            private String parentCode;
            private String name;
            private String shortName;
            private double longitude;
            private double latitude;
            private int level;
            private int sort;
            private int status;
            private long gmtCreate;
            private long gmtModify;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getCode() {
                return code;
            }

            public void setCode(String code) {
                this.code = code;
            }

            public String getParentCode() {
                return parentCode;
            }

            public void setParentCode(String parentCode) {
                this.parentCode = parentCode;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getShortName() {
                return shortName;
            }

            public void setShortName(String shortName) {
                this.shortName = shortName;
            }

            public double getLongitude() {
                return longitude;
            }

            public void setLongitude(double longitude) {
                this.longitude = longitude;
            }

            public double getLatitude() {
                return latitude;
            }

            public void setLatitude(double latitude) {
                this.latitude = latitude;
            }

            public int getLevel() {
                return level;
            }

            public void setLevel(int level) {
                this.level = level;
            }

            public int getSort() {
                return sort;
            }

            public void setSort(int sort) {
                this.sort = sort;
            }

            public int getStatus() {
                return status;
            }

            public void setStatus(int status) {
                this.status = status;
            }

            public long getGmtCreate() {
                return gmtCreate;
            }

            public void setGmtCreate(long gmtCreate) {
                this.gmtCreate = gmtCreate;
            }

            public long getGmtModify() {
                return gmtModify;
            }

            public void setGmtModify(long gmtModify) {
                this.gmtModify = gmtModify;
            }
        }

        public static class Level3AreaBean implements Serializable{
            private int id;
            private String code;
            private String parentCode;
            private String name;
            private String shortName;
            private double longitude;
            private double latitude;
            private int level;
            private int sort;
            private int status;
            private long gmtCreate;
            private long gmtModify;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getCode() {
                return code;
            }

            public void setCode(String code) {
                this.code = code;
            }

            public String getParentCode() {
                return parentCode;
            }

            public void setParentCode(String parentCode) {
                this.parentCode = parentCode;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getShortName() {
                return shortName;
            }

            public void setShortName(String shortName) {
                this.shortName = shortName;
            }

            public double getLongitude() {
                return longitude;
            }

            public void setLongitude(double longitude) {
                this.longitude = longitude;
            }

            public double getLatitude() {
                return latitude;
            }

            public void setLatitude(double latitude) {
                this.latitude = latitude;
            }

            public int getLevel() {
                return level;
            }

            public void setLevel(int level) {
                this.level = level;
            }

            public int getSort() {
                return sort;
            }

            public void setSort(int sort) {
                this.sort = sort;
            }

            public int getStatus() {
                return status;
            }

            public void setStatus(int status) {
                this.status = status;
            }

            public long getGmtCreate() {
                return gmtCreate;
            }

            public void setGmtCreate(long gmtCreate) {
                this.gmtCreate = gmtCreate;
            }

            public long getGmtModify() {
                return gmtModify;
            }

            public void setGmtModify(long gmtModify) {
                this.gmtModify = gmtModify;
            }
        }

        public static class Level4AreaBean implements Serializable{
            private int id;
            private String code;
            private String parentCode;
            private String name;
            private String shortName;
            private double longitude;
            private double latitude;
            private int level;
            private int sort;
            private int status;
            private long gmtCreate;
            private long gmtModify;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getCode() {
                return code;
            }

            public void setCode(String code) {
                this.code = code;
            }

            public String getParentCode() {
                return parentCode;
            }

            public void setParentCode(String parentCode) {
                this.parentCode = parentCode;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getShortName() {
                return shortName;
            }

            public void setShortName(String shortName) {
                this.shortName = shortName;
            }

            public double getLongitude() {
                return longitude;
            }

            public void setLongitude(double longitude) {
                this.longitude = longitude;
            }

            public double getLatitude() {
                return latitude;
            }

            public void setLatitude(double latitude) {
                this.latitude = latitude;
            }

            public int getLevel() {
                return level;
            }

            public void setLevel(int level) {
                this.level = level;
            }

            public int getSort() {
                return sort;
            }

            public void setSort(int sort) {
                this.sort = sort;
            }

            public int getStatus() {
                return status;
            }

            public void setStatus(int status) {
                this.status = status;
            }

            public long getGmtCreate() {
                return gmtCreate;
            }

            public void setGmtCreate(long gmtCreate) {
                this.gmtCreate = gmtCreate;
            }

            public long getGmtModify() {
                return gmtModify;
            }

            public void setGmtModify(long gmtModify) {
                this.gmtModify = gmtModify;
            }
        }
    }
}
