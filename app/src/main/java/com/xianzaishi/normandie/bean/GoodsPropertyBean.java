package com.xianzaishi.normandie.bean;

/**
 * quyang
 * 商品属性
 * Created by Administrator on 2016/8/26.
 */
public class GoodsPropertyBean {


    public String yongfa;
    public String baozhiqi;
    public String chandi;
    public String danzhong;


    @Override
    public String toString() {
        return "GoodsPropertyBean{" +
                "yongfa='" + yongfa + '\'' +
                ", baozhiqi='" + baozhiqi + '\'' +
                ", chandi='" + chandi + '\'' +
                ", danzhong='" + danzhong + '\'' +
                '}';
    }
}
