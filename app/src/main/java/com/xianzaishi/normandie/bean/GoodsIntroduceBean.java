package com.xianzaishi.normandie.bean;

import java.util.List;

/**
 * quyang goods introduction
 * <p/>
 * Created by Administrator on 2016/8/26.
 */
public class GoodsIntroduceBean {

    public int resultCode;
    public boolean succcess;
    public String errorMsg;
    public ModuleBean module;
    public boolean success;
    public long currentTime;


    public  static class ModuleBean {


        public int itemId;
        public List<String> picList;
        public String title;
        public String subtitle;
        public List<ItemSkuVOsBean> itemSkuVOs;
        public String introductionUrl;
        public String propertyUrl;


        public static  class ItemSkuVOsBean {

            public int skuId;
            public int itemId;
            public int inventory;//库存字段
            public int skuUnit;
            public String title;
            public int quantity;
            public Object promotionInfo;//促销信息
            public int price;
            public int discountPrice;
            public String priceYuanString;
            public String discountPriceYuanString;
            public String saleDetailInfo;//规格
            public String originplace; //产地
            public String tags;
            public TagDetailBean tagDetail;

            public static class TagDetailBean{
                public int skuId;
                public boolean mayPlus;
                public boolean mayOrder;
                public boolean maySale;
                public boolean specialTag;
                public String channel;
                public String tagContent;
            }
        }
    }


}

