package com.xianzaishi.normandie.bean;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Administrator on 2016/8/31.
 */
public class CategoryListViewBean {

    /**
     * resultCode : 1
     * success : true
     * errorMsg :
     * module : {"items":[{"itemId":10201604,"picUrl":"http://img.xianzaishi.com/1/1474544722902.png","title":"浩烨气调崇明白山羊肉气调带皮羊前腿400g","subtitle":"气调带皮羊前腿","price":1,"discountPrice":1,"priceYuanString":"0.01","discountPriceYuanString":"0.01","sku":"10004","feature":null,"inventory":1,"itemSkuVOs":[{"skuId":10004,"itemId":10201604,"inventory":1,"skuUnit":26,"title":"浩烨气调崇明白山羊肉气调带皮羊前腿400g","quantity":0,"promotionInfo":null,"price":1,"discountPrice":1,"priceYuanString":"0.01","discountPriceYuanString":"0.01","saleDetailInfo":"1盒","originplace":"上海"}]},{"itemId":10201605,"picUrl":"http://img.xianzaishi.com/1/1474544723060.png","title":"浩烨气调崇明白山羊肉气调羊肋排400g","subtitle":"气调羊肋排","price":1,"discountPrice":1,"priceYuanString":"0.01","discountPriceYuanString":"0.01","sku":"10005","feature":null,"inventory":1,"itemSkuVOs":[{"skuId":10005,"itemId":10201605,"inventory":1,"skuUnit":26,"title":"浩烨气调崇明白山羊肉气调羊肋排400g","quantity":0,"promotionInfo":null,"price":1,"discountPrice":1,"priceYuanString":"0.01","discountPriceYuanString":"0.01","saleDetailInfo":"1盒","originplace":"上海"}]}],"itemCount":2}
     * succcess : true
     */

    private int resultCode;
    private boolean success;
    private String errorMsg;
    /**
     * items : [{"itemId":10201604,"picUrl":"http://img.xianzaishi.com/1/1474544722902.png","title":"浩烨气调崇明白山羊肉气调带皮羊前腿400g","subtitle":"气调带皮羊前腿","price":1,"discountPrice":1,"priceYuanString":"0.01","discountPriceYuanString":"0.01","sku":"10004","feature":null,"inventory":1,"itemSkuVOs":[{"skuId":10004,"itemId":10201604,"inventory":1,"skuUnit":26,"title":"浩烨气调崇明白山羊肉气调带皮羊前腿400g","quantity":0,"promotionInfo":null,"price":1,"discountPrice":1,"priceYuanString":"0.01","discountPriceYuanString":"0.01","saleDetailInfo":"1盒","originplace":"上海"}]},{"itemId":10201605,"picUrl":"http://img.xianzaishi.com/1/1474544723060.png","title":"浩烨气调崇明白山羊肉气调羊肋排400g","subtitle":"气调羊肋排","price":1,"discountPrice":1,"priceYuanString":"0.01","discountPriceYuanString":"0.01","sku":"10005","feature":null,"inventory":1,"itemSkuVOs":[{"skuId":10005,"itemId":10201605,"inventory":1,"skuUnit":26,"title":"浩烨气调崇明白山羊肉气调羊肋排400g","quantity":0,"promotionInfo":null,"price":1,"discountPrice":1,"priceYuanString":"0.01","discountPriceYuanString":"0.01","saleDetailInfo":"1盒","originplace":"上海"}]}]
     * itemCount : 2
     */

    private ModuleBean module;
    private boolean succcess;

    public int getResultCode() {
        return resultCode;
    }

    public void setResultCode(int resultCode) {
        this.resultCode = resultCode;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public ModuleBean getModule() {
        return module;
    }

    public void setModule(ModuleBean module) {
        this.module = module;
    }

    public boolean isSucccess() {
        return succcess;
    }

    public void setSucccess(boolean succcess) {
        this.succcess = succcess;
    }

    public static class ModuleBean {
        private int itemCount;
        /**
         * itemId : 10201604
         * picUrl : http://img.xianzaishi.com/1/1474544722902.png
         * title : 浩烨气调崇明白山羊肉气调带皮羊前腿400g
         * subtitle : 气调带皮羊前腿
         * price : 1
         * discountPrice : 1
         * priceYuanString : 0.01
         * discountPriceYuanString : 0.01
         * sku : 10004
         * feature : null
         * inventory : 1
         * itemSkuVOs : [{"skuId":10004,"itemId":10201604,"inventory":1,"skuUnit":26,"title":"浩烨气调崇明白山羊肉气调带皮羊前腿400g","quantity":0,"promotionInfo":null,"price":1,"discountPrice":1,"priceYuanString":"0.01","discountPriceYuanString":"0.01","saleDetailInfo":"1盒","originplace":"上海"}]
         */

        private List<ItemsBean> items;

        public int getItemCount() {
            return itemCount;
        }

        public void setItemCount(int itemCount) {
            this.itemCount = itemCount;
        }

        public List<ItemsBean> getItems() {
            return items;
        }

        public void setItems(List<ItemsBean> items) {
            this.items = items;
        }

        public static class ItemsBean {
            private int itemId;
            private String picUrl;
            private String title;
            private String subtitle;
            private int price;
            private int discountPrice;
            private String priceYuanString;
            private String discountPriceYuanString;
            private String sku;
            private Object feature;
            private int inventory;
            /**
             * skuId : 10004
             * itemId : 10201604
             * inventory : 1
             * skuUnit : 26
             * title : 浩烨气调崇明白山羊肉气调带皮羊前腿400g
             * quantity : 0
             * promotionInfo : null
             * price : 1
             * discountPrice : 1
             * priceYuanString : 0.01
             * discountPriceYuanString : 0.01
             * saleDetailInfo : 1盒
             * originplace : 上海
             */

            private List<ItemSkuVOsBean> itemSkuVOs;

            public int getItemId() {
                return itemId;
            }

            public void setItemId(int itemId) {
                this.itemId = itemId;
            }

            public String getPicUrl() {
                return picUrl;
            }

            public void setPicUrl(String picUrl) {
                this.picUrl = picUrl;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getSubtitle() {
                return subtitle;
            }

            public void setSubtitle(String subtitle) {
                this.subtitle = subtitle;
            }

            public int getPrice() {
                return price;
            }

            public void setPrice(int price) {
                this.price = price;
            }

            public int getDiscountPrice() {
                return discountPrice;
            }

            public void setDiscountPrice(int discountPrice) {
                this.discountPrice = discountPrice;
            }

            public String getPriceYuanString() {
                return priceYuanString;
            }

            public void setPriceYuanString(String priceYuanString) {
                this.priceYuanString = priceYuanString;
            }

            public String getDiscountPriceYuanString() {
                return discountPriceYuanString;
            }

            public void setDiscountPriceYuanString(String discountPriceYuanString) {
                this.discountPriceYuanString = discountPriceYuanString;
            }

            public String getSku() {
                return sku;
            }

            public void setSku(String sku) {
                this.sku = sku;
            }

            public Object getFeature() {
                return feature;
            }

            public void setFeature(Object feature) {
                this.feature = feature;
            }

            public int getInventory() {
                return inventory;
            }

            public void setInventory(int inventory) {
                this.inventory = inventory;
            }

            public List<ItemSkuVOsBean> getItemSkuVOs() {
                return itemSkuVOs;
            }

            public void setItemSkuVOs(List<ItemSkuVOsBean> itemSkuVOs) {
                this.itemSkuVOs = itemSkuVOs;
            }

            public static class ItemSkuVOsBean {
                private int skuId;
                private int itemId;
                private int inventory;
                private int skuUnit;
                private String title;
                private int quantity;
                private Object promotionInfo;
                private int price;
                private int discountPrice;
                private String priceYuanString;
                private String discountPriceYuanString;
                private String saleDetailInfo;
                private String originplace;
                private String tags;
                private TagDetailBean tagDetail;


                public TagDetailBean getTagDetail() {
                    return tagDetail;
                }

                public void setTagDetail(TagDetailBean tagDetail) {
                    this.tagDetail = tagDetail;
                }

                public String getTags() {
                    return tags;
                }

                public int getSkuId() {
                    return skuId;
                }

                public void setSkuId(int skuId) {
                    this.skuId = skuId;
                }

                public int getItemId() {
                    return itemId;
                }

                public void setItemId(int itemId) {
                    this.itemId = itemId;
                }

                public int getInventory() {
                    return inventory;
                }

                public void setInventory(int inventory) {
                    this.inventory = inventory;
                }

                public int getSkuUnit() {
                    return skuUnit;
                }

                public void setSkuUnit(int skuUnit) {
                    this.skuUnit = skuUnit;
                }

                public String getTitle() {
                    return title;
                }

                public void setTitle(String title) {
                    this.title = title;
                }

                public int getQuantity() {
                    return quantity;
                }

                public void setQuantity(int quantity) {
                    this.quantity = quantity;
                }

                public Object getPromotionInfo() {
                    return promotionInfo;
                }

                public void setPromotionInfo(Object promotionInfo) {
                    this.promotionInfo = promotionInfo;
                }

                public int getPrice() {
                    return price;
                }

                public void setPrice(int price) {
                    this.price = price;
                }

                public int getDiscountPrice() {
                    return discountPrice;
                }

                public void setDiscountPrice(int discountPrice) {
                    this.discountPrice = discountPrice;
                }

                public String getPriceYuanString() {
                    return priceYuanString;
                }

                public void setPriceYuanString(String priceYuanString) {
                    this.priceYuanString = priceYuanString;
                }

                public String getDiscountPriceYuanString() {
                    return discountPriceYuanString;
                }

                public void setDiscountPriceYuanString(String discountPriceYuanString) {
                    this.discountPriceYuanString = discountPriceYuanString;
                }

                public String getSaleDetailInfo() {
                    return saleDetailInfo;
                }

                public void setSaleDetailInfo(String saleDetailInfo) {
                    this.saleDetailInfo = saleDetailInfo;
                }

                public String getOriginplace() {
                    return originplace;
                }

                public void setOriginplace(String originplace) {
                    this.originplace = originplace;
                }

                public static class TagDetailBean implements Serializable {
                    private int skuId;
                    private boolean mayPlus;
                    private boolean mayOrder;
                    private boolean maySale;
                    private boolean specialTag;
                    private String channel;
                    private String tagContent;

                    public int getSkuId() {
                        return skuId;
                    }

                    public void setSkuId(int skuId) {
                        this.skuId = skuId;
                    }

                    public boolean isMayPlus() {
                        return mayPlus;
                    }

                    public void setMayPlus(boolean mayPlus) {
                        this.mayPlus = mayPlus;
                    }

                    public boolean isMayOrder() {
                        return mayOrder;
                    }

                    public void setMayOrder(boolean mayOrder) {
                        this.mayOrder = mayOrder;
                    }

                    public boolean isMaySale() {
                        return maySale;
                    }

                    public void setMaySale(boolean maySale) {
                        this.maySale = maySale;
                    }

                    public boolean isSpecialTag() {
                        return specialTag;
                    }

                    public void setSpecialTag(boolean specialTag) {
                        this.specialTag = specialTag;
                    }

                    public String getChannel() {
                        return channel;
                    }

                    public void setChannel(String channel) {
                        this.channel = channel;
                    }

                    public String getTagContent() {
                        return tagContent;
                    }

                    public void setTagContent(String tagContent) {
                        this.tagContent = tagContent;
                    }
                }
            }
        }
    }
}
