package com.xianzaishi.normandie.bean;

import java.util.List;

/**
 * 商品skuInfo
 * quyang
 * Created by Administrator on 2016/9/2.
 */
public class SkuInfoBean {

    public String skuId;// 25,
    public String itemId;// 10201625,
    public String supplierId;// 1,
    public String skuCode;// 10030020005004,
    public String sku69code;// 69121321312,
    public String skuPluCode;// 10018,
    public String inventory;// 0,
    public String price;// 1,
    public String discountPrice;// 1,
    public String property;// s=19&0;22&15000;17&0;24&19900;15&0;16&0;26&0;13&15;14&15;12&20;21&13;3&朝鲜;20&13;2&风向标;6&1;32&0;5&69121321312;4&朝鲜;8&1;\np=18&72;23&75;33&89;34&93;25&75;39&95;27&77;28&78;11&68;29&77;10&68;1&1;7&68;31&83;9&68;,
    public String status;// 1,
    public String gmtCreate;// 1472742007000,
    public String gmtModified;// 1472742007000,
    public String skuUnit;// 68,
    public String skuCount;// 1,
    public String title;// 活雪蟹 800-1200g/只,
    public String feature;// sp&1只\nsu&1,
    public String tags;// null,
    public String checkedFailedReason;// null,
    public List<CheckerList> checkerList;// [],
    public PropertyInfo propertyInfo;// {},
    public String priceYuan;// 0.01,
    public String priceYuanString;// 0.01,
    public String discountPriceYuan;// 0.01,
    public String discountPriceYuanString;// 0.01,
    public String saleStatus;// 1,
    public String quantity;// 0,
    public String cost;// 0,
    public String skuType;// 1,
    public String promotionInfo;// null,
    public FrontProperty frontProperty;// {}


    public class CheckerList{}
    public class PropertyInfo{}
    public class FrontProperty{}


    @Override
    public String toString() {
        return "SkuInfoBean{" +
                "checkedFailedReason='" + checkedFailedReason + '\'' +
                ", skuId='" + skuId + '\'' +
                ", itemId='" + itemId + '\'' +
                ", supplierId='" + supplierId + '\'' +
                ", skuCode='" + skuCode + '\'' +
                ", sku69code='" + sku69code + '\'' +
                ", skuPluCode='" + skuPluCode + '\'' +
                ", inventory='" + inventory + '\'' +
                ", price='" + price + '\'' +
                ", discountPrice='" + discountPrice + '\'' +
                ", property='" + property + '\'' +
                ", status='" + status + '\'' +
                ", gmtCreate='" + gmtCreate + '\'' +
                ", gmtModified='" + gmtModified + '\'' +
                ", skuUnit='" + skuUnit + '\'' +
                ", skuCount='" + skuCount + '\'' +
                ", title='" + title + '\'' +
                ", feature='" + feature + '\'' +
                ", tags='" + tags + '\'' +
                ", checkerList=" + checkerList +
                ", propertyInfo=" + propertyInfo +
                ", priceYuan='" + priceYuan + '\'' +
                ", priceYuanString='" + priceYuanString + '\'' +
                ", discountPriceYuan='" + discountPriceYuan + '\'' +
                ", discountPriceYuanString='" + discountPriceYuanString + '\'' +
                ", saleStatus='" + saleStatus + '\'' +
                ", quantity='" + quantity + '\'' +
                ", cost='" + cost + '\'' +
                ", skuType='" + skuType + '\'' +
                ", promotionInfo='" + promotionInfo + '\'' +
                ", frontProperty=" + frontProperty +
                '}';
    }
}
