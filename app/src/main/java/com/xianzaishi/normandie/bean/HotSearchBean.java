package com.xianzaishi.normandie.bean;

import java.util.List;

/**
 * Created by Administrator on 2016/9/28.
 */

public class HotSearchBean {

    /**
     * resultCode : 1
     * success : true
     * errorMsg :
     * module : ["苹果","鸭梨","三文鱼","排骨"]
     * succcess : true
     */

    private int resultCode;
    private boolean success;
    private String errorMsg;
    private boolean succcess;
    private List<String> module;

    public int getResultCode() {
        return resultCode;
    }

    public void setResultCode(int resultCode) {
        this.resultCode = resultCode;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public boolean isSucccess() {
        return succcess;
    }

    public void setSucccess(boolean succcess) {
        this.succcess = succcess;
    }

    public List<String> getModule() {
        return module;
    }

    public void setModule(List<String> module) {
        this.module = module;
    }
}
