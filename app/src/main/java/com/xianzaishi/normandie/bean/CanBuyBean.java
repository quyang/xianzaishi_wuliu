package com.xianzaishi.normandie.bean;

/**
 * Created by Administrator on 2016/10/24.
 */

public class CanBuyBean {

    /**
     * close1 : true
     */

    private boolean close;

    public boolean isClose() {
        return close;
    }

    public void setClose(boolean close) {
        this.close = close;
    }
}
