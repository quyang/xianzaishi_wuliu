package com.xianzaishi.normandie.bean;

import java.util.List;

/**
 * http://139.196.92.240/wapcenter/requesthome/homepage?pageId=1&hasAllFloor=false
 * Created by Administrator on 2016/8/30.
 */
public class HomeTopBean {

    public int resultCode;// 1,
    public boolean success;//true,
    public String errorMsg;//
    public List<ModuleBean> module;//
    public boolean succcess;//true


    public class ModuleBean {

        public String title;// null,
        public List<ItemsBean> items;// null,
        public int stepType;// null
        public List<PicListBean> picList;//

        public class ItemsBean {
            public int itemId;// 10201659,
            public String picUrl;// http;////img.alicdn.com/imgextra/i3/496514980/TB2tdt5aVXXXXa0XXXXXXXXXXXX-496514980.jpg,
            public String title;// Perrier 巴黎水含气天然青柠味 330ml/瓶,
            public String subtitle;// 巴黎水青柠味,
            public String price;// 1,
            public String discountPrice;// 1,
            public String priceYuanString;// 0.01,
            public String discountPriceYuanString;// 0.01,
            public String sku;// 59,
            public Object feature;// null,
            public int inventory;// 1,
            public List<ItemSkuVOsBean> itemSkuVOs;//


            public class ItemSkuVOsBean {
                public String skuId;// 59,
                public String itemId;// 10201659,
                public int inventory;// 1,
                public String skuUnit;// 69,
                public String title;// Perrier 巴黎水含气天然青柠味 330ml/瓶,
                public String quantity;// 0,
                public Object promotionInfo;// null,
                public String price;// 1,
                public String discountPrice;// 1,
                public String priceYuanString;// 0.01,
                public String discountPriceYuanString;// 0.01,
                public String saleDetailInfo;// 1组4瓶,
                public String originplace;// 法国
            }
        }


        public class PicListBean {
            public String picUrl;// img.alicdn.com/imgextra/i3/496514980/TB2tdt5aVXXXXa0XXXXXXXXXXXX-496514980.jpg,
            public String targetId;// 1,
            public int targetType;// 1


            @Override
            public String toString() {
                return "PicListBean{" +
                        "picUrl='" + picUrl + '\'' +
                        ", targetId='" + targetId + '\'' +
                        ", targetType=" + targetType +
                        '}';
            }
        }

        @Override
        public String toString() {
            return "ModuleBean{" +
                    "items=" + items +
                    ", title=" + title +
                    ", stepType=" + stepType +
                    ", picList=" + picList +
                    '}';
        }
    }


    @Override
    public String toString() {
        return "HomeTopBean{" +
                "errorMsg='" + errorMsg + '\'' +
                ", resultCode=" + resultCode +
                ", success=" + success +
                ", module=" + module +
                ", succcess=" + succcess +
                '}';
    }
}

