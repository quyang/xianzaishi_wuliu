package com.xianzaishi.normandie.bean;

/**
 * Created by shenLang on 2016/10/8.
 * 购物车商品总数目
 */

public class ShoppingCarCountBean {

    /**
     * code : 1
     * message : 成功
     * data : 2
     * success : true
     * error : false
     */

    private int code;
    private String message;
    private String data;
    private boolean success;
    private boolean error;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }
}
