package com.xianzaishi.normandie.bean;

import java.util.List;

/**
 * quyang
 * <p/>
 * 订单详情
 * <p/>
 * Created by Administrator on 2016/8/25.
 */
public class OrderDetailsBean {

    public String code;// 1,
    public String message;// 成功,
    public DataBean data;
    public boolean success;// true,
    public boolean error;// false

    @Override
    public String toString() {
        return "OrderDetailsBean{" +
                "code='" + code + '\'' +
                ", message='" + message + '\'' +
                ", data=" + data +
                ", success=" + success +
                ", error=" + error +
                '}';
    }

    public class DataBean {

        public String payAmount;// 0,
        public String effeAmount;// 0.22,
        public String deviceId;// ,
        public String cashierId;// 0,
        public String userAddressId;// 73,
        public String userAddress;// 上海市上海市浦东新区塘桥街道1370号,
        public String attribute;// null,
        public String id;// 10028,
        public String seq;// 100000011,
        public String gmtCreate;// 1472821820000,
        public String gmtPay;// null,
        public String gmtEnd;// null,
        public String gmtDistribution;// null,
        public String status;// 2,
        public String statusString;// 待付款,
        public Object shopId;// null,
        public List<ItemsBean> items;//
        public Object logisticalInfo;// null
        public Double credit;
        public String couponId;
        public String userId;
        public int channelType;


        public class ItemsBean {

            public String id;// 10201625,
            public String name;// 活雪蟹,
            public String iconUrl;// http;////img.alicdn.com/imgextra/i3/496514980/TB2tdt5aVXXXXa0XXXXXXXXXXXX-496514980.jpg,
            public String price;// 0.01,
            public String effePrice;// 0.01,
            public String amount;// 0.22,
            public String effeAmount;// 0.22,
            public String count;// 22,
            public SkuBean skuInfo;// {\skuId\;//25,\itemId\;//10201625,\supplierId\;//1,\skuCode\;//10030020005004,\sku69code\;//\69121321312\,\skuPluCode\;//10018,\inventory\;//0,\price\;//1,\discountPrice\;//1,\property\;//\s=19&0;22&15000;17&0;24&19900;15&0;16&0;26&0;13&15;14&15;12&20;21&13;3&朝鲜;20&13;2&风向标;6&1;32&0;5&69121321312;4&朝鲜;8&1;\\np=18&72;23&75;33&89;34&93;25&75;39&95;27&77;28&78;11&68;29&77;10&68;1&1;7&68;31&83;9&68;\,\status\;//1,\gmtCreate\;//1472742007000,\gmtModified\;//1472742007000,\skuUnit\;//68,\skuCount\;//1.0,\title\;//\活雪蟹 800-1200g/只\,\feature\;//\sp&1只\\nsu&1\,\tags\;//null,\checkedFailedReason\;//null,\checkerList\;//[],\propertyInfo\;//{},\priceYuan\;//0.01,\priceYuanString\;//\0.01\,\discountPriceYuan\;//0.01,\discountPriceYuanString\;//\0.01\,\saleStatus\;//1,\quantity\;//0,\cost\;//0,\skuType\;//1,\promotionInfo\;//null,\frontProperty\;//{}},
            public Object categoryId;// null,
            public String skuId;// 25


            @Override
            public String toString() {
                return "ItemsBean{" +
                        "amount='" + amount + '\'' +
                        ", id='" + id + '\'' +
                        ", name='" + name + '\'' +
                        ", iconUrl='" + iconUrl + '\'' +
                        ", price='" + price + '\'' +
                        ", effePrice='" + effePrice + '\'' +
                        ", effeAmount='" + effeAmount + '\'' +
                        ", count='" + count + '\'' +
                        ", skuInfo=" + skuInfo +
                        ", categoryId=" + categoryId +
                        ", skuId='" + skuId + '\'' +
                        '}';
            }

            public class SkuBean{
                public Object spec;

                @Override
                public String toString() {
                    return "SkuBean{" +
                            "spec=" + spec +
                            '}';
                }
            }

        }

        @Override
        public String toString() {
            return "DataBean{" +
                    "attribute='" + attribute + '\'' +
                    ", payAmount='" + payAmount + '\'' +
                    ", effeAmount='" + effeAmount + '\'' +
                    ", deviceId='" + deviceId + '\'' +
                    ", cashierId='" + cashierId + '\'' +
                    ", userAddressId='" + userAddressId + '\'' +
                    ", userAddress='" + userAddress + '\'' +
                    ", id='" + id + '\'' +
                    ", seq='" + seq + '\'' +
                    ", gmtCreate='" + gmtCreate + '\'' +
                    ", gmtPay='" + gmtPay + '\'' +
                    ", gmtEnd='" + gmtEnd + '\'' +
                    ", gmtDistribution='" + gmtDistribution + '\'' +
                    ", status='" + status + '\'' +
                    ", statusString='" + statusString + '\'' +
                    ", shopId='" + shopId + '\'' +
                    ", items=" + items +
                    ", logisticalInfo='" + logisticalInfo + '\'' +
                    '}';
        }
    }
}
