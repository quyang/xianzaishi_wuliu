package com.xianzaishi.normandie.bean;

import java.io.Serializable;
import java.util.List;

/**
 * Created by ShenLang on 2017/1/4.
 * 订单结算页根据skuIds获取商品详情及优惠信息
 */

public class OrderActivityBean {

    /**
     * code : 1
     * data : {"currPrice":"148.18","discountInfos":[{"currPrice":"84.78","discountDesc":"满2件3折,已优惠￥197.82","discountPrice":"197.82","getDiscount":true,"originPrice":"282.60"},{"currPrice":"148.18","discountDesc":"满100元立减30元,已优惠￥30.00","discountPrice":"227.82","getDiscount":true,"originPrice":"376.00"}],"discountPrice":"227.82","freight":"0.00","integral":-1,"objects":[{"effeAmount":"59.90","id":2897,"itemCount":"1.00","itemEffePrice":"59.90","itemIconUrl":"http://img.xianzaishi.com/2/1478351211506.jpeg","itemId":10201964,"itemName":"佳沛金奇异果8粒装","itemPayPrice":"59.90","itemTags":"0","payAmount":"59.90","skuId":10361,"skuInfo":{"spec":"1盒"}},{"effeAmount":"33.50","id":2896,"itemCount":"1.00","itemEffePrice":"33.50","itemIconUrl":"http://img.xianzaishi.com/2/1476981222324.jpg","itemId":10202136,"itemName":"八喜朗姆冰淇淋550g","itemPayPrice":"33.50","itemTags":"0","payAmount":"33.50","skuId":10411,"skuInfo":{"spec":"1杯"}},{"effeAmount":"282.60","id":2902,"itemCount":"2.00","itemEffePrice":"141.30","itemIconUrl":"http://img.xianzaishi.com/2/1476979874484.jpg","itemId":10202013,"itemName":"张裕解百纳干红葡萄酒750ml 特选级出口德国标准","itemPayPrice":"141.30","itemTags":"0","payAmount":"282.60","skuId":20007,"skuInfo":{"spec":"1瓶"}}],"originPrice":"376.00","totalPrice":"148.18"}
     * error : false
     * message : 成功
     * success : true
     */

    private int code;
    /**
     * currPrice : 148.18
     * discountInfos : [{"currPrice":"84.78","discountDesc":"满2件3折,已优惠￥197.82","discountPrice":"197.82","getDiscount":true,"originPrice":"282.60"},{"currPrice":"148.18","discountDesc":"满100元立减30元,已优惠￥30.00","discountPrice":"227.82","getDiscount":true,"originPrice":"376.00"}]
     * discountPrice : 227.82
     * freight : 0.00
     * integral : -1
     * objects : [{"effeAmount":"59.90","id":2897,"itemCount":"1.00","itemEffePrice":"59.90","itemIconUrl":"http://img.xianzaishi.com/2/1478351211506.jpeg","itemId":10201964,"itemName":"佳沛金奇异果8粒装","itemPayPrice":"59.90","itemTags":"0","payAmount":"59.90","skuId":10361,"skuInfo":{"spec":"1盒"}},{"effeAmount":"33.50","id":2896,"itemCount":"1.00","itemEffePrice":"33.50","itemIconUrl":"http://img.xianzaishi.com/2/1476981222324.jpg","itemId":10202136,"itemName":"八喜朗姆冰淇淋550g","itemPayPrice":"33.50","itemTags":"0","payAmount":"33.50","skuId":10411,"skuInfo":{"spec":"1杯"}},{"effeAmount":"282.60","id":2902,"itemCount":"2.00","itemEffePrice":"141.30","itemIconUrl":"http://img.xianzaishi.com/2/1476979874484.jpg","itemId":10202013,"itemName":"张裕解百纳干红葡萄酒750ml 特选级出口德国标准","itemPayPrice":"141.30","itemTags":"0","payAmount":"282.60","skuId":20007,"skuInfo":{"spec":"1瓶"}}]
     * originPrice : 376.00
     * totalPrice : 148.18
     */

    private DataBean data;
    private boolean error;
    private String message;
    private boolean success;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public static class DataBean implements Serializable{
        private double currPrice;
        private double discountPrice;
        private double freight;
        private int integral;
        private double originPrice;
        private double totalPrice;
        /**
         * currPrice : 84.78
         * discountDesc : 满2件3折,已优惠￥197.82
         * discountPrice : 197.82
         * getDiscount : true
         * originPrice : 282.60
         */

        private List<DiscountInfosBean> discountInfos;
        /**
         * effeAmount : 59.90
         * id : 2897
         * itemCount : 1.00
         * itemEffePrice : 59.90
         * itemIconUrl : http://img.xianzaishi.com/2/1478351211506.jpeg
         * itemId : 10201964
         * itemName : 佳沛金奇异果8粒装
         * itemPayPrice : 59.90
         * itemTags : 0
         * payAmount : 59.90
         * skuId : 10361
         * skuInfo : {"spec":"1盒"}
         */

        private List<ObjectsBean> objects;

        public double getCurrPrice() {
            return currPrice;
        }

        public void setCurrPrice(double currPrice) {
            this.currPrice = currPrice;
        }

        public double getDiscountPrice() {
            return discountPrice;
        }

        public void setDiscountPrice(double discountPrice) {
            this.discountPrice = discountPrice;
        }

        public double getFreight() {
            return freight;
        }

        public void setFreight(double freight) {
            this.freight = freight;
        }

        public int getIntegral() {
            return integral;
        }

        public void setIntegral(int integral) {
            this.integral = integral;
        }

        public double getOriginPrice() {
            return originPrice;
        }

        public void setOriginPrice(double originPrice) {
            this.originPrice = originPrice;
        }

        public double getTotalPrice() {
            return totalPrice;
        }

        public void setTotalPrice(double totalPrice) {
            this.totalPrice = totalPrice;
        }

        public List<DiscountInfosBean> getDiscountInfos() {
            return discountInfos;
        }

        public void setDiscountInfos(List<DiscountInfosBean> discountInfos) {
            this.discountInfos = discountInfos;
        }

        public List<ObjectsBean> getObjects() {
            return objects;
        }

        public void setObjects(List<ObjectsBean> objects) {
            this.objects = objects;
        }

        public static class DiscountInfosBean implements Serializable{
            private String currPrice;
            private String discountDesc;
            private String discountPrice;
            private boolean getDiscount;
            private String originPrice;

            public String getCurrPrice() {
                return currPrice;
            }

            public void setCurrPrice(String currPrice) {
                this.currPrice = currPrice;
            }

            public String getDiscountDesc() {
                return discountDesc;
            }

            public void setDiscountDesc(String discountDesc) {
                this.discountDesc = discountDesc;
            }

            public String getDiscountPrice() {
                return discountPrice;
            }

            public void setDiscountPrice(String discountPrice) {
                this.discountPrice = discountPrice;
            }

            public boolean isGetDiscount() {
                return getDiscount;
            }

            public void setGetDiscount(boolean getDiscount) {
                this.getDiscount = getDiscount;
            }

            public String getOriginPrice() {
                return originPrice;
            }

            public void setOriginPrice(String originPrice) {
                this.originPrice = originPrice;
            }
        }

        public static class ObjectsBean implements Serializable{
            private String effeAmount;
            private int id;
            private double itemCount;
            private String itemEffePrice;
            private String itemIconUrl;
            private int itemId;
            private String itemName;
            private String itemPayPrice;
            private String itemTags;
            private String payAmount;
            private int skuId;
            /**
             * spec : 1盒
             */

            private SkuInfoBean skuInfo;

            public String getEffeAmount() {
                return effeAmount;
            }

            public void setEffeAmount(String effeAmount) {
                this.effeAmount = effeAmount;
            }

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public double getItemCount() {
                return itemCount;
            }

            public void setItemCount(double itemCount) {
                this.itemCount = itemCount;
            }

            public String getItemEffePrice() {
                return itemEffePrice;
            }

            public void setItemEffePrice(String itemEffePrice) {
                this.itemEffePrice = itemEffePrice;
            }

            public String getItemIconUrl() {
                return itemIconUrl;
            }

            public void setItemIconUrl(String itemIconUrl) {
                this.itemIconUrl = itemIconUrl;
            }

            public int getItemId() {
                return itemId;
            }

            public void setItemId(int itemId) {
                this.itemId = itemId;
            }

            public String getItemName() {
                return itemName;
            }

            public void setItemName(String itemName) {
                this.itemName = itemName;
            }

            public String getItemPayPrice() {
                return itemPayPrice;
            }

            public void setItemPayPrice(String itemPayPrice) {
                this.itemPayPrice = itemPayPrice;
            }

            public String getItemTags() {
                return itemTags;
            }

            public void setItemTags(String itemTags) {
                this.itemTags = itemTags;
            }

            public String getPayAmount() {
                return payAmount;
            }

            public void setPayAmount(String payAmount) {
                this.payAmount = payAmount;
            }

            public int getSkuId() {
                return skuId;
            }

            public void setSkuId(int skuId) {
                this.skuId = skuId;
            }

            public SkuInfoBean getSkuInfo() {
                return skuInfo;
            }

            public void setSkuInfo(SkuInfoBean skuInfo) {
                this.skuInfo = skuInfo;
            }

            public static class SkuInfoBean implements Serializable{
                private String spec;

                public String getSpec() {
                    return spec;
                }

                public void setSpec(String spec) {
                    this.spec = spec;
                }
            }
        }
    }
}
