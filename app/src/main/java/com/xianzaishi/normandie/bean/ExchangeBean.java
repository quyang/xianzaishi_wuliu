package com.xianzaishi.normandie.bean;

/**
 * Created by Administrator on 2016/12/15.
 */

public class ExchangeBean {

    /**
     * resultCode : 1
     * success : true
     * errorMsg :
     * module : true
     * succcess : true
     */

    private int resultCode;
    private boolean success;
    private String errorMsg;
    private boolean module;
    private boolean succcess;

    public int getResultCode() {
        return resultCode;
    }

    public void setResultCode(int resultCode) {
        this.resultCode = resultCode;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public boolean isModule() {
        return module;
    }

    public void setModule(boolean module) {
        this.module = module;
    }

    public boolean isSucccess() {
        return succcess;
    }

    public void setSucccess(boolean succcess) {
        this.succcess = succcess;
    }
}
