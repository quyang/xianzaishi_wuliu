package com.xianzaishi.normandie.bean;

/**
 * Created by Administrator on 2016/8/24.
 */
public class NewLoginBean {

    /**
     * resultCode : 1
     * success : true
     * errorMsg :
     * module : {"token":"02da683688b1c36f7ccbe607c5836cfe","newUser":false}
     * succcess : true
     */

    private int resultCode;
    private boolean success;
    private String errorMsg;
    /**
     * token : 02da683688b1c36f7ccbe607c5836cfe
     * newUser : false
     */

    private ModuleBean module;
    private boolean succcess;

    public int getResultCode() {
        return resultCode;
    }

    public void setResultCode(int resultCode) {
        this.resultCode = resultCode;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public ModuleBean getModule() {
        return module;
    }

    public void setModule(ModuleBean module) {
        this.module = module;
    }

    public boolean isSucccess() {
        return succcess;
    }

    public void setSucccess(boolean succcess) {
        this.succcess = succcess;
    }

    public static class ModuleBean {
        private String token;
        private boolean newUser;

        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }

        public boolean isNewUser() {
            return newUser;
        }

        public void setNewUser(boolean newUser) {
            this.newUser = newUser;
        }
    }
}
