package com.xianzaishi.normandie.bean;

import java.util.List;

/**
 * Created by Administrator on 2016/8/26.
 */
public class SelectAddressBean {

    /**
     * code : 1
     * message : 成功
     * data : [{"code":"110100","areaName":"北京市"},{"code":"120000","areaName":"天津市"},{"code":"130000","areaName":"河北省"},{"code":"140000","areaName":"山西省"},{"code":"150000","areaName":"内蒙古自治区"},{"code":"210000","areaName":"辽宁省"},{"code":"220000","areaName":"吉林省"},{"code":"230000","areaName":"黑龙江省"},{"code":"310000","areaName":"上海市"},{"code":"320000","areaName":"江苏省"},{"code":"330000","areaName":"浙江省"},{"code":"340000","areaName":"安徽省"},{"code":"350000","areaName":"福建省"},{"code":"360000","areaName":"江西省"},{"code":"370000","areaName":"山东省"},{"code":"410000","areaName":"河南省"},{"code":"420000","areaName":"湖北省"},{"code":"430000","areaName":"湖南省"},{"code":"440000","areaName":"广东省"},{"code":"450000","areaName":"广西壮族自治区"},{"code":"460000","areaName":"海南省"},{"code":"500000","areaName":"重庆市"},{"code":"510000","areaName":"四川省"},{"code":"520000","areaName":"贵州省"},{"code":"530000","areaName":"云南省"},{"code":"540000","areaName":"西藏自治区"},{"code":"610000","areaName":"陕西省"},{"code":"620000","areaName":"甘肃省"},{"code":"630000","areaName":"青海省"},{"code":"640000","areaName":"宁夏回族自治区"},{"code":"650000","areaName":"新疆维吾尔自治区"},{"code":"710000","areaName":"台湾"},{"code":"810000","areaName":"香港特别行政区"},{"code":"820000","areaName":"澳门特别行政区"}]
     * error : false
     * success : true
     */

    private int code;
    private String message;
    private boolean error;
    private boolean success;
    /**
     * code : 110100
     * areaName : 北京市
     */

    private List<DataBean> data;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        private String code;
        private String areaName;


        private int id;
        private int userId;
        private String name;
        private String areaCode;
        private int zipCode;
        private String address;
        private String phone;
        private int status;
        private long gmtCreate;
        private long gmtModify;
        private String codeAddress;
        private int shopId;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getAreaName() {
            return areaName;
        }

        public void setAreaName(String areaName) {
            this.areaName = areaName;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getUserId() {
            return userId;
        }

        public void setUserId(int userId) {
            this.userId = userId;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getAreaCode() {
            return areaCode;
        }

        public void setAreaCode(String areaCode) {
            this.areaCode = areaCode;
        }

        public int getZipCode() {
            return zipCode;
        }

        public void setZipCode(int zipCode) {
            this.zipCode = zipCode;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public long getGmtCreate() {
            return gmtCreate;
        }

        public void setGmtCreate(long gmtCreate) {
            this.gmtCreate = gmtCreate;
        }

        public long getGmtModify() {
            return gmtModify;
        }

        public void setGmtModify(long gmtModify) {
            this.gmtModify = gmtModify;
        }

        public String getCodeAddress() {
            return codeAddress;
        }

        public void setCodeAddress(String codeAddress) {
            this.codeAddress = codeAddress;
        }

        public int getShopId() {
            return shopId;
        }

        public void setShopId(int shopId) {
            this.shopId = shopId;
        }
    }
}
