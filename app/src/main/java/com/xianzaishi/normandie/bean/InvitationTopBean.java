package com.xianzaishi.normandie.bean;

/**
 * Created by Administrator on 2016/12/23.
 */

public class InvitationTopBean {

    /**
     * titleprice : 10
     * subtitle : 现金券
     * des_1 : {"titleprice":"10","subtitle":"现金券","content":"邀请友成功即可得10元无门槛>现金券，全场通用。多邀多得！","imgurl":"http:xianzaishi/2016/12/23.png","font":"9-3"}
     * des_2 : {"titleprice":"100","subtitle":"抵用券","content":"您的好友花1分钱即可获得无公害草莓一盒，同时在加100元购物大礼包！","imgurl":"http://xianzaishi/2016/12/23.png","font":"9-3"}
     * weixin_des : {"title":"鲜在时邀请券","content":"鲜在时邀请您参加体验,下载即送优惠券","imgurl":"http://xianzaishi/2016/12/23.png","targeturl":"http://m.xianzaishi.net/weixin/index.html"}
     */

    private String titleprice;
    private String subtitle;
    /**
     * titleprice : 10
     * subtitle : 现金券
     * content : 邀请友成功即可得10元无门槛>现金券，全场通用。多邀多得！
     * imgurl : http:xianzaishi/2016/12/23.png
     * font : 9-3
     */

    private Des1Bean des_1;
    /**
     * titleprice : 100
     * subtitle : 抵用券
     * content : 您的好友花1分钱即可获得无公害草莓一盒，同时在加100元购物大礼包！
     * imgurl : http://xianzaishi/2016/12/23.png
     * font : 9-3
     */

    private Des2Bean des_2;
    /**
     * title : 鲜在时邀请券
     * content : 鲜在时邀请您参加体验,下载即送优惠券
     * imgurl : http://xianzaishi/2016/12/23.png
     * targeturl : http://m.xianzaishi.net/weixin/index.html
     */

    private WeixinDesBean weixin_des;

    public String getTitleprice() {
        return titleprice;
    }

    public void setTitleprice(String titleprice) {
        this.titleprice = titleprice;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public Des1Bean getDes_1() {
        return des_1;
    }

    public void setDes_1(Des1Bean des_1) {
        this.des_1 = des_1;
    }

    public Des2Bean getDes_2() {
        return des_2;
    }

    public void setDes_2(Des2Bean des_2) {
        this.des_2 = des_2;
    }

    public WeixinDesBean getWeixin_des() {
        return weixin_des;
    }

    public void setWeixin_des(WeixinDesBean weixin_des) {
        this.weixin_des = weixin_des;
    }

    public static class Des1Bean {
        private String titleprice;
        private String subtitle;
        private String content;
        private String imgurl;
        private String font;

        public String getTitleprice() {
            return titleprice;
        }

        public void setTitleprice(String titleprice) {
            this.titleprice = titleprice;
        }

        public String getSubtitle() {
            return subtitle;
        }

        public void setSubtitle(String subtitle) {
            this.subtitle = subtitle;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public String getImgurl() {
            return imgurl;
        }

        public void setImgurl(String imgurl) {
            this.imgurl = imgurl;
        }

        public String getFont() {
            return font;
        }

        public void setFont(String font) {
            this.font = font;
        }
    }

    public static class Des2Bean {
        private String titleprice;
        private String subtitle;
        private String content;
        private String imgurl;
        private String font;

        public String getTitleprice() {
            return titleprice;
        }

        public void setTitleprice(String titleprice) {
            this.titleprice = titleprice;
        }

        public String getSubtitle() {
            return subtitle;
        }

        public void setSubtitle(String subtitle) {
            this.subtitle = subtitle;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public String getImgurl() {
            return imgurl;
        }

        public void setImgurl(String imgurl) {
            this.imgurl = imgurl;
        }

        public String getFont() {
            return font;
        }

        public void setFont(String font) {
            this.font = font;
        }
    }

    public static class WeixinDesBean {
        private String title;
        private String content;
        private String imgurl;
        private String targeturl;

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public String getImgurl() {
            return imgurl;
        }

        public void setImgurl(String imgurl) {
            this.imgurl = imgurl;
        }

        public String getTargeturl() {
            return targeturl;
        }

        public void setTargeturl(String targeturl) {
            this.targeturl = targeturl;
        }
    }
}
