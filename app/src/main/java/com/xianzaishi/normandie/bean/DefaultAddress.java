package com.xianzaishi.normandie.bean;

/**
 * Created by Administrator on 2016/9/29.
 */

public class DefaultAddress {

    /**
     * code : 1
     * message : 成功
     * data : {"id":91,"name":"海灵","phone":"18561530143","code":"310115008","level1Name":"上海市","level2Name":"上海市","level3Name":"浦东新区","level4Name":"塘桥街道","address":"1370号","defaultAddress":true}
     * success : true
     * error : false
     */

    private int code;
    private String message;
    /**
     * id : 91
     * name : 海灵
     * phone : 18561530143
     * code : 310115008
     * level1Name : 上海市
     * level2Name : 上海市
     * level3Name : 浦东新区
     * level4Name : 塘桥街道
     * address : 1370号
     * defaultAddress : true
     */

    private DataBean data;
    private boolean success;
    private boolean error;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public static class DataBean {
        private int id;
        private String name;
        private String phone;
        private String code;
        private String level1Name;
        private String level2Name;
        private String level3Name;
        private String level4Name;
        private String address;
        private boolean defaultAddress;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getLevel1Name() {
            return level1Name;
        }

        public void setLevel1Name(String level1Name) {
            this.level1Name = level1Name;
        }

        public String getLevel2Name() {
            return level2Name;
        }

        public void setLevel2Name(String level2Name) {
            this.level2Name = level2Name;
        }

        public String getLevel3Name() {
            return level3Name;
        }

        public void setLevel3Name(String level3Name) {
            this.level3Name = level3Name;
        }

        public String getLevel4Name() {
            return level4Name;
        }

        public void setLevel4Name(String level4Name) {
            this.level4Name = level4Name;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public boolean isDefaultAddress() {
            return defaultAddress;
        }

        public void setDefaultAddress(boolean defaultAddress) {
            this.defaultAddress = defaultAddress;
        }
    }
}
