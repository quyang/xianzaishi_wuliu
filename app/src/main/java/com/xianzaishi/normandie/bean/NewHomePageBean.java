package com.xianzaishi.normandie.bean;

import android.content.Context;

import java.io.Serializable;
import java.util.List;

/**
 * Created by ShenLang on 2016/12/24.
 * 首页数据模型
 */

public class NewHomePageBean {

    /**
     * resultCode : 1
     * success : true
     * errorMsg :
     * module : [{"pageName":"新首页2版本","stepId":134,"picList":[{"picUrl":"http://img.xianzaishi.com/1/1480430247170.jpeg","targetId":"0","targetType":0}],"title":"","items":[{"itemId":10202461,"picUrl":"http://img.xianzaishi.com/2/1478340610679.jpg","title":"光明优倍高品质鲜奶950ml","subtitle":"光明鲜奶","price":1780,"discountPrice":1780,"priceYuanString":"17.80","discountPriceYuanString":"17.80","sku":"10529","feature":null,"inventory":1,"itemSkuVOs":[{"skuId":10529,"itemId":10202461,"inventory":1,"skuUnit":26,"title":"光明优倍高品质鲜奶950ml","quantity":0,"promotionInfo":null,"price":1780,"discountPrice":1780,"priceYuanString":"17.80","discountPriceYuanString":"17.80","saleDetailInfo":"1盒","originplace":"上海","tags":null,"tagDetail":{"skuId":10529,"mayPlus":true,"mayOrder":true,"maySale":true,"specialTag":true,"channel":1,"tagContent":"热销"}}]}],"stepType":10,"catList":[{"picUrl":"1/shucaishuiguo.png","catId":107,"catName":"新鲜果蔬"}],"innerStepInfo":[{"pageName":"新首页2版本","stepId":137,"picList":null,"title":"2=4","items":[{"itemId":10204171,"picUrl":null,"title":"（限时抢）鲜在时生态荷兰黄瓜250g","subtitle":"","price":1399,"discountPrice":1399,"priceYuanString":"13.99","discountPriceYuanString":"13.99","sku":"11017","feature":null,"inventory":1,"itemSkuVOs":[{"skuId":11017,"itemId":10204171,"inventory":0,"skuUnit":26,"title":"（限时抢）鲜在时生态荷兰黄瓜250g","quantity":0,"promotionInfo":null,"price":1399,"discountPrice":1399,"priceYuanString":"13.99","discountPriceYuanString":"13.99","saleDetailInfo":"1盒","originplace":"中国","tags":"1000","tagDetail":{"skuId":11017,"mayPlus":true,"mayOrder":true,"maySale":true,"specialTag":true,"channel":1,"tagContent":"店长推荐"}}]}],"stepType":11,"catList":null,"innerStepInfo":null}]}]
     * currentTime : 1482550746330
     * succcess : true
     */

    private int resultCode;
    private boolean success;
    private String errorMsg;
    private long currentTime;
    private String color;
    private boolean succcess;
    /**
     * pageName : 新首页2版本
     * stepId : 134
     * picList : [{"picUrl":"http://img.xianzaishi.com/1/1480430247170.jpeg","targetId":"0","targetType":0}]
     * title :
     * items : [{"itemId":10202461,"picUrl":"http://img.xianzaishi.com/2/1478340610679.jpg","title":"光明优倍高品质鲜奶950ml","subtitle":"光明鲜奶","price":1780,"discountPrice":1780,"priceYuanString":"17.80","discountPriceYuanString":"17.80","sku":"10529","feature":null,"inventory":1,"itemSkuVOs":[{"skuId":10529,"itemId":10202461,"inventory":1,"skuUnit":26,"title":"光明优倍高品质鲜奶950ml","quantity":0,"promotionInfo":null,"price":1780,"discountPrice":1780,"priceYuanString":"17.80","discountPriceYuanString":"17.80","saleDetailInfo":"1盒","originplace":"上海","tags":null,"tagDetail":{"skuId":10529,"mayPlus":true,"mayOrder":true,"maySale":true,"specialTag":true,"channel":1,"tagContent":"热销"}}]}]
     * stepType : 10
     * catList : [{"picUrl":"1/shucaishuiguo.png","catId":107,"catName":"新鲜果蔬"}]
     * innerStepInfo : [{"pageName":"新首页2版本","stepId":137,"picList":null,"title":"2=4","items":[{"itemId":10204171,"picUrl":null,"title":"（限时抢）鲜在时生态荷兰黄瓜250g","subtitle":"","price":1399,"discountPrice":1399,"priceYuanString":"13.99","discountPriceYuanString":"13.99","sku":"11017","feature":null,"inventory":1,"itemSkuVOs":[{"skuId":11017,"itemId":10204171,"inventory":0,"skuUnit":26,"title":"（限时抢）鲜在时生态荷兰黄瓜250g","quantity":0,"promotionInfo":null,"price":1399,"discountPrice":1399,"priceYuanString":"13.99","discountPriceYuanString":"13.99","saleDetailInfo":"1盒","originplace":"中国","tags":"1000","tagDetail":{"skuId":11017,"mayPlus":true,"mayOrder":true,"maySale":true,"specialTag":true,"channel":1,"tagContent":"店长推荐"}}]}],"stepType":11,"catList":null,"innerStepInfo":null}]
     */

    private List<ModuleBean> module;

    public int getResultCode() {
        return resultCode;
    }

    public void setResultCode(int resultCode) {
        this.resultCode = resultCode;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public String getColor() {
        return color;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public long getCurrentTime() {
        return currentTime;
    }

    public void setCurrentTime(long currentTime) {
        this.currentTime = currentTime;
    }

    public boolean isSucccess() {
        return succcess;
    }

    public void setSucccess(boolean succcess) {
        this.succcess = succcess;
    }

    public List<ModuleBean> getModule() {
        return module;
    }

    public void setModule(List<ModuleBean> module) {
        this.module = module;
    }

    public static class ModuleBean implements Serializable{
        private String pageName;
        private int stepId;
        private String title;
        private int stepType;
        private long begin;
        private long end;

        public long getBegin() {
            return begin;
        }

        public long getEnd() {
            return end;
        }

        /**
         * picUrl : http://img.xianzaishi.com/1/1480430247170.jpeg
         * targetId : 0
         * targetType : 0
         */

        private List<PicListBean> picList;
        /**
         * itemId : 10202461
         * picUrl : http://img.xianzaishi.com/2/1478340610679.jpg
         * title : 光明优倍高品质鲜奶950ml
         * subtitle : 光明鲜奶
         * price : 1780
         * discountPrice : 1780
         * priceYuanString : 17.80
         * discountPriceYuanString : 17.80
         * sku : 10529
         * feature : null
         * inventory : 1
         * itemSkuVOs : [{"skuId":10529,"itemId":10202461,"inventory":1,"skuUnit":26,"title":"光明优倍高品质鲜奶950ml","quantity":0,"promotionInfo":null,"price":1780,"discountPrice":1780,"priceYuanString":"17.80","discountPriceYuanString":"17.80","saleDetailInfo":"1盒","originplace":"上海","tags":null,"tagDetail":{"skuId":10529,"mayPlus":true,"mayOrder":true,"maySale":true,"specialTag":true,"channel":1,"tagContent":"热销"}}]
         */

        private List<ItemsBean> items;
        /**
         * picUrl : 1/shucaishuiguo.png
         * catId : 107
         * catName : 新鲜果蔬
         */

        private List<CatListBean> catList;
        /**
         * pageName : 新首页2版本
         * stepId : 137
         * picList : null
         * title : 2=4
         * items : [{"itemId":10204171,"picUrl":null,"title":"（限时抢）鲜在时生态荷兰黄瓜250g","subtitle":"","price":1399,"discountPrice":1399,"priceYuanString":"13.99","discountPriceYuanString":"13.99","sku":"11017","feature":null,"inventory":1,"itemSkuVOs":[{"skuId":11017,"itemId":10204171,"inventory":0,"skuUnit":26,"title":"（限时抢）鲜在时生态荷兰黄瓜250g","quantity":0,"promotionInfo":null,"price":1399,"discountPrice":1399,"priceYuanString":"13.99","discountPriceYuanString":"13.99","saleDetailInfo":"1盒","originplace":"中国","tags":"1000","tagDetail":{"skuId":11017,"mayPlus":true,"mayOrder":true,"maySale":true,"specialTag":true,"channel":1,"tagContent":"店长推荐"}}]}]
         * stepType : 11
         * catList : null
         * innerStepInfo : null
         */
        private List<ContentListBean> contentList;//快报

        public List<ContentListBean> getContentList() {
            return contentList;
        }

        private List<InnerStepInfoBean> innerStepInfo;

        public String getPageName() {
            return pageName;
        }

        public void setPageName(String pageName) {
            this.pageName = pageName;
        }

        public int getStepId() {
            return stepId;
        }

        public void setStepId(int stepId) {
            this.stepId = stepId;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public int getStepType() {
            return stepType;
        }

        public void setStepType(int stepType) {
            this.stepType = stepType;
        }

        public List<PicListBean> getPicList() {
            return picList;
        }

        public void setPicList(List<PicListBean> picList) {
            this.picList = picList;
        }

        public List<ItemsBean> getItems() {
            return items;
        }

        public void setItems(List<ItemsBean> items) {
            this.items = items;
        }

        public List<CatListBean> getCatList() {
            return catList;
        }

        public void setCatList(List<CatListBean> catList) {
            this.catList = catList;
        }

        public List<InnerStepInfoBean> getInnerStepInfo() {
            return innerStepInfo;
        }

        public void setInnerStepInfo(List<InnerStepInfoBean> innerStepInfo) {
            this.innerStepInfo = innerStepInfo;
        }
        public static class ContentListBean implements Serializable{
            private String content;
            private String targetId;
            private int targetType;
            private String titleInfo;

            public String getTitleInfo() {
                return titleInfo;
            }

            public String getContent() {
                return content;
            }

            public String getTargetId() {
                return targetId;
            }

            public int getTargetType() {
                return targetType;
            }
        }
        public static class PicListBean implements Serializable{
            private String picUrl;
            private String targetId;
            private int targetType;
            private String titleInfo;

            public String getTitleInfo() {
                return titleInfo;
            }
            public String getPicUrl() {
                return picUrl;
            }

            public void setPicUrl(String picUrl) {
                this.picUrl = picUrl;
            }

            public String getTargetId() {
                return targetId;
            }

            public void setTargetId(String targetId) {
                this.targetId = targetId;
            }

            public int getTargetType() {
                return targetType;
            }

            public void setTargetType(int targetType) {
                this.targetType = targetType;
            }
        }

        public static class ItemsBean implements Serializable{
            private int itemId;
            private String picUrl;
            private String title;
            private String subtitle;
            private int price;
            private int discountPrice;
            private String priceYuanString;
            private String discountPriceYuanString;
            private String sku;
            private Object feature;
            private int inventory;
            /**
             * skuId : 10529
             * itemId : 10202461
             * inventory : 1
             * skuUnit : 26
             * title : 光明优倍高品质鲜奶950ml
             * quantity : 0
             * promotionInfo : null
             * price : 1780
             * discountPrice : 1780
             * priceYuanString : 17.80
             * discountPriceYuanString : 17.80
             * saleDetailInfo : 1盒
             * originplace : 上海
             * tags : null
             * tagDetail : {"skuId":10529,"mayPlus":true,"mayOrder":true,"maySale":true,"specialTag":true,"channel":1,"tagContent":"热销"}
             */

            private List<ItemSkuVOsBean> itemSkuVOs;

            public int getItemId() {
                return itemId;
            }

            public void setItemId(int itemId) {
                this.itemId = itemId;
            }

            public String getPicUrl() {
                return picUrl;
            }

            public void setPicUrl(String picUrl) {
                this.picUrl = picUrl;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getSubtitle() {
                return subtitle;
            }

            public void setSubtitle(String subtitle) {
                this.subtitle = subtitle;
            }

            public int getPrice() {
                return price;
            }

            public void setPrice(int price) {
                this.price = price;
            }

            public int getDiscountPrice() {
                return discountPrice;
            }

            public void setDiscountPrice(int discountPrice) {
                this.discountPrice = discountPrice;
            }

            public String getPriceYuanString() {
                return priceYuanString;
            }

            public void setPriceYuanString(String priceYuanString) {
                this.priceYuanString = priceYuanString;
            }

            public String getDiscountPriceYuanString() {
                return discountPriceYuanString;
            }

            public void setDiscountPriceYuanString(String discountPriceYuanString) {
                this.discountPriceYuanString = discountPriceYuanString;
            }

            public String getSku() {
                return sku;
            }

            public void setSku(String sku) {
                this.sku = sku;
            }

            public Object getFeature() {
                return feature;
            }

            public void setFeature(Object feature) {
                this.feature = feature;
            }

            public int getInventory() {
                return inventory;
            }

            public void setInventory(int inventory) {
                this.inventory = inventory;
            }

            public List<ItemSkuVOsBean> getItemSkuVOs() {
                return itemSkuVOs;
            }

            public void setItemSkuVOs(List<ItemSkuVOsBean> itemSkuVOs) {
                this.itemSkuVOs = itemSkuVOs;
            }

            public static class ItemSkuVOsBean implements Serializable{
                private int skuId;
                private int itemId;
                private int inventory;
                private int skuUnit;
                private String title;
                private int quantity;
                private Object promotionInfo;
                private int price;
                private int discountPrice;
                private String priceYuanString;
                private String discountPriceYuanString;
                private String saleDetailInfo;
                private String originplace;
                private String tags;
                /**
                 * skuId : 10529
                 * mayPlus : true
                 * mayOrder : true
                 * maySale : true
                 * specialTag : true
                 * channel : 1
                 * tagContent : 热销
                 */

                private TagDetailBean tagDetail;

                public int getSkuId() {
                    return skuId;
                }

                public void setSkuId(int skuId) {
                    this.skuId = skuId;
                }

                public int getItemId() {
                    return itemId;
                }

                public void setItemId(int itemId) {
                    this.itemId = itemId;
                }

                public int getInventory() {
                    return inventory;
                }

                public void setInventory(int inventory) {
                    this.inventory = inventory;
                }

                public int getSkuUnit() {
                    return skuUnit;
                }

                public void setSkuUnit(int skuUnit) {
                    this.skuUnit = skuUnit;
                }

                public String getTitle() {
                    return title;
                }

                public void setTitle(String title) {
                    this.title = title;
                }

                public int getQuantity() {
                    return quantity;
                }

                public void setQuantity(int quantity) {
                    this.quantity = quantity;
                }

                public Object getPromotionInfo() {
                    return promotionInfo;
                }

                public void setPromotionInfo(Object promotionInfo) {
                    this.promotionInfo = promotionInfo;
                }

                public int getPrice() {
                    return price;
                }

                public void setPrice(int price) {
                    this.price = price;
                }

                public int getDiscountPrice() {
                    return discountPrice;
                }

                public void setDiscountPrice(int discountPrice) {
                    this.discountPrice = discountPrice;
                }

                public String getPriceYuanString() {
                    return priceYuanString;
                }

                public void setPriceYuanString(String priceYuanString) {
                    this.priceYuanString = priceYuanString;
                }

                public String getDiscountPriceYuanString() {
                    return discountPriceYuanString;
                }

                public void setDiscountPriceYuanString(String discountPriceYuanString) {
                    this.discountPriceYuanString = discountPriceYuanString;
                }

                public String getSaleDetailInfo() {
                    return saleDetailInfo;
                }

                public void setSaleDetailInfo(String saleDetailInfo) {
                    this.saleDetailInfo = saleDetailInfo;
                }

                public String getOriginplace() {
                    return originplace;
                }

                public void setOriginplace(String originplace) {
                    this.originplace = originplace;
                }

                public String getTags() {
                    return tags;
                }

                public void setTags(String tags) {
                    this.tags = tags;
                }

                public TagDetailBean getTagDetail() {
                    return tagDetail;
                }

                public void setTagDetail(TagDetailBean tagDetail) {
                    this.tagDetail = tagDetail;
                }

                public static class TagDetailBean implements Serializable{
                    private int skuId;
                    private boolean mayPlus;
                    private boolean mayOrder;
                    private boolean maySale;
                    private boolean specialTag;
                    private String channel;
                    private String tagContent;

                    public int getSkuId() {
                        return skuId;
                    }

                    public void setSkuId(int skuId) {
                        this.skuId = skuId;
                    }

                    public boolean isMayPlus() {
                        return mayPlus;
                    }

                    public void setMayPlus(boolean mayPlus) {
                        this.mayPlus = mayPlus;
                    }

                    public boolean isMayOrder() {
                        return mayOrder;
                    }

                    public void setMayOrder(boolean mayOrder) {
                        this.mayOrder = mayOrder;
                    }

                    public boolean isMaySale() {
                        return maySale;
                    }

                    public void setMaySale(boolean maySale) {
                        this.maySale = maySale;
                    }

                    public boolean isSpecialTag() {
                        return specialTag;
                    }

                    public void setSpecialTag(boolean specialTag) {
                        this.specialTag = specialTag;
                    }

                    public String getChannel() {
                        return channel;
                    }

                    public void setChannel(String channel) {
                        this.channel = channel;
                    }

                    public String getTagContent() {
                        return tagContent;
                    }

                    public void setTagContent(String tagContent) {
                        this.tagContent = tagContent;
                    }
                }
            }
        }

        public static class CatListBean implements Serializable{
            private String picUrl;
            private int catId;
            private String catName;

            public String getPicUrl() {
                return picUrl;
            }

            public void setPicUrl(String picUrl) {
                this.picUrl = picUrl;
            }

            public int getCatId() {
                return catId;
            }

            public void setCatId(int catId) {
                this.catId = catId;
            }

            public String getCatName() {
                return catName;
            }

            public void setCatName(String catName) {
                this.catName = catName;
            }
        }

        public static class InnerStepInfoBean implements Serializable{
            private String pageName;
            private int stepId;
            private Object picList;
            private String title;
            private int stepType;
            private Object catList;
            private Object innerStepInfo;

            private boolean checked;

            public boolean isChecked() {
                return checked;
            }

            public void setChecked(boolean checked) {
                this.checked = checked;
            }

            /**
             * itemId : 10204171
             * picUrl : null
             * title : （限时抢）鲜在时生态荷兰黄瓜250g
             * subtitle :
             * price : 1399
             * discountPrice : 1399
             * priceYuanString : 13.99
             * discountPriceYuanString : 13.99
             * sku : 11017
             * feature : null
             * inventory : 1
             * itemSkuVOs : [{"skuId":11017,"itemId":10204171,"inventory":0,"skuUnit":26,"title":"（限时抢）鲜在时生态荷兰黄瓜250g","quantity":0,"promotionInfo":null,"price":1399,"discountPrice":1399,"priceYuanString":"13.99","discountPriceYuanString":"13.99","saleDetailInfo":"1盒","originplace":"中国","tags":"1000","tagDetail":{"skuId":11017,"mayPlus":true,"mayOrder":true,"maySale":true,"specialTag":true,"channel":1,"tagContent":"店长推荐"}}]
             */

            private List<ItemsBean> items;

            public String getPageName() {
                return pageName;
            }

            public void setPageName(String pageName) {
                this.pageName = pageName;
            }

            public int getStepId() {
                return stepId;
            }

            public void setStepId(int stepId) {
                this.stepId = stepId;
            }

            public Object getPicList() {
                return picList;
            }

            public void setPicList(Object picList) {
                this.picList = picList;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public int getStepType() {
                return stepType;
            }

            public void setStepType(int stepType) {
                this.stepType = stepType;
            }

            public Object getCatList() {
                return catList;
            }

            public void setCatList(Object catList) {
                this.catList = catList;
            }

            public Object getInnerStepInfo() {
                return innerStepInfo;
            }

            public void setInnerStepInfo(Object innerStepInfo) {
                this.innerStepInfo = innerStepInfo;
            }

            public List<ItemsBean> getItems() {
                return items;
            }

            public void setItems(List<ItemsBean> items) {
                this.items = items;
            }

            public static class ItemsBean implements Serializable{
                private int itemId;
                private String picUrl;
                private String title;
                private String subtitle;
                private int price;
                private int discountPrice;
                private String priceYuanString;
                private String discountPriceYuanString;
                private String sku;
                private Object feature;
                private int inventory;
                /**
                 * skuId : 11017
                 * itemId : 10204171
                 * inventory : 0
                 * skuUnit : 26
                 * title : （限时抢）鲜在时生态荷兰黄瓜250g
                 * quantity : 0
                 * promotionInfo : null
                 * price : 1399
                 * discountPrice : 1399
                 * priceYuanString : 13.99
                 * discountPriceYuanString : 13.99
                 * saleDetailInfo : 1盒
                 * originplace : 中国
                 * tags : 1000
                 * tagDetail : {"skuId":11017,"mayPlus":true,"mayOrder":true,"maySale":true,"specialTag":true,"channel":1,"tagContent":"店长推荐"}
                 */

                private List<ItemSkuVOsBean> itemSkuVOs;

                public int getItemId() {
                    return itemId;
                }

                public void setItemId(int itemId) {
                    this.itemId = itemId;
                }

                public String getPicUrl() {
                    return picUrl;
                }

                public void setPicUrl(String picUrl) {
                    this.picUrl = picUrl;
                }

                public String getTitle() {
                    return title;
                }

                public void setTitle(String title) {
                    this.title = title;
                }

                public String getSubtitle() {
                    return subtitle;
                }

                public void setSubtitle(String subtitle) {
                    this.subtitle = subtitle;
                }

                public int getPrice() {
                    return price;
                }

                public void setPrice(int price) {
                    this.price = price;
                }

                public int getDiscountPrice() {
                    return discountPrice;
                }

                public void setDiscountPrice(int discountPrice) {
                    this.discountPrice = discountPrice;
                }

                public String getPriceYuanString() {
                    return priceYuanString;
                }

                public void setPriceYuanString(String priceYuanString) {
                    this.priceYuanString = priceYuanString;
                }

                public String getDiscountPriceYuanString() {
                    return discountPriceYuanString;
                }

                public void setDiscountPriceYuanString(String discountPriceYuanString) {
                    this.discountPriceYuanString = discountPriceYuanString;
                }

                public String getSku() {
                    return sku;
                }

                public void setSku(String sku) {
                    this.sku = sku;
                }

                public Object getFeature() {
                    return feature;
                }

                public void setFeature(Object feature) {
                    this.feature = feature;
                }

                public int getInventory() {
                    return inventory;
                }

                public void setInventory(int inventory) {
                    this.inventory = inventory;
                }

                public List<ItemSkuVOsBean> getItemSkuVOs() {
                    return itemSkuVOs;
                }

                public void setItemSkuVOs(List<ItemSkuVOsBean> itemSkuVOs) {
                    this.itemSkuVOs = itemSkuVOs;
                }

                public static class ItemSkuVOsBean implements Serializable{
                    private int skuId;
                    private int itemId;
                    private int inventory;
                    private int skuUnit;
                    private String title;
                    private int quantity;
                    private Object promotionInfo;
                    private int price;
                    private int discountPrice;
                    private String priceYuanString;
                    private String discountPriceYuanString;
                    private String saleDetailInfo;
                    private String originplace;
                    private String tags;
                    /**
                     * skuId : 11017
                     * mayPlus : true
                     * mayOrder : true
                     * maySale : true
                     * specialTag : true
                     * channel : 1
                     * tagContent : 店长推荐
                     */

                    private TagDetailBean tagDetail;

                    public int getSkuId() {
                        return skuId;
                    }

                    public void setSkuId(int skuId) {
                        this.skuId = skuId;
                    }

                    public int getItemId() {
                        return itemId;
                    }

                    public void setItemId(int itemId) {
                        this.itemId = itemId;
                    }

                    public int getInventory() {
                        return inventory;
                    }

                    public void setInventory(int inventory) {
                        this.inventory = inventory;
                    }

                    public int getSkuUnit() {
                        return skuUnit;
                    }

                    public void setSkuUnit(int skuUnit) {
                        this.skuUnit = skuUnit;
                    }

                    public String getTitle() {
                        return title;
                    }

                    public void setTitle(String title) {
                        this.title = title;
                    }

                    public int getQuantity() {
                        return quantity;
                    }

                    public void setQuantity(int quantity) {
                        this.quantity = quantity;
                    }

                    public Object getPromotionInfo() {
                        return promotionInfo;
                    }

                    public void setPromotionInfo(Object promotionInfo) {
                        this.promotionInfo = promotionInfo;
                    }

                    public int getPrice() {
                        return price;
                    }

                    public void setPrice(int price) {
                        this.price = price;
                    }

                    public int getDiscountPrice() {
                        return discountPrice;
                    }

                    public void setDiscountPrice(int discountPrice) {
                        this.discountPrice = discountPrice;
                    }

                    public String getPriceYuanString() {
                        return priceYuanString;
                    }

                    public void setPriceYuanString(String priceYuanString) {
                        this.priceYuanString = priceYuanString;
                    }

                    public String getDiscountPriceYuanString() {
                        return discountPriceYuanString;
                    }

                    public void setDiscountPriceYuanString(String discountPriceYuanString) {
                        this.discountPriceYuanString = discountPriceYuanString;
                    }

                    public String getSaleDetailInfo() {
                        return saleDetailInfo;
                    }

                    public void setSaleDetailInfo(String saleDetailInfo) {
                        this.saleDetailInfo = saleDetailInfo;
                    }

                    public String getOriginplace() {
                        return originplace;
                    }

                    public void setOriginplace(String originplace) {
                        this.originplace = originplace;
                    }

                    public String getTags() {
                        return tags;
                    }

                    public void setTags(String tags) {
                        this.tags = tags;
                    }

                    public TagDetailBean getTagDetail() {
                        return tagDetail;
                    }

                    public void setTagDetail(TagDetailBean tagDetail) {
                        this.tagDetail = tagDetail;
                    }

                    public static class TagDetailBean implements Serializable{
                        private int skuId;
                        private boolean mayPlus;
                        private boolean mayOrder;
                        private boolean maySale;
                        private boolean specialTag;
                        private String channel;
                        private String tagContent;

                        public int getSkuId() {
                            return skuId;
                        }

                        public void setSkuId(int skuId) {
                            this.skuId = skuId;
                        }

                        public boolean isMayPlus() {
                            return mayPlus;
                        }

                        public void setMayPlus(boolean mayPlus) {
                            this.mayPlus = mayPlus;
                        }

                        public boolean isMayOrder() {
                            return mayOrder;
                        }

                        public void setMayOrder(boolean mayOrder) {
                            this.mayOrder = mayOrder;
                        }

                        public boolean isMaySale() {
                            return maySale;
                        }

                        public void setMaySale(boolean maySale) {
                            this.maySale = maySale;
                        }

                        public boolean isSpecialTag() {
                            return specialTag;
                        }

                        public void setSpecialTag(boolean specialTag) {
                            this.specialTag = specialTag;
                        }

                        public String getChannel() {
                            return channel;
                        }

                        public void setChannel(String channel) {
                            this.channel = channel;
                        }

                        public String getTagContent() {
                            return tagContent;
                        }

                        public void setTagContent(String tagContent) {
                            this.tagContent = tagContent;
                        }
                    }
                }
            }
        }
    }
}
