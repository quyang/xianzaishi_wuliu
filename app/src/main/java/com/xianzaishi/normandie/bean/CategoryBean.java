package com.xianzaishi.normandie.bean;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Administrator on 2016/8/30.
 */
public class CategoryBean {

    /**
     * resultCode : 1
     * succcess : true
     * errorMsg :
     * module : [{"catId":1000,"name":"蔬菜水果","memo":"新鲜时令","pic":"http://139.196.92.240/itemcenter/image/0.jpg"},{"catId":1001,"name":"肉类禽蛋","memo":"野生放养","pic":"http://139.196.92.240/itemcenter/image/1.jpg"},{"catId":1002,"name":"海鲜水产","memo":"新鲜味美","pic":"http://139.196.92.240/itemcenter/image/2.jpg"},{"catId":1003,"name":"冷冻冷藏","memo":"保质保鲜","pic":"http://139.196.92.240/itemcenter/image/3.jpg"},{"catId":1004,"name":"乳制品","memo":"纯真奶源","pic":"http://139.196.92.240/itemcenter/image/4.jpg"},{"catId":1005,"name":"食品饮料","memo":"酒水饮料","pic":"http://139.196.92.240/itemcenter/image/5.jpg"},{"catId":1006,"name":"粮油副食","memo":"饮食必备","pic":"http://139.196.92.240/itemcenter/image/6.jpg"},{"catId":1007,"name":"日用百货","memo":"生活必备","pic":"http://139.196.92.240/itemcenter/image/7.jpg"},{"catId":1008,"name":"舒适料理","memo":"三文鱼/料理","pic":"http://139.196.92.240/itemcenter/image/8.jpg"},{"catId":1009,"name":"精致烘焙","memo":"糕点/蛋糕","pic":"http://139.196.92.240/itemcenter/image/9.jpg"}]
     */

    private int resultCode;
    private boolean succcess;
    private String errorMsg;
    /**
     * catId : 1000
     * name : 蔬菜水果
     * memo : 新鲜时令
     * pic : http://139.196.92.240/itemcenter/image/0.jpg
     */

    private List<ModuleBean> module;

    public int getResultCode() {
        return resultCode;
    }

    public void setResultCode(int resultCode) {
        this.resultCode = resultCode;
    }

    public boolean isSucccess() {
        return succcess;
    }

    public void setSucccess(boolean succcess) {
        this.succcess = succcess;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public List<ModuleBean> getModule() {
        return module;
    }

    public void setModule(List<ModuleBean> module) {
        this.module = module;
    }

    public static class ModuleBean implements Serializable {
        private int catId;
        private String name;
        private String memo;
        private String pic;

        public int getCatId() {
            return catId;
        }

        public void setCatId(int catId) {
            this.catId = catId;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getMemo() {
            return memo;
        }

        public void setMemo(String memo) {
            this.memo = memo;
        }

        public String getPic() {
            return pic;
        }

        public void setPic(String pic) {
            this.pic = pic;
        }
    }
}
