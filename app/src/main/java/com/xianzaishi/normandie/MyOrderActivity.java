package com.xianzaishi.normandie;

import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.google.gson.Gson;
import com.umeng.analytics.MobclickAgent;
import com.xianzaishi.normandie.bean.OrdersListBean01;
import com.xianzaishi.normandie.fragment.MineFragment;
import com.xianzaishi.normandie.global.Contants;
import com.xianzaishi.normandie.http.PostProtocol;
import com.xianzaishi.normandie.interfaces.OnDataFromPostServerListener;
import com.xianzaishi.normandie.utils.SPUtils;
import com.xianzaishi.normandie.utils.UiUtils;
import java.util.List;
import okhttp3.FormBody;
import okhttp3.RequestBody;

/**
 * quyang  我的订单
 * <p/>
 * 2016年8月19日20:30:27
 */
public class MyOrderActivity extends BaseActivity01 {

    private String[] stringArray;
    private OrdersListBean01 data;
    private OrdersListBean01.DataBean mDataBean;
    private OrdersListBean01.DataBean.PaginationBean mPagination;
    private OrdersListBean01.DataBean.ObjectsBean mObjects;
    private List<OrdersListBean01.DataBean.ObjectsBean> mObjectsBeanList;


    //加载布局
    public View createSuccessView() {

        stringArray = UiUtils.getStringArray(R.array.my_order_string_array);

        //填充布局
        View view = View.inflate(MyOrderActivity.this, R.layout.shopping_my_order, null);

        ImageView ivBack = (ImageView) view.findViewById(R.id.iv_left_icon);
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        TextView tvCenter = (TextView) view.findViewById(R.id.tv_center_text);
        final ViewPager viewpager = (ViewPager) view.findViewById(R.id.viewPager01);

        tvCenter.setText("我的订单");

        //设置适配器
        viewpager.setAdapter(new MyFragmentPagerAdapter(getSupportFragmentManager()));
        TabLayout tabLayout= (TabLayout) view.findViewById(R.id.my_order_tabLayout);
        tabLayout.setupWithViewPager(viewpager);

        //设置默认显示页
        viewpager.setCurrentItem(0);

        return view;
    }


    //加载数据,运行在子线程
    @Override
    public void loadDataFromNet() {

        String uid = SPUtils.getStringValue(this, Contants.SP_NAME, Contants.UID, null);
        if (uid == null) {
            Toast.makeText(MyOrderActivity.this, "您还未登录", Toast.LENGTH_LONG).show();
            return;
        }

        RequestBody formoody = new FormBody.Builder()
                .add("uid", uid)
                .add("pageSize", "20")
                .add("curPage", "1")
                .add("status", "0")
                .build();

        PostProtocol protocol = new PostProtocol();
        protocol.setUrl(Contants.ORDER_LIST);
        protocol.setRequestBody(formoody);
        protocol.getDataByPOST();
        protocol.setOnDataFromPostServerListener(new OnDataFromPostServerListener() {
            @Override
            public void onSuccess(String result) {
                //解析json
                parseJson(result);

            }

            @Override
            public void onFail() {
                mLoadingView.setVisibility(View.INVISIBLE);
                mErrorView.setVisibility(View.VISIBLE);
            }
        });
    }


    public void parseJson(String json) {
        Gson gson = new Gson();
        data = gson.fromJson(json, OrdersListBean01.class);
        if (data != null) {
            if (data.success) {
                //获取订单集合
                mDataBean = this.data.data;
                mPagination = mDataBean.pagination;
                mObjectsBeanList = mDataBean.objects;
                if (mObjectsBeanList.size() != 0) {
                    mLoadingView.setVisibility(View.INVISIBLE);
                    mErrorView.setVisibility(View.INVISIBLE);
                    mEmptyView.setVisibility(View.INVISIBLE);
                    //显示成功界面
                    View view = createSuccessView();
                    view.setVisibility(View.VISIBLE);
                    flContainer.addView(view);
                } else {
                    //显示空数据界面
                    mEmptyView.setVisibility(View.VISIBLE);
                    mErrorView.setVisibility(View.INVISIBLE);
                    mLoadingView.setVisibility(View.INVISIBLE);
                }
            } else {
                //显示空数据界面
                mErrorView.setVisibility(View.INVISIBLE);
                mLoadingView.setVisibility(View.INVISIBLE);
                mEmptyView.setVisibility(View.VISIBLE);
            }
        }
    }

    //viewpager的适配器
    private class MyFragmentPagerAdapter extends FragmentPagerAdapter {

        String[] stringArray;

        public MyFragmentPagerAdapter(FragmentManager fm) {
            super(fm);
            stringArray = UiUtils.getStringArray(R.array.my_order_string_array);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return stringArray[position];
        }

        @Override
        public Fragment getItem(int position) {
            //return MineFragment.FragmentFactory.getFragment(position);
            return null;
        }

        @Override
        public int getCount() {
            return stringArray.length;
        }
    }


    public void onResume() {
        super.onResume();
        MobclickAgent.onPageStart("SplashScreen");
        MobclickAgent.onResume(this);
    }

    public void onPause() {
        super.onPause();
        MobclickAgent.onPageEnd("SplashScreen");
        MobclickAgent.onPause(this);
    }


    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.from_left_in, R.anim.to_right_out);
    }

    @Override
    public void startActivity(Intent intent) {
        super.startActivity(intent);
        overridePendingTransition(R.anim.from_right_in, R.anim.to_left_out);
    }


    //获取所有订单页的数据
    public OrdersListBean01 getAllOrderInfo() {
        return data;
    }

}
