package com.xianzaishi.normandie;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.view.Window;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.umeng.analytics.MobclickAgent;
import com.xianzaishi.normandie.global.Contants;
import com.xianzaishi.normandie.utils.SPUtils;

import java.util.Timer;
import java.util.TimerTask;

/**
 * 闪屏页面
 */
public class SplashActivity extends CheckPermissionsActivity implements View.OnClickListener{
    private ImageView rootIv;
    private Button countBt;
    private String adPicUrl;
    private Timer timer;
    private CountDownTimer countDownTimer;
    private static final int TIME_OF_DURATION=5000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_splash);

        rootIv = (ImageView) findViewById(R.id.iv);
        countBt= (Button) findViewById(R.id.button_count);
        countBt.setOnClickListener(this);
        MobclickAgent.openActivityDurationTrack(false);//友盟禁止默认的页面统计方式

        MobclickAgent.enableEncrypt(true);

        adPicUrl=SPUtils.getStringValue(MyApplication.getContext(),Contants.SP_NAME,"adPicUrl","");

        initTimerTask();
        initAdPic();
    }

    /**
     *  计时开始的同时加载广告图
     */
    private void initAdPic() {
        if (adPicUrl.equals("")){
            return;
        }
        Glide.with(this)
                .load(adPicUrl)
                .listener(new RequestListener<String, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                        return false;
                    }
                    @Override
                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        //加载成功则终止计时
                        timer.cancel();
                        //显示出倒计时按钮 并开始倒计时
                        initTimeCountDown();
                        return false;
                    }
                })
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .error(R.mipmap.splash)
                .placeholder(R.mipmap.splash)
                .into(rootIv);
    }

    private void initTimeCountDown() {
        countBt.setVisibility(View.VISIBLE);
        countDownTimer=new CountDownTimer(TIME_OF_DURATION,1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                int seconds= (int) (millisUntilFinished/1000);
                countBt.setText("关闭 "+seconds);
            }

            @Override
            public void onFinish() {
                enterMainActivity();
            }
        };
        countDownTimer.start();
    }

    /**
     *  启动页计时功能  在计时时间内加载广告页  加载成功终止计时 展示广告页  失败则在计时
     *  结束时启动首页
     */
    private void initTimerTask() {
        timer=new Timer();
        TimerTask task=new TimerTask() {
            @Override
            public void run() {
                enterMainActivity();
            }
        };
        timer.schedule(task,TIME_OF_DURATION);
    }


    private void initAnimation(View view) {

        AlphaAnimation alphaAnimation = new AlphaAnimation(0.4f, 1.0f);
        alphaAnimation.setDuration(4000);
        alphaAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
//                //judget the version
//                if(NetUtils.hasNet(getApplicationContext())){
//                    requestNet();
//                }else{
//                    Toast.makeText(SplashActivity.this,"当前网络不可用",Toast.LENGTH_LONG).show();
//                }
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                enterMainActivity();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        view.startAnimation(alphaAnimation);

    }



    public void enterMainActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }



    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onClick(View view) {
        countDownTimer.cancel();
        enterMainActivity();
    }
}
