package com.xianzaishi.normandie;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.xianzaishi.normandie.global.Contants;
import com.xianzaishi.normandie.utils.SPUtils;

/**
 * quyang
 * 我要反馈
 * 2016年9月23日16:19:02
 */
public class FanKuiActivity extends BaseActivity {
    private String yijian;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fan_kui);

        tvCenterText.setText("我有话说");


        TextView name = (TextView) findViewById(R.id.name);
        final EditText des = (EditText) findViewById(R.id.des);
        Button ok = (Button) findViewById(R.id.ok);
        String nameStr= SPUtils.getStringValue(MyApplication.getContext(), Contants.SP_NAME,"userName","");
        name.setText(nameStr);

        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                yijian= des.getText().toString().trim();
                if(yijian.isEmpty()){
                    Toast.makeText(getApplicationContext(),"提交内容不能为空！",Toast.LENGTH_LONG).show();
                }else {
                    Toast.makeText(getApplicationContext(),"提交成功！",Toast.LENGTH_LONG).show();
                    finish();
                }
            }
        });


    }
}
