package com.xianzaishi.normandie.wxapi;


import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.tencent.mm.sdk.constants.ConstantsAPI;
import com.tencent.mm.sdk.modelbase.BaseReq;
import com.tencent.mm.sdk.modelbase.BaseResp;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.IWXAPIEventHandler;
import com.tencent.mm.sdk.openapi.WXAPIFactory;
import com.umeng.weixin.callback.WXCallbackActivity;
import com.xianzaishi.normandie.AliPayResultActivity;
import com.xianzaishi.normandie.MyApplication;
import com.xianzaishi.normandie.global.Contants;
import com.xianzaishi.normandie.utils.SPUtils;


public class WXPayEntryActivity extends WXCallbackActivity implements IWXAPIEventHandler{

	
    private IWXAPI api;
	private String oid,pay,times;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    	api = WXAPIFactory.createWXAPI(this, Contants.APP_ID);
        api.handleIntent(getIntent(), this);
    }

	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		setIntent(intent);
        api.handleIntent(intent, this);
	}

	@Override
	public void onReq(BaseReq req) {
	}

	@Override
	public void onResp(BaseResp resp) {
		if (resp.getType() == ConstantsAPI.COMMAND_PAY_BY_WX) {
			if (resp.errCode==0){
				Intent intent=new Intent(this, AliPayResultActivity.class);
				oid= SPUtils.getStringValue(MyApplication.getContext(),Contants.SP_NAME,"aliResult","0");
				pay=SPUtils.getStringValue(MyApplication.getContext(),Contants.SP_NAME,"pay","0");
				times=SPUtils.getStringValue(MyApplication.getContext(),Contants.SP_NAME,"time","0");

				intent.putExtra("aliResult",oid);
				intent.putExtra("pay",pay);
				intent.putExtra("time",times);
				intent.putExtra("channel","微信");
				startActivity(intent);
			}else if (resp.errCode==-2){
				Toast.makeText(WXPayEntryActivity.this,"取消支付！",Toast.LENGTH_SHORT).show();
			}else {
				Toast.makeText(WXPayEntryActivity.this,"支付失败！",Toast.LENGTH_SHORT).show();
			}
			finish();
		}
	}
}
