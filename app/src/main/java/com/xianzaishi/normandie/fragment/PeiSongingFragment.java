package com.xianzaishi.normandie.fragment;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.xianzaishi.normandie.R;
import com.xianzaishi.normandie.adapter.OrderAdapter;
import com.xianzaishi.normandie.adapter.SwipeListener;
import com.xianzaishi.normandie.bean.OrdersListBean;
import com.xianzaishi.normandie.bean.OrdersListBean01;
import com.xianzaishi.normandie.global.Contants;
import com.xianzaishi.normandie.http.PostProtocol;
import com.xianzaishi.normandie.interfaces.OnDataFromPostServerListener;
import com.xianzaishi.normandie.utils.SPUtils;
import com.xianzaishi.normandie.utils.UiUtils;

import java.util.List;

import okhttp3.FormBody;

/**
 * quyang
 * <p/>
 * Created by wsl on 2016/8/19.
 */
public class PeiSongingFragment extends BaseFragment implements View.OnClickListener {
    private List dataList;
    private View view;
    private RecyclerView recycler;
    private Button zhifu;
    private Button cancel;
    private TextView tvDengMoney;
    private OrderAdapter adapter;

    private RelativeLayout emptyView;

    private List<OrdersListBean.DataBean> allOrderInfo;

    private SwipeRefreshLayout mSwipeRefreshWidget;
    private List<OrdersListBean01.DataBean.ObjectsBean> mObjectsBeanList;
    private int mTotal;
    private int mCurrentPage;
    private RelativeLayout mErrorView;

    @Override
    public void initData() {


        FormBody body = new FormBody.Builder()
                .add("uid", "" + SPUtils.getStringValue(activity, Contants.SP_NAME, Contants.UID, null))
                .add("pageSize", "10")
                .add("curPage", "1")
                .add("status", "6")
                .build();
        PostProtocol protocol = new PostProtocol();
        protocol.setUrl(Contants.ORDER_LIST);
        protocol.setRequestBody(body);
        protocol.getDataByPOST();
        protocol.setOnDataFromPostServerListener(new OnDataFromPostServerListener() {
            @Override
            public void onSuccess(String result) {
                Gson gson = new Gson();
                OrdersListBean01 ordersListBean = gson.fromJson(result, OrdersListBean01.class);

                setData(ordersListBean);
            }

            @Override
            public void onFail() {
                emptyView.setVisibility(View.INVISIBLE);
                mErrorView.setVisibility(View.VISIBLE);
            }
        });
    }

    //适配数据
    private void setData(OrdersListBean01 ordersListBean) {

        Toast.makeText(activity,""+ordersListBean.toString(),Toast.LENGTH_LONG).show();


        if (ordersListBean.success) {
//            allOrderInfo = ordersListBean.data;

            if (ordersListBean.data != null) {

                OrdersListBean01.DataBean.PaginationBean pagination = ordersListBean.data.pagination;
                mTotal = pagination.total;
                mCurrentPage = pagination.currentPage;
                mObjectsBeanList = ordersListBean.data.objects;
                if (mObjectsBeanList.size() != 0) {
                    emptyView.setVisibility(View.INVISIBLE);


                    //设置适配器
                    adapter = new OrderAdapter(mObjectsBeanList, activity);
                    recycler.setAdapter(this.adapter);

                    //初始化swipelayout
                    FormBody body = new FormBody.Builder()
                            .add("uid", "" + SPUtils.getStringValue(activity, Contants.SP_NAME, Contants.UID, null))
                            .add("pageSize", "10")
                            .add("curPage", mCurrentPage + 1 + "")
                            .add("status", "6")
                            .build();
                    SwipeListener swipeListener = new SwipeListener(mObjectsBeanList, adapter, mSwipeRefreshWidget,
                            activity, body, Contants.ORDER_LIST, mCurrentPage, mTotal);
                    swipeListener.inimSwipeRefreshWidget();

                } else {
                    emptyView.setVisibility(View.VISIBLE);
                }

            } else {
                emptyView.setVisibility(View.VISIBLE);
                mErrorView.setVisibility(View.INVISIBLE);
            }


        } else {
            emptyView.setVisibility(View.VISIBLE);
            mErrorView.setVisibility(View.INVISIBLE);
        }
    }


    @Override
    public View initView() {
        Toast.makeText(activity, "配送中订单", Toast.LENGTH_LONG).show();

        //填充布局
        view = UiUtils.inflateView(R.layout.myorderlistview);

        //找到组件
        emptyView = (RelativeLayout) view.findViewById(R.id.empty_view);
        mErrorView = (RelativeLayout) view.findViewById(R.id.error_view);
        TextView iconError = (TextView) view.findViewById(R.id.retry);
        iconError.setOnClickListener(this);
        recycler = (RecyclerView) view.findViewById(R.id.recycler);
        mSwipeRefreshWidget = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_widge);




        //设置布局管理器
        recycler.setLayoutManager(new LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false));


        return view;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.retry:
                retry();
                break;
        }
    }
}
