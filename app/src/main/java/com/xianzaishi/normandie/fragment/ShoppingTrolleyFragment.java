package com.xianzaishi.normandie.fragment;


import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.umeng.analytics.MobclickAgent;
import com.xianzaishi.normandie.MainActivity;
import com.xianzaishi.normandie.MyApplication;
import com.xianzaishi.normandie.NewLoginActivity;
import com.xianzaishi.normandie.OrderActivity;
import com.xianzaishi.normandie.R;
import com.xianzaishi.normandie.bean.CreditBean;
import com.xianzaishi.normandie.bean.MakeSignsBean;
import com.xianzaishi.normandie.bean.ShoppingTrolleyBean;
import com.xianzaishi.normandie.common.Urls;
import com.xianzaishi.normandie.customs.CustomViewPager;
import com.xianzaishi.normandie.global.Contants;
import com.xianzaishi.normandie.utils.GetBeanClass;
import com.xianzaishi.normandie.utils.GlideUtils;
import com.xianzaishi.normandie.utils.SPUtils;
import com.xianzaishi.normandie.utils.ToastUtils;
import com.xianzaishi.normandie.utils.UiUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class ShoppingTrolleyFragment extends Fragment {
    private ListView listView;
    private ImageView ivSelectAll;
    private TextView tvGotoPay;
    private MyBaseAdapter mAdapter;
    private double allPrice;
    private TextView tvAllPrice;
    private boolean isCheck = true;
    private LinearLayout bottom,couponsContain,freightView;
    private List<ShoppingTrolleyBean.DataBean.ObjectsBean> list;
    private List<MakeSignsBean.ModuleBean> signList;
    private ViewHolder viewHolder;
    private OkHttpClient okHttpClient = MyApplication.getOkHttpClient();
    private boolean isFirst=true,isSelected;
    private Button toHome;
    private View emptyView;
    private int credit;
    private String sku;
    private TextView shoppingTrolleyCountText;
    private int shoppingTrolleyCount;
    private int itemCount,storageNumber;
    private TextView goBuy,footHint,couponsType,couponsDetail;
    private String checkListStr;//选中的商品sku集合字符串
    private String uid= SPUtils.getStringValue(MyApplication.getContext(), Contants.SP_NAME,Contants.UID,null);
    private String token=SPUtils.getStringValue(MyApplication.getContext(),Contants.SP_NAME,Contants.TOKEN,null);



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_shopping_trolley, container, false);
        //找控件
        initView(view);
        //netWorkGetData();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        isCheck=true;
        netWorkGetData();
        //友盟统计
        MobclickAgent.onPageStart("ShoppingCar");
    }



    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        boolean countChanged=SPUtils.getBooleanValue(MyApplication.getContext(),Contants.SP_NAME,"countChanged",false);
        if (countChanged){
            netWorkGetData();
            SPUtils.putBooleanValue(MyApplication.getContext(),Contants.SP_NAME,"countChanged",false);
        }
        if (isVisibleToUser){
            if (uid==null){
                startActivity(new Intent(getActivity(), NewLoginActivity.class));
            }
        }
    }

    private void initView(View view) {
        LayoutInflater inflater=getActivity().getLayoutInflater();
        couponsContain= (LinearLayout) view.findViewById(R.id.llDiscount);
        couponsType= (TextView) view.findViewById(R.id.coupons_hint_type);
        couponsDetail= (TextView) view.findViewById(R.id.coupons_hint_detail);

        listView = (ListView) view.findViewById(R.id.listView);
        ivSelectAll = (ImageView) view.findViewById(R.id.iv_select_all);
        tvGotoPay = (TextView) view.findViewById(R.id.tvGotoPay);
        tvAllPrice = (TextView) view.findViewById(R.id.tvAllPrice);
        bottom = (LinearLayout) view.findViewById(R.id.shopping_trolley_bottom);
        emptyView=inflater.inflate(R.layout.empty_view_shopping,null);
        toHome= (Button) emptyView.findViewById(R.id.shopping_trolley_toHome);//购物车为空时去逛逛按钮
        freightView= (LinearLayout) view.findViewById(R.id.freight_container);
        goBuy= (TextView) view.findViewById(R.id.shopping_trolley_foot_buy);
        footHint= (TextView) view.findViewById(R.id.shopping_trolley_foot_hint);
        //设置监听
        initOnClick();
        /**
         *  购物车计数
         */
        shoppingTrolleyCountText= (TextView) getActivity().findViewById(R.id.shopping_trolley_count);

    }

    private void initOnClick() {
        ivSelectAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isSelected=false;//将是否选择了有效商品标记复位
                if (list.isEmpty()) {
                    System.out.println("数据为空");
                    return;
                }
                isCheck = !isCheck;
                ivSelectAll.setSelected(isCheck);
                int position=0;

                final StringBuilder checkList=new StringBuilder("[");
                for (ShoppingTrolleyBean.DataBean.ObjectsBean objectsBean : list) {
                    if (objectsBean.getNumber()>0&&signList.get(position).isMayPlus()){//库存不足的不能被选中
                        objectsBean.setSelected(isCheck);
                        if (isCheck) {
                            checkList.append(objectsBean.getSkuId()).append(",");
                        }
                    }
                    position++;
                    shoppingTrolleyCount+=objectsBean.getItemCount();
                }
                if (checkList.length()>1){
                    checkList.replace(checkList.length()-1,checkList.length(),"]");
                }else {
                    checkList.append("]");
                }
                mAdapter.notifyDataSetChanged();
                RequestBody formBody = new FormBody.Builder()
                        .add("token",token)
                        .add("checkList",checkList.toString())
                        .build();
                Request request = new Request.Builder()
                        .url(Urls.SHOPPING_CAR_SELECT)
                        .post(formBody)
                        .build();
                okHttpClient.newCall(request).enqueue(new Callback() {
                    @Override
                    public void onFailure(Call call, IOException e) {
                    }

                    @Override
                    public void onResponse(Call call, Response response) throws IOException {
                        if (!response.isSuccessful()){
                            response.close();
                            return;
                        }

                        final ShoppingTrolleyBean stBean = (ShoppingTrolleyBean) GetBeanClass.getBean(response, ShoppingTrolleyBean.class);
                        response.close();
                        UiUtils.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (stBean.getCode()!=1){
                                    return;
                                }
                                if (stBean.getData().getDiscountInfo()==null){
                                    isSelected=false;
                                    String freightDesc=stBean.getData().getFreightDesc();
                                    initUi("",freightDesc,"0.00");
                                }else {
                                    isSelected=true;
                                    String discountDesc=stBean.getData().getDiscountInfo().getDiscountDesc();
                                    String currPrice=stBean.getData().getDiscountInfo().getCurrPrice();
                                    String freightDesc=stBean.getData().getFreightDesc();
                                    initUi(discountDesc,freightDesc,currPrice);
                                }

                                checkListStr=checkList.toString();
                            }
                        });
                    }
                });
                //updatePrice();
            }
        });
        tvGotoPay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MobclickAgent.onEvent(getActivity(),"goToCreatOrder");
                /**
                 *  点击跳转订单详情页，需要带过去的数据
                 *      1、商品信息
                 *      2、总价
                 */
                if (isSelected) {
                    Intent intent = new Intent(MyApplication.getContext(), OrderActivity.class);
                    //创建一个传递数据的集合
                    //ArrayList<ShoppingTrolleyBean.DataBean.ObjectsBean> list1 = new ArrayList<ShoppingTrolleyBean.DataBean.ObjectsBean>();
                    StringBuilder skuIds=new StringBuilder();
                    for (ShoppingTrolleyBean.DataBean.ObjectsBean objectsBean : list) {
                        //遍历数据集合，把选中的对象都传递给订单页面
                        if (objectsBean.isSelected()) {
                            skuIds.append(objectsBean.getSkuId()).append(",");
                        }
                    }
                    if (skuIds.length()>0) {
                        sku = skuIds.substring(0, skuIds.length() - 1);
                    }else {
                        sku="";
                    }

                    intent.putExtra("credit", credit);
                    intent.putExtra("skuIds", sku);
                    startActivity(intent);
                    isSelected=false;
                }else {
                    ToastUtils.showToast("请选择有效的商品");
                }
            }
        });
        /**
         *  购物车为空  去逛逛 跳转到首页
         */
        toHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final MainActivity mainActivity = (MainActivity) getActivity();
                mainActivity.setFragment2Fragment(new MainActivity.Fragment2Fragment() {
                    @Override
                    public void gotoFragment(CustomViewPager viewPager) {
                        viewPager.setCurrentItem(0);
                    }
                });
                mainActivity.forSkip();
            }
        });

        /**
         *  footerView 跳转到首页
         */
        goBuy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final MainActivity mainActivity = (MainActivity) getActivity();
                mainActivity.setFragment2Fragment(new MainActivity.Fragment2Fragment() {
                    @Override
                    public void gotoFragment(CustomViewPager viewPager) {
                        viewPager.setCurrentItem(0);
                    }
                });
                mainActivity.forSkip();
            }
        });
    }

    private void netWorkGetData() {
        uid= SPUtils.getStringValue(MyApplication.getContext(), Contants.SP_NAME,Contants.UID,null);
        token=SPUtils.getStringValue(MyApplication.getContext(),Contants.SP_NAME,Contants.TOKEN,null);
        if (uid==null){
            ViewGroup vg= (ViewGroup) listView.getParent();
            if(emptyView.getParent()==null){
                vg.addView(emptyView);
            }
            bottom.setVisibility(View.GONE);
            freightView.setVisibility(View.GONE);
            list=null;
            listView.setEmptyView(emptyView);
            return;
        }
        RequestBody formBody=new FormBody.Builder()
                .add("token",token)
                .build();
        Request request = new Request.Builder()
                .url(Urls.SHOPPING_CAR_URL)
                .post(formBody)
                .build();
        okHttpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if(!response.isSuccessful()) throw new IOException("Unexpected code"+response);

                final ShoppingTrolleyBean stBean= (ShoppingTrolleyBean) GetBeanClass.getBean(response,ShoppingTrolleyBean.class);
                response.close();
                UiUtils.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if(stBean!=null&&stBean.getData()!=null&&stBean.getData().getObjects()!=null){
                            ShoppingTrolleyBean.DataBean dataBean=stBean.getData();
                            list=stBean.getData().getObjects();
                            if (dataBean.getDiscountInfo()!=null){
                                initUi(dataBean.getDiscountInfo().getDiscountDesc(),dataBean.getFreightDesc(),dataBean.getDiscountInfo().getCurrPrice());
                            }else {
                                initUi("",dataBean.getFreightDesc(),"0.00");
                            }

                            //请求打标信息
                            makeSign();
                        }else {
                            list=null;

                            if(isFirst){isFirst=false;}
                            mAdapter = new MyBaseAdapter();
                            listView.setAdapter(mAdapter);
                            ViewGroup vg= (ViewGroup) listView.getParent();
                            if(emptyView.getParent()==null){
                                vg.addView(emptyView);
                            }
                            listView.setEmptyView(emptyView);
                            if(mAdapter.isEmpty()){
                                bottom.setVisibility(View.GONE);
                                freightView.setVisibility(View.GONE);
                                couponsContain.setVisibility(View.GONE);
                            }else {
                                bottom.setVisibility(View.VISIBLE);
                                freightView.setVisibility(View.VISIBLE);
                                couponsContain.setVisibility(View.VISIBLE);
                            }
                        }

                        getCredit();

                    }
                });

            }
        });
    }

    /**
     * 更新优惠提示信息，运费相关信息，当前总价
     * @param discountDesc 优惠提示文本
     * @param freightDesc 运费提示文本
     * @param currPrice 当前总价
     */
    private void initUi(String discountDesc, String freightDesc, String currPrice) {
        if(discountDesc==null||discountDesc.equals("")){
            couponsContain.setVisibility(View.GONE);
        }else {
            couponsContain.setVisibility(View.VISIBLE);
            couponsDetail.setText(discountDesc);
        }
        //运费相关提示
        footHint.setText(freightDesc);
        //商品总价
        tvAllPrice.setText(currPrice);
    }

    /**
     *  购物车商品打标
     */
    private void makeSign() {
        StringBuilder str=new StringBuilder();
        for(ShoppingTrolleyBean.DataBean.ObjectsBean bean:list){
            str.append(bean.getSkuId()).append(",");
        }
        String skuIds=str.substring(0,str.length()-1);
        FormBody form=new FormBody.Builder()
                .add("skuIds",skuIds)
                .build();
        Request request=new Request.Builder()
                .post(form)
                .url(Urls.MAKE_SIGN)
                .build();
        okHttpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (!response.isSuccessful()){
                    response.close();
                    return;
                }
                MakeSignsBean bean= (MakeSignsBean) GetBeanClass.getBean(response,MakeSignsBean.class);
                if (!bean.isSuccess()){
                    response.close();
                    return;
                }
                signList=bean.getModule();
                UiUtils.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        shoppingTrolleyCount=0;
                        ivSelectAll.setSelected(isCheck);
                        int position=0;

                        StringBuilder checkList=new StringBuilder("[");
                        for (ShoppingTrolleyBean.DataBean.ObjectsBean objectsBean : list) {
                            if (objectsBean.getNumber()>0&&signList.get(position).isMayPlus()){//库存不足的不能被选中
                                objectsBean.setSelected(isCheck);

                                checkList.append(objectsBean.getSkuId()).append(",");
                                isSelected=true;
                            }
                            position++;
                            shoppingTrolleyCount+=objectsBean.getItemCount();
                        }
                        if (checkList.length()>1){
                            checkList.replace(checkList.length()-1,checkList.length(),"]");
                        }else {
                            checkList.append("]");
                        }

                        checkListStr=checkList.toString();
                        mAdapter = new MyBaseAdapter();
                        listView.setAdapter(mAdapter);
                        ViewGroup vg= (ViewGroup) listView.getParent();
                        if(emptyView.getParent()==null){
                            vg.addView(emptyView);
                        }
                        listView.setEmptyView(emptyView);
                        if(mAdapter.isEmpty()){
                            bottom.setVisibility(View.GONE);
                            freightView.setVisibility(View.GONE);
                        }else {
                            bottom.setVisibility(View.VISIBLE);
                            freightView.setVisibility(View.VISIBLE);
                        }
                    }
                });

            }
        });
    }



    private void getCredit(){
        String uid=SPUtils.getStringValue(MyApplication.getContext(),Contants.SP_NAME,Contants.UID,"");
        RequestBody formBody=new FormBody.Builder()
                .add("uid",uid)
                .build();
        Request request = new Request.Builder()
                .url(Urls.GET_CREDIT)
                .post(formBody)
                .build();
        okHttpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if(!response.isSuccessful()) throw new IOException("Unexpected code"+response);
                final CreditBean creditBean= (CreditBean) GetBeanClass.getBean(response,CreditBean.class);
                response.close();
                UiUtils.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (creditBean.getCode()==1){
                            credit=creditBean.getData();
                            tvGotoPay.setEnabled(true);
                        }
                    }
                });

            }
        });
    }

   /* private void updatePrice() {
        if (!list.isEmpty() && list.size() >= 1) {
            allPrice = 0.0f;
            int i=0;
            for (ShoppingTrolleyBean.DataBean.ObjectsBean objectsBean : list) {
                if (objectsBean.isSelected()) {
                    Double price = objectsBean.getItemEffePrice();
                    Double count = (double) objectsBean.getItemCount();
                    //使用BigDicmal防止浮点数计算不精准
                    //单价乘以数量，一类商品的总价
                    double mul = DataCalcUtils.mul(price, count);
                    //遍历完集合，所有商品总价相加
                    allPrice = DataCalcUtils.add(allPrice, mul);
                    isSelected=true;//标记有有效商品
                    i++;
                }
            }
            if (i==0){
                isSelected=false;
            }
            *//*if (allPrice<59){
                String hint="再满"+String.format(Locale.CHINESE,"%.2f",59-allPrice)+"元，可免配送费";
                footHint.setText(hint);
            }else {
                String hint="您已享受“满59元”，免配送费";
                footHint.setText(hint);
            }*//*
            tvAllPrice.setText(allPrice + "");
            //给控件设置完以后要把allPrice重新初始化，解决总价一直加问题
            allPrice = 0.0f;
        }
    }*/

    class ViewHolder {
        private ImageView isChecked;
        private ImageView ivShoppingIcon;
        private TextView tvTitle;
        private TextView tvDes;
        private TextView redSign;
        private TextView tvPrice;
        private ImageView ivAdd;
        private ImageView ivMinus;
        private TextView tvCount;
        private ImageView ivDelete;
        private LinearLayout container;
        private TextView itemHint;
    }

    public class MyBaseAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return list==null?0:list.size();
        }

        @Override
        public ShoppingTrolleyBean.DataBean.ObjectsBean getItem(int i) {
            return list.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            if (view == null) {
                viewHolder = new ViewHolder();
                view = View.inflate(getContext(), R.layout.shopping_item, null);
                //图片，是否被选中
                viewHolder.isChecked = (ImageView) view.findViewById(R.id.ivShoppingIsCheck);
                //商品图片
                viewHolder.ivShoppingIcon = (ImageView) view.findViewById(R.id.ivShoppingIcon);
                //商品标识
                viewHolder.redSign= (TextView) view.findViewById(R.id.red_sign);
                //商品标题
                viewHolder.tvTitle = (TextView) view.findViewById(R.id.tvShoppingTitle);
                //商品描述
                viewHolder.tvDes = (TextView) view.findViewById(R.id.tvDes);
                //添加数量
                viewHolder.ivAdd = (ImageView) view.findViewById(R.id.iv_add);
                //减少数量
                viewHolder.ivMinus = (ImageView) view.findViewById(R.id.iv_minus);
                //价格描述
                viewHolder.tvPrice = (TextView) view.findViewById(R.id.tvPrice);
                //个数描述
                viewHolder.tvCount = (TextView) view.findViewById(R.id.tvCount);
                //一处购物车按钮
                viewHolder.ivDelete = (ImageView) view.findViewById(R.id.ivDelete);
                //用来改变背景色的根布局
                viewHolder.container= (LinearLayout) view.findViewById(R.id.ll);
                //每一个商品的优惠提示
                viewHolder.itemHint= (TextView) view.findViewById(R.id.shopping_trolley_item_hint);
                view.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) view.getTag();
            }
            ShoppingTrolleyBean.DataBean.ObjectsBean objectsBean=getItem(i);
            MakeSignsBean.ModuleBean signBean=signList.get(i);
            //根据是否选中来确定UI
            viewHolder.isChecked.setSelected(objectsBean.isSelected());
            final int position = i;
            //根据每个对象信息确定UI
            viewHolder.tvTitle.setText(objectsBean.getItemName());
            viewHolder.tvDes.setText("规格："+objectsBean.getSkuInfo().getSpec());
            int tvCount= (int) objectsBean.getItemCount();
            viewHolder.tvCount.setText(tvCount+"");
            viewHolder.tvPrice.setText(objectsBean.getItemEffePrice()+"");
            GlideUtils.LoadImage(getActivity(),objectsBean.getItemIconUrl(),viewHolder.ivShoppingIcon);
            //根据每个item是否有优惠信息来改变item的样式
            if (objectsBean.getDiscountInfo()!=null){
                viewHolder.itemHint.setVisibility(View.VISIBLE);
                viewHolder.itemHint.setText(objectsBean.getDiscountInfo().getDiscountDesc());
                viewHolder.container.setBackgroundColor(getResources().getColor(R.color.shoppingCarPink));
            }else {
                viewHolder.itemHint.setVisibility(View.GONE);
                viewHolder.container.setBackgroundColor(getResources().getColor(R.color.colorWhiteBackGround));
            }
            /**
             *  商品打标
             */
            if (objectsBean.getNumber()<=0){
                viewHolder.redSign.setVisibility(View.VISIBLE);
                viewHolder.redSign.setBackgroundResource(R.drawable.grey_sign);
                viewHolder.redSign.setText("卖光啦");

                viewHolder.ivAdd.setEnabled(false);
                viewHolder.ivMinus.setEnabled(false);
                viewHolder.isChecked.setEnabled(false);
            }else if (!signBean.isMayPlus()){
                viewHolder.ivAdd.setEnabled(false);
                viewHolder.ivMinus.setEnabled(false);
                viewHolder.isChecked.setEnabled(false);
                if (signBean.getChannel()==null||signBean.getTagContent()==null){
                    viewHolder.redSign.setVisibility(View.GONE);
                }else {
                    viewHolder.redSign.setVisibility(View.VISIBLE);
                    viewHolder.redSign.setBackgroundResource(R.drawable.grey_sign);
                    viewHolder.redSign.setText(signBean.getTagContent());
                }
            }else {
                viewHolder.ivAdd.setEnabled(true);
                viewHolder.ivMinus.setEnabled(true);
                viewHolder.isChecked.setEnabled(true);
                if (signBean.getChannel()==null||signBean.getTagContent()==null||signBean.getChannel().equals("2")){
                    viewHolder.redSign.setVisibility(View.GONE);
                }else {
                    viewHolder.redSign.setVisibility(View.VISIBLE);
                    viewHolder.redSign.setBackgroundResource(R.drawable.red_sign);
                    viewHolder.redSign.setText(signBean.getTagContent());
                }
            }
            /**
             *  加号逻辑
             */
            viewHolder.ivAdd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    viewHolder.ivAdd.setEnabled(false);
                    final ShoppingTrolleyBean.DataBean.ObjectsBean objectsBean1 = list.get(position);
                    int itemId=objectsBean1.getItemId();
                    int skuId=objectsBean1.getSkuId();
                    itemCount = (int) objectsBean1.getItemCount();
                    itemCount=++itemCount;

                    RequestBody formBody = new FormBody.Builder()
                            .add("token",token)
                            .add("itemId", String.valueOf(itemId))
                            .add("itemCount", String.valueOf(itemCount))
                            .add("skuId", String.valueOf(skuId))
                            .add("checkList",checkListStr)
                            .build();
                    Request request = new Request.Builder()
                            .url(Urls.CHANGE_ITEM_COUNT)
                            .post(formBody)
                            .build();
                    final int finalItemCount = itemCount;
                    okHttpClient.newCall(request).enqueue(new Callback() {
                        @Override
                        public void onFailure(Call call, IOException e) {
                            UiUtils.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    viewHolder.ivAdd.setEnabled(true);
                                    itemCount = --itemCount;
                                }
                            });
                        }

                        @Override
                        public void onResponse(Call call, Response response) throws IOException {
                            if (!response.isSuccessful())
                                throw new IOException("Unexpected code" + response);
                            final ShoppingTrolleyBean stBean = (ShoppingTrolleyBean) GetBeanClass.getBean(response, ShoppingTrolleyBean.class);
                            response.close();
                            UiUtils.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (stBean.getCode() == 1) {
                                        objectsBean1.setItemCount(finalItemCount);
                                        //updatePrice();
                                        String discountDesc=stBean.getData().getDiscountInfo().getDiscountDesc();
                                        String currPrice=stBean.getData().getDiscountInfo().getCurrPrice();
                                        String freightDesc=stBean.getData().getFreightDesc();
                                        if(stBean.getData().getObjects()!=null&&stBean.getData().getObjects().get(0).getDiscountInfo()!=null){
                                            objectsBean1.getDiscountInfo().setDiscountDesc(stBean.getData().getObjects().get(0).getDiscountInfo().getDiscountDesc());
                                        }
                                        initUi(discountDesc,freightDesc,currPrice);
                                        shoppingTrolleyCount++;
                                        //更新购物车指示器显示
                                        shoppingTrolleyCountText.setText(String.valueOf(shoppingTrolleyCount));
                                        notifyDataSetChanged();
                                        viewHolder.ivAdd.setEnabled(true);
                                    } else {
                                        viewHolder.ivAdd.setEnabled(true);
                                        itemCount = --itemCount;
                                    }
                                }
                            });
                        }
                    });
                }
            });
            //是否选中逻辑
            viewHolder.isChecked.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    isSelected=false;//复位标记是否有有效商品的值
                    viewHolder.isChecked.setEnabled(false);
                    final ShoppingTrolleyBean.DataBean.ObjectsBean objectsBean1 = list.get(position);
                    objectsBean1.setSelected(!objectsBean1.isSelected());
                    int selectedCount=0;
                    int position=0;
                    int size=list.size();

                    final StringBuilder checkList=new StringBuilder("[");
                    for (ShoppingTrolleyBean.DataBean.ObjectsBean objectsBean : list) {
                        if (!objectsBean.isSelected()&&objectsBean.getNumber()>0&&signList.get(position).isMayPlus()){//库存不足的不能被选中
                            selectedCount++;
                        }else if(objectsBean.isSelected()&&objectsBean.getNumber()>0&&signList.get(position).isMayPlus()){

                            checkList.append(objectsBean.getSkuId()).append(",");

                            isSelected=true;//复位标记是否有有效商品的值
                        }
                        position++;
                    }
                    if (checkList.length()>1){
                     checkList.replace(checkList.length()-1,checkList.length(),"]");
                    }else {
                        checkList.append("]");
                    }
                    final int finalSelectCount=selectedCount;
                    //请求网络 更改选中状态
                    int skuId=objectsBean1.getSkuId();
                    RequestBody formBody = new FormBody.Builder()
                            .add("token",token)
                            .add("skuId", String.valueOf(skuId))
                            .add("checkList",checkList.toString())
                            .build();
                    Request request = new Request.Builder()
                            .url(Urls.SHOPPING_CAR_SELECT)
                            .post(formBody)
                            .build();
                    okHttpClient.newCall(request).enqueue(new Callback() {
                        @Override
                        public void onFailure(Call call, IOException e) {
                            objectsBean1.setSelected(!objectsBean1.isSelected());
                            UiUtils.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    viewHolder.isChecked.setEnabled(true);
                                }
                            });
                        }

                        @Override
                        public void onResponse(Call call, Response response) throws IOException {
                            if (!response.isSuccessful()){
                                response.close();
                                return;
                            }

                            final ShoppingTrolleyBean stBean = (ShoppingTrolleyBean) GetBeanClass.getBean(response, ShoppingTrolleyBean.class);
                            response.close();
                            UiUtils.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    viewHolder.isChecked.setEnabled(true);
                                    if (stBean.getCode()!=1){
                                        objectsBean1.setSelected(!objectsBean1.isSelected());
                                        return;
                                    }
                                    if (finalSelectCount==0){
                                        isCheck=true;
                                        ivSelectAll.setSelected(isCheck);
                                    }else {
                                        isCheck=false;
                                        ivSelectAll.setSelected(isCheck);
                                    }

                                    if (stBean.getData().getDiscountInfo()==null){
                                        String freightDesc=stBean.getData().getFreightDesc();
                                        initUi("",freightDesc,"0.00");
                                    }else {
                                        String discountDesc=stBean.getData().getDiscountInfo().getDiscountDesc();
                                        String currPrice=stBean.getData().getDiscountInfo().getCurrPrice();
                                        String freightDesc=stBean.getData().getFreightDesc();
                                        initUi(discountDesc,freightDesc,currPrice);
                                    }
                                    notifyDataSetChanged();
                                    checkListStr=checkList.toString();
                                }
                            });
                        }
                    });
                    //notifyDataSetChanged();
                    //updatePrice();

                }
            });
            //减号逻辑
            viewHolder.ivMinus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    viewHolder.ivMinus.setEnabled(false);
                    final ShoppingTrolleyBean.DataBean.ObjectsBean objectsBean1 = list.get(position);
                    int itemId=objectsBean1.getItemId();
                    int skuId=objectsBean1.getSkuId();
                    itemCount = (int) objectsBean1.getItemCount();
                    itemCount=--itemCount;
                    if(itemCount==0){
                        dialog(position);
                    }else {
                        RequestBody formBody = new FormBody.Builder()
                                .add("token",token)
                                .add("itemId", String.valueOf(itemId))
                                .add("itemCount", String.valueOf(itemCount))
                                .add("skuId", String.valueOf(skuId))
                                .add("checkList",checkListStr)
                                .build();
                        Request request = new Request.Builder()
                                .url(Urls.CHANGE_ITEM_COUNT)
                                .post(formBody)
                                .build();
                        final int finalItemCount = itemCount;
                        okHttpClient.newCall(request).enqueue(new Callback() {
                            @Override
                            public void onFailure(Call call, IOException e) {
                                UiUtils.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        viewHolder.ivMinus.setEnabled(true);
                                        itemCount=++itemCount;
                                    }
                                });
                            }

                            @Override
                            public void onResponse(Call call, Response response) throws IOException {
                                if (!response.isSuccessful())
                                    throw new IOException("Unexpected code" + response);
                                final ShoppingTrolleyBean stBean = (ShoppingTrolleyBean) GetBeanClass.getBean(response, ShoppingTrolleyBean.class);
                                response.close();
                                UiUtils.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                    if (stBean.getCode() == 1) {
                                        shoppingTrolleyCount--;
                                        //更新购物车指示器显示
                                        shoppingTrolleyCountText.setText(String.valueOf(shoppingTrolleyCount));
                                        if (finalItemCount < 1) {
                                            //小于1  删除该条数据
                                            list.remove(position);
                                            signList.remove(position);
                                            if (getCount() == 0) {
                                                bottom.setVisibility(View.GONE);
                                                freightView.setVisibility(View.GONE);
                                            }
                                        } else {
                                            objectsBean1.setItemCount(finalItemCount);
                                            bottom.setVisibility(View.VISIBLE);
                                            freightView.setVisibility(View.VISIBLE);
                                        }
                                        if(stBean.getData().getObjects()!=null&&stBean.getData().getObjects().get(0).getDiscountInfo()!=null){
                                            objectsBean1.getDiscountInfo().setDiscountDesc(stBean.getData().getObjects().get(0).getDiscountInfo().getDiscountDesc());
                                        }
                                        notifyDataSetChanged();
                                        //updatePrice();
                                        String discountDesc=stBean.getData().getDiscountInfo().getDiscountDesc();
                                        String currPrice=stBean.getData().getDiscountInfo().getCurrPrice();
                                        String freightDesc=stBean.getData().getFreightDesc();
                                        initUi(discountDesc,freightDesc,currPrice);
                                        viewHolder.ivMinus.setEnabled(true);
                                    }else {
                                        itemCount=++itemCount;
                                        viewHolder.ivMinus.setEnabled(true);
                                    }
                                    }
                                });
                            }
                        });
                    }
                }
            });
            //删除按钮逻辑
            viewHolder.ivDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog(position);
                }
            });
            //updatePrice();
            return view;
        }
    }
    /**
     *  删除商品时弹出dialog
     */
    private void dialog(final int position){

        LayoutInflater inflater=getActivity().getLayoutInflater();
        View view=inflater.inflate(R.layout.commodity_delete_dialog,null);
        final Dialog dialog=new Dialog(getActivity(),R.style.Dialog);
        dialog.setCancelable(false);
        dialog.setContentView(view);
        TextView sure= (TextView) view.findViewById(R.id.commodity_dialog_sure);
        TextView cancel= (TextView) view.findViewById(R.id.commodity_dialog_cancel);
        sure.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                viewHolder.ivMinus.setEnabled(false);
                final ShoppingTrolleyBean.DataBean.ObjectsBean objectsBean1 = list.get(position);
                int itemId=objectsBean1.getItemId();
                int skuId=objectsBean1.getSkuId();
                final int count= (int) objectsBean1.getItemCount();
                final int itemCount = 0;
                RequestBody formBody=new FormBody.Builder()
                        .add("token",token)
                        .add("itemId", String.valueOf(itemId))
                        .add("itemCount", String.valueOf(itemCount))
                        .add("skuId", String.valueOf(skuId))
                        .add("checkList",checkListStr)
                        .build();
                Request request=new Request.Builder()
                        .url(Urls.CHANGE_ITEM_COUNT)
                        .post(formBody)
                        .build();
                okHttpClient.newCall(request).enqueue(new Callback() {
                    @Override
                    public void onFailure(Call call, IOException e) {
                        UiUtils.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                viewHolder.ivMinus.setEnabled(true);
                            }
                        });
                    }
                    @Override
                    public void onResponse(Call call, Response response) throws IOException {
                        if(!response.isSuccessful()) throw new IOException("Unexpected code"+response);
                        final ShoppingTrolleyBean stBean= (ShoppingTrolleyBean) GetBeanClass.getBean(response,ShoppingTrolleyBean.class);
                        response.close();
                        if (stBean!=null&&stBean.getCode()==1){
                            UiUtils.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    shoppingTrolleyCount-=count;
                                    //更新购物车指示器显示
                                    shoppingTrolleyCountText.setText(String.valueOf(shoppingTrolleyCount));
                                    if(list.size()==1){
                                        shoppingTrolleyCountText.setVisibility(View.GONE);
                                        bottom.setVisibility(View.GONE);
                                        freightView.setVisibility(View.GONE);
                                    }
                                    list.remove(position);
                                    signList.remove(position);
                                    mAdapter.notifyDataSetChanged();
                                    //updatePrice();

                                    if (stBean.getData().getDiscountInfo()==null){
                                        String freightDesc=stBean.getData().getFreightDesc();
                                        initUi("",freightDesc,"0.00");
                                    }else {
                                        String discountDesc=stBean.getData().getDiscountInfo().getDiscountDesc();
                                        String currPrice=stBean.getData().getDiscountInfo().getCurrPrice();
                                        String freightDesc=stBean.getData().getFreightDesc();
                                        initUi(discountDesc,freightDesc,currPrice);
                                    }
                                }
                            });
                        }
                    }
                });
                dialog.dismiss();
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.show();
        Window dialogWindow = dialog.getWindow();
        WindowManager m = getActivity().getWindowManager();
        Display d = m.getDefaultDisplay(); // 获取屏幕宽、高度
        if (dialogWindow==null){
            return;
        }
        WindowManager.LayoutParams p = dialogWindow.getAttributes(); // 获取对话框当前的参数值
        //p.height = (int) (d.getHeight() * 0.5); // 高度设置为屏幕的0.6，根据实际情况调整
        p.width = (int) (d.getWidth() * 0.6); // 宽度设置为屏幕的0.6，根据实际情况调整
        dialogWindow.setAttributes(p);
    }

    @Override
    public void onPause() {
        super.onPause();
        MobclickAgent.onPageEnd("ShoppingCar");
    }
}