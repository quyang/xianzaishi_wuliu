package com.xianzaishi.normandie.fragment.child_fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.xianzaishi.normandie.R;
import com.xianzaishi.normandie.adapter.LazyoneRecyclerAdapter;

/**
 * A simple {@link Fragment} subclass.
 */
public class Lazybones1Fragment extends Fragment {

    private RecyclerView recyclerView;

    public Lazybones1Fragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_lazybones1, container, false);
        recyclerView= (RecyclerView) view.findViewById(R.id.recycler_lazy_one);
        LinearLayoutManager linearLayoutManager=new LinearLayoutManager(getActivity(),LinearLayoutManager.HORIZONTAL,false);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(new LazyoneRecyclerAdapter(getActivity()));
        return view;
    }

}
