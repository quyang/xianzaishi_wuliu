package com.xianzaishi.normandie.fragment.child_fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.xianzaishi.normandie.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class Lazybones2Fragment extends Fragment {


    public Lazybones2Fragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_lazybones2, container, false);
    }

}
