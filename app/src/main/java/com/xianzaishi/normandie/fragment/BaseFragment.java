package com.xianzaishi.normandie.fragment;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.xianzaishi.normandie.MyOrderActivity;
import com.xianzaishi.normandie.R;

/**
 * quyang
 *
 * Created by wsl on 2016/8/19.
 */
public abstract class BaseFragment extends Fragment{

    public MyOrderActivity activity;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = (MyOrderActivity) getActivity();
    }

    //显示fragment要显示的view
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return initView();
    }

    //fragmen依靠的activity创建好后调用
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initData();
    }


    public abstract void initData();
    public abstract View initView();


    public void changeTextColor(View view) {
        String str ="退款在3~5个工作日";
        int bstart=str.indexOf("3~5个工作日");
        int bend=bstart+"3~5个工作日".length();
        int fstart=str.indexOf("3~5个工作日");
        int fend=fstart+"3~5个工作日".length();
        SpannableStringBuilder style=new SpannableStringBuilder(str);
        style.setSpan(new ForegroundColorSpan(Color.RED),fstart,fend, Spannable.SPAN_EXCLUSIVE_INCLUSIVE);
        TextView tvColor=(TextView)view.findViewById(R.id.tv_tuiqian);
        tvColor.setText(style);
    }


    public void retry() {
        initData();
    }
}
