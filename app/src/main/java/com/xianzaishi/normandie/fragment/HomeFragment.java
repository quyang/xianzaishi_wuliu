package com.xianzaishi.normandie.fragment;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.telephony.TelephonyManager;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.umeng.analytics.MobclickAgent;
import com.xianzaishi.normandie.CaptureActivity;
import com.xianzaishi.normandie.CouponsActivity;
import com.xianzaishi.normandie.DistributionActivity;
import com.xianzaishi.normandie.MainActivity;
import com.xianzaishi.normandie.MyApplication;
import com.xianzaishi.normandie.R;
import com.xianzaishi.normandie.SearchActivity;
import com.xianzaishi.normandie.bean.Home2DownBean;
import com.xianzaishi.normandie.bean.NewHomePageBean;
import com.xianzaishi.normandie.customs.MyRefreshHeader;
import com.xianzaishi.normandie.global.Contants;
import com.xianzaishi.normandie.home_page.Home1_3nView;
import com.xianzaishi.normandie.home_page.Home2nView;
import com.xianzaishi.normandie.home_page.Home5nView;
import com.xianzaishi.normandie.home_page.HomeBannerView;
import com.xianzaishi.normandie.home_page.HomeCategoryView;
import com.xianzaishi.normandie.home_page.HomeNewsView;
import com.xianzaishi.normandie.home_page.HomeSingleImgView;
import com.xianzaishi.normandie.home_page.HomeTimeCountDownView;
import com.xianzaishi.normandie.home_page.HomeTimeLimitView;
import com.xianzaishi.normandie.home_page.HomeTitleListView;
import com.xianzaishi.normandie.utils.GetBeanClass;
import com.xianzaishi.normandie.utils.SPUtils;
import com.xianzaishi.normandie.utils.ToastUtils;
import com.xianzaishi.normandie.utils.UiUtils;

import java.io.IOException;

import in.srain.cube.views.ptr.PtrDefaultHandler;
import in.srain.cube.views.ptr.PtrFrameLayout;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static android.content.Context.TELEPHONY_SERVICE;

/**
 * Created by ShenLang on 2016/8/30.
 * 首页
 */
public class HomeFragment extends Fragment implements View.OnClickListener {
    private static final MediaType MEDIA_TYPE
            = MediaType.parse("application/json; charset=utf-8");
    private OkHttpClient client= MyApplication.getOkHttpClient();
    private PtrFrameLayout mPtrFrameLayout;
    private long currentTime;
    private String color;
    private MainActivity mainActivity;
    private LinearLayout container;
    private String IMEI="";


    @Override
    public void onResume() {
        super.onResume();
        //友盟统计
        MobclickAgent.onPageStart("Home");
        boolean isNewUser=SPUtils.getBooleanValue(MyApplication.getContext(),Contants.SP_NAME,"isNewUser",false);
        if(isNewUser){
            SPUtils.putBooleanValue(MyApplication.getContext(),Contants.SP_NAME,"isNewUser",false);
            newGiftDialog();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_home, container, false);
        initView(view);
        return view;
    }

    private void initView(View view) {
        mainActivity= (MainActivity) getActivity();
        TelephonyManager tm= (TelephonyManager) mainActivity.getSystemService(TELEPHONY_SERVICE);
        if (tm!=null){
            IMEI=tm.getDeviceId();
        }

        container= (LinearLayout) view.findViewById(R.id.home_container);
        /**
         *  初始化刷新相关
         */
        mPtrFrameLayout= (PtrFrameLayout) view.findViewById(R.id.material_style_ptr_frame);
        MyRefreshHeader header=new MyRefreshHeader(getActivity());
        mPtrFrameLayout.disableWhenHorizontalMove(true);
        mPtrFrameLayout.setInterceptEventWhileWorking(true);
        mPtrFrameLayout.setHeaderView(header);
        mPtrFrameLayout.addPtrUIHandler(header);
        mPtrFrameLayout.setPtrHandler(new PtrDefaultHandler() {
            @Override
            public void onRefreshBegin(PtrFrameLayout frame) {
                loadFirstPageFromNet();
            }
        });

        mPtrFrameLayout.autoRefresh(true);

        /**
         *  顶部标题栏
         */
        //配送的店，点击可以查看地图
        TextView distributionShop= (TextView) view.findViewById(R.id.actionbar_address);
        //扫一扫
        ImageView ivScanner = (ImageView) view.findViewById(R.id.actionbar_scanner);
        //搜索
        ImageView ivSearch = (ImageView) view.findViewById(R.id.actionbar_search);

        ivSearch.setOnClickListener(this);
        ivScanner.setOnClickListener(this);
        distributionShop.setOnClickListener(this);
    }

    /**
     * 请求首屏的数据
     */
    private void loadFirstPageFromNet(){
        String str = "{\"pageId\":\"30\",\"hasAllFloor\":\"false\",\"version\":\"1\",\"device\":\""+IMEI+"\"}";

        RequestBody requestBody = RequestBody.create(MEDIA_TYPE, str);

        Request request = new Request.Builder()
                .post(requestBody)
                .url(Contants.HOME_URL)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                mPtrFrameLayout.refreshComplete();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                mPtrFrameLayout.refreshComplete();
                if (!response.isSuccessful()){
                    response.close();
                    return;
                }
                final NewHomePageBean homePageBean= (NewHomePageBean) GetBeanClass.getBean(response,NewHomePageBean.class);
                response.close();
                if (homePageBean!=null&&!homePageBean.isSuccess()){
                    ToastUtils.showToast("请求数据失败！");
                    return;
                }

                currentTime=homePageBean.getCurrentTime();
                color=homePageBean.getColor();

                loadLeftPageFromNet();

                UiUtils.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (container.getChildCount()!=0){
                            container.removeAllViews();
                        }
                        for (NewHomePageBean.ModuleBean moduleBean:homePageBean.getModule()){
                            setViewByType(moduleBean);
                        }
                    }
                });
            }
        });
    }

    /**
     * 请求剩下的数据
     */
    private void loadLeftPageFromNet(){
        String str = "{\"pageId\":\"30\",\"hasAllFloor\":\"true\",\"version\":\"1\",\"device\":\""+IMEI+"\"}";

        RequestBody requestBody = RequestBody.create(MEDIA_TYPE, str);

        Request request = new Request.Builder()
                .post(requestBody)
                .url(Contants.HOME_URL)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (!response.isSuccessful()){
                    response.close();
                    return;
                }
                final NewHomePageBean homePageBean= (NewHomePageBean) GetBeanClass.getBean(response,NewHomePageBean.class);
                response.close();
                if (homePageBean!=null&&!homePageBean.isSuccess()){
                    ToastUtils.showToast("请求数据失败！");
                    return;
                }


                UiUtils.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        for (NewHomePageBean.ModuleBean moduleBean:homePageBean.getModule()){
                            setViewByType(moduleBean);
                        }
                    }
                });
            }
        });
    }


    /**
     * 根据不同的type加载不同的视图
     */
    private void setViewByType(NewHomePageBean.ModuleBean moduleBean) {

        switch (moduleBean.getStepType()){
            case 10://轮播图
                HomeBannerView homeBannerView=new HomeBannerView(mainActivity);
                homeBannerView.fillView(moduleBean.getPicList(),container,currentTime,color);
                break;
            case 17://分类
                HomeCategoryView homeCategoryView=new HomeCategoryView(mainActivity);
                homeCategoryView.fillView(moduleBean.getCatList(),container,currentTime,color);
                break;
            case 12://单图
                HomeSingleImgView homeSingleImgView=new HomeSingleImgView(mainActivity);
                homeSingleImgView.fillView(moduleBean.getPicList(),container,currentTime,color);
                break;
            case 18://多段的限时抢购
                HomeTimeLimitView homeTimeLimitView=new HomeTimeLimitView(mainActivity);
                homeTimeLimitView.fillView(moduleBean,container,currentTime,color);
                break;
            case 19://倒计时抢购
                HomeTimeCountDownView homeTimeCountDownView=new HomeTimeCountDownView(mainActivity);
                homeTimeCountDownView.fillView(moduleBean,container,currentTime,color);
                break;
            case 13://1+3n
                Home1_3nView home1_3nView=new Home1_3nView(mainActivity);
                home1_3nView.fillView(moduleBean.getItems(),container,currentTime,color);
                break;
            case 21://5n
                Home5nView home5nView=new Home5nView(mainActivity,getChildFragmentManager());
                home5nView.fillView(moduleBean.getItems(),container,currentTime,color);
                break;
            case 15://垂直方向list
                HomeTitleListView homeTitleListView=new HomeTitleListView(mainActivity);
                homeTitleListView.fillView(moduleBean,container,currentTime,color);
                break;
            case 20://2n
                Home2nView home2nView=new Home2nView(mainActivity);
                home2nView.fillView(moduleBean.getPicList(),container,currentTime,color);
                break;
            case 22://鲜在时快报
                HomeNewsView homeNewsView=new HomeNewsView(mainActivity);
                homeNewsView.fillView(moduleBean.getContentList(),container,currentTime,color);
                break;
        }
    }



    /**
     *  根据楼层类型展示数据
     */
    private void setDownDataByType(int type,Home2DownBean.ModuleBean downModuleBean) {
        switch (type){
            /*case 12://单图
                GlideUtils.LoadImage(getActivity(),downModuleBean.getPicList().get(0).getPicUrl(),openShoppingimg);
                openShoppingimg.setOnClickListener(this);
                break;
            case 13://1+3n
                View openView=inflater.inflate(R.layout.shopping_crazy_view,null);
                theOpenContainer.addView(openView);
                new OpenCrazyAdapter(getActivity(),downModuleBean,openView);
                break;
            case 14://单图+5n
                View theNewView=inflater.inflate(R.layout.taste_new_view,null);
                ImageView banner= (ImageView) theNewView.findViewById(R.id.home_the_new_banner);
                banner.setOnClickListener(this);
                home2Container.addView(theNewView);
                new TasteNewAdapter(getActivity(),downModuleBean,theNewView,getChildFragmentManager());
                break;
            case 15://文字列表
                View view3 = UiUtils.getLayoutInflater().inflate(R.layout.foor_three, null);
                home2Container.addView(view3);
                new HotSaleAdapter().set(getActivity(),view3,downModuleBean);
                break;*/
        }
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()){

            case R.id.actionbar_search://搜索
                getActivity().startActivity(new Intent(getActivity(), SearchActivity.class));
                break;
            case R.id.actionbar_address://地址
                getActivity().startActivity(new Intent(getActivity(), DistributionActivity.class));
                break;
            case R.id.actionbar_scanner://扫一扫
                Intent intent1 = new Intent(getActivity(), CaptureActivity.class);
                getActivity().startActivity(intent1);
                break;
        }
    }



    /**
     *  新人大礼包 浮层
     */
    private void newGiftDialog(){

        LayoutInflater inflater=getActivity().getLayoutInflater();
        View view=inflater.inflate(R.layout.activity_new_gift,null);
        final Dialog dialog=new Dialog(getActivity(),R.style.Dialog);
        dialog.setCancelable(false);
        dialog.setContentView(view);
        Button sure= (Button) view.findViewById(R.id.newGift_goLook);
        Button cancel= (Button) view.findViewById(R.id.newGift_cancel);
        sure.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(getActivity(),CouponsActivity.class));
                dialog.dismiss();
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.show();
        //Animation animation= AnimationUtils.loadAnimation(this,R.anim.animation_store_detail);
        Window dialogWindow = dialog.getWindow();
        WindowManager m = getActivity().getWindowManager();
        Display d = m.getDefaultDisplay(); // 获取屏幕宽、高度
        WindowManager.LayoutParams p = dialogWindow.getAttributes(); // 获取对话框当前的参数值
        p.height = d.getHeight(); // 高度设置为屏幕的高度，根据实际情况调整
        p.width = d.getWidth(); // 宽度设置为屏幕的宽度，根据实际情况调整
        dialogWindow.setAttributes(p);
    }

    @Override
    public void onPause() {
        super.onPause();
        //友盟统计
        MobclickAgent.onPageEnd("Home");
    }
}
