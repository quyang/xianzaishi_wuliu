package com.xianzaishi.normandie.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.xianzaishi.normandie.R;
import com.xianzaishi.normandie.adapter.AchievementListViewAdapter;
import com.xianzaishi.normandie.bean.AchievementBean;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class DetailAchievementFragment extends Fragment {

    private ListView listView;
    private int height;
    private ArrayList<AchievementBean.ModuleBean.AchievementDetailBean> list;
    public DetailAchievementFragment() {
        // Required empty public constructor
    }

    @Override
    public void setArguments(Bundle args) {
        super.setArguments(args);
        list= (ArrayList<AchievementBean.ModuleBean.AchievementDetailBean>) args.getSerializable("list");
        height=args.getInt("height");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_deatail_achievement, container, false);
        listView= (ListView) view.findViewById(R.id.achievement_list_view);
        listView.setAdapter(new AchievementListViewAdapter(list,getActivity(),height));
        return view;
    }

}
