package com.xianzaishi.normandie.fragment;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.alipay.sdk.app.PayTask;
import com.xianzaishi.normandie.MyApplication;
import com.xianzaishi.normandie.MyOrderActivity;
import com.xianzaishi.normandie.OrderActivity;
import com.xianzaishi.normandie.R;
import com.xianzaishi.normandie.adapter.OrderAdapter;
import com.xianzaishi.normandie.adapter.SwipeListener;
import com.xianzaishi.normandie.bean.GoPayBean;
import com.xianzaishi.normandie.bean.OrdersListBean01;
import com.xianzaishi.normandie.common.Urls;
import com.xianzaishi.normandie.global.Contants;
import com.xianzaishi.normandie.utils.GetBeanClass;
import com.xianzaishi.normandie.utils.SPUtils;
import com.xianzaishi.normandie.utils.UiUtils;

import java.io.IOException;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * quyang all orders
 * Created by wsl on 2016/8/19.
 */
public class AllOrderFragment extends BaseFragment {

    public View view;

    public RecyclerView recycler;


    private SwipeRefreshLayout mSwipeRefreshWidget;

    private OrderAdapter adapter;

    private LinearLayoutManager manager;

    public boolean isLoaidng = false;

    private OkHttpClient client = MyApplication.getOkHttpClient();

    private int mCurrentPage;

    private int mTotal;

    private List<OrdersListBean01.DataBean.ObjectsBean> mAllOrderInfo;

    @Override
    public void initData() {

        //获取数据集合,从所依靠的activity
        MyOrderActivity orderActivity = (MyOrderActivity) this.activity;
        OrdersListBean01 allBean = orderActivity.getAllOrderInfo();
        OrdersListBean01.DataBean dataBean = allBean.data;
        if (dataBean != null) {
            OrdersListBean01.DataBean.PaginationBean pagination = allBean.data.pagination;
            //当前页
            mCurrentPage = pagination.currentPage;


            //订单总数
            mTotal = pagination.total;

            mAllOrderInfo = allBean.data.objects;

            //设置适配器
            adapter = new OrderAdapter(mAllOrderInfo, activity);
            recycler.setAdapter(adapter);

            //设置swipelayout
            FormBody body = new FormBody.Builder()
                    .add(Contants.UID, "" + SPUtils.getStringValue(activity, Contants.SP_NAME, Contants.UID, null))
                    .add("pageSize", "10")
                    .add("curPage", mCurrentPage + 1 + "")
                    .add("status", "0")
                    .build();

            SwipeListener swipeListener = new SwipeListener(mAllOrderInfo, adapter, mSwipeRefreshWidget,
                    activity, body, Contants.ORDER_LIST, mCurrentPage, mTotal);
            swipeListener.inimSwipeRefreshWidget();
        }


    }

    @Override
    public View initView() {

        //填充布局
        view = UiUtils.inflateView(R.layout.myorderlistview);

        //找到组件
        recycler = (RecyclerView) view.findViewById(R.id.recycler);
        mSwipeRefreshWidget = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_widge);

        //设置布局管理器
        manager = new LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false);
        recycler.setLayoutManager(manager);

        return view;
    }


    //访问服务器获取订单号
    private void getOrderInfo() {
        String uid = SPUtils.getStringValue(activity, Contants.SP_NAME, Contants.UID, null);

        String url = "http://trade.xianzaishi.net/sc/toOrder.json?" +
                "uid=" + uid + "&items=items" + "&couponId=couponId" + "&adderssId=adderssId";
        Request request = new Request.Builder()
                .url(url)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (!response.isSuccessful()) throw new IOException("Unexpected code" + response);
                final GoPayBean goPayBean = (GoPayBean) GetBeanClass.getBean(response, GoPayBean.class);
                response.close();
                if (goPayBean.getCode() == 1) {
                    oid = goPayBean.getData();
//                    handler.sendEmptyMessage(1);
                }
            }
        });
    }

    String oid;

    //阿里支付
    private void toAliPay(final OrderActivity orderActivity) {


        Request request = new Request.Builder()
                .url(String.format(Urls.GET_ALIPAY_SIGN, oid))
                .build();
        client.newCall(request).enqueue(new okhttp3.Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (!response.isSuccessful()) throw new IOException("Unexpected code" + response);
                final GoPayBean goPayBean = (GoPayBean) GetBeanClass.getBean(response, GoPayBean.class);
                response.close();
                if (goPayBean.getCode() == 1) {
                    String payInfo = goPayBean.getData();
                    // 构造PayTask 对象
                    PayTask alipay = new PayTask(orderActivity);
                    // 调用支付接口，获取支付结果
                    String result = alipay.pay(payInfo, true);

//                    Message msg = new Message();
//                    msg.what = SDK_PAY_FLAG;
//                    msg.obj = result;
//                    orderActivity.handler.sendMessage(msg);
                }
            }
        });
    }

}
