package com.xianzaishi.normandie.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.umeng.analytics.MobclickAgent;
import com.xianzaishi.normandie.GoodsDetailsActivity;
import com.xianzaishi.normandie.MainActivity;
import com.xianzaishi.normandie.MyApplication;
import com.xianzaishi.normandie.R;
import com.xianzaishi.normandie.adapter.CategoryIndicatorAdapter;
import com.xianzaishi.normandie.adapter.CategoryIndicatorListViewAdapter;
import com.xianzaishi.normandie.adapter.CategoryListViewAdapter;
import com.xianzaishi.normandie.bean.CategoryListViewBean;
import com.xianzaishi.normandie.bean.LeafCategoryBean;
import com.xianzaishi.normandie.common.Urls;
import com.xianzaishi.normandie.global.Contants;
import com.xianzaishi.normandie.utils.GetBeanClass;
import com.xianzaishi.normandie.utils.GridSpacingItemDecoration;
import com.xianzaishi.normandie.utils.UiUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * A simple {@link Fragment} subclass.
 * 外卖页
 */
public class TakeOutFragment extends Fragment {
    private int catID=10002;
    private RecyclerView recyclerViewIndicator;
    private ListView listView;
    private static int SPAN_COUNT=4;
    private okhttp3.OkHttpClient client;
    private List<LeafCategoryBean.ModuleBean.CategoriesBean> list;
    private RelativeLayout footView;
    private int[] parentLocation,endLocation;
    private int visibleLastIndex = 0;   //最后的可视项索引
    private int PageNumber=1;
    private List<CategoryListViewBean.ModuleBean.ItemsBean> list2;
    private List<LeafCategoryBean.ModuleBean.ItemsBean> list3;
    private CategoryIndicatorListViewAdapter adapter;
    private TextView emptyView;
    private MainActivity activity;

    @Override
    public void onResume() {
        super.onResume();
        MobclickAgent.onPageStart("LeafCategoryFragment");
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_take_out, container, false);
        initView(view);
        netWork();
        return view;
    }

    private void initView(View view) {
        activity= (MainActivity) getActivity();
        footView= (RelativeLayout) activity.findViewById(R.id.main_root_view);
        RadioButton shoppingCar= (RadioButton) activity.findViewById(R.id.shoppingTrolley_button);
        parentLocation=new int[2];
        endLocation=new int[2];
        footView.getLocationInWindow(parentLocation);
        shoppingCar.getLocationInWindow(endLocation);


        client= MyApplication.getOkHttpClient();
        LayoutInflater inflater=activity.getLayoutInflater();
        View headerView=inflater.inflate(R.layout.leaf_category_header,null);
        listView= (ListView) view.findViewById(R.id.leafCategory_listView);
        listView.addHeaderView(headerView,null,false);
        emptyView= (TextView) view.findViewById(R.id.leafCategory_emptyView);//空布局
        /**
         *  导航相关
         */
        recyclerViewIndicator= (RecyclerView) headerView.findViewById(R.id.leafCategory_indicator);
        GridLayoutManager gridLayoutManager=new GridLayoutManager(activity,SPAN_COUNT);
        recyclerViewIndicator.setLayoutManager(gridLayoutManager);
        recyclerViewIndicator.addItemDecoration(new GridSpacingItemDecoration(SPAN_COUNT,20,false));
    }

    /**
     *  访问服务器 获取导航以及listView的数据
     */
    private void netWork() {
        Request request=new Request.Builder()
                .url(String.format(Urls.LEAF_CATEGORY,catID,0))
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
            }
            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if(!response.isSuccessful()) throw new IOException("Unexpected code"+response);
                final LeafCategoryBean lcBean= (LeafCategoryBean) GetBeanClass.getBean(response,LeafCategoryBean.class);
                response.close();
                UiUtils.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (lcBean != null && lcBean.getModule() != null) {
                            List<LeafCategoryBean.ModuleBean.CategoriesBean> list1 = lcBean.getModule().getCategories();


                            list = new ArrayList<LeafCategoryBean.ModuleBean.CategoriesBean>();
                            LeafCategoryBean.ModuleBean.CategoriesBean categoriesBean = new LeafCategoryBean.ModuleBean.CategoriesBean();
                            categoriesBean.setCatName("全部");
                            categoriesBean.setCatId(-2);
                            list.add(categoriesBean);
                            for (LeafCategoryBean.ModuleBean.CategoriesBean categoriesBean1 : list1) {
                                list.add(categoriesBean1);
                            }
                            int size = list.size();
                            /**
                             * 默认选中导航第一项
                             */
                            for (int i = 0; i < size; i++) {
                                if (i == 0) {
                                    list.get(i).setSelect(true);
                                } else {
                                    list.get(i).setSelect(false);
                                }
                            }
                            /**
                             *  根据size判断导航是否需要展开
                             */
                            if (size < 9) {
                                noExpandIndicator();
                            } else {
                                expandIndicator();
                            }

                            list3 = lcBean.getModule().getItems();
                            /**
                             *  当数据为空时显示伪空布局
                             */
                            if (list3 == null) {
                                emptyView.setVisibility(View.VISIBLE);
                            } else {
                                emptyView.setVisibility(View.GONE);
                            }


                            final CategoryListViewAdapter clvAdapter = new CategoryListViewAdapter(activity, list3, parentLocation, endLocation, footView);
                            listView.setAdapter(clvAdapter);
                            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                @Override
                                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                                    MobclickAgent.onEvent(activity, "tapItem");

                                    i = i - 1;
                                    int itemId = list3.get(i).getItemId();
                                    Intent intent = new Intent(activity, GoodsDetailsActivity.class);
                                    intent.putExtra(Contants.ITEM_ID, itemId);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(intent);
                                }
                            });

                            /**
                             *  listView分页加载
                             */
                            listView.setOnScrollListener(new AbsListView.OnScrollListener() {
                                @Override
                                public void onScroll(AbsListView absListView, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                                    visibleLastIndex = firstVisibleItem + visibleItemCount - 1;
                                }

                                @Override
                                public void onScrollStateChanged(AbsListView absListView, int scrollState) {
                                    int itemsLastIndex = clvAdapter.getCount() - 1 + 1;    //数据集最后一项的索引,加上头布局
                                    //int lastIndex = itemsLastIndex + 1;             //加上底部的loadMoreView项
                                    if (scrollState == AbsListView.OnScrollListener.SCROLL_STATE_IDLE && visibleLastIndex == itemsLastIndex) {
                                        String s = String.format(Urls.LEAF_CATEGORY, catID, PageNumber++);
                                        Request request = new Request.Builder()
                                                .url(s)
                                                .build();
                                        client.newCall(request).enqueue(new Callback() {
                                            @Override
                                            public void onFailure(Call call, IOException e) {
                                            }

                                            @Override
                                            public void onResponse(Call call, Response response) throws IOException {
                                                if (!response.isSuccessful())
                                                    throw new IOException("Unexpected code" + response);
                                                final LeafCategoryBean lcBean = (LeafCategoryBean) GetBeanClass.getBean(response, LeafCategoryBean.class);
                                                response.close();
                                                UiUtils.runOnUiThread(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        if (lcBean!=null&&lcBean.isSuccess()) {
                                                            List<LeafCategoryBean.ModuleBean.ItemsBean> list4 = lcBean.getModule().getItems();
                                                            if (list4 != null) {
                                                                list3.addAll(list4);
                                                                clvAdapter.notifyDataSetChanged();
                                                            } else {
                                                                UiUtils.runOnUiThread(new Runnable() {
                                                                    @Override
                                                                    public void run() {
                                                                        Toast.makeText(activity, "没有更多数据了！", Toast.LENGTH_SHORT).show();
                                                                        PageNumber--;
                                                                    }
                                                                });
                                                            }
                                                        }
                                                    }
                                                });
                                            }
                                        });
                                    }
                                }
                            });
                        }
                    }
                });
            }
        });
    }

    /**
     *  不需要展开的导航
     */
    private void noExpandIndicator() {
        CategoryIndicatorAdapter indicatorAdapter=new CategoryIndicatorAdapter(activity,list);
        recyclerViewIndicator.setAdapter(indicatorAdapter);
        setIndicatorClick(indicatorAdapter);
    }

    /**
     * 需要展开的导航
     */
    private void expandIndicator() {
        List<LeafCategoryBean.ModuleBean.CategoriesBean> listMoreThan8e=new ArrayList<LeafCategoryBean.ModuleBean.CategoriesBean>();
        for (int i = 0; i < 8; i++) {
            if (i==7){
                LeafCategoryBean.ModuleBean.CategoriesBean categoriesBean=new LeafCategoryBean.ModuleBean.CategoriesBean();
                categoriesBean.setCatId(0);
                categoriesBean.setCatName("更多");
                listMoreThan8e.add(categoriesBean);
            }else{
                listMoreThan8e.add(list.get(i));
            }
        }
        CategoryIndicatorAdapter indicatorAdapter=new CategoryIndicatorAdapter(activity,listMoreThan8e);
        recyclerViewIndicator.setAdapter(indicatorAdapter);
        setIndicatorClick(indicatorAdapter);
    }
    /**
     *  从折叠到展开的导航
     */
    private void expandedIndicator(){
        int size=list.size();
        List<LeafCategoryBean.ModuleBean.CategoriesBean> listMoreThan8 = new ArrayList<LeafCategoryBean.ModuleBean.CategoriesBean>();
        for (int i1 = 0; i1 < size + 1; i1++) {
            if (i1 == size) {
                LeafCategoryBean.ModuleBean.CategoriesBean categoriesBean = new LeafCategoryBean.ModuleBean.CategoriesBean();
                categoriesBean.setCatId(-1);
                categoriesBean.setCatName("收起");
                listMoreThan8.add(categoriesBean);
            } else {
                listMoreThan8.add(list.get(i1));
            }
        }
        CategoryIndicatorAdapter indicatorAdapter=new CategoryIndicatorAdapter(activity,listMoreThan8);
        recyclerViewIndicator.setAdapter(indicatorAdapter);
        setIndicatorClick(indicatorAdapter);
    }

    /**
     * 导航的点击
     */
    private void setIndicatorClick(final CategoryIndicatorAdapter indicatorAdapter) {
        indicatorAdapter.setOnItemClickListener(new CategoryIndicatorAdapter.OnItemClickListener() {
            @Override
            public void OnItemClickListener(View view, int position) {
                /**
                 *  选中项改变选中状态 其它项复位
                 */
                if (indicatorAdapter.getList().get(position).getCatId()==0) {//此项为展开更多
                    expandedIndicator();
                } else if (indicatorAdapter.getList().get(position).getCatId()==-1) {
                    expandIndicator();
                } else {
                    PageNumber=1;
                    int size=list.size();
                    for (int i = 0; i < size; i++) {
                        if (i == position) {
                            list.get(i).setSelect(true);
                        } else {
                            list.get(i).setSelect(false);
                        }
                    }
                    if(position==0){
                        setAllList();
                    }else {
                        setList(position);
                    }

                    indicatorAdapter.notifyDataSetChanged();
                }
            }
        });
    }

    /**
     *  点击导航第一项即“全部”list展示的内容
     */
    private void setAllList() {
        Request request=new Request.Builder()
                .url(String.format(Urls.LEAF_CATEGORY,catID,0))
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (!response.isSuccessful())
                    throw new IOException("Unexpected code" + response);
                final LeafCategoryBean lcBean = (LeafCategoryBean) GetBeanClass.getBean(response, LeafCategoryBean.class);
                response.close();
                UiUtils.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (lcBean != null && lcBean.getModule() != null) {
                            list3 = lcBean.getModule().getItems();
                            /**
                             *  当数据为空时显示伪空布局
                             */
                            if (list3 == null) {
                                emptyView.setVisibility(View.VISIBLE);
                            } else {
                                emptyView.setVisibility(View.GONE);
                            }


                            final CategoryListViewAdapter clvAdapter = new CategoryListViewAdapter(activity, list3, parentLocation, endLocation, footView);
                            listView.setAdapter(clvAdapter);
                            /**
                             *  listView分页加载
                             */
                            listView.setOnScrollListener(new AbsListView.OnScrollListener() {
                                @Override
                                public void onScroll(AbsListView absListView, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                                    visibleLastIndex = firstVisibleItem + visibleItemCount - 1;

                                }

                                @Override
                                public void onScrollStateChanged(AbsListView absListView, int scrollState) {
                                    int itemsLastIndex = clvAdapter.getCount() - 1 + 1;    //数据集最后一项的索引，加上头布局
                                    //int lastIndex = itemsLastIndex + 1;             //加上底部的loadMoreView项
                                    if (scrollState == AbsListView.OnScrollListener.SCROLL_STATE_IDLE && visibleLastIndex == itemsLastIndex) {
                                        String s = String.format(Urls.LEAF_CATEGORY, catID, PageNumber++);
                                        Request request = new Request.Builder()
                                                .url(s)
                                                .build();
                                        client.newCall(request).enqueue(new Callback() {
                                            @Override
                                            public void onFailure(Call call, IOException e) {

                                            }

                                            @Override
                                            public void onResponse(Call call, Response response) throws IOException {
                                                if (!response.isSuccessful())
                                                    throw new IOException("Unexpected code" + response);
                                                final LeafCategoryBean lcBean = (LeafCategoryBean) GetBeanClass.getBean(response, LeafCategoryBean.class);
                                                response.close();
                                                UiUtils.runOnUiThread(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        if (lcBean!=null&&lcBean.isSuccess()) {
                                                            List<LeafCategoryBean.ModuleBean.ItemsBean> list4 = lcBean.getModule().getItems();
                                                            if (list4 != null) {
                                                                list3.addAll(list4);
                                                                clvAdapter.notifyDataSetChanged();
                                                            } else {
                                                                UiUtils.runOnUiThread(new Runnable() {
                                                                    @Override
                                                                    public void run() {
                                                                        Toast.makeText(activity, "没有更多数据了！", Toast.LENGTH_SHORT).show();
                                                                        PageNumber--;
                                                                    }
                                                                });
                                                            }
                                                        }
                                                    }
                                                });
                                            }
                                        });
                                    }
                                }
                            });
                        }
                    }
                });
            }
        });
    }

    /**
     * 点击导航改变list内容
     */
    private void setList(final int position){
        final MediaType MEDIA_TYPE_MARKDOWN
                = MediaType.parse("text/x-markdown; charset=utf-8");
        final String postBody = "{\"cmCat2Ids\":%d,\"pageSize\":10,\"pageNum\":%d}";
        String postBody1=String.format(postBody,list.get(position).getCatId(),0);
        Request request1=new Request.Builder()
                .url(Urls.INDICATOR_CATEGORY)
                .post(RequestBody.create(MEDIA_TYPE_MARKDOWN,postBody1))
                .build();
        client.newCall(request1).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                UiUtils.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(activity,"网络请求失败！",Toast.LENGTH_SHORT).show();
                    }
                });
            }
            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if(!response.isSuccessful()) throw new IOException("Unexpected code"+response);
                final CategoryListViewBean clvBean=(CategoryListViewBean) GetBeanClass.getBean(response,CategoryListViewBean.class);
                response.close();
                UiUtils.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if(clvBean!=null&&clvBean.getResultCode()==1){
                            list2= clvBean.getModule().getItems();
                            /**
                             *  当数据为空时显示伪空布局
                             */
                            if (list2==null){
                                emptyView.setVisibility(View.VISIBLE);
                            }else {
                                emptyView.setVisibility(View.GONE);
                            }
                            adapter=new CategoryIndicatorListViewAdapter(activity,2,list2,
                                    parentLocation,endLocation,footView);
                            listView.setAdapter(adapter);
                            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                @Override
                                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                                    MobclickAgent.onEvent(activity,"tapItem");

                                    i=i-1;
                                    if(list2.get(i)==null){
                                        return;
                                    }
                                    int itemId=list2.get(i).getItemId();
                                    Intent intent = new Intent(activity, GoodsDetailsActivity.class);
                                    intent.putExtra(Contants.ITEM_ID, itemId);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(intent);
                                }
                            });
                            /**
                             * listView分页加载
                             */
                            listView.setOnScrollListener(new AbsListView.OnScrollListener() {
                                @Override
                                public void onScroll(AbsListView absListView, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                                    visibleLastIndex = firstVisibleItem + visibleItemCount - 1;
                                }

                                @Override
                                public void onScrollStateChanged(AbsListView absListView,int scrollState) {
                                    int itemsLastIndex = adapter.getCount() - 1+1;    //数据集最后一项的索引 加上头布局
                                    //int lastIndex = itemsLastIndex + 1;             //加上底部的loadMoreView项
                                    if (scrollState == AbsListView.OnScrollListener.SCROLL_STATE_IDLE && visibleLastIndex ==itemsLastIndex) {
                                        //异步加载数据的代码
                                        Request request1=new Request.Builder()
                                                .url(Urls.INDICATOR_CATEGORY)
                                                .post(RequestBody.create(MEDIA_TYPE_MARKDOWN,String.format(postBody,list.get(position).getCatId(),PageNumber++)))
                                                .build();
                                        client.newCall(request1).enqueue(new Callback() {
                                            @Override
                                            public void onFailure(Call call, IOException e) {

                                            }

                                            @Override
                                            public void onResponse(Call call, Response response) throws IOException {
                                                if(!response.isSuccessful()) throw new IOException("Unexpected code"+response);
                                                final CategoryListViewBean clvBean= (CategoryListViewBean) GetBeanClass.getBean(response,CategoryListViewBean.class);
                                                response.close();
                                                UiUtils.runOnUiThread(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        if (clvBean!=null&&clvBean.getResultCode()==1){
                                                            List<CategoryListViewBean.ModuleBean.ItemsBean> list3= clvBean.getModule().getItems();
                                                            if (list3!=null&adapter!=null){
                                                                list2.addAll(list3);
                                                                adapter.notifyDataSetChanged();
                                                            }else {
                                                                UiUtils.runOnUiThread(new Runnable() {
                                                                    @Override
                                                                    public void run() {
                                                                        Toast.makeText(activity,"没有更多数据了！",Toast.LENGTH_SHORT).show();
                                                                    }
                                                                });
                                                            }
                                                        }
                                                    }
                                                });

                                            }
                                        });
                                    }
                                }
                            });
                        }
                    }
                });
            }
        });
    }

    @Override
    public void onPause() {
        super.onPause();

        MobclickAgent.onPageEnd("LeafCategoryFragment");
    }
}
