package com.xianzaishi.normandie.fragment;


import android.app.Dialog;
import android.content.ComponentName;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.umeng.analytics.MobclickAgent;
import com.xianzaishi.normandie.AddressActivity;
import com.xianzaishi.normandie.CouponsActivity;
import com.xianzaishi.normandie.ExchangeInvitationActivity;
import com.xianzaishi.normandie.GeRenCenerActivity;
import com.xianzaishi.normandie.MainActivity;
import com.xianzaishi.normandie.MyApplication;
import com.xianzaishi.normandie.MyCreditActivity;
import com.xianzaishi.normandie.MyMessageActivity;
import com.xianzaishi.normandie.MyOrderNewActivity;
import com.xianzaishi.normandie.NewLoginActivity;
import com.xianzaishi.normandie.R;
import com.xianzaishi.normandie.ServiceHelpActivity;
import com.xianzaishi.normandie.SetActivity;
import com.xianzaishi.normandie.ShareForCouponsActivity;
import com.xianzaishi.normandie.bean.CreditBean;
import com.xianzaishi.normandie.bean.MineDataBean;
import com.xianzaishi.normandie.common.Urls;
import com.xianzaishi.normandie.global.Contants;
import com.xianzaishi.normandie.utils.GetBeanClass;
import com.xianzaishi.normandie.utils.SPUtils;
import com.xianzaishi.normandie.utils.ToastUtils;
import com.xianzaishi.normandie.utils.UiUtils;
import com.xianzaishi.normandie.zxing.encoding.EncodingHandler;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 *  “我的” 界面
 */
public class MineFragment extends Fragment implements View.OnClickListener{
    private Button buttonLogin;
    private LinearLayout order,discount,address,message,help,set,unLogin,credit,invitation,exchange;
    private RelativeLayout logined;
    private ImageView mineScanner,discountPoint,messagePoint;
    private TextView mineUser,mineMember,mineDetail;
//    private SimpleDraweeView headIcon;
    private ImageView headIcon;
    private MediaType MEDIA_TYPE_MARKDOWN;
    private String token;
    private boolean isFirstIn=true;
    private boolean isCreatQRSuccess;
    private String uid=null;
    private Bitmap QR;
    private OkHttpClient client= MyApplication.getOkHttpClient();
    private MineDataBean mineDataBean;
    private MainActivity mainActivity;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_mine, container, false);
        initView(view);
        netWork();
        return view;
    }

    private void initView(View view) {
        mainActivity= (MainActivity) getActivity();
        SPUtils.putBooleanValue(MyApplication.getContext(),Contants.SP_NAME,"isCreatQRSuccess",false);
        ImageView aboveLogin= (ImageView) view.findViewById(R.id.image1);
        aboveLogin.setOnClickListener(this);
        buttonLogin= (Button) view.findViewById(R.id.mine_login_button);
        buttonLogin.setOnClickListener(this);
        invitation= (LinearLayout) view.findViewById(R.id.mine_invitation);
        invitation.setOnClickListener(this);
        exchange= (LinearLayout) view.findViewById(R.id.mine_exchange);
        exchange.setOnClickListener(this);
        order= (LinearLayout) view.findViewById(R.id.mine_order);
        order.setOnClickListener(this);
        discount= (LinearLayout) view.findViewById(R.id.mine_discount);
        discount.setOnClickListener(this);
        address= (LinearLayout) view.findViewById(R.id.mine_address);
        address.setOnClickListener(this);
        message= (LinearLayout) view.findViewById(R.id.mine_message);
        message.setOnClickListener(this);
        help= (LinearLayout) view.findViewById(R.id.mine_help);
        help.setOnClickListener(this);
        set= (LinearLayout) view.findViewById(R.id.mine_set);
        set.setOnClickListener(this);
        credit= (LinearLayout) view.findViewById(R.id.mine_credit);
        credit.setOnClickListener(this);
//        headIcon= (SimpleDraweeView) view.findViewById(R.id.head_icon);//用户头像
        headIcon= (ImageView) view.findViewById(R.id.head_icon);//用户头像
        headIcon.setOnClickListener(this);//点击头像进入个人中心
        mineScanner= (ImageView) view.findViewById(R.id.mine_scanner);//显示验证用户身份的二维码
        mineScanner.setOnClickListener(this);//点击二维码显示大的二维码
        unLogin= (LinearLayout) view.findViewById(R.id.mine_unLogin);//未登录时显示登录按钮的布局
        logined= (RelativeLayout) view.findViewById(R.id.mine_already_login);//登录后显示用户信息
        discountPoint= (ImageView) view.findViewById(R.id.mine_discount_point);//优惠提示红点
        messagePoint= (ImageView) view.findViewById(R.id.mine_message_point);//我的消息提示红点
        mineUser= (TextView) view.findViewById(R.id.mine_user);//用户名
        mineMember= (TextView) view.findViewById(R.id.text_member);//用户积分数
        mineDetail= (TextView) view.findViewById(R.id.mine_detail);//显示有几张优惠券
    }

    @Override
    public void onResume() {
        super.onResume();
        uid=SPUtils.getStringValue(MyApplication.getContext(),Contants.SP_NAME,Contants.UID,null);
        if (isFirstIn){
            isFirstIn=false;
        }else {
            netWork();
        }
        isCreatQRSuccess=SPUtils.getBooleanValue(MyApplication.getContext(),Contants.SP_NAME,"isCreatQRSuccess",false);
        if (uid!=null&&!isCreatQRSuccess){
            String QRStr=String.format(Contants.QRSTING,uid);
            creatQR(QRStr);
            SPUtils.putBooleanValue(MyApplication.getContext(),Contants.SP_NAME,"isCreatQRSuccess",true);
        }
        if (uid!=null){
            getCredit();
        }

        //友盟统计
        MobclickAgent.onPageStart("Mine");
    }

    private void getCredit(){
        RequestBody formBody=new FormBody.Builder()
                .add("uid",uid)
                .build();
        /**
         *  当前积分
         */
        Request request1 = new Request.Builder()
                .url(Urls.GET_CREDIT)
                .post(formBody)
                .build();
        client.newCall(request1).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
            }
            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if(!response.isSuccessful()) throw new IOException("Unexpected code"+response);
                final CreditBean creditBean= (CreditBean) GetBeanClass.getBean(response,CreditBean.class);
                response.close();
                UiUtils.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (creditBean!=null&&creditBean.getCode()==1){
                            String credits=String.valueOf(creditBean.getData());
                            mineMember.setText("鲜在时积分："+credits);
                        }
                    }
                });

            }
        });
    }

    @Override
    public void onPause() {
        super.onPause();
        MobclickAgent.onPageEnd("Mine");
    }

    private void creatQR(String str) {
        QR= EncodingHandler.createQRCode(str,1000);
        mineScanner.setImageBitmap(QR);
    }

    private void netWork() {
        token= SPUtils.getStringValue(MyApplication.getContext(), Contants.SP_NAME,"token",null);
        MEDIA_TYPE_MARKDOWN=MediaType.parse("text/x-markdown;charset=utf-8");
        String postBody="{\"token\":\"%s\"}";
        Request request=new Request.Builder()
                .url(Urls.MINE_DATA)
                .post(RequestBody.create(MEDIA_TYPE_MARKDOWN,String.format(postBody,token)))
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                ToastUtils.showToast("请求失败！");
            }
            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if(!response.isSuccessful()) throw new IOException("Unexpected code"+response);
                mineDataBean= (MineDataBean) GetBeanClass.getBean(response,
                        MineDataBean.class);
                response.close();
                mainActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if(mineDataBean.getResultCode()==1){
                            unLogin.setVisibility(View.GONE);
                            logined.setVisibility(View.VISIBLE);
                            //设置头像
                            headIcon.setImageResource(R.mipmap.headicon);
                            //String pic = "http://139.196.92.240/itemcenter/image/floor2.jpg";
                            //Uri uri = Uri.parse(pic);
//                            headIcon.setImageURI(uri);
                            //GlideUtils.LoadImage(UiUtils.getContext(),pic,headIcon);
                            MineDataBean.ModuleBean moduleBean=mineDataBean.getModule();
                            SPUtils.putStringValue(MyApplication.getContext(),Contants.SP_NAME,"userName",moduleBean.getName());
                            mineUser.setText(moduleBean.getName());
                            /**
                             * 优惠券显示控制
                             */
                            if(moduleBean.getCouponCount()==0){
                                discountPoint.setVisibility(View.INVISIBLE);
                                mineDetail.setText("");
                            }else {
                                discountPoint.setVisibility(View.VISIBLE);
                                mineDetail.setText(moduleBean.getCouponCount()+"张");
                            }
                            /**
                             * 我的消息显示控制
                             */
                            if(moduleBean.getMessageCount()==0){
                                messagePoint.setVisibility(View.INVISIBLE);
                            }else {
                                messagePoint.setVisibility(View.VISIBLE);
                            }
                        }else {
                            unLogin.setVisibility(View.VISIBLE);
                            logined.setVisibility(View.GONE);
                            SPUtils.removeData(MyApplication.getContext(),Contants.SP_NAME,Contants.TOKEN);
                            SPUtils.removeData(MyApplication.getContext(), Contants.SP_NAME,"uid");
                            SPUtils.removeData(MyApplication.getContext(),Contants.SP_NAME,"isNewUser");
                        }
                    }
                });

            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.mine_login_button:
                startActivity(new Intent(mainActivity,NewLoginActivity.class));
                break;
            case R.id.image1:
                startActivity(new Intent(mainActivity,NewLoginActivity.class));
                break;
            case R.id.mine_order:
                if (uid==null){
                    startActivity(new Intent(mainActivity,NewLoginActivity.class));
                }else {
                    /*Intent intent=new Intent();
                    String packageName="com.xianzaishi.normandie";
                    String activityName=".MyOrderNewActivity";
                    intent.setComponent(new ComponentName(packageName,packageName+activityName));
                    mainActivity.startActivity(intent);*/
                    mainActivity.startActivity(new Intent(mainActivity,MyOrderNewActivity.class));
                }
                break;
            case R.id.mine_discount:
                if (uid==null){
                    startActivity(new Intent(mainActivity,NewLoginActivity.class));
                }else {
                    mainActivity.startActivity(new Intent(mainActivity, CouponsActivity.class));
                }
                break;
            case R.id.mine_address:
                if (uid==null){
                    startActivity(new Intent(mainActivity,NewLoginActivity.class));
                }else {
                    mainActivity.startActivity(new Intent(mainActivity, AddressActivity.class));
                }
                break;
            case R.id.mine_message:
                if (uid==null){
                    startActivity(new Intent(mainActivity,NewLoginActivity.class));
                }else {
                    mainActivity.startActivity(new Intent(mainActivity, MyMessageActivity.class));
                }
                break;
            case R.id.mine_help://客服与帮助
                mainActivity.startActivity(new Intent(mainActivity, ServiceHelpActivity.class));
                break;
            case R.id.mine_set:
                mainActivity.startActivity(new Intent(mainActivity, SetActivity.class));
                break;
            case R.id.mine_credit://我的积分
                if (uid==null){
                    startActivity(new Intent(mainActivity,NewLoginActivity.class));
                }else {
                    mainActivity.startActivity(new Intent(mainActivity, MyCreditActivity.class));
                }
                break;
            case R.id.mine_invitation://邀请好友
                if (uid==null){
                    startActivity(new Intent(mainActivity,NewLoginActivity.class));
                }else {
                    mainActivity.startActivity(new Intent(mainActivity, ShareForCouponsActivity.class));
                }
                break;
            case R.id.mine_exchange:
                if (uid==null){
                    startActivity(new Intent(mainActivity,NewLoginActivity.class));
                }else {
                    mainActivity.startActivity(new Intent(mainActivity, ExchangeInvitationActivity.class));
                }
                break;
            case R.id.head_icon://点击头像进入个人中心
                if (uid==null){
                    startActivity(new Intent(mainActivity,NewLoginActivity.class));
                }else {
                    Intent intent = new Intent(mainActivity, GeRenCenerActivity.class);
                    String name=mineDataBean.getModule().getName();
                    int sex=mineDataBean.getModule().getSex();
                    String birthday=mineDataBean.getModule().getBirthday();
                    intent.putExtra("name", name);
                    intent.putExtra("sex",sex);
                    intent.putExtra("birthday",birthday);
                    startActivity(intent);
                }
                break;
            case R.id.mine_scanner://扫一扫
                if (QR!=null) {
                    dialog();
                }
                break;
        }
    }







    /**
     *  二维码详情
     */
    private void dialog(){
        LayoutInflater inflater=mainActivity.getLayoutInflater();
        WindowManager m = mainActivity.getWindowManager();
        Display d = m.getDefaultDisplay(); // 获取屏幕宽、高度
        View view=inflater.inflate(R.layout.qr_image,null);
        final Dialog dialog=new Dialog(mainActivity,R.style.Dialog);
        dialog.setCancelable(true);
        dialog.setContentView(view);
        ImageView qrImage= (ImageView) view.findViewById(R.id.qr_image);
        qrImage.getLayoutParams().width= (int) (d.getWidth() * 0.7);
        qrImage.getLayoutParams().height=qrImage.getLayoutParams().width;
        qrImage.setImageBitmap(QR);
        dialog.show();
        Window dialogWindow = dialog.getWindow();
        if(dialogWindow==null){
            return;
        }
        WindowManager.LayoutParams p = dialogWindow.getAttributes(); // 获取对话框当前的参数值
        p.width = (int) (d.getWidth() * 0.7); // 宽度设置为屏幕的0.6，根据实际情况调整
        //p.height = (int) (d.getHeight()*0.7); // 高度设置为屏幕的0.6，根据实际情况调整
        dialogWindow.setGravity(Gravity.CENTER);
        dialogWindow.setAttributes(p);
    }
}
