package com.xianzaishi.normandie.fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.xianzaishi.normandie.R;
import com.xianzaishi.normandie.adapter.StaggeredTasteNewAdapter;
import com.xianzaishi.normandie.bean.Home2DownBean;
import com.xianzaishi.normandie.bean.NewHomePageBean;
import com.xianzaishi.normandie.customs.MyRecyclerView;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class TasteNewFragment extends Fragment {

    private MyRecyclerView recyclerView;
    private final static int SPAN_COUNT=3;
    private ArrayList<NewHomePageBean.ModuleBean.ItemsBean> list;
    public TasteNewFragment() {
        // Required empty public constructor
    }

    @Override
    public void setArguments(Bundle args) {
        super.setArguments(args);
        list= (ArrayList<NewHomePageBean.ModuleBean.ItemsBean>) args.getSerializable("tasteList");


    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_taste_new, container, false);
        initView(view);
        return view;
    }

    private void initView(View view) {
        recyclerView= (MyRecyclerView) view.findViewById(R.id.home_taste_new_recycler_view);
        recyclerView.setLayoutManager(new StaggeredGridLayoutManager(SPAN_COUNT,StaggeredGridLayoutManager.VERTICAL){
            @Override
            public boolean canScrollVertically() {
                return false;
            }
        });
        recyclerView.setAdapter(new StaggeredTasteNewAdapter(getActivity(),list));
    }

}
