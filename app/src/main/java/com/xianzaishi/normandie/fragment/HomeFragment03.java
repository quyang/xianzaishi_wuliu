/*
package com.xianzaishi.normandie.fragment;

import android.app.Dialog;
import android.content.Intent;
import android.content.SyncStatusObserver;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.umeng.analytics.MobclickAgent;
import com.umeng.socialize.utils.SocializeUtils;
import com.xianzaishi.normandie.CaptureActivity;
import com.xianzaishi.normandie.CouponsActivity;
import com.xianzaishi.normandie.DistributionActivity;
import com.xianzaishi.normandie.HuoDongActivity;
import com.xianzaishi.normandie.MainActivity;
import com.xianzaishi.normandie.MyApplication;
import com.xianzaishi.normandie.NewLoginActivity;
import com.xianzaishi.normandie.R;
import com.xianzaishi.normandie.SearchActivity;
import com.xianzaishi.normandie.adapter.AutoRecyclePagerAdapter;
import com.xianzaishi.normandie.adapter.FoorTwoAdapter;
import com.xianzaishi.normandie.adapter.MorePictrueAdapter;
import com.xianzaishi.normandie.adapter.MorePictrueAdapterOne;
import com.xianzaishi.normandie.adapter.NoPicAdapter;
import com.xianzaishi.normandie.adapter.NoPicAdapterTwo;
import com.xianzaishi.normandie.adapter.PicsLieBiaoAdapter;
import com.xianzaishi.normandie.adapter.PicsLieBiaoAdapterTwo;
import com.xianzaishi.normandie.adapter.TextLieBiaoAdapter;
import com.xianzaishi.normandie.adapter.TextLieBiaoAdapterTwo;
import com.xianzaishi.normandie.bean.CategoryBean;
import com.xianzaishi.normandie.bean.HomeDownBean;
import com.xianzaishi.normandie.bean.HomeTopBean;
import com.xianzaishi.normandie.customs.FixedSpeedScroller;
import com.xianzaishi.normandie.customs.MyRefreshHeader;
import com.xianzaishi.normandie.global.Contants;
import com.xianzaishi.normandie.http.GeneralProtocal;
import com.xianzaishi.normandie.interfaces.OnDataFromServerListener;
import com.xianzaishi.normandie.utils.GlideUtils;
import com.xianzaishi.normandie.utils.SPUtils;
import com.xianzaishi.normandie.utils.ToastUtils;
import com.xianzaishi.normandie.utils.UiUtils;


import java.io.IOException;
import java.lang.ref.WeakReference;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import in.srain.cube.views.ptr.PtrDefaultHandler;
import in.srain.cube.views.ptr.PtrFrameLayout;
import in.srain.cube.views.ptr.PtrHandler;

*/
/**
 * quyang
 * Created by Administrator on 2016/8/30.
 *//*

public class HomeFragment03 extends BaseFragment01 implements View.OnClickListener {

    public static final MediaType MEDIA_TYPE
            = MediaType.parse("application/json; charset=utf-8");
    private PtrFrameLayout mPtrFrameLayout;
    */
/**
     * 点击事件的控件
     *//*

    private TextView tvAddress;
    private ImageView ivScanner;
    private ImageView ivSearch;
    private String[] arrUrl;
    private View view;
    private LinearLayout llContainer;
    private boolean isFirst = true;
    private boolean isLoading = true;
    private boolean isLoading_two = true;
    private ViewPager viewPagerTop;
    private String des;
    private List<HomeTopBean.ModuleBean> mModuleBeen;
    private HomeTopBean mHomeTopBean;
    private HomeDownBean mHomeDownBean;
    private List<HomeDownBean.ModuleBean> mModuleBe;
    private LinearLayout guideParent;
    private List<ImageView> guideArray=new ArrayList<>();
    private boolean isNewUser;//是否为新注册用户
    private int targetType;//不是新注册用户判断顶部单图应该跳转的页面
    */
/**
     * 实现viewpager自滚动的相关变量
     *//*

    private final int KEEP_SCROLL=1;
    private final int KEEP_PAUSE=2;
    private final long DELAY_TIME=3000;
    private boolean FIRST;
    private boolean RESET;
    private MyHandler myHandler;
    private boolean isRefresh;

    @Override
    public void onResume() {
        super.onResume();
        boolean isNew=SPUtils.getBooleanValue(MyApplication.getContext(),Contants.SP_NAME,"LoginChecked",false);
        boolean isNU=SPUtils.getBooleanValue(MyApplication.getContext(),Contants.SP_NAME,"isNewUser",true);
        if(isNew){
            SPUtils.putBooleanValue(MyApplication.getContext(),Contants.SP_NAME,"isNewUser",false);
            SPUtils.putBooleanValue(MyApplication.getContext(),Contants.SP_NAME,"LoginChecked",false);
            isNewUser=false;
            isNU=false;
            setNewGift();
            newGiftDialog();
        }
        if(isNU!=isNewUser){
            isNewUser=isNU;
            setNewGift();
        }
        //重新获得焦点时 发送消息通知轮播图滚动  并且只发送这一次
        if(RESET) {
            myHandler.sendEmptyMessageDelayed(KEEP_SCROLL, DELAY_TIME);
            RESET=false;
        }
        //友盟统计
        MobclickAgent.onPageStart("Home");
    }


    //请求第一页数据
    @Override
    public void getDataFromNet() {
        myHandler=new MyHandler((MainActivity) getActivity());
        requestOnePager();
    }

    //UpLoad Data Of First Page
    private void requestOnePager(){
        isNewUser=SPUtils.getBooleanValue(MyApplication.getContext(),Contants.SP_NAME,"isNewUser",true);
        OkHttpClient okHttpClient = new OkHttpClient();
        String str = "{\n" +
                "    \"pageId\": \"1\",\n" +
                "    \"hasAllFloor\": \"false\"\n" +
                "}";

        RequestBody requestBody = RequestBody.create(MEDIA_TYPE, str);

        Request request = new Request.Builder()
                .post(requestBody)
                .url(Contants.HOME_URL)
                .build();
        okHttpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {
                System.out.println("第一页请求失败");
            }

            @Override
            public void onResponse(com.squareup.okhttp.Response response) throws IOException {
                if (response.isSuccessful()) {
                    if (isRefresh){
                        mPtrFrameLayout.refreshComplete();
                        isRefresh=false;
                        if (myHandler.hasMessages(KEEP_SCROLL)){
                            myHandler.removeMessages(KEEP_SCROLL);
                        }
                    }

                    String string = response.body().string();
                    parseJson1(string);
                    //继续请求下一页数据
                    requestTwoPager();
                }

            }
        });
    }
    //请求第2页数据
    private void requestTwoPager() {

        OkHttpClient okHttpClient = new OkHttpClient();
        String str = "{\n" +
                "    \"pageId\": \"1\",\n" +
                "    \"hasAllFloor\": \"true\"\n" +
                "}";

        RequestBody requestBody = RequestBody.create(MEDIA_TYPE, str);

        Request request = new Request.Builder()
                .post(requestBody)
                .url(Contants.HOME_URL)
                .build();


        okHttpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Request request, IOException e) {

            }
            @Override
            public void onResponse(com.squareup.okhttp.Response response) throws IOException {
                if(response.isSuccessful()){
                    String string = response.body().string();
                    parseJson2(string);
                }

            }
        });
    }


    //解析第二页数据
    public void parseJson2(String json) {
        Gson gson = new Gson();
        mHomeDownBean = gson.fromJson(json, HomeDownBean.class);
        boolean succcess = mHomeDownBean.succcess;
        if (succcess) {
            mModuleBe = mHomeDownBean.module;

            int size = mModuleBe.size();

            initTwoPageData(mModuleBe, size);

        } else {
            ToastUtils.showToast("请求第二页数据有误");
        }




    }

    //适配第二页数据
    private void initTwoPageData(List<HomeDownBean.ModuleBean> moduleBe, int size) {

        for (int i = 0; i < size; i++) {
            HomeDownBean.ModuleBean ban = moduleBe.get(i);
            int stepType = ban.stepType;
            initDataTwo(stepType, i);
        }
    }

    //解析json   1
    public void parseJson1(String json) {
        Gson gson = new Gson();
        mHomeTopBean = gson.fromJson(json, HomeTopBean.class);

        //int size = mHomeTopBean.module.size();

        if (mHomeTopBean.success) {
            mModuleBeen = mHomeTopBean.module;
            UiUtils.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    //隐藏其他界面
                    loadingView.setVisibility(View.INVISIBLE);
                    errorView.setVisibility(View.INVISIBLE);
                    //显示成功界面
                    final View view2 = getSuccessView();
                    view.setVisibility(View.VISIBLE);
                    flContainer.addView(view2);
                    mPtrFrameLayout = (PtrFrameLayout) view2.findViewById(R.id.material_style_ptr_frame);
                    MyRefreshHeader header=new MyRefreshHeader(getContext());
                    mPtrFrameLayout.setHeaderView(header);
                    mPtrFrameLayout.addPtrUIHandler(header);
                    mPtrFrameLayout.setPtrHandler(new PtrDefaultHandler() {
                        @Override
                        public void onRefreshBegin(final PtrFrameLayout frame) {
                            isRefresh=true;
                            requestOnePager();
                        }
                    });
                }
            });
        }
    }

    //获取正确的布局
    public View getSuccessView() {
        // 填充布局
        view = UiUtils.inflateView(R.layout.fragment_home05);
        int sizeTop = mModuleBeen.size();
        //找到容器
        llContainer = (LinearLayout) view.findViewById(R.id.container05);

        //搜索栏
        initSearch();
        for (int i = 0; i < sizeTop; i++) {
            int stepType = mModuleBeen.get(i).stepType;
            if (stepType == 0) {
                //初始化一层轮播图
                initViewPagerTop();
                continue;
            }
            if (stepType == 4) {
                setNewGift();//新人礼
                continue;
            }
            initCategoryFloor();//初始化首页分类类目
            initDataOne(stepType, i);
        }
        return view;
    }
    */
/**
     *  新人礼模块填充数据
     *//*

    private void setNewGift(){
        HomeTopBean.ModuleBean.PicListBean bean;
        if (isNewUser){
            bean = mModuleBeen.get(1).picList.get(0);
        }else {
            bean=mModuleBeen.get(1).picList.get(1);
            targetType=bean.targetType;
        }
        String url = bean.picUrl;
        ImageView iconTwo = (ImageView) view.findViewById(R.id.iv_icon_two);
        GlideUtils.LoadImage(activity, url, iconTwo);
        iconTwo.setOnClickListener(this);
    }
    private void initSearch() {

        tvAddress = (TextView) view.findViewById(R.id.actionbar_address);
        ivScanner = (ImageView) view.findViewById(R.id.actionbar_scanner);
        ivSearch = (ImageView) view.findViewById(R.id.actionbar_search);//搜索

        des = tvAddress.getText().toString().trim();

        ivScanner.setOnClickListener(this);
        ivSearch.setOnClickListener(this);
        tvAddress.setOnClickListener(this);

    }

    //初始化首页分类
    private void initCategoryFloor() {
        */
/**
         *  获取首页分类类目数据
         *//*

        GeneralProtocal<CategoryBean> proocalCategory = new GeneralProtocal<CategoryBean>(CategoryBean.class);
        proocalCategory.getDataByGET(Contants.CATEGORY_URL);
        proocalCategory.setOnDataFromServerListener(new OnDataFromServerListener() {
            @Override
            public void onSuccess(Object result) {
                CategoryBean categoryBean = (CategoryBean) result;
                setCategoryData(categoryBean);
            }
            @Override
            public void onFail() {
            }
        });
    }

    */
/**
     * 首页分类模块填充数据
     *//*

    private void setCategoryData(CategoryBean categoryBean) {

        //get data
        ArrayList<CategoryBean.ModuleBean> categoryBeanModule = (ArrayList<CategoryBean.ModuleBean>) categoryBean.getModule();

        //找到组件

        RecyclerView recyclerTwo = (RecyclerView) view.findViewById(R.id.recycler_view_two);

        //设置布局管理器
        recyclerTwo.setLayoutManager(new LinearLayoutManager(UiUtils.getContext(), LinearLayoutManager.HORIZONTAL, false));

        //设配数据
        FoorTwoAdapter foorTwoAdapter = new FoorTwoAdapter(categoryBeanModule, activity);
        recyclerTwo.setAdapter(foorTwoAdapter);

    }

    */
/**
     *  初始化顶部轮播图
     *//*

    private void initViewPagerTop() {
        //获取数据
        final List<HomeTopBean.ModuleBean.PicListBean> picList = mModuleBeen.get(0).picList;
        final int listSize=picList.size();
        picList.clear();
        viewPagerTop = (ViewPager) view.findViewById(R.id.viewPager_top);
        //设配数据
        viewPagerTop.setAdapter(new AutoRecyclePagerAdapter(getActivity(),picList));
        viewPagerTop.setCurrentItem(listSize*1000,true);
        LayoutInflater inflater=LayoutInflater.from(getActivity());
        guideParent= (LinearLayout) view.findViewById(R.id.guide_parent);
        guideArray.clear();

        guideParent.removeAllViews();

        for (int i = 0; i < picList.size(); i++) {
            View view=  inflater.inflate(R.layout.viewpager_guide_circle,null);
            ImageView guide= (ImageView) view.findViewById(R.id.image_guide);

            guideArray.add(guide);
            guideParent.addView(view);
        }
        guideArray.get(0).setEnabled(false);
        */
/**
         * 通过反射改变viewpager的滑动速度
         *//*

        try {
            Field mScroller;
            mScroller = ViewPager.class.getDeclaredField("mScroller");
            mScroller.setAccessible(true);
            FixedSpeedScroller scroller = new FixedSpeedScroller(getContext());
            scroller.setmDuration(700);   //改变滑动速度
            mScroller.set(viewPagerTop,scroller);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        viewPagerTop.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                position%=listSize;
                for (int i = 0; i < listSize; i++) {
                    if (i==position){
                        guideArray.get(i).setEnabled(false);
                    }else {
                        guideArray.get(i).setEnabled(true);
                    }
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                switch (state){
                    case ViewPager.SCROLL_STATE_DRAGGING:
                        myHandler.sendEmptyMessage(KEEP_PAUSE);
                        FIRST=true;
                        break;
                    case ViewPager.SCROLL_STATE_IDLE:
                        if(FIRST){
                            myHandler.sendEmptyMessageDelayed(KEEP_SCROLL,DELAY_TIME);
                            FIRST=false;
                        }
                }
            }
        });
        myHandler.sendEmptyMessageDelayed(KEEP_SCROLL,DELAY_TIME);
    }




    */
/**
     *  handler 用来控制轮播图自动滚动
     *//*

    private class MyHandler extends Handler{
        WeakReference<MainActivity> weakReference;
        public MyHandler(MainActivity activity){
            weakReference=new WeakReference<MainActivity>(activity);
        }

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            FragmentActivity activity=weakReference.get();

            if (activity!=null){
                switch (msg.what){
                    case KEEP_SCROLL:
                        int position= viewPagerTop.getCurrentItem()+1;
                        viewPagerTop.setCurrentItem(position,true);
                        myHandler.sendEmptyMessageDelayed(KEEP_SCROLL,DELAY_TIME);
                        break;
                    case KEEP_PAUSE:
                        myHandler.removeMessages(KEEP_SCROLL);
                        break;
                }
            }
        }
    }
    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.actionbar_search://搜索
                getActivity().startActivity(new Intent(getActivity(), SearchActivity.class));
                break;
            case R.id.actionbar_address://地址
                getActivity().startActivity(new Intent(getActivity(), DistributionActivity.class));
                break;
            case R.id.actionbar_scanner://扫一扫
                Intent intent1 = new Intent(getActivity(), CaptureActivity.class);
                getActivity().startActivity(intent1);
                break;
            case R.id.iv_icon_two://新人礼
                if (isNewUser){
                    getActivity().startActivity(new Intent(getActivity(), NewLoginActivity.class));
                }else {
                    switch (targetType){
                        case 0://不能点击 无反应
                            break;
                        case 1://跳转活动页
                            Intent intent = new Intent(getActivity(), HuoDongActivity.class);
                            String targetId=mModuleBeen.get(1).picList.get(1).targetId;
                            intent.putExtra("targetId", targetId + "");
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            getActivity().startActivity(intent);
                            break;
                        case 2://跳转商品详情页
                            break;
                    }
                }
                break;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        myHandler.removeCallbacksAndMessages(null);
    }


    //获取steptype对应的view
    public void initDataOne(final int type, final int i) {
        UiUtils.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                switch (type) {//多图
                    case 5:
                        View view0 = UiUtils.getLayoutInflater().inflate(R.layout.four_pics, null);
                        RecyclerView recyclerView= (RecyclerView) view0.findViewById(R.id.four_pics_recyclerView);
                        recyclerView.setLayoutManager(new GridLayoutManager(activity,2, GridLayout.VERTICAL,false));
                        recyclerView.setAdapter(new MorePictrueAdapterOne(mModuleBeen.get(i).picList,activity));
                        llContainer.addView(view0);
                        break;
                    case 1://一图+列表
                        View view1 = UiUtils.getLayoutInflater().inflate(R.layout.foor_four, null);
                        //适配数据
                        new PicsLieBiaoAdapter().set(view1, i, mModuleBeen, activity);
                        llContainer.addView(view1);

                        break;
                    case 2://listview 层
                        View view2 = UiUtils.getLayoutInflater().inflate(R.layout.last_list, null);
                        new NoPicAdapter().set(view2, mModuleBeen, i, activity);
                        llContainer.addView(view2);
                        break;
                    case 3://文字+列表
                        View view3 = UiUtils.getLayoutInflater().inflate(R.layout.foor_three, null);
                        TextView name = (TextView) view3.findViewById(R.id.tv_xianshi_buy);
                        new TextLieBiaoAdapter().set(view3, i, mModuleBeen, activity);
                        llContainer.addView(view3);
                        break;
                    case 4://单图

                        break;
                    case 0://多图轮播

                        break;

                }

            }
        });
    }

    //获取steptype对应的view
    public void initDataTwo(final int type, final int i) {
        UiUtils.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                switch (type) {//多图
                    case 5:
                        View view0 = UiUtils.getLayoutInflater().inflate(R.layout.four_pics, null);
                        RecyclerView recyclerView= (RecyclerView) view0.findViewById(R.id.four_pics_recyclerView);
                        recyclerView.setLayoutManager(new GridLayoutManager(activity,2, GridLayout.VERTICAL,false));
                        //recyclerView.setAdapter(new MorePictrueAdapter(mModuleBe.get(i).picList,activity));
                        llContainer.addView(view0);
                        break;
                    case 1://一图+列表
                        View view1 = UiUtils.getLayoutInflater().inflate(R.layout.foor_four, null);
                        //适配数据
                        new PicsLieBiaoAdapterTwo().set(view1, i, mModuleBe, activity);

                        llContainer.addView(view1);

                        break;
                    case 2://无图片商品楼层
                        View view2 = UiUtils.getLayoutInflater().inflate(R.layout.last_list, null);
                        new NoPicAdapterTwo().set(view2, mModuleBe, i, activity);
                        llContainer.addView(view2);
                        break;
                    case 3://文字+列表
                        View view3 = UiUtils.getLayoutInflater().inflate(R.layout.foor_three_two, null);

                        new TextLieBiaoAdapterTwo().set(view3, i, mModuleBe, activity);

                        llContainer.addView(view3);
                        break;
                    case 4://单图
                        break;
                    case 0://多图轮播
                        break;

                }
            }
        });
    }

    */
/**
     *  新人大礼包 浮层
     *//*

    private void newGiftDialog(){

        LayoutInflater inflater=getActivity().getLayoutInflater();
        View view=inflater.inflate(R.layout.activity_new_gift,null);
        final Dialog dialog=new Dialog(getActivity(),R.style.Dialog);
        dialog.setCancelable(false);
        dialog.setContentView(view);
        Button sure= (Button) view.findViewById(R.id.newGift_goLook);
        Button cancel= (Button) view.findViewById(R.id.newGift_cancel);
        sure.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(getActivity(),CouponsActivity.class));
                dialog.dismiss();
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.show();
        //Animation animation= AnimationUtils.loadAnimation(this,R.anim.animation_store_detail);
        Window dialogWindow = dialog.getWindow();
        WindowManager m = getActivity().getWindowManager();
        Display d = m.getDefaultDisplay(); // 获取屏幕宽、高度
        WindowManager.LayoutParams p = dialogWindow.getAttributes(); // 获取对话框当前的参数值
        p.height = d.getHeight(); // 高度设置为屏幕的高度，根据实际情况调整
        p.width = d.getWidth(); // 宽度设置为屏幕的宽度，根据实际情况调整
        dialogWindow.setAttributes(p);
    }
    @Override
    public void onPause() {
        super.onPause();
        //失去焦点时 停止滚动
        if (myHandler.hasMessages(KEEP_SCROLL)){
            myHandler.removeMessages(KEEP_SCROLL);
            RESET=true;
        }
        //友盟统计
        MobclickAgent.onPageEnd("Home");
    }
}
*/
