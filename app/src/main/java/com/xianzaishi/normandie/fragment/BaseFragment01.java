package com.xianzaishi.normandie.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;

import com.xianzaishi.normandie.MainActivity;
import com.xianzaishi.normandie.R;
import com.xianzaishi.normandie.utils.UiUtils;


/**
 * quyang
 * Created by Administrator on 2016/8/30.
 */

public abstract   class BaseFragment01 extends Fragment {

//    public FragmentActivity activity;
    public View errorView;
    public View loadingView;
    public View emptyView;
    public FrameLayout flContainer;

    public MainActivity activity;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = (MainActivity) getActivity();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = UiUtils.inflateView(R.layout.mybasefragment);
        flContainer = (FrameLayout) view.findViewById(R.id.flContainer);


        //创建布局
        errorView = createErrorView();
        loadingView = createLoadingView();
        emptyView = createEmptyView();

        //设置状态
        errorView.setVisibility(View.INVISIBLE);
        emptyView.setVisibility(View.INVISIBLE);

        //添加布局
        flContainer.addView(errorView);
        flContainer.addView(loadingView);

        return view;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        //请求网络,当请求数据成功后将正确的布局添加到帧布局;
        getDataFromNet();

    }


    //获取数据
    public abstract void getDataFromNet();

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    //创建空布局
    private View createEmptyView() {
        View view = UiUtils.inflateView(R.layout.layout_empty);
        return view;
    }

    //创建正加载的布局
    private View createLoadingView() {
        View view = UiUtils.inflateView(R.layout.layout_loading);
        return view;
    }

    //加载失败的布局
    private View createErrorView() {
        View view = UiUtils.inflateView(R.layout.layout_error);
        Button btn_retry = (Button) view.findViewById(R.id.btn_retry);
        btn_retry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDataFromNet();
            }
        });
        return view;
    }


}
