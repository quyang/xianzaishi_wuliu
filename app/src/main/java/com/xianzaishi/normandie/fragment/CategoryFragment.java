package com.xianzaishi.normandie.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.umeng.analytics.MobclickAgent;
import com.xianzaishi.normandie.LeafCategory01Activity;
import com.xianzaishi.normandie.MyApplication;
import com.xianzaishi.normandie.R;
import com.xianzaishi.normandie.adapter.CategoryGridAdapter;
import com.xianzaishi.normandie.bean.CategoryBean;
import com.xianzaishi.normandie.common.Urls;
import com.xianzaishi.normandie.customs.MyRefreshHeader;
import com.xianzaishi.normandie.utils.DividerGridItemDecoration;
import com.xianzaishi.normandie.utils.GetBeanClass;
import com.xianzaishi.normandie.utils.UiUtils;

import java.io.IOException;
import java.util.ArrayList;

import in.srain.cube.views.ptr.PtrDefaultHandler;
import in.srain.cube.views.ptr.PtrFrameLayout;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * 首页上一级分类页面
 */
public class CategoryFragment extends Fragment {

    private RecyclerView categoryGridView;
    private CategoryGridAdapter adapter;
    private OkHttpClient client= MyApplication.getOkHttpClient();
    private PtrFrameLayout mPtrFrameLayout;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_classification, container, false);
        categoryGridView= (RecyclerView) view.findViewById(R.id.classification_gridView);
        mPtrFrameLayout= (PtrFrameLayout) view.findViewById(R.id.material_style_ptr_frame);
        initRefresh();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        MobclickAgent.onPageStart("Category");
    }
    private void initRefresh() {
        MyRefreshHeader header=new MyRefreshHeader(getActivity());
        mPtrFrameLayout.setHeaderView(header);
        mPtrFrameLayout.addPtrUIHandler(header);
        mPtrFrameLayout.setPtrHandler(new PtrDefaultHandler() {
            @Override
            public void onRefreshBegin(PtrFrameLayout frame) {
                initGridView();
            }
        });

        mPtrFrameLayout.autoRefresh(true);
    }

    private void initGridView() {
        GridLayoutManager gridLayoutManager=new GridLayoutManager(getActivity(),2);
        categoryGridView.setLayoutManager(gridLayoutManager);
        categoryGridView.addItemDecoration(new DividerGridItemDecoration(getActivity()));//加分割线
        /**
         * 访问服务器获得数据源
         */
        Request request=new Request.Builder()
                .url(Urls.CATEGORY)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                UiUtils.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mPtrFrameLayout.refreshComplete();
                        Toast.makeText(getActivity(),"网络请求失败！",Toast.LENGTH_SHORT).show();
                    }
                });
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if(!response.isSuccessful()) throw new IOException("Unexpected code"+response);
                mPtrFrameLayout.refreshComplete();
                final CategoryBean categoryBean= (CategoryBean) GetBeanClass.getBean(response,CategoryBean.class);
                response.close();
                UiUtils.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (categoryBean.getResultCode()==1){
                            final ArrayList<CategoryBean.ModuleBean> list= (ArrayList<CategoryBean.ModuleBean>) categoryBean.getModule();
                            adapter=new CategoryGridAdapter(getActivity(),list);
                            categoryGridView.setAdapter(adapter);
                            adapter.setOnItemClickListener(new CategoryGridAdapter.OnItemClickListener() {
                                @Override
                                public void OnItemClickListener(View view, int position) {
                                    //友盟点击二级分类的统计
                                    MobclickAgent.onEvent(getActivity(),"clickFirstSort");

                                    Intent intent=new Intent(getActivity(), LeafCategory01Activity.class);
                                    intent.putExtra("categoryId",categoryBean.getModule().get(position).getCatId());
                                    intent.putExtra("list",list);
                                    intent.putExtra("position",position);
                                    getActivity().startActivity(intent);
                                }
                            });
                        }
                    }
                });
            }
        });

    }



    @Override
    public void onPause() {
        super.onPause();
        MobclickAgent.onPageEnd("Category");
    }
}
