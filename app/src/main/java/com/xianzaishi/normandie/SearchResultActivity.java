package com.xianzaishi.normandie;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.umeng.analytics.MobclickAgent;
import com.xianzaishi.normandie.adapter.CategoryIndicatorListViewAdapter;
import com.xianzaishi.normandie.bean.CategoryListViewBean;
import com.xianzaishi.normandie.bean.ShoppingCarCountBean;
import com.xianzaishi.normandie.common.Urls;
import com.xianzaishi.normandie.customs.SearchView;
import com.xianzaishi.normandie.global.Contants;
import com.xianzaishi.normandie.utils.GetBeanClass;
import com.xianzaishi.normandie.utils.SPUtils;
import com.xianzaishi.normandie.utils.UiUtils;
import java.io.IOException;
import java.util.List;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


public class SearchResultActivity extends AppCompatActivity implements View.OnClickListener{
    private ListView commodity_list;
    private ImageView back,trolley;
    private TextView emptyView;
    private SearchView searchView;
    private String search;
    private TextView shoppingCount;
    private RelativeLayout footView;
    private List<CategoryListViewBean.ModuleBean.ItemsBean> list;
    private CategoryIndicatorListViewAdapter adapter;
    private int[] parentLocation=new int[2];
    private int[] endLocation=new int[2];
    private int visibleLastIndex=0;//最后一项可见index
    private int pageNo=1;
    private OkHttpClient client=MyApplication.getOkHttpClient();

    @Override
    protected void onResume() {
        super.onResume();
        getShoppingCount();

        MobclickAgent.onResume(this);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_result);
        initView();
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        footView.getLocationInWindow(parentLocation);
        trolley.getLocationInWindow(endLocation);
    }

    private void initView() {
        Intent intent=getIntent();
        search=intent.getStringExtra("search").trim();
        commodity_list= (ListView) findViewById(R.id.commodity_list);//商品列表
        back= (ImageView) findViewById(R.id.go_back);//返回上一级
        back.setOnClickListener(this);
        trolley= (ImageView) findViewById(R.id.shopping_trolley);
        trolley.setOnClickListener(this);
        searchView= (SearchView) findViewById(R.id.search_text);
        searchView.setTexts(search);
        emptyView= (TextView) findViewById(R.id.search_result1_emptyView);
        initListView(search);
        footView= (RelativeLayout) findViewById(R.id.search_result1_footView);
        /**
         *  购物车商品数量指示器
         */
        shoppingCount= (TextView) findViewById(R.id.shopping_trolley_count);
    }



    private void getShoppingCount() {
        OkHttpClient client=MyApplication.getOkHttpClient();
        String uid= SPUtils.getStringValue(MyApplication.getContext(), Contants.SP_NAME,Contants.UID,"");
        RequestBody requestForm=new FormBody.Builder()
                .add("uid",uid)
                .build();
        final Request request=new Request.Builder()
                .url(Urls.GET_ALL_COUNT)
                .post(requestForm)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if(!response.isSuccessful()) throw new IOException("Unexpected code"+response);
                final ShoppingCarCountBean shoppingCarCountBean = (ShoppingCarCountBean) GetBeanClass.getBean(response,ShoppingCarCountBean.class);
                response.close();
                if (shoppingCarCountBean.getCode()==1){
                    UiUtils.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if(!"0".equals(shoppingCarCountBean.getData())){
                                shoppingCount.setVisibility(View.VISIBLE);
                                shoppingCount.setText(shoppingCarCountBean.getData());
                            }
                        }
                    });
                }
            }
        });
    }
    /**
     *  根据搜索内容请求服务器获取搜索结果
     */
    public void initListView(final String search) {
        Request request=new Request.Builder()
                .url(String.format(Urls.SEARCH,search,0))
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                UiUtils.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                    Toast.makeText(SearchResultActivity.this,"网络请求失败！",Toast.LENGTH_SHORT).show();
                    }
                });
            }
            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if(!response.isSuccessful()) throw new IOException("Unexpected code"+response);
                final CategoryListViewBean clvBean=(CategoryListViewBean) GetBeanClass.getBean(response,CategoryListViewBean.class);
                response.close();
                UiUtils.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if(clvBean.getModule().getItems()!=null){
                            list= clvBean.getModule().getItems();
                            adapter=new CategoryIndicatorListViewAdapter(SearchResultActivity.this,1,list,
                                    parentLocation,endLocation,footView);
                            commodity_list.setAdapter(adapter);
                            commodity_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                @Override
                                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                                    MobclickAgent.onEvent(SearchResultActivity.this,"tapItem");

                                    int itemId=list.get(i).getItemId();
                                    Intent intent = new Intent(SearchResultActivity.this, GoodsDetailsActivity.class);
                                    intent.putExtra(Contants.ITEM_ID, itemId);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(intent);
                                }
                            });
                            /**
                             * listView分页加载
                             */
                            commodity_list.setOnScrollListener(new AbsListView.OnScrollListener() {
                                @Override
                                public void onScroll(AbsListView absListView, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                                    visibleLastIndex = firstVisibleItem + visibleItemCount - 1;
                                }

                                @Override
                                public void onScrollStateChanged(AbsListView absListView,int scrollState) {
                                    int itemsLastIndex = adapter.getCount() - 1;    //数据集最后一项的索引
                                    //int lastIndex = itemsLastIndex + 1;             //加上底部的loadMoreView项
                                    if (scrollState == AbsListView.OnScrollListener.SCROLL_STATE_IDLE && visibleLastIndex ==itemsLastIndex) {
                                        //异步加载数据的代码
                                        String s=String.format(Urls.SEARCH,search,pageNo);
                                        Request request=new Request.Builder()
                                                .url(s)
                                                .build();
                                        client.newCall(request).enqueue(new Callback() {
                                            @Override
                                            public void onFailure(Call call, IOException e) {

                                            }

                                            @Override
                                            public void onResponse(Call call, Response response) throws IOException {
                                                if(!response.isSuccessful()) throw new IOException("Unexpected code"+response);
                                                final CategoryListViewBean clvBean= (CategoryListViewBean) GetBeanClass.getBean(response,CategoryListViewBean.class);
                                                response.close();
                                                UiUtils.runOnUiThread(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        if (clvBean.getResultCode()==1){
                                                            pageNo++;
                                                            List<CategoryListViewBean.ModuleBean.ItemsBean> list2= clvBean.getModule().getItems();
                                                            if (list2!=null&adapter!=null){
                                                                list.addAll(list2);
                                                                adapter.notifyDataSetChanged();
                                                            }else {
                                                                UiUtils.runOnUiThread(new Runnable() {
                                                                    @Override
                                                                    public void run() {
                                                                        Toast.makeText(SearchResultActivity.this,"没有更多数据了！",Toast.LENGTH_SHORT).show();
                                                                        pageNo--;
                                                                    }
                                                                });
                                                            }
                                                        }
                                                    }
                                                });

                                            }
                                        });
                                    }
                                }
                            });
                        }else {
                            list=null;
                            adapter=new CategoryIndicatorListViewAdapter(SearchResultActivity.this,1,list,
                                    parentLocation,endLocation,footView);
                            commodity_list.setAdapter(adapter);
                            SpannableString spannableString=new SpannableString("搜不到"+search+"商品");
                            int length=3+search.length();
                            ForegroundColorSpan span=new ForegroundColorSpan(Color.parseColor("#76B357"));
                            spannableString.setSpan(span,3,length,spannableString.SPAN_EXCLUSIVE_EXCLUSIVE);
                            emptyView.setText(spannableString);
                            commodity_list.setEmptyView(emptyView);
                        }
                    }
                });
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.go_back:
                finish();
                break;
            case R.id.shopping_trolley:
                Intent intent=new Intent(SearchResultActivity.this,MainActivity.class);
                intent.putExtra("pageNo",3);
                startActivity(intent);
                break;
            case R.id.search_text:
                startActivity(new Intent(SearchResultActivity.this,SearchActivity.class));
                break;
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        MobclickAgent.onPause(this);
    }
}
