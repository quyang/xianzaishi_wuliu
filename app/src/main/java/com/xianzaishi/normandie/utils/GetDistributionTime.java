package com.xianzaishi.normandie.utils;

import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by Administrator on 2016/11/11.
 */

public class GetDistributionTime {
    public enum TYPE{
        DAY,HOURS
    }
    public static  ArrayList getTimeRange(int startTime,int endTime,long currentTimeMillis,TYPE type){
        Calendar cal=Calendar.getInstance();
        cal.setTimeInMillis(currentTimeMillis);
        int hour=cal.get(Calendar.HOUR_OF_DAY);
        int minute=cal.get(Calendar.MINUTE);

        switch (type){
            case DAY:
                ArrayList dayArray = new ArrayList<>();
                    if (hour<endTime-1){
                        dayArray.add("今天");
                        dayArray.add("明天");
                    }else {
                        dayArray.add("明天");
                    }
                return dayArray;
            case HOURS:
                ArrayList<ArrayList<String>> hoursArray = new ArrayList<>();
                ArrayList<String> hoursArray_01=new ArrayList<>();
                ArrayList<String> hoursArray_02=new ArrayList<>();
                ArrayList<String> hours=new ArrayList<>();
                /**
                 * 生成明天的小时时段
                 */
                for (int i=startTime;i<endTime;i++){
                    if(i<10){
                        hours.add("0"+i + ":" + "30");
                    }else {
                        hours.add(i + ":" + "30");
                    }
                }
                int size1 = hours.size();
                for (int i = 0; i < size1-1; i++) {
                    hoursArray_02.add(hours.get(i) +"-"+ hours.get(i + 1));
                }

                /**
                 * 生成今天的小时时段
                 */
                if (hour<endTime-1) {
                    hours.clear();
                    if (hour < startTime) {//今天凌晨零点到开始配送的时间段内
                        hoursArray.add(hoursArray_02);
                        hoursArray.add(hoursArray_02);
                    } else {//在正常时段内
                        if (minute < 30) {
                            for (int i = hour; i < endTime; i++) {
                                if (i < 10) {
                                    hours.add("0" + i + ":" + "30");
                                } else {
                                    hours.add(i + ":" + "30");
                                }
                            }
                        } else {
                            for (int i = hour + 1; i < endTime + 1; i++) {
                                if (i < 10) {
                                    hours.add("0" + i + ":" + "00");
                                } else {
                                    hours.add(i + ":" + "00");
                                }
                            }
                        }
                        int size = hours.size();
                        hoursArray_01.add("尽快送达");
                        for (int i = 0; i < size-1; i++) {
                            hoursArray_01.add(hours.get(i) + "-" + hours.get(i + 1));
                        }
                        hoursArray.add(hoursArray_01);
                        hoursArray.add(hoursArray_02);
                    }
                }else {
                    hoursArray.add(hoursArray_02);
                }
                return hoursArray;
            default:
                return null;
        }
    }
}
