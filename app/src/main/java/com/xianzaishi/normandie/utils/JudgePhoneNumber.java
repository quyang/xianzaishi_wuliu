package com.xianzaishi.normandie.utils;

import android.content.Context;
import android.text.TextUtils;
import android.widget.Toast;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Administrator on 2016/8/16.
 */
public class JudgePhoneNumber {
    private static boolean isMobileNumber(String mobileNumber){
        Pattern p = Pattern.compile
                ("(^(13[0-9]|14[5|7]|15[012356789]|17[13678]|18[0-9])\\d{8}$)|(^(170[059])\\d{7}$)");
        Matcher m = p.matcher(mobileNumber);
        return m.matches();
    }

    public static boolean judgeMobileNumber(Context context,String mobileNumber){
        if(TextUtils.isEmpty(mobileNumber)){
            Toast.makeText(context,"电话号码不能为空！",Toast.LENGTH_SHORT).show();
            return false;
        }else {
            if (isMobileNumber(mobileNumber)){
                //new ValidateCountDownTimer(60000,1000,getValidate).start();//点击获取验证码后开始倒计时
                return true;
            }else {
                Toast.makeText(context,"请输入正确的电话号码！",Toast.LENGTH_SHORT).show();
                return false;
            }
        }
    }
}
