package com.xianzaishi.normandie.utils;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

/**
 * 
 * quyang
 * 
 * 图片的二级缓存类
 *
 */
public class LocalCacheUtils {
	
	private String mAppName;
	public LocalCacheUtils(String appName){
		mAppName = appName;
	}

	
	/**
	 * 
	 * 将图片缓存到本地的这个目录下:
	 * 		/mnt/sdcard/Android/data/com.example.test/cache
	 * @param url String 图片的url, 没有经过md5加密
	 * @param bm bitmap Bitmap
	 */
	public  void setBitmap2Local(Context context, String url, Bitmap bm) {
		
		File dir = context.getExternalCacheDir();
		File file = new File(dir, mAppName);
		
		//判断是否存在这一的
		if (!file.exists() || file.isFile()) {
			file.mkdirs();
		}
		
	
		String md5 = MD5Utils.getMd5(url);
		
		File text = new File(file, md5);
		
		try {
			
			bm.compress(CompressFormat.JPEG, 100, new FileOutputStream(text));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	
	
	/**
	 * 获取本地缓存
	 * @param url String 图片的url
	 * @return bitmap Bitmap
	 */
	public  Bitmap getCacheFromSD(Context context, String url){
		
		
		File sdDirectory = context.getExternalCacheDir();
		File dir = new File(sdDirectory, mAppName);
		
		String md5 = MD5Utils.getMd5(url);
		
		
		File file = new File(dir, md5);
		
		System.out.println("本地获取的file = "+file.toString());
		
  		if (file.exists()) {
			Bitmap bitmap = BitmapFactory.decodeFile(file.getAbsolutePath());
			return bitmap;
		}
		
		return null;
	}
	
	
	
	
	
}
