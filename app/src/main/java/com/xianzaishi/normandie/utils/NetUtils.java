package com.xianzaishi.normandie.utils;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.xianzaishi.normandie.HTML5Activity;
import com.xianzaishi.normandie.MainActivity;
import com.xianzaishi.normandie.MyApplication;
import com.xianzaishi.normandie.NewLoginActivity;
import com.xianzaishi.normandie.global.Contants;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by ShenLang on 2016/9/8.
 * 领取优惠券
 */
public class NetUtils {
    /**
     * 判断是否有网络
     * @param context Context
     * @return boolean
     */
    public static boolean hasNet(Context context) {
        ConnectivityManager service = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = service.getActiveNetworkInfo();
        return networkInfo.isConnected();
    }
    /**
     * 判断URL是否有效
     */
    public static boolean isValidUrl(String strLink) {
        Pattern p = Pattern.compile
                ("^(http://|https://){1}.+$");
        Matcher m = p.matcher(strLink);
        return m.matches();
    }
    /**
     *  领取优惠券
     */
    public static void obtainCoupons(MainActivity mainActivity, String url) {
        String token= SPUtils.getStringValue(mainActivity, Contants.SP_NAME,Contants.TOKEN,null);
        OkHttpClient client= MyApplication.getOkHttpClient();
        if (token!=null){
           /* FormBody form=new FormBody.Builder()
                    .add("token",token)
                    .add("type","1")
                    .add("couponId",couponId)
                    .build();*/
            Request request=new Request.Builder()
                    .url(url+"&token="+token)
                    .build();
            client.newCall(request).enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                }
                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    if(response.isSuccessful()){
                        String jsonStr=response.body().string();
                        try {
                            JSONObject jsonObject=new JSONObject(jsonStr);
                            ToastUtils.showToast(jsonObject.getString("errorMsg"));
                            /*int resultCode=jsonObject.getInt("resultCode");
                            switch (resultCode){
                                case 1:
                                    ToastUtils.showToast("您已领取成功！");
                                    break;
                                case -30:
                                    ToastUtils.showToast("优惠券已领取成功，不可重复领取");
                                    break;
                                case -50:
                                    ToastUtils.showToast("优惠券已经领光了！");
                                    break;
                                default:
                                    ToastUtils.showToast("系统正忙，请稍后再试！");
                            }*/
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
        }else {
            mainActivity.startActivity(new Intent(mainActivity, NewLoginActivity.class));
        }
    }

    public static void toHTML5(MainActivity activity,String url,String title){
        Intent intent=new Intent(activity, HTML5Activity.class);
        intent.putExtra("url",url)
        .putExtra("title",title);
        activity.startActivity(intent);
    }
}
