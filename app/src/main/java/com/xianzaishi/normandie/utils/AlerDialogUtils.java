package com.xianzaishi.normandie.utils;

import android.graphics.Color;
import android.support.v7.app.AlertDialog;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.xianzaishi.normandie.MyOrderActivity;
import com.xianzaishi.normandie.R;

/**
 * quyang
 * 我的订单弹出对话框
 * Created by Administrator on 2016/9/3.
 */
public class AlerDialogUtils {

    public MyOrderActivity mActivity;

    public AlerDialogUtils(MyOrderActivity activity){
        mActivity = activity;
    }

    public  void newDialog() {
        // 获取构建器对象
        AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);

        // 给对话框设置自定义对话框
        final View view = View.inflate(UiUtils.getContext(), R.layout.alertdialog, null);   //填充布局, 这个password是一个布局文件


        TextView ok = (TextView) view.findViewById(R.id.tv_ok);
        TextView consider = (TextView) view.findViewById(R.id.tv_condider);


        // 设置布局
        builder.setView(view);  //将填充好的布局设置给构造器

        // 构建一个空的对话框
        final AlertDialog dialog = builder.create();  //创建对话框

        dialog.setCancelable(false);

        //后面给确定和取消按钮设置监听,利用一般的onclicklistener;
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                newAnotherDialog();
            }
        });

        consider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.show();


    }

    public void newAnotherDialog() {

        // 获取构建器对象
        AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);

        // 给对话框设置自定义对话框
        final View view = View.inflate(UiUtils.getContext(), R.layout.dialog_order_cancled, null);   //填充布局, 这个password是一个布局文件
        Button ok = (Button) view.findViewById(R.id.btn_ok);

        changeTextColor(view);


        // 设置布局
        builder.setView(view);  //将填充好的布局设置给构造器

        // 构建一个空的对话框
        final AlertDialog dialog = builder.create();  //创建对话框

        dialog.setCancelable(false);

        //后面给确定和取消按钮设置监听,利用一般的onclicklistener;
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });


        dialog.show();

    }


    public void changeTextColor(View view) {
        String str ="退款在3~5个工作日";
        int bstart=str.indexOf("3~5个工作日");
        int bend=bstart+"3~5个工作日".length();
        int fstart=str.indexOf("3~5个工作日");
        int fend=fstart+"3~5个工作日".length();
        SpannableStringBuilder style=new SpannableStringBuilder(str);
        style.setSpan(new ForegroundColorSpan(Color.RED),fstart,fend, Spannable.SPAN_EXCLUSIVE_INCLUSIVE);
        TextView tvColor=(TextView)view.findViewById(R.id.tv_tuiqian);
        tvColor.setText(style);
    }
}
