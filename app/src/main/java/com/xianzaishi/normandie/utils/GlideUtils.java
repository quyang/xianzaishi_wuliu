package com.xianzaishi.normandie.utils;

import android.content.Context;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.xianzaishi.normandie.R;

/**
 * Created by Administrator on 2016/8/30.
 */
public class GlideUtils {
    public static void LoadImage(final Context context, final String url, final ImageView imageView){

        UiUtils.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Glide.with(context)
                        .load(url)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .error(R.mipmap.zhanwei120)
                        .placeholder(R.mipmap.zhanwei120)
                        .into(imageView);
            }
        });

    }
}
