package com.xianzaishi.normandie.utils;

import android.view.View;

import com.xianzaishi.normandie.R;

/**
 * quyang
 * 获取各种状态的布局
 * Created by Administrator on 2016/9/29.
 */

public class ViewUtils {


    /**
     *
     * 获取没有数据时候的view
     * @return
     */
    public static View getEmptyView(){
        return UiUtils.inflateView(R.layout.layout_empty);
    }

    /**
     * 获取请求网络失败后的view
     * @return
     */
    public static View getErrorView(){
        return UiUtils.inflateView(R.layout.layout_error);
    }
}
