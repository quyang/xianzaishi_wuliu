package com.xianzaishi.normandie.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * quyang
 * Created by Administrator on 2016/9/28.
 */

public class StringUtils {


    /**
     * judge whether the string is null or ""
     * @param str String wo input
     * @return String wo get
     */
    public static boolean juege(String str) {

        if (str != null || !"".equals(str)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * get the right length String wo want
     * @param txt String wo put
     */
    public static String getString(String txt) {
        Pattern p = Pattern.compile("[0-9]*");
        Matcher m = p.matcher(txt);
        if (m.matches()) {
            //数字
        }

        p = Pattern.compile("[a-zA-Z]");
        m = p.matcher(txt);
        if (m.matches()) {
            //字母
            txt = txt.substring(0, 20);
        }

        p = Pattern.compile("[\u4e00-\u9fa5]");
        m = p.matcher(txt);
        if (m.matches()) {
            //汉字
            txt = txt.substring(0, 10);
        }

        return txt;
    }
}
