package com.xianzaishi.normandie.utils;

import android.app.Activity;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.StateListDrawable;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;

import com.xianzaishi.normandie.MyApplication;

import java.util.ArrayList;
import java.util.List;

/**
 * ui操作处理的工具类
 */
public class UiUtils {
    /**
     *  打标方法
     */
    public static String signTag(String tags){
        if (tags!=null){
            char[] tag= tags.toCharArray();
            int len=tag.length;
            if(len==6&&tag[0]=='1'){
                return "满40减20";
            }else if (tag[len-1]=='1'){
                return "满99减40";
            }else {
                return null;
            }
            /*for (int i = 0; i <len; i++) {
                if (tag[i]=='1'){
                    switch (len-i){
                        case 1:
                            return "满99减40";
                        case 2:
                            break;
                        case 3:
                            break;
                        case 4:
                            break;
                        case 5:
                            break;
                        case 6:
                            return "满40减20";
                        default:
                            return null;
                    }
                }
            }*/
        }
        return null;
    }
    /**
     * 获取宽高 集合的第一个是屏幕宽,第二个是屏幕高
     *
     * @param activity
     * @return list
     */
    public static List<Integer> getScreenHeight(Activity activity) {
        DisplayMetrics metrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int widthPixels = metrics.widthPixels;
        int heightPixels = metrics.heightPixels;
        List<Integer> list = new ArrayList<>();
        list.add(widthPixels);
        list.add(heightPixels);

        return list;
    }
    //获取屏幕宽度
    public static int getScreenWidth(Activity activity){
        Display display=activity.getWindowManager().getDefaultDisplay();
        return display.getWidth();
    }

    //获取布局填充器
    public static LayoutInflater getLayoutInflater() {
        return (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    // 获取context对象
    public static Context getContext() {
        return MyApplication.getContext();
    }

    // 获取handler对象
    public static Handler getMainThreadHandler() {
        return MyApplication.getHandler();
    }

    // 获取主线程的线程id
    public static int getMainThreadId() {
        return MyApplication.getMainThreadId();
    }

    // 获取字符串资源
    public static String getString(int resId) {
        return getContext().getResources().getString(resId);
    }

    // 获取字符串数组资源
    public static String[] getStringArray(int resId) {
        return getContext().getResources().getStringArray(resId);
    }

    // 获取drawable
    public static Drawable getDrawable(int resId) {
        return getContext().getResources().getDrawable(resId);
    }

    // 获取color
    public static int getColor(int resId) {
        return getContext().getResources().getColor(resId);
    }

    // 获取颜色的状态选择器
    public static ColorStateList getColorStateList(int resId) {
        return getContext().getResources().getColorStateList(resId);
    }

    // 获取dimen下的值
    public static int getDimen(int resId) {
        return getContext().getResources().getDimensionPixelSize(resId);
    }

    // dp--px
    public static int getDp2px(int dp) {
        float density = getContext().getResources().getDisplayMetrics().density;// 获取屏幕密度
        return (int) (dp * density + 0.5);
    }

    // px--dp
    public static int getPxToDp(int px) {
        float density = getContext().getResources().getDisplayMetrics().density;// 获取屏幕密度
        return (int) (px / density + 0.5);
    }

    // 判断当前线程是否是主线程
    public static boolean isRunOnUiThread() {
        // 获取主线程的线程id
        int mainThreadId = getMainThreadId();
        // 获取当前线程的id
        int currentThreadId = android.os.Process.myTid();

        return mainThreadId == currentThreadId;
    }

    // 保证r一定运行在主线程中
    public static void runOnUiThread(Runnable r) {
        if (isRunOnUiThread()) {
            r.run();
        } else {
            getMainThreadHandler().post(r);// 将r丢到主线程的消息队列里面
        }
    }

    public static View inflateView(int resId) {
        return View.inflate(getContext(), resId, null);
    }

//	//圆角矩形的创建，和xml中的selector一样
//	public static GradientDrawable getGradientDrawable(float radius, int color) {
//		GradientDrawable drawable = new GradientDrawable();
//		drawable.setCornerRadius(radius);//设置圆角半径
//		drawable.setGradientType(GradientDrawable.RECTANGLE);//设置类型为矩形
//		drawable.setColor(color);//设置颜色
//		return drawable;
//	}

    //状态选择器
    public static StateListDrawable getSelector(Drawable pressedDrawable,
                                                Drawable normalDrawable) {
        StateListDrawable stateListDrawable = new StateListDrawable();

        stateListDrawable.addState(new int[]{android.R.attr.state_pressed},
                pressedDrawable);//增加点击状态的图片
        stateListDrawable.addState(new int[]{}, normalDrawable);//增加默认状态的图片

        return stateListDrawable;
    }


}
