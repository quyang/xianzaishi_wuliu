package com.xianzaishi.normandie.utils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * quyang
 *
 * 这个工具类是用于将从服务器返回的输入流转换为字符串的方法
 * 	 返回值:	String, 将输入流中的字节流转为字符串
 * 			-1	表示转换失败
 * 	默认编码: utf-8
 *
 */

public class UtilsStreamToString {
	
	/**
	 * 将字节流转为字符串的方法
	 * @param is	输入流,这个流是从服务器连接对象中获取的输入流对象
	 * @return		String, 本质是将输入流中的字节转为字符串
	 * 				-1	表示转换失败
	 * 默认编码: utf-8
	 * 
	 */
	public static String parseStreamToString(InputStream is) {

		String data = "";

		try {
			ByteArrayOutputStream baos = new ByteArrayOutputStream();//这个流不用关闭,底层是一个字节数据,
			//大小根据数据的大小变化;

			byte[] buffer = new byte[1024 * 8];
			int len = -1;
			while ((len = is.read(buffer)) != -1) {
				baos.write(buffer, 0, len);
			}

			data = new String(baos.toByteArray(), "utf-8");// 默认是utf-8

		} catch (IOException e) {
			e.printStackTrace();
			data = "-1";
		}

		return data;

	}
}
