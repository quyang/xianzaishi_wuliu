package com.xianzaishi.normandie.utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * quyang
 * 2016年8月21日15:20:40
 */
public class MD5Utils {

	/**
	 * 将一个字符串进行md5
	 * @param password
	 * @return
	 */
	public static String getMd5(String password) {
		try {
			MessageDigest digest = MessageDigest.getInstance("MD5");
			byte[] result = digest.digest(password.getBytes());

			StringBuffer sb = new StringBuffer();
			for (byte b : result) {
				int i = b & 0xff;
				String hexString = Integer.toHexString(i);

				if (hexString.length() == 1) {
					hexString = "0" + hexString;
				}

				sb.append(hexString);
			}

			return sb.toString();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}

		return "";
	}

	/**
	 * 将要一个路径进行md5
	 * @param path String
	 * @return String
	 */
	public static String getFileMd5(String path) {
		FileInputStream in = null;
		try {
			MessageDigest digest = MessageDigest.getInstance("MD5");
			 in = new FileInputStream(path);

			int len = 0;
			byte[] buffer = new byte[1024];

			while ((len = in.read(buffer)) != -1) {
				digest.update(buffer, 0, len);
			}

			byte[] result = digest.digest();

			StringBuffer sb = new StringBuffer();
			for (byte b : result) {
				int i = b & 0xff;
				String hexString = Integer.toHexString(i);

				if (hexString.length() == 1) {
					hexString = "0" + hexString;
				}

				sb.append(hexString);
			}

			return sb.toString();

		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			try {
				in.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		return "";
	}
}
