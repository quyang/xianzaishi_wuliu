package com.xianzaishi.normandie.utils;

import com.google.gson.Gson;

import java.io.IOException;

import okhttp3.Response;

/**
 * Created by Administrator on 2016/8/24.
 */
public class GetBeanClass {
    public static Object getBean(Response response,Class clazz){
        Gson gson=new Gson();
        try {
            return gson.fromJson(response.body().string(),clazz);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}
