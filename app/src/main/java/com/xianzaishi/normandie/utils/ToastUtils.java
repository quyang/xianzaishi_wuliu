package com.xianzaishi.normandie.utils;

import android.view.Gravity;
import android.widget.Toast;

/**
 * quyang
 * <p/>
 * Created by Administrator on 2016/9/3.
 */
public class ToastUtils {

    /**
     * 弹吐司
     *
     * @param msg
     */
    public static void showToast(final String msg) {

        UiUtils.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast toast= Toast.makeText(UiUtils.getContext(), msg, Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.CENTER,0,0);
                toast.show();
            }
        });
    }
}
