package com.xianzaishi.normandie.utils;

import android.app.Activity;
import android.app.ProgressDialog;
import android.view.Window;

/**
 * quyang
 * Created by Administrator on 2016/10/11.
 */

public class DialogUtils {

    private ProgressDialog dialog;

    public DialogUtils(String content,Activity activity) {
        if (dialog != null && dialog.isShowing()) return;

        dialog = new ProgressDialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.setMessage(content);
    }


    /**
     * 显示请求网络的进度框
     */
    public void showDialog() {
        dialog.show();
    }


    public void hideDialog() {
        dialog.dismiss();
    }
}
