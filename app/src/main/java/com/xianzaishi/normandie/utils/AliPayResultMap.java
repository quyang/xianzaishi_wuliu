package com.xianzaishi.normandie.utils;

import android.text.TextUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Administrator on 2016/10/20.
 */

public class AliPayResultMap {
    private static final String SPLIT_PROPERTY_GROUT_KEY = "&";
    public static HashMap<String, String> getResultMap(String result) {
        HashMap<String, String> proM = new HashMap<>();
        if (!TextUtils.isEmpty(result)) {
            String[] propertyArrays = result.split(SPLIT_PROPERTY_GROUT_KEY);
            for (String tmp : propertyArrays) {
                if (!TextUtils.isEmpty(tmp)) {
                    String[] keyValue = tmp.split("=");
                    if (keyValue.length != 2) {
                    } else {
                        proM.put(keyValue[0], keyValue[1]);
                    }
                }
            }
        }
        return proM;
    }
}
