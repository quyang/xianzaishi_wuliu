package com.xianzaishi.normandie.utils;

import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.widget.ImageView;

/**
 * Created by Administrator on 2016/9/8.
 */
public class ComponentColorUtils {

    /**
     * 将一个图片变为灰色
     * @param view
     */
    public static void setLightBlack(ImageView view){

        ColorMatrix matrix = new ColorMatrix();
        matrix.setSaturation(0.2f);
        ColorMatrixColorFilter filter = new ColorMatrixColorFilter(matrix);
        view.setColorFilter(filter);
    }

    public static void resetColor(ImageView imageView){

        ColorMatrix matrix = new ColorMatrix();
        matrix.setSaturation(1f);
        ColorMatrixColorFilter filter = new ColorMatrixColorFilter(matrix);
        imageView.setColorFilter(filter);
    }
}
