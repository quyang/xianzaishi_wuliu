package com.xianzaishi.normandie.utils;

import android.util.Log;

import java.io.Closeable;
import java.io.IOException;

/**
 * quyang
 *
 * 2016年8月22日17:43:36
 */
public class IOUtils {

	public static final String TAG = "IOUtils";

	/** 关闭流 */
	public static boolean close(Closeable io) {
		if (io != null) {
			try {
				io.close();
			} catch (IOException e) {
				Log.e(TAG, e.getMessage());
			}
		}
		return true;
	}
}
