package com.xianzaishi.normandie.utils;

import android.os.CountDownTimer;
import android.widget.Button;

/**
 * Created by Administrator on 2016/8/16.
 */
public class ValidateCountDownTimer extends CountDownTimer {
    private Button mButton;
    public ValidateCountDownTimer(long millisInFuture, long countDownInterval,Button mButton) {
        super(millisInFuture, countDownInterval);
        this.mButton=mButton;
    }

    @Override
    public void onTick(long millisInFuture) {
        mButton.setClickable(false);
        mButton.setText("还剩"+millisInFuture/1000+"秒");
    }

    @Override
    public void onFinish() {
        mButton.setClickable(true);
        mButton.setText("获取验证码");
    }
}
