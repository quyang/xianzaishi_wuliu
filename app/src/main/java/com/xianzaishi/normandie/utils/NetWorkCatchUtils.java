package com.xianzaishi.normandie.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.widget.ImageView;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * quyang
 *
 * 从网络获取图片
 *
 * 2016年8月21日15:20:57
 */
public class NetWorkCatchUtils {

	private MemoryCacheUtils mMemoryCacheUtils;
	private LocalCacheUtils mLocalCache;
	private Context mContext;

	public NetWorkCatchUtils(Context context,
			MemoryCacheUtils memoryCacheUtils, LocalCacheUtils localCache) {
		mMemoryCacheUtils = memoryCacheUtils;
		mLocalCache = localCache;
		mContext = context;
	}

	/**
	 * 从网络获取bitmap,并设置给iv;
	 * 
	 * @param iv
	 *            imageview
	 * @param url
	 *            String 图片的url,没有经过md5
	 */
	public void getBitmapFromNet(ImageView iv, String url) {
		System.out.println("获取网络缓存");

		Task task = new Task();
		task.execute(iv, url);
	}

	class Task extends AsyncTask<Object, Integer, Bitmap> {

		private ImageView iv;
		private String url;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}

		@Override
		protected Bitmap doInBackground(Object... params) {
			iv = (ImageView) params[0];
			url = (String) params[1];

			Bitmap bitmap = download(url);

			return bitmap;
		}

		@Override
		protected void onProgressUpdate(Integer... values) {
			super.onProgressUpdate(values);
		}

		@Override
		protected void onPostExecute(Bitmap result) {
			super.onPostExecute(result);

			if (result != null) {
				iv.setImageBitmap(result);

				mMemoryCacheUtils.setBitmap2Memory(url, result);

				mLocalCache.setBitmap2Local(mContext, url, result);

			}
		}

	}

	private Bitmap download(String url) {

		try {
			HttpURLConnection con = (HttpURLConnection) new URL(url)
					.openConnection();

			con.setConnectTimeout(4 * 1000);
			con.setRequestMethod("GET");
			con.setReadTimeout(5 * 1000);

			con.connect();

			int responseCode = con.getResponseCode();
			if (responseCode == 200) {
				InputStream inputStream = con.getInputStream();

				return BitmapFactory.decodeStream(inputStream);

			} 

		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;

	}

}
