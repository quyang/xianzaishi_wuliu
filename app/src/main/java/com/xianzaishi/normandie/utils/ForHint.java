package com.xianzaishi.normandie.utils;

import android.text.SpannableString;
import android.text.Spanned;
import android.text.SpannedString;
import android.text.style.AbsoluteSizeSpan;

/**
 * Created by Administrator on 2016/8/3.
 */
public class ForHint {
    public static SpannedString getHint(CharSequence charSequence){
        SpannableString spannableString=new SpannableString(charSequence);
        AbsoluteSizeSpan absoluteSizeSpan=new AbsoluteSizeSpan(12,true);
        spannableString.setSpan(absoluteSizeSpan,0,charSequence.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        SpannedString hint= new SpannedString(spannableString);
        return hint;
    }
}
