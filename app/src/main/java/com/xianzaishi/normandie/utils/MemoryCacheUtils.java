
package com.xianzaishi.normandie.utils;

import android.graphics.Bitmap;
import android.os.Handler;
import android.support.v4.util.LruCache;

/**
 * quyang
 *
 * 从内存中获取图片缓存
 */
public class MemoryCacheUtils {
	
	Handler j = new Handler();
	private LruCache<String, Bitmap> lru;
	
	
	
	public MemoryCacheUtils(){
		long maxMemory = Runtime.getRuntime().maxMemory();
		lru = new LruCache<String, Bitmap>((int) (maxMemory/8)){
			
			@Override
			protected int sizeOf(String key, Bitmap value) {
//				int byteCount = value.getByteCount();
				int i = value.getRowBytes()*value.getHeight();
				return i;
			}
		};
		
	}
	
	
	/**
	 * 将bitmap存储在内存
	 * @param url String bitmap的url
	 * @param bm bitmap
	 */
	public void setBitmap2Memory(String url, Bitmap bm){
		lru.put(url, bm);
	}
	
	/**
	 *从内存中获取bitmap对象
	 * @param url String
	 */
	public Bitmap getBitmapFromMemory(String url){
	 return	lru.get(url);
	}
	
	
	

}
