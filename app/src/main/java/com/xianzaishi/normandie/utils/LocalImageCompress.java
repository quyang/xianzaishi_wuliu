package com.xianzaishi.normandie.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;

/**
 * 获取本地图片缓存(大图)
 *
 * @author Administrator
 */
public class LocalImageCompress {

    /**
     * 获取simpleSize, 即宽或高中一个最大的比例值,压缩图片使用;缓存图片的宽和要求的宽的比或缓存图片的 高与要求图片的高的比例
     *
     * @param options   Options 封装了缓存中图片的真实宽高
     * @param reqWidth  int 我们要求的图片的宽度
     * @param reqHeight int 我们要求的图片的高度
     */
    public static int calculateInSampleSize(Options options, int reqWidth,
                                            int reqHeight) {

            int width = options.outWidth;
            int height = options.outHeight;

            int inSampleSize = 1;

            if (width > reqWidth || height > reqHeight) {
                int widthRadio = Math.round(width * 1.0f / reqWidth);
                int heightRadio = Math.round(height * 1.0f / reqHeight);

                inSampleSize = Math.max(widthRadio, heightRadio);
            }

            return inSampleSize;

    }

    /**
     * 获取压缩过的bitmap对象
     *
     * @param path      String 本地图片缓存的路径
     * @param reqWidth  我们要求的宽
     * @param reqHeight 我们要求的高
     * @return Bitmap
     */
    public static Bitmap decodeSampledBitmapFromResource(String path,
                                                         int reqWidth, int reqHeight) {

        // 给定的BitmapFactory设置解码的参数
        Options options = new Options();

        // 从解码器中获取原始图片的宽高，这样避免了直接申请内存空间
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);

        // 计算 inSampleSize比例值
        options.inSampleSize = calculateInSampleSize(options, reqWidth,
                reqHeight);

        // 压缩完后便可以将inJustDecodeBounds设置为false了。
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeFile(path, options);
    }


}
