package com.xianzaishi.normandie.utils;

import android.app.Activity;
import android.widget.ImageView;

/**
 * quyang
 *
 * 根据xutils中bitmapUtils的自己写的MyBitmapUtils
 *
 * 2016年8月21日15:20:46
 */
public class MyBitmapUtils {

	private Activity mActivity;
	private NetWorkCatchUtils netWorkCatchUtils;
	private LocalCacheUtils localCacheUtils;
	private MemoryCacheUtils memoryCacheUtils;

	/**
	 * 将图片的二级缓存存放在本地的sd卡/Android/data/应用名/cache
	 * 先从内存读取,没有存本地读取,最后请求网络
	 * @param activity
	 */
	public MyBitmapUtils(Activity activity, String appName) {
		mActivity = activity;
		memoryCacheUtils = new MemoryCacheUtils();
		localCacheUtils = new LocalCacheUtils(appName);
		netWorkCatchUtils = new NetWorkCatchUtils(activity, memoryCacheUtils,
				localCacheUtils);
	}

	/**
	 * 在imageview上显示从网络获取的bitmap
	 * 
	 * @param iv
	 *            imageview
	 * @param url
	 *            String
	 */
	public void display(ImageView iv, String url) {
		// 获取内存缓存有的话就设置
		if (memoryCacheUtils.getBitmapFromMemory(url) != null) {
			iv.setImageBitmap(memoryCacheUtils.getBitmapFromMemory(url));
			System.out.println("读取内存缓存");
			return;
		}

		// 获取本地缓存
		if (localCacheUtils.getCacheFromSD(mActivity, url) != null) {
			iv.setImageBitmap(localCacheUtils.getCacheFromSD(mActivity, url));
			System.out.println("读取本地缓存");
			return;
		}

		// 获取网络数据
		netWorkCatchUtils.getBitmapFromNet(iv, url);

	}

}
