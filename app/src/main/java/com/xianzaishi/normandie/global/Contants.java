package com.xianzaishi.normandie.global;

/**
 * Created by ShenLang on 2016/8/21.
 * urls
 */
public class Contants {
    private static final String TEST="http://item.xianzaishi.net/wapcenter/";
    private static final String OFFICIAL="http://item.xianzaishi.com/";
    public static final String TEST_TRADE="http://trade.xianzaishi.net/";
    private static final String OFFICIAL_TRADE="http://trade.xianzaishi.com/";

    private static final String LOCAL_HOST=OFFICIAL;
    private static final String LOCAL_HOST_TRADE=OFFICIAL_TRADE;

    public static final String APPNAME = "xianzaishi";

    /**
     * 添加到购物车的根url
     */
    public static final String ADD_SHOPPING_CAR = LOCAL_HOST_TRADE+"sc/appendItem.json";
    /**
     * 清空购物车的根url
     */
    public static final String CLEAN_SHOPPING_CAR = LOCAL_HOST_TRADE+"sc/clean.json";
    /**
     * 商品详情页url  http://139.196.92.240/wapcenter/requestcommodity/itemdetail?itemId=10201625
     */
    public static final String GOODS_DETAILS = LOCAL_HOST+"requestcommodity/itemdetail?itemId=10201609";
    /**
     * 商品种类数量
     */
    public static final String GOODS_COUNT = "goodscount";
    /**
     * 获取服务器新版本的version
     */
    public static final String URL_NEW_APP = "http://www.baidu.com";

    /**
     * 判断用户是否第一次进入向导页面
     */
    public static final String IS_FIRST = "isFirst";

    public static final String APP_ID = "wxdf3989b2edb80672";
    public static final String APP_KEY="d7Da7b62646b2a7dd0d62a2b3d0d123T";

    public static final String PARTER_ID = "1385693802";
    /**
     * 用户uid的key
     */
    public static final String UID = "uid";


    /**
     * sp文件的名字
     */
    public static final String SP_NAME = "sp_file";

    /**
     * sp文件的名字 shenglang
     */
    public static final String SAVE_FILE_NAME = "save_file_name";

    /**
     * 从商品详情页到购物车带回来的商品个数
     */
    public static final String COUNT = "count";


    /**
     * 使用是否第一次进入个人中心
     */
    public static final String IS_FIRST_IN = "isFirstIn";
    /**
     * 首页分页类目url
     */
    public static final String CATEGORY_URL = LOCAL_HOST+"requestcategory/category?homePageCategory=true";
    /**
     * 首页2pager
     */
    public static final String SHOUYE_TWO = LOCAL_HOST+"requesthome/homepage?pageId=1&hasAllFloor=true";
    /**
     * 商品的itemid
     */
    public static final String ITEM_ID = "itemId";
    /**
     * 获取订单列表
     */
    public static final String ORDER_LIST = LOCAL_HOST_TRADE+"order/getOrders.json";
    /**
     * 获取购物车订单数
     */
    public static final String GOODS_COUNTS = LOCAL_HOST_TRADE+"sc/getItemCount.json";
    /**
     * 更新购物车订单数
     */
    public static final String UPDATE_GOODS_COUNTS = LOCAL_HOST_TRADE+"sc/changeItemCount.json";
    /**
     * 取消订单
     */
    public static final String ORDER_STATUS = LOCAL_HOST_TRADE+"order/cancelTrade.json";

    /**
     * 获取订单详情
     */
    public static final String ORDER_DETAILS = LOCAL_HOST_TRADE+"order/getOrderDetail.json";

    /**
     * 首页根url
     */
    public static final String HOME_URL = LOCAL_HOST+"requesthome/homepage";

    /**
     * 活动页根url
     */
    public static final String HUODONG_URL = LOCAL_HOST+"requesthome/homepage";

    /**
     * 商品详情页的url
     */
    public static final String GOODS_DETAILS_URL = LOCAL_HOST+"requestcommodity/itemdetail";

    /**
     * 活动页的参数"targetId"
     */
    public static final String TARGET_ID = "targetId";
    /**
     * 用户登录后返回的token
     */
    public static final String TOKEN = "token";

    /**
     * 提交pic时候的key
     */
    public static final String IMG_KEY = "img";

    /**
     * 提交pic时候的参数type
     */
    public static final String IMG_TYPE = "type";


    /**
     * 提交pic时候的progressbar中显示的message
     */
    public static final String IMG_PROGRESSBAR_MSG = "提交反馈信息中...";


    /**
     * 提交pic的url
     */
    public static final String IMG_COMMIT_URL= "http://static.xianzaishi.com:7009/pic/upload";

    /**
     *  生成二维码的string
     */
    public static final String QRSTING="{\"qrInfoType\": \"mobile\",\"queryDetail\": {\n" +
            "\"queryType\": \"get\",\"queryParameter\": {\n" +
            "\"id\": \"%s\"\n" +
            "},\"data\": \"user\"\n" +
            "}\n" +
            "}";


}
