package com.xianzaishi.normandie;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.xianzaishi.normandie.adapter.SelectAddressListAdapter;
import com.xianzaishi.normandie.bean.SelectAddressBean;
import com.xianzaishi.normandie.bean.SelectAddressBean1;

import java.util.ArrayList;
import java.util.List;

import okhttp3.OkHttpClient;

public class SelectAddressActivity extends AppCompatActivity implements View.OnClickListener,
        AdapterView.OnItemClickListener{
    private ImageView backIcon;
    private ListView listView;
    private List<SelectAddressBean.DataBean> datas;
    private OkHttpClient client=MyApplication.getOkHttpClient();
    private List<SelectAddressBean1.DataBean.AreaBean> list=new ArrayList<>();

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_address);
        initView();
    }

    private void initView() {
        View view=findViewById(R.id.select_address_title);
        TextView title= (TextView) view.findViewById(R.id.text_title);
        title.setText("新增收货地址");
        backIcon= (ImageView) view.findViewById(R.id.address_go_back);
        backIcon.setOnClickListener(this);

        listView= (ListView) findViewById(R.id.select_address_listView);
        listView.setOnItemClickListener(this);
        initListView();
    }

    private void initListView() {
        SelectAddressBean1.DataBean.AreaBean areaBean = null;
        switch (getIntent().getIntExtra("code",0)){
            case 1:
                List<SelectAddressBean1.DataBean.Level1AreaBean> list1= (List<SelectAddressBean1.DataBean.Level1AreaBean>) getIntent().getSerializableExtra("list");
                for (int i = 0; i < list1.size(); i++) {
                    areaBean=new SelectAddressBean1.DataBean.AreaBean();
                    areaBean.setName(list1.get(i).getShortName());
                    areaBean.setCode(list1.get(i).getCode());
                    list.add(areaBean);
                }
                break;
            case 2:
                List<SelectAddressBean1.DataBean.Level2AreaBean> list2= (List<SelectAddressBean1.DataBean.Level2AreaBean>) getIntent().getSerializableExtra("list");
                for (int i = 0; i < list2.size(); i++) {
                    areaBean=new SelectAddressBean1.DataBean.AreaBean();
                    areaBean.setName(list2.get(i).getName());
                    areaBean.setCode(list2.get(i).getCode());
                    list.add(areaBean);
                }
                break;
            case 3:
                List<SelectAddressBean1.DataBean.Level3AreaBean> list3= (List<SelectAddressBean1.DataBean.Level3AreaBean>) getIntent().getSerializableExtra("list");
                for (int i = 0; i < list3.size(); i++) {
                    areaBean=new SelectAddressBean1.DataBean.AreaBean();
                    areaBean.setName(list3.get(i).getName());
                    areaBean.setCode(list3.get(i).getCode());
                    list.add(areaBean);
                }
                break;
            case 4:
                List<SelectAddressBean1.DataBean.Level4AreaBean> list4= (List<SelectAddressBean1.DataBean.Level4AreaBean>) getIntent().getSerializableExtra("list");
                for (int i = 0; i < list4.size(); i++) {
                    areaBean=new SelectAddressBean1.DataBean.AreaBean();
                    areaBean.setName(list4.get(i).getName());
                    areaBean.setCode(list4.get(i).getCode());
                    list.add(areaBean);
                }
                break;
        }
        listView.setAdapter(new SelectAddressListAdapter(this,list));
    }

    @Override
    public void onClick(View view) {
        this.finish();
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        String addr=list.get(i).getName();
        String codes=list.get(i).getCode();
        Intent intent=new Intent();
        intent.putExtra("code",codes);
        intent.putExtra("area",addr);
        setResult(RESULT_OK,intent);
        finish();
    }
}
