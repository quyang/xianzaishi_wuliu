package com.xianzaishi.normandie;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;


/**
 * quyang
 *
 * 我的消息页
 */
public class MyMessageActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView title;
    private ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_message);
        initView();
    }

    private void initView() {
        View view = findViewById(R.id.message_head);
        ImageView back = (ImageView) findViewById(R.id.address_go_back);
        title = (TextView) view.findViewById(R.id.text_title);
        RelativeLayout rl = (RelativeLayout) findViewById(R.id.rl_Container);
        TextView tvDes = (TextView) findViewById(R.id.tv_des);
        ImageView icon = (ImageView) findViewById(R.id.icon);


        title.setText("我的消息");
        listView = (ListView) findViewById(R.id.message_list);
        listView.setAdapter(new MyBaseAdapter());


        back.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.address_go_back:
                finish();
                break;
        }
    }

    //lv适配器
    private class MyBaseAdapter extends BaseAdapter {
        @Override
        public int getCount() {

            return 0;
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            return null;
        }
    }
}
