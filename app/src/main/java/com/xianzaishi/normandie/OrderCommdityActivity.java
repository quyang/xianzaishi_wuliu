package com.xianzaishi.normandie;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.xianzaishi.normandie.adapter.ListBaseAdapter;
import com.xianzaishi.normandie.bean.OrderActivityBean;
import com.xianzaishi.normandie.bean.OrderDetailsBean;
import com.xianzaishi.normandie.bean.ShoppingTrolleyBean;
import com.xianzaishi.normandie.utils.GlideUtils;

import java.util.ArrayList;
import java.util.List;

public class OrderCommdityActivity extends BaseActivity {

    private ListView lvOrderCommList;
    private MyAdapter mAdapter;
    private ArrayList<OrderActivityBean.DataBean.ObjectsBean> mList;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_commdity);
        initView();
        getDate();
    }

    private void initView() {
        TextView title= (TextView) findViewById(R.id.tv_base_center_text);
        title.setText("订单商品列表");
        lvOrderCommList = (ListView) findViewById(R.id.lvOrderCommList);
    }

    private void getDate() {
        mList = initDate();

        setDate2Lv();
    }

    private ArrayList<OrderActivityBean.DataBean.ObjectsBean> initDate() {
        ArrayList<OrderActivityBean.DataBean.ObjectsBean> list= (ArrayList<OrderActivityBean.DataBean.ObjectsBean>) getIntent().getSerializableExtra("list");
        return list;
    }

    private void setDate2Lv() {
        mAdapter = new MyAdapter(MyApplication.getContext(),mList);
        lvOrderCommList.setAdapter(mAdapter);
    }


    class MyAdapter extends ListBaseAdapter {


        public MyAdapter(Context context, List datas) {
            super(context, datas);
        }

        @Override
        public View getItemView(int position, View convertView, ViewGroup viewGroup) {
            MyViewHolder viewHolder = null;
            if (convertView == null) {
                viewHolder = new MyViewHolder();
                convertView = View.inflate(MyApplication.getContext(), R.layout.order_commdity_item, null);
                //订单商品列表标题
                viewHolder.tvOrderCommTitle = (TextView) convertView.findViewById(R.id.tvOrderCommTitle);
                viewHolder.tvOrderCommDes = (TextView) convertView.findViewById(R.id.tvOrderCommDes);
                viewHolder.tvOrderCommPrice = (TextView) convertView.findViewById(R.id.tvOrderCommPrice);
                viewHolder.tvOrderCommCounts = (TextView) convertView.findViewById(R.id.tvOrderCommCounts);
                viewHolder.ivOrderCommIcon= (ImageView) convertView.findViewById(R.id.ivOrderCommIcon);
                convertView.setTag(viewHolder);

            } else {
                viewHolder = (MyViewHolder) convertView.getTag();
            }
            OrderActivityBean.DataBean.ObjectsBean item = (OrderActivityBean.DataBean.ObjectsBean) getItem(position);
            GlideUtils.LoadImage(OrderCommdityActivity.this,item.getItemIconUrl(),viewHolder.ivOrderCommIcon);
            viewHolder.tvOrderCommTitle.setText(item.getItemName());
            viewHolder.tvOrderCommDes.setText(item.getSkuInfo().getSpec());
            viewHolder.tvOrderCommPrice.setText(item.getItemEffePrice()+"");
            viewHolder.tvOrderCommCounts.setText(item.getItemCount()+"");
            return convertView;
        }
    }


    static class MyViewHolder {
        public ImageView ivOrderCommIcon;
        public TextView tvOrderCommTitle;
        public TextView tvOrderCommDes;
        public TextView tvOrderCommPrice;
        public TextView tvOrderCommCounts;
    }

}
