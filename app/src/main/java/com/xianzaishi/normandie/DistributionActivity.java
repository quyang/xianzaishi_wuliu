package com.xianzaishi.normandie;


import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.amap.api.location.AMapLocation;
import com.amap.api.location.AMapLocationClient;
import com.amap.api.location.AMapLocationClientOption;
import com.amap.api.location.AMapLocationListener;
import com.amap.api.maps2d.AMap;
import com.amap.api.maps2d.AMapUtils;
import com.amap.api.maps2d.CameraUpdateFactory;
import com.amap.api.maps2d.MapView;
import com.amap.api.maps2d.model.BitmapDescriptorFactory;
import com.amap.api.maps2d.model.CameraPosition;
import com.amap.api.maps2d.model.CircleOptions;
import com.amap.api.maps2d.model.LatLng;
import com.amap.api.maps2d.model.Marker;
import com.amap.api.maps2d.model.MarkerOptions;
import com.amap.api.maps2d.model.Polygon;
import com.amap.api.maps2d.model.PolygonOptions;
import com.umeng.analytics.MobclickAgent;
import com.xianzaishi.normandie.adapter.AutoRecyclePagerAdapter;
import com.xianzaishi.normandie.adapter.RecyclePagerAdapter;
import com.xianzaishi.normandie.global.Contants;
import com.xianzaishi.normandie.utils.SPUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DistributionActivity extends CheckPermissionsActivity implements AMapLocationListener
        , View.OnClickListener, AMap.OnInfoWindowClickListener, AMap.OnMapLoadedListener {

    private MapView mMapView;
    private TextView address, location_result,choose_city;
    private LinearLayout guideParent;
    private FrameLayout viewPagerParent;
    private ImageView back_icon;
    private ViewPager mViewPager;
    private List<ImageView> imageViews;
    private ImageView[] guideArray;
    private Button changeButton;
    private AMap mAMap;
    private AMapLocationClient mLocationClient;
    private AMapLocationClientOption mLocationOption;
    private LatLng mLatLng;
    private RelativeLayout distribution_container;
    //private LinearLayout select_container;
    private List<LatLng> latLngs;
    private Polygon polygon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_distribution);
        mMapView = (MapView) findViewById(R.id.map_display);
        mMapView.onCreate(savedInstanceState);
        initView();
        initAMap();
        initLocation();
        initViewPager();
    }


    private void initView() {
        mLatLng = new LatLng(31.211878,121.52861);//长宁店坐标
        latLngs=Arrays.asList(new LatLng(31.195279,121.50181),new LatLng(31.192048,121.502449),
                new LatLng(31.188377,121.502277),new LatLng(31.187257,121.504187),new LatLng(31.186174,121.506139),
                new LatLng(31.18711,121.508285),new LatLng(31.187385,121.509551),new LatLng(31.18845,121.512663),
                new LatLng(31.189606,121.515688),new LatLng(31.189533,121.515752),new LatLng(31.191993,121.521203),
                new LatLng(31.196012,121.523971),new LatLng(31.196435,121.527919),new LatLng(31.196435,121.527983),
                new LatLng(31.198518,121.529828),new LatLng(31.197325,121.534785),new LatLng(31.197087,121.538004),
                new LatLng(31.19805,121.543808),new LatLng(31.199482,121.54707),new LatLng(31.202189,121.55119),
                new LatLng(31.203859,121.555846),new LatLng(31.210558,121.550696),new LatLng(31.21032,121.549023),
                new LatLng(31.212247,121.541877),new LatLng(31.215311,121.543444),new LatLng(31.217935,121.546534),
                new LatLng(31.219605,121.551447),new LatLng(31.236009,121.544237),new LatLng(31.234193,121.538079),
                new LatLng(31.237513,121.536255),new LatLng(31.235275,121.529002),new LatLng(31.231816,121.521245),
                new LatLng(31.230633,121.517576),new LatLng(31.229853,121.515108),new LatLng(31.229119,121.511868),
                new LatLng(31.231766,121.510178),new LatLng(31.231766,121.510178),new LatLng(31.229903,121.503795),
                new LatLng(31.227078,121.506681),new LatLng(31.224023,121.509202),new LatLng(31.220142,121.51123),
                new LatLng(31.215848,121.512893),new LatLng(31.215747,121.512614),new LatLng(31.214434,121.513),
                new LatLng(31.212379,121.512217),new LatLng(31.211608,121.511884),new LatLng(31.210874,121.51271),
                new LatLng(31.208342,121.510811),new LatLng(31.204832,121.508413),new LatLng(31.20374,121.51072),
                new LatLng(31.201859,121.506241),new LatLng(31.200748,121.505243),new LatLng(31.196747,121.502936),
                new LatLng(31.195279,121.50181));
        address = (TextView) findViewById(R.id.address);
        location_result = (TextView) findViewById(R.id.location_result);
        location_result.setOnClickListener(this);
        changeButton = (Button) findViewById(R.id.change_address);
        changeButton.setOnClickListener(this);
        ImageView back= (ImageView) findViewById(R.id.distribution_backIcon);
        back.setOnClickListener(this);
        distribution_container= (RelativeLayout) findViewById(R.id.distribution_area_container);
        //select_container= (LinearLayout) findViewById(R.id.select_address_container);
        choose_city= (TextView) findViewById(R.id.choose_city);
    }

    private void initViewPager() {
        guideParent = (LinearLayout) findViewById(R.id.guide_parent_distribution);
        viewPagerParent = (FrameLayout) findViewById(R.id.detail_picture);
        mViewPager = (ViewPager) findViewById(R.id.viewpager_detail);

        back_icon= (ImageView) findViewById(R.id.back_icon);
        back_icon.setOnClickListener(this);

        imageViews = new ArrayList<>();

        int count = guideParent.getChildCount();
        //实例化ImageView数组
        guideArray = new ImageView[count];
        //给数组赋值
        for (int i = 0; i < count; i++) {
            guideArray[i] = (ImageView) guideParent.getChildAt(i);
        }
        guideArray[0].setEnabled(false);
        //实例化images集合，实例化ViewPagerTop里需要填充的ImageView并添加到images集合中

        LayoutInflater inflater = LayoutInflater.from(this);
        ImageView img1 = (ImageView) inflater.inflate(R.layout.view_pager_top_item, null);
        img1.setImageResource(R.mipmap.ic_launcher);
        ImageView img2 = (ImageView) inflater.inflate(R.layout.view_pager_top_item, null);
        img2.setImageResource(R.mipmap.ic_launcher);
        ImageView img3 = (ImageView) inflater.inflate(R.layout.view_pager_top_item, null);
        img3.setImageResource(R.mipmap.ic_launcher);
        ImageView img4 = (ImageView) inflater.inflate(R.layout.view_pager_top_item, null);
        img4.setImageResource(R.mipmap.ic_launcher);

        imageViews.add(img1);
        imageViews.add(img2);
        imageViews.add(img3);
        imageViews.add(img4);


        mViewPager.setAdapter(new RecyclePagerAdapter(this, imageViews));
        mViewPager.setCurrentItem(imageViews.size() * 1000);
        //给mViewPager设置监听事件
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                //同步guide指示器
                int count = guideArray.length;
                position %= count;
                for (int i = 0; i < count; i++) {
                    guideArray[i].setEnabled(true);
                }
                guideArray[position].setEnabled(false);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }

    private void initAMap() {
        mAMap = mMapView.getMap();
        mAMap.setOnMapLoadedListener(this);
        mAMap.setOnInfoWindowClickListener(this);
        addMark();
    }

    private void addMark() {
        /**
         * 为长宁店添加Marker
         */
        Marker mMarker = mAMap.addMarker(new MarkerOptions().
                anchor(0.5f, 0.5f).
                position(mLatLng).
                title("鲜在时蓝村路店").
                //snippet("点击查看店内详情").
                icon((BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ROSE))));
        mMarker.showInfoWindow();


        /**
         * 为长宁店划出配送范围
         */
        PolygonOptions polygonOptions=new PolygonOptions();
        polygonOptions.
                addAll(latLngs).
                fillColor(R.color.colorFillColorGreen).
                strokeWidth(4).
                strokeColor(Color.GREEN);

        /*CircleOptions mCircleOptions = new CircleOptions();
        mCircleOptions.
                center(mLatLng).
                radius(1000).
                fillColor(R.color.colorFillColorGreen).
                strokeWidth(4).
                strokeColor(Color.GREEN);*/
        //mAMap.addCircle(mCircleOptions);
        polygon=mAMap.addPolygon(polygonOptions);
    }

    private void initLocation() {
        mLocationClient = new AMapLocationClient(DistributionActivity.this);
        mLocationOption = new AMapLocationClientOption();
        mLocationOption.setLocationMode(AMapLocationClientOption.AMapLocationMode.Hight_Accuracy);
        mLocationOption.setOnceLocation(true);
        mLocationClient.setLocationOption(mLocationOption);
        mLocationClient.setLocationListener(this);
        mLocationClient.startLocation();
    }

    @Override
    public void onMapLoaded() {
        /**
         * 设置MAP的镜头直接显示围栏区域
         */
        mAMap.moveCamera(new CameraUpdateFactory().newCameraPosition(new CameraPosition(mLatLng,14, 0, 0)));

        /*LatLngBounds bounds = new LatLngBounds.Builder()
                .include(mLatLng).build();
        mAMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 15));*/

    }

    @Override
    public void onLocationChanged(AMapLocation aMapLocation) {
        if (aMapLocation != null) {
            if (aMapLocation.getErrorCode() == 0) {
                address.setText(aMapLocation.getAddress());
                LatLng myLatLng = new LatLng(aMapLocation.getLatitude(), aMapLocation.getLongitude());

                if (polygon.contains(myLatLng)) {
                    location_result.setText("可由鲜在时上海蓝村路店配送");
                } else {
                    location_result.setText("您当前地址不在配送范围，请修改配送地址");
                    location_result.setTextColor(Color.RED);
                }
            } else {
                address.setText("您当前未开启定位功能");
                location_result.setText("点此开启我的手机定位");
                location_result.setTextColor(Color.RED);
            }
        }
    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        /*Animation animation= AnimationUtils.loadAnimation(this,R.anim.animation_store_detail);
        viewPagerParent.setVisibility(View.VISIBLE);
        viewPagerParent.startAnimation(animation);*/
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.back_icon:
                Animation animation=AnimationUtils.loadAnimation(this,R.anim.animation_store_close);
                viewPagerParent.setVisibility(View.GONE);
                viewPagerParent.startAnimation(animation);
                break;
            case R.id.change_address:
                //distribution_container.setVisibility(View.GONE);
                //select_container.setVisibility(View.VISIBLE);
                //choose_city.setText("选择地址");
                String uid= SPUtils.getStringValue(MyApplication.getContext(), Contants.SP_NAME,Contants.UID,null);
                if(uid==null){
                    startActivity(new Intent(DistributionActivity.this,NewLoginActivity.class));
                }else {
                    startActivity(new Intent(DistributionActivity.this,AddAddressActivity.class));
                }
                break;
            case R.id.distribution_backIcon:
                finish();
                break;

        }
    }

    /**
     * 方法必须重写
     */
    @Override
    protected void onResume() {
        super.onResume();
        mMapView.onResume();

        MobclickAgent.onResume(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        mMapView.onPause();

        MobclickAgent.onPause(this);
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mMapView.onSaveInstanceState(outState);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        mLocationClient.stopLocation();
        if (mLocationClient != null) {
            mLocationClient.onDestroy();
            mLocationClient = null;
            mLocationOption = null;
        }
    }

}
