package com.xianzaishi.normandie;

import android.app.Application;
import android.content.Context;
import android.os.Handler;
import android.os.Process;
import android.support.multidex.MultiDex;

import com.facebook.drawee.backends.pipeline.Fresco;

import com.umeng.socialize.PlatformConfig;
import com.umeng.socialize.UMShareAPI;
import com.xianzaishi.normandie.global.Contants;

import java.io.File;
import java.util.concurrent.TimeUnit;

import cn.jpush.android.api.JPushInterface;
import okhttp3.Cache;
import okhttp3.OkHttpClient;

/**
 * Created by ShenLang on 2016/8/17.
 *  application
 */
public class MyApplication extends Application {




    private static Context context;
    public static Handler handler;
    public static  int mainId;


    private static OkHttpClient okHttpClient;
    {
        PlatformConfig.setWeixin(Contants.APP_ID,Contants.APP_KEY);
    }
    @Override
    public void onCreate() {
        super.onCreate();
        MultiDex.install(this);
        JPushInterface.init(this);     		// 初始化 JPush


        JPushInterface.setDebugMode(true);
        UMShareAPI.get(this); //友盟分享初始化

        Fresco.initialize(this);
        context = getApplicationContext();

        handler = new Handler();

        mainId = Process.myTid();

        Process.myPid();


        initOkHttpClient();

    }


    public void initOkHttpClient() {
        int cacheSize = 10 * 1024 * 1024;//10M
        String okHttpCachePath = getCacheDir().getPath() + File.separator + "okHttp";
        File okHttpCache = new File(okHttpCachePath);
        if (!okHttpCache.exists()) {
            okHttpCache.mkdirs();
        }
        Cache cache = new Cache(okHttpCache, cacheSize);
        okHttpClient = new OkHttpClient.Builder()
                .cache(cache)//设置网络请求缓存
                .connectTimeout(5, TimeUnit.SECONDS)
                .build();

    }

    /**
     *获取OkHttpClient
     */
    public static OkHttpClient getOkHttpClient() {
        return okHttpClient;
    }



    /**
     * 获取context对象的方法
     */
    public static Context getContext(){
        return context;
    }

    /**
     * 获取消息处理器
     */
    public static Handler getHandler(){
        return handler;
    }

    /**
     * 获取主线程id
     * @return int
     */
    public static int getMainThreadId(){return mainId;}


}
