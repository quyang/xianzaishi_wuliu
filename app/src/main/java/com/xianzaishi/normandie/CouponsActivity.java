package com.xianzaishi.normandie;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.umeng.analytics.MobclickAgent;
import com.xianzaishi.normandie.adapter.CouponsMultipleAdapter;
import com.xianzaishi.normandie.bean.CouponsBean;
import com.xianzaishi.normandie.bean.CouponsQRBean;
import com.xianzaishi.normandie.bean.SystemTimeBean;
import com.xianzaishi.normandie.common.Urls;
import com.xianzaishi.normandie.global.Contants;
import com.xianzaishi.normandie.utils.GetBeanClass;
import com.xianzaishi.normandie.utils.GetDistributionTime;
import com.xianzaishi.normandie.utils.SPUtils;
import com.xianzaishi.normandie.utils.UiUtils;
import com.xianzaishi.normandie.zxing.encoding.EncodingHandler;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class CouponsActivity extends AppCompatActivity implements View.OnClickListener{
    private RecyclerView recyclerView;
    private ImageView back;
    private String token;
    private List<CouponsBean.ModuleBean> list;
    private View emptyView;
    private int callback;
    private String buySkuDetailJson;
    private OkHttpClient client=MyApplication.getOkHttpClient();
    private long currentTime;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_coupons);
        initView();
    }

    private void initView() {
        View view=findViewById(R.id.coupons_title);
        back = (ImageView) findViewById(R.id.address_go_back);
        back.setOnClickListener(this);
        TextView title= (TextView) view.findViewById(R.id.text_title);
        title.setText("优惠券");
        emptyView=findViewById(R.id.coupons_emptyView);
        recyclerView= (RecyclerView) findViewById(R.id.coupons_list);
        LinearLayoutManager linearLayoutManager=new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false);
        recyclerView.setLayoutManager(linearLayoutManager);
        callback=getIntent().getIntExtra("callback",0);
        buySkuDetailJson=getIntent().getStringExtra("buySkuDetailJson");
        getTimeFromServer();
    }
    /**
     *  从服务器获取时间戳
     */
    private void getTimeFromServer() {
        Request request=new Request.Builder()
                .url(Urls.GET_SYSTEM_TIME)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

            }
            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (response.isSuccessful()){
                    SystemTimeBean timeBean= (SystemTimeBean) GetBeanClass.getBean(response,SystemTimeBean.class);
                    if (timeBean!=null&&timeBean.isSuccess()){
                        currentTime=timeBean.getModule();
                        screenOut();
                    }
                }
            }
        });
    }

    private void screenOut() {
        token= SPUtils.getStringValue(MyApplication.getContext(), Contants.SP_NAME,Contants.TOKEN,"");
        if (callback==1){
            FormBody formBody=new FormBody.Builder()
                    .add("token",token)
                    .add("queryType","1")
                    .add("payType","1")
                    .add("buySkuDetailJson",buySkuDetailJson)
                    .build();
            netWork(formBody);
        }else {
            FormBody formBody=new FormBody.Builder()
                    .add("queryType","0")
                    .add("token",token)
                    .build();
            netWork(formBody);
        }
    }

    /**
     * 访问服务器获取优惠券信息
     */
    private void netWork(FormBody formBody) {
        Request request=new Request.Builder()
                .url(Urls.GET_COUPONS)
                .post(formBody)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if(!response.isSuccessful()) throw new IOException("Unexpected code"+response);
                final CouponsBean couponsBean= (CouponsBean) GetBeanClass.getBean(response,CouponsBean.class);
                response.close();
                if (couponsBean!=null&&couponsBean.getCode()==1){
                    UiUtils.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            list=couponsBean.getModule();
                            CouponsMultipleAdapter adapter=new CouponsMultipleAdapter(CouponsActivity.this,list,currentTime);
                            recyclerView.setAdapter(adapter);
                            adapter.setOnCouponsItemClickListener(new CouponsMultipleAdapter.OnCouponsItemClickListener() {
                                @Override
                                public void onClick(View view, int position) {
                                if(callback==1) {
                                    CouponsBean.ModuleBean dataBean = list.get(position);
                                    String couponsId = dataBean.getId() + "";
                                    String couponsTitle = dataBean.getCouponTitle();
                                    double couponsPrice = dataBean.getAmountYuanDescription();
                                    Intent intent = new Intent();
                                    intent.putExtra("couponsId", couponsId);
                                    intent.putExtra("couponsPrice", couponsPrice);
                                    intent.putExtra("couponsTitle", couponsTitle);
                                    setResult(RESULT_OK, intent);
                                    finish();
                                }else if(list.get(position).getCouponType()==77) {
                                    String token=SPUtils.getStringValue(MyApplication.getContext(),Contants.SP_NAME,"token",null);
                                    String cid=String.valueOf(list.get(position).getId());
                                    getCouponsQR(token,cid);
                                }
                                }
                            });

                            if (list==null){
                                emptyView.setVisibility(View.VISIBLE);
                            }else if (list.isEmpty()){
                                emptyView.setVisibility(View.VISIBLE);
                            }
                        }
                    });

                }
            }
        });
    }

    private void getCouponsQR(String token, String cid) {
        RequestBody formBody=new FormBody.Builder()
                .add("token",token)
                .add("cid",cid)
                .build();
        Request request=new Request.Builder()
                .url(Urls.GET_COUPONS_QR)
                .post(formBody)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if(!response.isSuccessful()) throw new IOException("Unexpected code"+response);
                final CouponsQRBean couponsBean= (CouponsQRBean) GetBeanClass.getBean(response,CouponsQRBean.class);
                response.close();
                if(couponsBean.isSuccess()){
                    UiUtils.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            String str=String.format(Urls.CREAT_QR_FOR_COUPON,couponsBean.getData().getSecurity(),couponsBean.getData().getSign());
                            Bitmap QR= EncodingHandler.createQRCode(str,1000);
                            dialog(QR);
                        }
                    });

                }
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.address_go_back:
                finish();
                break;
        }
    }
    /**
     *  二维码详情
     */
    private void dialog(Bitmap QR){
        LayoutInflater inflater=this.getLayoutInflater();
        WindowManager m = this.getWindowManager();
        Display d = m.getDefaultDisplay(); // 获取屏幕宽、高度
        View view=inflater.inflate(R.layout.qr_image,null);
        final Dialog dialog=new Dialog(this,R.style.Dialog);
        dialog.setCancelable(true);
        dialog.setContentView(view);
        ImageView qrImage= (ImageView) view.findViewById(R.id.qr_image);
        qrImage.getLayoutParams().width= (int) (d.getWidth() * 0.7);
        qrImage.getLayoutParams().height=qrImage.getLayoutParams().width;
        qrImage.setImageBitmap(QR);
        dialog.show();
        Window dialogWindow = dialog.getWindow();

        WindowManager.LayoutParams p = dialogWindow.getAttributes(); // 获取对话框当前的参数值
        p.width = (int) (d.getWidth() * 0.7); // 宽度设置为屏幕的0.6，根据实际情况调整
        //p.height = (int) (d.getHeight()*0.7); // 高度设置为屏幕的0.6，根据实际情况调整
        dialogWindow.setGravity(Gravity.CENTER);
        dialogWindow.setAttributes(p);
    }
    @Override
    protected void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
    }
}
